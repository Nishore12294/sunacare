import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_font.dart';

import 'app_constants.dart';
import 'app_text_style.dart';

class AlertOverlayWithTwoButtons extends StatefulWidget {
  String title,subTitle,buttonTitle;
  AlertOverlayWithTwoButtons(this.title,this.subTitle,this.buttonTitle);
  @override
  State<StatefulWidget> createState() => AlertOverlayWithTwoButtonsState(this.title,this.subTitle,this.buttonTitle);
}

class AlertOverlayWithTwoButtonsState extends State<AlertOverlayWithTwoButtons>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  String title,subTitle,buttonTitle;
  AlertOverlayWithTwoButtonsState(this.title,this.subTitle,this.buttonTitle);
  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: 180.0,

              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10.0, left: 20.0, right: 20.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[

                                Text(
                                  widget.title,

                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightExtraBold,
                                      fontSize: 17
                                  ),

                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN/4,
                                ),
                                Text(
                                  widget.subTitle!= null ?widget.subTitle:"",
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      fontWeight: AppFont.fontWeightRegular,
                                      fontSize: (widget.subTitle!= null && widget.subTitle.length > 10)?15: 17
                                  ),
                                ),
                              ])


                      )),

                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN*4,
                            height: 45,
                            child:   RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                              },
                              color: colorDarkRed,
                              textColor: colorWhite,
                              elevation: 1,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonTitle,
                                style: getStyleButtonText(context).copyWith(
                                    color: colorWhite,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),

                          SizedBox(
                            width: AppConstants.SIDE_MARGIN*4,
                            height: 45,
                            child:   RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                              },
                              color: colorDarkRed,
                              textColor: colorWhite,
                              elevation: 1,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonTitle,
                                style: getStyleButtonText(context).copyWith(
                                    color: colorWhite,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              )),
        ),
      ),
    );
  }
}