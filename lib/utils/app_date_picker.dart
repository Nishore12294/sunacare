import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'app_colors.dart';

Future<void> openDatePicker(
    BuildContext context, TextEditingController textEditController) async {
  final DateTime dataPicker = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1930),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
            data: ThemeData(
                primaryColor: colorPrimary, accentColor: colorPrimary),
            child: child);
      });

  textEditController.text =
      dataPicker == null ? '' : DateFormat('dd-MM-yyyy').format(dataPicker);
}

Future<void> fromDatePicker(
    BuildContext context, TextEditingController textEditController) async {
  final DateTime dataPicker = await showDatePicker(
      context: context,
      initialDate: DateTime(1999),
      firstDate: DateTime(1970),
      lastDate: DateTime(
      DateTime.now().year, DateTime.now().month, DateTime.now().day),
      builder: (BuildContext context, Widget child) {
        return Theme(
            data: ThemeData(
                primaryColor: colorPrimary, accentColor: colorPrimary),
            child: child);
      });

  textEditController.text =
      dataPicker == null ? '' : DateFormat('yyyy-MM-dd').format(dataPicker);
}

Future<void> timePicker(
    BuildContext context, TextEditingController textEditController) async {
  final DateTime dataPicker = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate:  DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day),
      lastDate: DateTime(2021),
      builder: (BuildContext context, Widget child) {
        return Theme(data: ThemeData(primaryColor: colorPrimary, accentColor: colorPrimary), child: child);
      });
}
