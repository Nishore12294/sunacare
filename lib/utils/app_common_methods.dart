import 'package:intl/intl.dart';

String getBuildFormedDate(String serviceDate) {
  try {
    DateTime dateTime = DateFormat("yyyy-MM-dd").parse(serviceDate);
    String date = DateFormat("dd-MM-yyyy").format(dateTime);
    return date;
  } catch (e) {
    return serviceDate;
  }
}

DateTime getBuildFormedDateDataType(String serviceDate) {
  try {
    DateTime dateTime = DateFormat("yyyy-MM-dd").parse(serviceDate);
    return dateTime;
  } catch (e) {
    return DateTime.now();
  }
}