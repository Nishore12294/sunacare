import 'dart:math' as math;

import 'package:flutter/material.dart';

colorRandom() => Color((math.Random().nextDouble() * 0xFFFFFF).toInt() << 0)
    .withOpacity(1.0);
const colorPrimary = const Color(0xFFFFC30B);
//sconst colorAccent = const Color(0xFFFFC30B);
const colorAccent = const Color(0xFFFFC30B);
const colorPrimaryLight = Color(0xFFFDF7E6);
const colorAccentMild = const Color(0xFFffe9eb);


//App Background colors
const colorLitePinkBG = const Color(0xFFF1FAF7); //App Background color
const colorLitePinkFilterBG = const Color(0xFFF0FBF7);
const colorLitePinkViewProfileBG = const Color(0xFFF0FBF8);

//General color
const colorBlack = const Color(0xFF000000);
const colorWhite = const Color(0xFFFFFFFF);
const colorDarkGrey = const Color(0xFF808080);
const colorLabelHintText = const Color(0xFF757575);
const colorStarGrey = const Color(0xFFD3D3D3);
const colorDividerGrey = const Color(0xFFD3D3D3);
const colorCloseIconImage = const Color(0xFF979597);
const colorDropDownHeaderColor = const Color(0xFFFDEEF2);
const colorTextContentRedColor = const Color(0xFF912B36);
const colorQtyHintColor = const Color(0xFF1A181A);
const colorGrey = const Color(0xFFf5f5f5);
const colorYellow = const Color(0xFFFEC230);
const colorYellowTransparent = const Color(0x44FEC230);
const colorYoutubeGrey = const Color(0xFFF1F1F1);
const colorFbBlue = const Color(0xFF3b5999);
const colorGMailRed = const Color(0xFFdc4e42);
const colorTwitterBlue = const Color(0xFF55acef);

const colorLightBlue = const Color(0xFF0B80FF);
const colorGradientPink = const Color(0xFFF62C81);
const colorGradientPurple = const Color(0xFF674B98);

const colorTextLabel = const Color(0xFF7D7F7F);
const colorLightGrey = const Color(0xFF91A5A6);
const colorFlashGreen = const Color(0xFF46cf7d);
const colorFlashDarkGreen = const Color(0xFF2ecc71);
const colorOrange = const Color(0xFFED7C24);

const colorAppBg = const Color(0xFFFFFEFB);
const colorTabButtonDefaultColor = const Color(0xFFFFE1E2);
const colorNavyBlue = const Color(0xFF34495E);
const colorPencilBlack = const Color(0xFF1B2241);
const colorBorderLine = const Color(0xFFC4C4C4);

const colorDashActiveProd = const Color(0xFF2ecc71);
const colorDashInActiveProd = const Color(0xFFbc2026);
const colorDashTotalSales = const Color(0xFF27ae60);
const colorDashTotalOrders = const Color(0xFF3498db);
const colorDashTotalProdSold = const Color(0xFFe67e22);
const colorAppGreen = const Color(0xFF00b894);
const colorDashOrderCancelled = const Color(0xFFe64433);
const colorDashOrderShipped = const Color(0xFF8e44ad);
const colorAppCyan = const Color(0xFF00A7B7);
const colorDashOrderReturned = const Color(0xFFe67e22);
const colorDashNoOfPromotions = const Color(0xFF2c2c54);
const colorDashProdMinQty = const Color(0xFFe55039);
const colorGradientAppTheme = <Color>[colorGradientPink, colorGradientPurple];
const colorGradientOrange = <Color>[Color(0xFFFEB630), Color(0xFFF87777)];
const colorGradientGreen = <Color>[Color(0xFF459BE4), Color(0xFF4EE2C6)];

const colorMyChatBG = const Color(0xFFe1f7cb);
const colorOtherChatBG = const Color(0xFFF2F2F2);
const colorOtherChatBorder = const Color(0xFFF5F5F5);

//Dashboard Colors
const colorTabLiteBG = const Color(0xFFFFF8E7);
const colorTabDarkBG = const Color(0xFFFFC63D);
const colorTransparent = const Color(0xFFFF00);
const colorSearchBarGradiant = <Color>[Color(0xFFFFC03A), Color(0xFFFFC03A)];
const colorSearchBarBg = const Color(0xFFFFC03A);

//Profile or me
const colorLiteYellow = const Color(0xFFFFF9EB);
const colorLiteRed = const Color(0xFFFFEAE7);

//APP
const colorDarkRed = const Color(0xFFE0665E);
const colorLiteOrange = const Color(0xFFFF8B6F);
const colorLiteWhiteBorder = const Color(0xFFFFE5E5);
const colorLiteWhite = const Color(0x00F0F8FF);
const colorLiteWhiteBG = const Color(0x00ECECEC);
const colorGreen = const Color(0xFF60B086);
const colorGray = const Color(0xFFD6E3E0);
const colorLightGray = const Color(0xFFCADED9);
const colorMildGray = const Color(0xFFDCDCDC);
const colorLitePink = const Color(0xFFF8EBE7);
const colorLiteGreen = const Color(0xFFCADED9);
const colorLiteBlue = const Color(0xFFE3E4E7);
const colorLiteBG = const Color(0xFFE4EEEC);


const colorsOffer = [Colors.red, Colors.blue, Colors.green];

