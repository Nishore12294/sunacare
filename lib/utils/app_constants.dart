import 'package:flutter/cupertino.dart';
import 'package:suna_care/ui/home/slides/tab2_offers_list_screen.dart';
import 'package:suna_care/utils/app_images.dart';

class AppConstants {

  /////////////
  ///CURRENTLY USING common text
  ///

  //size
  static const double TAB_TEXT_SPACE = 20.0;
  static const double SMALL_SPACE = 5.0;
  static const double MEDIUM_SPACE = 10.0;
  static const double SIDE_MARGIN = 20.0;
  static const double APP_BAR_HIGH = 80.0;
  static const double AVAILABILITY_TAB = 0.0;
  static const double OPEN_OFFER_TAB = 0.0;
  static const double PROFILE_TAB = 0.0;
  static const double CARD_CORNER_RADIUS = 6;
  //SnackBar ShowUp Time In seconds
  static const int TIME_SHOW_SNACK_BAR = 5;
  //Header title
  static const double HEADER_TITLE_SIZE = 21.5;
  static const String GOOGLE_API_KEY = "AIzaSyB4pEga9hqkS1RVkQLWC7bNUxzc9ydrG-A";





  ///LOGIN
  static const String APP_NAME = "Sunacare";
  static const String LOGIN = "Login";
  static const String LOGOUT = 'Logout';
  static const String ENTER_EMAIL_PASSWORD = 'Please enter your email & password.';
  static const String SIGN_IN_ALERT = 'Sign in';
  static const String CLOSE = 'Close';
  static const String HELLO = 'Hello,';
  static const String PLEASE_LOGIN = 'please log in';
  static const String PLEASE_TRY_AGAIN = "Please try again";
  static const String PERSONAL_INFORMATION = "Personal Information";
  static const String SOME_THING_WENT_WRONG = 'Something went wrong. Please try again later.';
  static const String NO_INTERNET_CONNECTION_TITLE = 'No network connection';
  static const String NO_INTERNET_CONNECTION = 'No internet connection. Connect to the internet and try again.';

  //LOGIN API
  //static const String EMAIL_IS_INVALID = 'Email is invalid';
  static const String EMAIL_IS_INVALID = 'Invalid Email';
  static const String ADDITIONAL_INFORMATION = "Additional Information";
  static const String EMAIL_IS_VALID_MATCH_MULTI_CG_REC = 'Email is valid but match to multiple CG records';
  static const String EMAIL_VALID_CG_INACTIVE_RESIGNED = 'Email/Password are valid, CG is inactive and resigned';
  static const String EMAIL_VALID_PASSWORD_NOT_VALID  = 'Email are valid, Password is not valid';
  //static const String EMAIL_PASSWORD_VALID_CG_INACTIVE_BLACKLISTED  = 'Email/Password are valid, CG is inactive and blacklisted';
  static const String EMAIL_PASSWORD_VALID_CG_INACTIVE_BLACKLISTED  = 'Your account has been blacklisted. Please contact the administrator.';
  static const String EMAIL_VALID_PASSWORD_CG_NOT_BLACKLIST_ACTIVE  = 'Email/Password are valid, CG Not blacklisted and is Active';
  static const String UNEXPECTED_ERROR  = 'Unexpected error';
  static const String TOKEN_IS_INVALID = 'Token is invalid';
  static const String TOKEN_IS_VALID_MATCH_MULTI_CG_REC = 'Token is valid but match to multiple CG records';
  static const String TOKEN_VALID_CG_INACTIVE_RESIGNED = 'Token are valid, CG is resigned and inactive';
  static const String TOKEN_VALID_CG_INACTIVE_BLACKLISTED  = 'Token are valid, CG is blacklisted and inactive';



  // SIGN UP
  static const String SIGN_UP = "Sign up";
  static const String SIGN_UP_HERE = "Sign up here";
  static const String SIGN_IN = "Sign In";
  static const String FIRST_NAME = "First name";
  static const String LAST_NAME = "Last name";
  static const String MOBILE_NUMBER = "Mobile number";
  static const String EMAIL_ADDRESS = "Email address";
  static const String ENTER_EMAIL_ADDRESS = "Enter e-mail address";
  static const String ENTER_PASSWORD = "Enter password";
  static const String VD_ENTER_PASSWORD = "Please enter your password.";
  static const String EMAIL_ID = "Email ID";
  static const String GENDER = "Gender";
  static const String DONT_HAVE_AN_ACCOUNT = "Don't have an account?";
  static const String ALREADY_HAVE_AN_ACCOUNT = "Already have an account?";
  static const String LOGIN_HERE = "Login here";
  static const String CANCEL = "Cancel";
  static const String SUBMIT = "Submit";
  static const String NEXT = "Next";
  static const String EMAIL = "Email";
  static const String DRIVER_LICENCE = "Driver's licence";
  static const String SMOKING = "Smoking";
  static const String SPEAKING_GERMAN_LANGUAGE = "Speaking German Language";
  static const String EXPERIENCE_IN_CARE = "Experience in care";
  static const String WILLING_TO_DO_TRANFER = "Willing to do transfer from bed to wheelchair";
  static const String WILLING_TO_DO_NIGHT_SHIFTS = "Willing to do night shifts";
  static const String ABLE_TO_TAKE_CARE_2PEOPLE = "Able to take care of 2 people";
  static const String YES = 'Yes';
  static const String NO = 'No';
  static const String ALERT = 'Alert';
  static const String SIGNUP_ALERT = 'Signup';
  static const String WARNING = 'Warning';
  static const String DISCARD_DRAFT = 'Discard draft';
  static const String FILL_DETAILS = 'Fill the details';
  static const String ENTER_FIRSTNAME = 'Please enter your first name.';
  static const String ENTER_LASTNAME = 'Please enter your last name.';
  static const String ENTER_EMAILID = 'Please enter your email address.';
  static const String ENTER_VALID_EMAIL = 'Please enter valid email address.';
  static const String ENTER_INVALID_EMAIL_PASSWORD = 'You have entered an invalid email or password.';
  static const String ENTER_VALID_PHONENO = 'Please enter valid mobile number.';
  static const String ENTER_MOBILE = 'Please enter your mobile number.';
  static const String ENTER_GENDER = 'Enter gender';
  static const String PLEASE_SELECT_GENDER = 'Please select your gender.';
  static const String ENTER_OLD_PASSWORD = 'Enter old password';
  static const String ENTER_NEW_PASSWORD = 'Enter new password';
  static const String ENTER_CONFIRM_PASSWORD = 'Confirm new password';
  static const String VD_ENTER_NEW_PASSWORD = 'Please enter new password';
  static const String VD_ENTER_CONFIRM_PASSWORD = 'Please enter confirm new password';
  static const String ENTER_PASSWORDS_NOT_EQUAL = 'Passwords do not match';
  //static const String ENTER_PASSWORDS_NOT_EQUAL = 'Passwords do not match';



  //SIGN UP API
  static const String NAME_IS_BLANK = 'Name is blank';
  static const String EMAIL_IS_BLANK = 'Email is blank';
  static const String DIRVER_LICENSE_INVALID_VALUE = 'Driver License contain invalid value';
  static const String SMOKING_CONTAIN_INVALID_VALUE = 'Smoking contain invalid value';
  static const String SPEAKING_GERMAN_LANG_INVALID_VALUE = 'Speaking German Language contain invalid value';
  static const String EXPERIENCE_CONTAIN_INVALID_VALUE = 'Experience In Care contain invalid value';
  static const String PLEASE_SELECT_EXPERIENCE_CONTAIN = 'Please select experience in care.';
  static const String WILLING_TO_DO_TRANS_CONTAIN_INVALID_VALUE = 'Willing To Do Transfer contain invalid value';
  static const String WILLING_TO_DO_NIGHTSHIFTS_CONTAIN_INVALID_VALUE = 'Willing to do Night Shifts contain invalid value';
  static const String ABLE_TO_TAKE_CARE_OF_2PEOPLE = 'Able to take care of 2people contain invalid value';
  static const String LEAD_CREATED = 'Lead created';
  static const String EMAIL_EXISTS = 'Email exists';
  static const String YOUR_EMAIL_ALREADY_USED_LOGIN_INSTEAD = 'Your email address is already used.\n Would you like to login instead?';
  static const String PLEASE_SELECT_OPTIONS_SUBMIT = 'Please select the options & then submit.';
  static const String ARE_YOU_SURE_CANCEL_SIGNUP = 'Are you sure want to cancel the signup process?';
  static const String EMAIL_ALREADY_USED_PENDING_APPROVAL = 'Your email address is already used and its pending approval.';


  /// screen 'Change Password'
  static const String CHANGE_PASSWORD = 'Change Password';
  static const String CHANGE_PASSWORD_ALERT = 'Change Password';
  static const String PASSWORD = 'Password';
  static const String OLD_PASSWORD = 'Old Password';
  static const String NEW_PASSWORD = 'Enter new password';
  static const String CONFIRM_PASSWORD = 'Confirm new password';
  static const String DONE = "Done";
  static const String PASSWORD_UPDATED_SUCCESSFULLY = "Password updated successfully.";
  static const String PROFILE_BLACKLISTED = "Your profile blacklisted.";
  static const String INVALID_OLD_APP_PASSWORD = "Invalid old app password.";
  static const String PASSWORD_NOT_SET = "Password not set";
  static const String PLEASE_ENTER_NEW_PASSWORD = "Please enter new password.";
  static const String PLEASE_ENTER_CONFIRM_NEW_PASSWORD = "Please enter confirm new password.";
  static const String PASSWORD_DO_NOT_MATCH = "Password do not match.";




  //Forgot password
  static const String FORGOT_PASSWORD = 'Forgot Password';
  static const String FORGOT_PASSWORD_ALERT = 'Forgot password?';
  static const String FORGOT_PASSWORD_TIPS= 'Enter your registered email below to \n receive password rest instruction';
  static const String REMEMBER_PASSWORD = 'Remember password';
  static const String SEND = 'Send';
  static const String VERIFY_CODE = 'Verify code';
  static const String LOG_INTO_APP = 'Log into the app using credential received in email.';
  static const String CODE_RESENT_SUCCESSFULLY =   "Code resent successfully.";

  //Create password
  static const String CREAET_NEW_A_PASSWORD = 'Create a new password';
  static const String CREATE_PASSWORD_ALERT = 'Create a new password';
  static const String CREAET_NEW_PASSWORD = 'New password';
  static const String CREATE_PASSWORD_TIPS = 'Your new password must be different \n from previous used passwords.';
  static const String RESET_PASSWORD = 'Reset password';
  static const String INVALID_VERIFICATION_CREATE_PASSWORD = 'The verification code you have entered is invalid. Please enter a valid verification code.';
  static const String CREATE_PASSWORD_CHANGED_SUCCESSFULLY = 'Password changed Successfully';

  //Verification Code
  static const String  CONFIRMATION = 'Confirmation';
  static const String VERIFICATION_CODE = 'Verification Code';
  static const String VERFICATION_CODE_TIPS = 'Please type the verification code \n sent to your email address: ';
  static const String VERIFICATION_ALERT = 'Verification code';
  static const String ENTER_VERFICIATION_CODE = 'Please enter the verification code.';
  static const String ENTER_VALID_VERFICIATION_CODE = 'You have entered incorrect verification code.';
  static const String RESEND_CODE = 'Resend code';


  /// screen 'Dashboard'
  static const String PENDING_APPROVAL = 'Pending Approval';
  static const String WE_ARE_EVALUATIING = 'We\'re evaluating your profile';
  static const String WE_WILL_EMAIL_OR_MESSAGE = 'We will email or message you when \n we\'ve approved your profile';
  static const String OKAY = 'OK';


  /// screen 'Dashboard'
  static const String HOME = 'Home';
  static const String AVAILABILITY = 'Availability';
  static const String OPEN_OFFER = 'Open Offer';
  static const String ADD_UNAVAILABLE_DATES = 'Add Unavailable dates';
  static const String SELECT_DATE_RANGE = 'Select unavailable date range';
  static const String FROM = 'From';
  static const String TO = 'To';
  static const String SELECT_RANGE_INFO = "Select date range and click on \"Add \n unavailable button\" to set your\n unavailability";
  static const String SELECT_UNAVAILABLE_DATE_RANGE = "Select unavailable date range";
  static const String CONTRACT_DATES = "Contract dates";
  static const String UNAVAILABLE_DATES = "Unavailable dates";
  static const String TO_DATE_SHOULD_BE_GRATER_THEN_FROM_DATE = "To date should be greater than From date";
  static const String FUTURE_CONTRACT_DATES = "Future contract dates";
  static const String SCHEDULE_DETAILS = "Schedule details";
  static const String EDIT_UNAVAILABLE_DATE = "Edit unavailable dates";
  static const String UNAVAILABLE_DATE_ALERT = "Unavailable dates";
  static const String HOLIDAY_DATE_ALERT = "Holiday dates";
  static const String YOU_CANNOT_CHANGE_HOLIDAY_DATE = "You cannot change unavailable date. Please, edit it.";
  static const String ONGOING_CONTRACT_ALERT = "On-going contract";
  static const String YOU_CANNOT_CHANGE_DELETE_ONGOING_CONTRACT = "You cannot change or delete on going schedules.";
  static const String YOU_HAVE_SUCCESSFULLY_SET_YOUR_UNAVAILABLE_DATES = "You have successfully set your unavailable dates.";
  static const String YOU_HAVE_SUCCESSFULLY_DELETED_YOUR_UNAVAILABLE_DATES = "You have successfully deleted your unavailable dates.";
  static const String YOU_HAVE_SUCCESSFULLY_UPDATED_YOUR_UNAVAILABLE_DATES = "You have successfully updated your unavailable dates.";
  static const String PLEASE_ADD_UNAVAILABLE_DATES = "Please select unavailable dates.";
  static const String  TOKEN_VALID_HOLIDAYCGSCHEDULE_INVALID = "Token is valid but HolidayCgSchedule Guid is invalid.";
  static const String  TOKEN_INVALID = "Token is invalid.";
  static const String  TOKEN_VALID_HOLIDAYCGSCHEDULE_VALID_START_END_DATE_UPDATED = "Token and HolidayCGSchedule Guid is valid and StartDate, EndDate is Updated.";
  static const String  TOKEN_VALID_HOLIDAYCGSCHEDULE_VALID_DATE_OVERLAPPED = "Token and HolidayCGSchedule Guid is valid but date is Overlapped.";
  static const String  TOKEN_VALID_HOLIDAYCGSCHEDULEID_INVALID = "Token and HolidayCGSchedule Guid is invalid.";
  static const String  TOKEN_VALID_HOLIDAYCGSCHEDULE_VALID_DATE_DELETED = "Token and HolidayCGSchedule Guid is valid and Record is deleted.";
  static const String  UNAVAILABLE_DATE_DELETED_SUCCESSFULLY = "Unavailable dates deleted successfully.";
  static const String  TOKEN_VALID_AND_HOLIDAY_ADDED = "Token is valid and Holiday Added.";
  static const String  TOKEN_VALID_AND_HOLIDAY_SET_SUCCESSFULLY = "You have successfully set your unavailable dates.";
  static const String  TOKEN_VALID_HOLIDAY_OVERLAPEED = "Token is valid but holiday is overlapped.";
  static const String  TOKEN_VALID_NO_CG_HOLIDAY = "Token is Valid but no CG holidays.";
  static const String  TOKEN_VALID_NO_CG_ASSIGNMENTS = "Token is Valid but no CG assignments.";
  static const String  YOU_CANNOT_ADD_EDIT_UNAVAILABLE_DATES = "You can't add or edit the previous month unavailable dates.";



  //OffersScreen
  static const String  OPEN_OFFERS = "Open Offers";
  static const String OPEN_OFFERS_ALERT = 'Open Offers';
  static const String  LOCATIONS = "Locations";
  static const String  SELECT_YOUR_LOCATIONS = "Select your location";
  static const String  PLEASE_SELECT_LOCATION = "Please select locations.";

  static const String  GERMAN_SKILLS = "German skills";
  static const String  SELECT_GERMAN_SKILLS = "Select your german skills";
  static const String  PLEASE_SELECT_GERMAN_SKILLS = "Please select german skills.";
  static const String  CONTRACT_DAYS = "Contract days";
  static const String  SELECT_YOUR_SLAB = "Select your slab";

  static const String  PLEASE_SELECT_CONTRACT_DAYS = "Please select contract days.";

  static const String  FILTERS_OPTIONS = "Filters Options";
  static const String  OFFERS_DETAILS = "Offer Details";
  static const String  APPLY = "Apply";
  static const String  YOU_CANNOT_VIEW_THE_OFFERS_UNTIL_PROF_UPLOAD = "You cannot view the offers until you \n upload your profile picture.";

  static const String  UPLOAD_PROFILE_PHOTO = "Upload profile photo";
  static const String  OFFERS_UNAVAILABLE = "Offers not available";
  static const String CAMERA = 'Capture Camera';
  static const String GALLERY = 'Pick from Gallery';

  static const String CURRENTLY_NO_AGENTS_AVAILABLE = 'Currently there are no agents available.';

  static const String CAREGIVER_NEEDED = 'People needing care';
  static const String ARE_YOU_SURE_LOGOUT = 'Are you sure that you want to sign out?';


  //Profile
  static const String PROFILE = 'Profile';
  static const String VIEW_PROFILE = 'View profile';
  static const String PROFILE_ALERT = 'Profile';
  static const String PROFILE_SIGNOUT_ALERT = 'Sign out';
  static const String VIEW_PROFILE_TITLE = 'View Profile';
  static const String PROILE_ACCOUNT = 'Account';
  static const String PROFILE_CHANGE_PASSWORD = 'Change password';
  static const String PROFILE_CHANGE_LANGUAGE = 'Change language';
  static const String PROFILE_NOTIFICATION = 'Notifications';
  static const String PROFILE_LOGOUT = 'Logout';
  static const String PROFILE_INFORMATION = 'Information';
  static const String PROFILE_PRIVACY= 'Privacy';
  static const String PROFILE_PRIMARY_DETAILS = 'Primary details';
  static const String PROFILE_FIRST_NAME = 'First name';
  static const String PROFILE_SECOND_NAME = 'Second name';
  static const String PROFILE_EMAIL = 'Email';
  static const String PROFILE_MOBILE = 'Mobile';
  static const String PROFILE_GENDER = 'Gender';
  static const String PROFILE_IMAGE_UPLOADED = 'Image Uploaded successfully';
  static const String PROFILE_PIC_UPLOADED_UNDER_VALIDATION = 'Your profile picture is uploaded and it’s under validation.';
  static const String PROFILE_FACE_ID = 'Face ID';
  static const String PROFILE_FINGERPRINT= 'Fingerprint';


  // Preferred language screen
  static const String PREFERRED_LANGUAGE = 'Preferred language';
  static const String PREFERRED_LANGUAGE_ALERT = 'Preferred language';
  static const String CHOOSE_YOUR_PREF_LANGUAGE = 'Choose your \n preferred language';
  static const String SAVE = 'Save';
  static const String ENGLISH = 'English';
  static const String POLISH = 'Polish';
  static const String LANGUAGE_UPDATED_SUCCESSFULLY = 'You have successfully changed your preferred language.';

  //Notifications
  static const String NOTIFICATIONS = 'Notifications';
  static const String NOTIFICATIONS_SETTINGS = 'Notification settings';
  static const String PUSH_NOTIFICATIONS = 'Push notifications';
  static const String AVAILABILITY_SET = 'Availability set';
  static const String PROFILE_UPLOAD = 'Profile upload';
  static const String UPDATE_ON_AVAILABILITY = 'Update on availability';
  static const String NEW_OFFERS = 'New offers';
  static const String NOTIFICATION_SETTINGS_ALERT = 'Notification Settings';
  static const String NOTIFICATION_SETTINGS_UPDATED_SUCCESSFULLY = 'You have successfully updated your notification setting.';



  /// Call request
  static const String CALL_REQUEST_TITLE = 'Call request';
  static const String CALL_REQUEST_CURRENTLY_THERE = 'Currently there are \n no agents available.';
  static const String CALL_REQUEST_PLEASE_SELECT= 'Please select options below to setup an appointment or leave us a message';
  static const String SET_UP_APPOINTMENT= 'Set up appointment';


  /// Schedule meeting
  static const String SCHEDULE_HEADER_TITLE = 'Select a date';
  static const String SCHEDULE_TIME_TITLE = 'Select a time slot';
  static const String SCHEDULE_LEAVE_US_MSG_TITLE = 'Leave us a message';
  static const String SELECT_A_TIME = 'Select a Time';
  static const String DURATION = '(Duration: 15 min)';
  static const String CONFIRM = 'Confirm';
  static const String FIX_SCHEDULE = 'Fix schedule';
  static const String FIFTY_MINUTES_MEETING = '15 minutes meeting';
  static const String PLEASE_SHARE_ANYTHING = 'Please share anything that will help\n prepare for our meeting.';
  static const String ENTER_MESSAGE = 'Enter message';
  static const String SCHDULE_APPOINTMENT = 'Schedule an appointment';

  //Leave us message
  static const String LEAVE_US_A_MESSAGE = 'Leave us a message';
  static const String SCHEDULE_APPOINTMENT = 'Schedule appointment';
  static const String PLEASE_SELECT_DATE = 'Please select date';
  static const String APPOINTMENT_SCHEDULED_SUCCESS = 'You have successfully scheduled your appointment';
  static const String SET_UP_APPOINTMENT_LEAVE_US_A_MESSAGE = 'Please select options below to set up an appointment or leave us a message';


//////////////////




  ///shared Preference
  static const String KEY_TOKEN_USER = 'user_token';
  static const String KEY_USER_EMAIL_ID = 'user_email_id';
  static const String KEY_USER_PASSWORD = 'user_password';
  static const String KEY_IS_FIRST_TIME_OPENED = 'is_first_time_app_opened';
  static const String KEY_LANG_ENGLISH = 'is_pref_language_english';
  static const String KEY_LANG_FIRST_LOGIN = 'first_login';
  static const String KEY_FINGER_PRINT_ENABLED = 'finger_print_enabled';








}
