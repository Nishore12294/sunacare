import 'package:flutter/material.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_text_style.dart';

buildCustomDividerWithLabel(
    {@required BuildContext context,
    @required String label,
    bool canShowSeeAll = false,
    VoidCallback onTapSeeAll}) {
  return Container(
    padding: EdgeInsets.only(
        top: AppConstants.SIDE_MARGIN + AppConstants.SIDE_MARGIN / 3,
        bottom: AppConstants.SIDE_MARGIN + AppConstants.SIDE_MARGIN / 3),
    child: Row(
      children: <Widget>[
        Text(
          label,
          style: getStyleSubTitle(context).copyWith(
              color: colorBlack, fontWeight: AppFont.fontWeightSemiBold),
        ),
        SizedBox(
          width: AppConstants.SIDE_MARGIN,
        ),
        Expanded(
            child: Container(
          alignment: Alignment.center,
          height: 1,
          color: colorDividerGrey,
        )),
        (canShowSeeAll)
            ? SizedBox(
                width: AppConstants.SIDE_MARGIN,
              )
            : Container(),
        (canShowSeeAll)
            ? InkWell(
                onTap: onTapSeeAll,
                child: Text(
                  "SEE_ALL",
                  style: getStyleSubTitle(context).copyWith(
                      color: colorGradientPink,
                      decoration: TextDecoration.underline,
                      fontWeight: AppFont.fontWeightSemiBold),

                ),
              )
            : Container()
      ],
    ),
  );
}
