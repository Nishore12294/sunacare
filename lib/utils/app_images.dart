import 'package:suna_care/ui/home/slides/tab2_offers_list_screen.dart';

class AppImages {
  static const IMAGE_BG_SPLASH_SCREEN = 'assets/splash_screen_icon.png';
  static const IMAGE_APP_LOGO = 'assets/logo.png';
  static const IMAGE_APP_LOGO_WITH_TEXT = 'assets/logo_with_text.png';
  static const IMAGE_EMAIL = 'assets/email.png';
  static const IMAGE_PASSWORD = 'assets/password.png';
  static const IMAGE_BACK = 'assets/back_icon.png';
  static const IMAGE_CHANGEPWD_LOCK = 'assets/changepwd_lock.png';
  static const IMAGE_NAME = 'assets/name.png';
  static const IMAGE_MOBILE = 'assets/mobile.png';
  static const IMAGE_GENDER = 'assets/gender.png';
  static const IMAGE_LOGIN_BG = 'assets/login_bg.png';
  static const IMAGE_LOGIN_BG_WITH_CIRCLE= 'assets/login_bg_with_circle.png';
  static const IMAGE_SIGNUP_BG = 'assets/signup_bg.png';
  static const IMAGE_SIGNUP_BG_WITH_CIRCLE = 'assets/signup_bg_with_circles.png';
  static const IMAGE_VERFI_CODE = 'assets/verificationcode.png';

  //Home
  static const IMAGE_HOME_SCREEN_AVAIL_GRAY = 'assets/availability_gray.png';
  static const IMAGE_HOME_SCREEN_HOME_GREEN = 'assets/availability_green.png';
  static const IMAGE_HOME_SCREEN_OFFER_GRAY = 'assets/offers_gray.png';
  static const IMAGE_HOME_SCREEN_OFFER_GREEN = 'assets/offer_green.png';
  static const IMAGE_HOME_SCREEN_PROFILE_GRAY = 'assets/profile_gray.png';
  static const IMAGE_HOME_SCREEN_PROFILE_GREEN = 'assets/profile_green.png';
  static const IMAGE_NOTIFICATION_BELL = 'assets/notification_bell.png';

  //Pending Approval
  static const IMAGE_PENDING_APPROVAL = 'assets/approval_illustration.png';

  //Schedule Details
  static const IMAGE_DELETE = 'assets/delete.png';

  //Schedule Details
  static const IMAGE_FILTERS = 'assets/filters.png';
  static const IMAGE_ADDRESS = 'assets/address.png';
  static const IMAGE_DATE = 'assets/date.png';
  static const IMAGE_EURO = 'assets/euro.png';
  static const IMAGE_LANGUAGE = 'assets/language1.png';

  //Offers
  static const IMAGE_UPLOAD = 'assets/upload.png';
  static const IMAGE_CALL = 'assets/call.png';
  static const IMAGE_FORWARD = 'assets/forward_icon.png';
  static const IMAGE_CLOCK = 'assets/clock.png';
  static const IMAGE_DROPDOWN_ARROW = 'assets/dropdown_arrow.png';
  static const IMAGE_DURATION = 'assets/icon_duration.png';

  //Profile
  static const IMAGE_PROFILE_PHOTO_PLACEHOLDER = 'assets/user_photo_placeholder.png';
  static const IMAGE_PROFILE_PHOTO_REUPLOAD = 'assets/profile_reupload.png';
  static const IMAGE_PROFILE_VIEWPROFILE = 'assets/viewprofile.png';
  static const IMAGE_PROFILE_PASSWORD = 'assets/password.png';
  static const IMAGE_PROFILE_LANGUAGE = 'assets/language.png';
  static const IMAGE_PROFILE_NOTIFICATIONS = 'assets/notifications.png';
  static const IMAGE_PROFILE_LOGOUT = 'assets/logout.png';
  static const IMAGE_PROFILE_FACEID = 'assets/face_id.png';
  static const IMAGE_PROFILE_FINGER_PRINT = 'assets/finger_print.png';

  static const IMAGE_INITIAL_LEAVE_US_MESSAGE = 'assets/initial_leave_us_message.png';
  static const IMAGE_CALL_REQUEST = 'assets/call_request.png';
  static const IMAGE_SELECT_DATE_BG_WITH_CIRCLE = 'assets/select_date_bg.png';


  //Notifications List
  static const IMAGE_NOTIFICATION_SETTINGS = 'assets/notification_settings.png';




}
