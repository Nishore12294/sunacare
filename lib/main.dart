import 'dart:async';

//import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:key_guardmanager/key_guardmanager.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/router.dart';
import 'package:suna_care/ui/welcome_screen.dart';
import 'package:suna_care/utils/AppLanguage.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_text_style.dart';



void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

  WidgetsFlutterBinding.ensureInitialized();
  AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();
  runApp(
      RestartWidget(
        child: MyApp(appLanguage: appLanguage,)
      ),
      );

  //runApp(MyApp1());


}


class MyApp extends StatelessWidget {
  final AppLanguage appLanguage;

  MyApp({this.appLanguage});

  @override
  Widget build(BuildContext context) {
    // Set portrait orientation
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    return ChangeNotifierProvider<AppLanguage>(
      builder: (_) => appLanguage,
      child: Consumer<AppLanguage>(
          builder: (context, model, child) {
            return MaterialApp(
              locale: AppLanguage.appLocal,
              //locale: Locale('pl',''),
              supportedLocales: [
                Locale('en', 'US'),
                Locale('pl', ''),
              ],
              localizationsDelegates: [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],

              theme: ThemeData(
                fontFamily: AppFont.fontFamilyName,
                textTheme: TextTheme(
                  button: TextStyle(fontWeight: AppFont.fontWeightRegular),
                  caption: TextStyle(fontWeight: AppFont.fontWeightRegular),
                  body1: TextStyle(fontWeight: AppFont.fontWeightRegular),
                  body2: TextStyle(fontWeight: AppFont.fontWeightRegular),
                  subhead: TextStyle(fontWeight: AppFont.fontWeightRegular),
                  title: TextStyle(fontWeight: AppFont.fontWeightRegular),
                  headline: TextStyle(fontWeight: AppFont.fontWeightRegular),
                  display1: TextStyle(fontWeight: AppFont.fontWeightRegular),
                  // display2: TextStyle(fontWeight: AppFont.fontWeightRegulSar),
                  display3: TextStyle(fontWeight: AppFont.fontWeightRegular),
                ),
                primaryColor: colorYellow,
                primaryColorDark: colorFlashGreen,
                accentColor: colorYellow,
                buttonColor: colorPrimary,
                buttonTheme: const ButtonThemeData(buttonColor: colorPrimary, textTheme: ButtonTextTheme.primary),
              ),
              debugShowCheckedModeBanner: false,
              // initialRoute: HowCameYouScreen.routeName,
              initialRoute: WelcomeScreen.routeName,

              onGenerateRoute: MyRouter.generateRoute,
            );
          }),
    );
  }
}


class MyApp1 extends StatefulWidget {
  @override
  _MyAppState1 createState() => _MyAppState1();
}

class _MyAppState1 extends State<MyApp1> {
  final LocalAuthentication auth = LocalAuthentication();
  _SupportState _supportState = _SupportState.unknown;
  bool _canCheckBiometrics;
  List<BiometricType> _availableBiometrics;
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;

  @override
  void initState() {
    super.initState();
    // auth.isDeviceSupported().then(
    //       (isSupported) => setState(() => _supportState = isSupported
    //       ? _SupportState.supported
    //       : _SupportState.unsupported),
    // );
  }

  Future<void> _checkBiometrics() async {
     bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      canCheckBiometrics = false;
      print(e);
    }
    if (!mounted) return;

    setState(()  {
      _canCheckBiometrics = canCheckBiometrics;
      if(_canCheckBiometrics){
        _authenticateWithBiometrics();

      }else{
        //Open the keygurad.
        openKeyGuard();
      }

    });
  }

  Future<void> _getAvailableBiometrics() async {
     List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      availableBiometrics = <BiometricType>[];
      print(e);
    }
    if (!mounted) return;

    setState(() {
      _availableBiometrics = availableBiometrics;
    });
  }

  Future<void> _authenticate() async {
    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });
     // authenticated = await auth.authenticate(
      authenticated = await auth.authenticateWithBiometrics(
          localizedReason: 'Let OS determine authentication method',
          useErrorDialogs: false,
          stickyAuth: true);
      setState(() {
        _isAuthenticating = false;
        _authorized = 'Authenticating';
      });
    } on PlatformException catch (e) {
      print(e);
      setState(() {
        _isAuthenticating = false;
        _authorized = "Error - ${e.message}";
      });
      return;
    }
    if (!mounted) return;

    setState(
      () => _authorized = authenticated ? 'Authorized' : 'Not Authorized');
      }

  Future<void> _authenticateWithBiometrics() async {
    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });
     // authenticated = await auth.authenticate(
     //      localizedReason:
     //      'Scan your fingerprint (or face or whatever) to authenticate',
     //      useErrorDialogs: true,
     //      stickyAuth: true,
     //      biometricOnly: true);

      authenticated = await auth.authenticateWithBiometrics(
          localizedReason:
          'Scan your fingerprint (or face or whatever) to authenticate',
          useErrorDialogs: false,
          stickyAuth: true);
      setState(() {
        _isAuthenticating = false;
        _authorized = 'Authenticating';
      });
    } on PlatformException catch (e) {
      print(e);
      setState(()  {
        _isAuthenticating = false;
        _authorized = "Error - ${e.message}";

        if(e.message == "No Biometrics enrolled on this device."){
          openKeyGuard();
        }else if(e.message == "PasscodeNotSet, Phone not secured by PIN, pattern or password, or SIM is currently locked."){
          openKeyGuard();
        }else if(e.message == "NotEnrolled, No Biometrics enrolled on this device."){
          openKeyGuard();
        }

      });

      return;
    }
    if (!mounted) return;

    final String message = authenticated ? 'Authorized' : 'Not Authorized';
    setState(() {
      _authorized = message;
    });
  }


  void openKeyGuard() async{

      //Open the keygurad.
      String platformAuth = "false";
      try {
        platformAuth = await KeyGuardmanager.authStatus;
        print("platformAuth :==>"+platformAuth);
      } on PlatformException catch (e) {
        print("PlatformException keyguard :==>"+e.message);
        platformAuth = 'Failed to get platform auth.';
      }catch(e){
        print("Exception keyguard :==>"+e.message);
      }

  }

  void _cancelAuthentication() async {
    await auth.stopAuthentication();
    setState(() => _isAuthenticating = false);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: ListView(
          padding: const EdgeInsets.only(top: 30),
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (_supportState == _SupportState.unknown)
                  CircularProgressIndicator()
                else if (_supportState == _SupportState.supported)
                  Text("This device is supported")
                else
                  Text("This device is not supported"),
                Divider(height: 100),
                Text('Can check biometrics: $_canCheckBiometrics\n'),
                ElevatedButton(
                  child: const Text('Check biometrics'),
                  onPressed: _checkBiometrics,
                ),
                Divider(height: 100),
                Text('Available biometrics: $_availableBiometrics\n'),
                ElevatedButton(
                  child: const Text('Get available biometrics'),
                  onPressed: _getAvailableBiometrics,
                ),
                Divider(height: 100),
                Text('Current State: $_authorized\n'),
                (_isAuthenticating)
                    ? ElevatedButton(
                  onPressed: _cancelAuthentication,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Cancel Authentication"),
                      Icon(Icons.cancel),
                    ],
                  ),
                )
                    : Column(
                  children: [
                    ElevatedButton(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text('Authenticate'),
                          Icon(Icons.perm_device_information),
                        ],
                      ),
                      onPressed: _authenticate,
                    ),
                    ElevatedButton(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(_isAuthenticating
                              ? 'Cancel'
                              : 'Authenticate: biometrics only'),
                          Icon(Icons.fingerprint),
                        ],
                      ),
                      onPressed: _authenticateWithBiometrics,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

enum _SupportState {
  unknown,
  supported,
  unsupported,
}


////Key guard /////
class MyApp2 extends StatefulWidget {
  @override
  _MyAppState2 createState() => _MyAppState2();
}

class _MyAppState2 extends State<MyApp2>{
  String _checkAuth = 'false';

  @override
  void initState() {
    super.initState();
    initPlatformState();


  }


  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformAuth = "";
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformAuth = await KeyGuardmanager.authStatus;

    }  on PlatformException {
      platformAuth = 'Failed to get platform auth.';
    }catch (e) {
      print("catch keyguard :==>"+e.message);
    }




    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
   if (!mounted) return;

    setState(() {
      _checkAuth = platformAuth;
    });

    if(_checkAuth == 'true')
      print("platformAuth keyguard bool :==>"+_checkAuth);
    else
      print("platformAuth keyguard bool :==>"+_checkAuth);
  }

  @override
  Widget build(BuildContext context) {
    // if(_checkAuth == 'true')
    //   print("Widget build keyguard bool :==>"+_checkAuth);
    // else
    //   print("Widget build keyguard bool :==>"+_checkAuth);

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text('Running on: $_checkAuth\n'),
        ),
      ),
    );
  }
}


class RestartWidget extends StatefulWidget {
  RestartWidget({this.child});

  final Widget child;

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_RestartWidgetState>().restartApp();
  }

  @override
  _RestartWidgetState createState() => _RestartWidgetState();
}

class _RestartWidgetState extends State<RestartWidget> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: widget.child,
    );
  }
}
