import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' show Client;
import 'package:suna_care/utils/app_log_helper.dart';

import 'app_url.dart';
import 'method.dart';

class NetworkProvider {
  final log = getLogger('NetworkProvider HTTP');

  // singleton boilerplate
  NetworkProvider._internal();

  static final NetworkProvider _singleInstance = NetworkProvider._internal();

  factory NetworkProvider() => _singleInstance;

  Client client = Client();

  final String _tagRequest = '::::: Request :::::';
  final String _tagResponse = '----- Response -----';
  final String _tagResponseDecoded = '----- Response Decoded-----';

  Future callFullRequestEncoded(
      {@required String pathUrl,
      var queryParam,
      headers,
      Encoding encoding,
      @required Method method,
      Map<String, dynamic> body}) async {
    var responseData;

    var url;


    if (queryParam == null) {
      url = AppUrl.baseUrl + pathUrl;
    } else {
      url = Uri.http(AppUrl.baseHost, pathUrl, queryParam);
    }
    log.i('$_tagRequest \n $url');
    log.i('Request body ::: ${json.encode(body).toString()}');
    switch (method) {
      case Method.GET:
        responseData = await client.get(url, headers: headers);
        break;
      case Method.POST:
        responseData =
            await client.post(url, body: json.encode(body), headers: headers);
        break;
      case Method.PUT:
        responseData = await client.put(url,
            body: body, headers: headers, encoding: encoding);
        break;
      case Method.DELETE:
        responseData = await client.delete(url, headers: headers);
        break;
    }
    log.i('$_tagResponse \n ${responseData.body}');

    return responseData;
  }

  Future call(
      {@required String pathUrl,
      var queryParam,
      headers,
      Encoding encoding,
      @required Method method,
      Map<String, dynamic> body}) async {
    var responseData;

    var url;

//    ///todo temp-
//    if (pathUrl ==
//         'http://wallet.kanchitraditions.com/public/api/v1/get_voucher_list?discount_type=0' ||
//        pathUrl =='http://wallet.kanchitraditions.com/public/api/v1/get_redeemed_voucher_list?discount_type=0') {
//      url = pathUrl;
//    } else
//
    if (queryParam == null) {
      url = AppUrl.baseUrl + pathUrl;
    } else {
      url = Uri.http(AppUrl.baseHost, pathUrl, queryParam);
    }
    log.i('$_tagRequest \n $url');
    log.i('Request body ::: ${body.toString()}');
    switch (method) {
      case Method.GET:
        responseData = await client.get(url, headers: headers);
        break;
      case Method.POST:
        responseData =
            await client.post(url, body: body, headers: headers);
//             await client.post(url, body: body, headers: headers).then((response){
//               print(response.statusCode.toString());
//               print(response.body);
//             });
        break;
      case Method.PUT:
        responseData = await client.put(url,
            body: body, headers: headers, encoding: encoding);
        break;
      case Method.DELETE:
        responseData = await client.delete(url, headers: headers);
        break;
    }
    log.i('$_tagResponse \n ${responseData.body}');
    log.i('$_tagResponseDecoded \n ${json.decode(responseData.body)}');
    log.i('"Reponse network provider" \n ${responseData.body}');

    return responseData;
  }


}
