
//UAT Url
class AppUrl {
  //https://cg-app.azurewebsites.net/LoginAPI.asmx/VerifyCGCredentials
  static final baseHost = "";
 // static final baseUrl = "http://$baseHost";
  static final baseUrl = "https://cg-app.azurewebsites.net";
  static final baseUrlMedia = "$baseUrl//";

  //login
  static final pathLogin = "/LoginAPI.asmx/VerifyCGCredentials"; //POST
  //SignUp
  static final pathSignUp = "/LeadAPI.asmx/CreateLead"; //POST

  //Forgot password
  static final pathForgotPassword = "/LoginAPI.asmx/ForgotPassword"; //POST

  // Forgot password
  static final pathForgotSetPassword = "/LoginAPI.asmx/ForgotSetPassword"; //POST

  //Change password
  static final pathChangePassword = "/LoginAPI.asmx/ChangeAppPassword"; //POST

  // Forgot password
  static final pathVerifyAccessToken = "/LoginAPI.asmx/VerifyAccessToken"; //POST


  // Get Assignment List
  static final pathGetAssignmentList = "/AssignmentAPI.asmx/GetCGAssignmentsList"; //POST


  // Get Assignment List
  static final pathHolidayList = "/AssignmentAPI.asmx/GetCGHolidaysList"; //POST

//  AddHoliday
  static final pathAddHoliday = "/AssignmentAPI.asmx/AddHolidayCGSchedule"; //POST



// Edit holiday
  static final pathEditHoliday = "/AssignmentAPI.asmx/EditHolidayCGSchedule"; //POST

  // Delete holiday
  static final pathDeletHoliday = "/AssignmentAPI.asmx/DeleteHolidayCGSchedule"; //POST

  // Offers List
  static final pathOffersList = "/CareGiverAPI.asmx/GetAllCGOffers"; //POST

  // GetCGOne OfferDetails
  static final pathOfferDetails = "/CareGiverAPI.asmx/GetCGOneOfferDetails"; //POST

  // Get CGApproved Image
  static final pathCGApprovedImage = "/CaregiverAPI.asmx/GetCGApprovedImage"; //POST

  // Get CGApproved Image
  static final pathUploadCGApprovedImage = "/CaregiverAPI.asmx/SetCGProfilePicture"; //POST

  // Get CGApproved Image
  static final pathCGProfile = "/CareGiverAPI.asmx/GetCGDetails"; //POST

  // Set CG Language
  static final pathSetLanguage = "/CaregiverAPI.asmx/SetCGPreferredLanguage"; //POST

  // Get CG Language
  static final pathGetLanguage = "/CaregiverAPI.asmx/GetCGPreferredLanguage"; //POST

  // Get CG OffersCities
  static final pathGetCGOffersCities = "/CaregiverAPI.asmx/GetAllCGOffersCities"; //POST

  // Set CG Notification
  static final pathSetCGNotification = "/CaregiverAPI.asmx/SetCGNotifications"; //POST


  // Get CG Notification
  static final pathGetCGNotification = "/CaregiverAPI.asmx/GetCGNotifications"; //POST

  // Call request
  static final pathGetCallRequest = "/CaregiverAPI.asmx/GetCallRequest"; //POST


  // Leave us message
  static final pathLeaveUsMessage = "/CaregiverAPI.asmx/GetCGLeaveMessage"; //POST


  // Schedule Appointment or meeting
  static final pathSchdeuleAppointment = "/CaregiverAPI.asmx/ScheduleAppointment"; //POST




}



//Dev Url

/*

class AppUrl {
  static final baseHost = "18.220.98.106";
  // static final baseUrl = "http://$baseHost";
  static final baseUrl = "https://crm.direction.biz";
  static final baseUrlMedia = "$baseUrl//";
  static final defaultServiceImage =
      'https://user-images.githubusercontent.com/15840617/31617371-ae68471e-b297-11e7-9981-269c9bb17330.png';
  static final defaultVoucherImage =
      "https://i7.uihere.com/icons/886/434/514/voucher-01a2248ccc92b5962d94a382adfb71e4.png";

  //login
  static final pathLogin = "/sunacaredevpl/Webservice/CGApp/LoginAPI.asmx/VerifyCGCredentials"; //POST
  //SignUp
  static final pathSignUp = "/sunacaredevpl/Webservice/CGApp/LeadAPI.asmx/CreateLead"; //POST

  //Forgot password
  static final pathForgotPassword = "/sunacaredevpl/Webservice/CGApp/LoginAPI.asmx/ForgotPassword"; //POST

  // Forgot password
  static final pathForgotSetPassword = "/sunacaredevpl/Webservice/CGApp/LoginAPI.asmx/ForgotSetPassword"; //POST

  //Change password
  static final pathChangePassword = "/sunacaredevpl/Webservice/CGApp/LoginAPI.asmx/ChangeAppPassword"; //POST

  // Forgot password
  static final pathVerifyAccessToken = "/sunacaredevpl/Webservice/CGApp/LoginAPI.asmx/VerifyAccessToken"; //POST


  // Get Assignment List
  static final pathGetAssignmentList = "/sunacaredevpl/Webservice/CGApp/AssignmentAPI.asmx/GetCGAssignmentsList"; //POST


  // Get Assignment List
  static final pathHolidayList = "/sunacaredevpl/Webservice/CGApp/AssignmentAPI.asmx/GetCGHolidaysList"; //POST

//  AddHoliday
  static final pathAddHoliday = "/sunacaredevpl/Webservice/CGApp/AssignmentAPI.asmx/AddHolidayCGSchedule"; //POST



// Edit holiday
  static final pathEditHoliday = "/sunacaredevpl/Webservice/CGApp/AssignmentAPI.asmx/EditHolidayCGSchedule"; //POST

  // Delete holiday
  static final pathDeletHoliday = "/sunacaredevpl/Webservice/CGApp/AssignmentAPI.asmx/DeleteHolidayCGSchedule"; //POST

  // Offers List
  static final pathOffersList = "/sunacaredevpl/Webservice/CGApp/CareGiverAPI.asmx/GetAllCGOffers"; //POST

  // GetCGOne OfferDetails
  static final pathOfferDetails = "/sunacaredevpl/Webservice/CGApp/CareGiverAPI.asmx/GetCGOneOfferDetails"; //POST

  // Get CGApproved Image
  static final pathCGApprovedImage = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/GetCGApprovedImage"; //POST

  // Get CGApproved Image
  static final pathUploadCGApprovedImage = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/SetCGProfilePicture"; //POST

  // Get CGApproved Image
  static final pathCGProfile = "/sunacaredevpl/Webservice/CGApp/CareGiverAPI.asmx/GetCGDetails"; //POST

  // Set CG Language
  static final pathSetLanguage = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/SetCGPreferredLanguage"; //POST

  // Get CG Language
  static final pathGetLanguage = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/GetCGPreferredLanguage"; //POST

  // Get CG OffersCities
  static final pathGetCGOffersCities = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/GetAllCGOffersCities"; //POST

  // Set CG Notification
  static final pathSetCGNotification = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/SetCGNotifications"; //POST


  // Get CG Notification
  static final pathGetCGNotification = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/GetCGNotifications"; //POST

  // Call request
  static final pathGetCallRequest = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/GetCallRequest"; //POST


  // Leave us message
  static final pathLeaveUsMessage = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/GetCGLeaveMessage"; //POST


// Schedule Appointment or meeting
  static final pathSchdeuleAppointment = "/sunacaredevpl/Webservice/CGApp/CaregiverAPI.asmx/ScheduleAppointment"; //POST




}
*/


