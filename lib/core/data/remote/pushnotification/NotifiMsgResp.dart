// To parse this JSON data, do
//
//     final notifiMsgResp = notifiMsgRespFromJson(jsonString);

import 'dart:convert';

NotifiMsgResp notifiMsgRespFromJson(String str) => NotifiMsgResp.fromJson(json.decode(str));

String notifiMsgRespToJson(NotifiMsgResp data) => json.encode(data.toJson());

class NotifiMsgResp {
  NotifiMsgResp({
    this.channelKey,
    this.id,
    this.title,
    this.body,
  });

  String channelKey;
  int id;
  String title;
  String body;

  factory NotifiMsgResp.fromJson(Map<String, dynamic> json) => NotifiMsgResp(
    channelKey: json["channelKey"],
    id: json["id"],
    title: json["title"],
    body: json["body"],
  );

  Map<String, dynamic> toJson() => {
    "channelKey": channelKey,
    "id": id,
    "title": title,
    "body": body,
  };
}
