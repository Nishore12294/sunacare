// To parse this JSON data, do
//
//     final offersDetailsResponse = offersDetailsResponseFromJson(jsonString);

// To parse this JSON data, do
//
//     final offersDetailsResponse = offersDetailsResponseFromJson(jsonString);

import 'dart:convert';

OffersDetailsResponse offersDetailsResponseFromJson(String str) => OffersDetailsResponse.fromJson(json.decode(str));

String offersDetailsResponseToJson(OffersDetailsResponse data) => json.encode(data.toJson());

class OffersDetailsResponse {
  OffersDetailsResponse({
    this.sessionStatus,
    this.webMethodOutputRows,
  });

  OffersDetailsResponseSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;

  factory OffersDetailsResponse.fromJson(Map<String, dynamic> json) => OffersDetailsResponse(
    sessionStatus: OffersDetailsResponseSessionStatus.fromJson(json["SessionStatus"]),
    webMethodOutputRows: WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": sessionStatus.toJson(),
    "WebMethodOutputRows": webMethodOutputRows.toJson(),
  };
}

class OffersDetailsResponseSessionStatus {
  OffersDetailsResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory OffersDetailsResponseSessionStatus.fromJson(Map<String, dynamic> json) => OffersDetailsResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<Map<String, String>> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<Map<String, String>>.from(json["WebMethodOutputRows"].map((x) => Map.from(x).map((k, v) => MapEntry<String, String>(k, v)))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))),
  };
}

