// To parse this JSON data, do
//
//     final cgOffersCitiesList = cgOffersCitiesListFromJson(jsonString);

import 'dart:convert';

CgOffersCitiesList cgOffersCitiesListFromJson(String str) => CgOffersCitiesList.fromJson(json.decode(str));

String cgOffersCitiesListToJson(CgOffersCitiesList data) => json.encode(data.toJson());

class CgOffersCitiesList {
  CgOffersCitiesList({
    this.sessionStatus,
    this.webMethodOutputRows,
  });

  CgOffersCitiesListSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;

  factory CgOffersCitiesList.fromJson(Map<String, dynamic> json) => CgOffersCitiesList(
    sessionStatus: CgOffersCitiesListSessionStatus.fromJson(json["SessionStatus"]),
    webMethodOutputRows: WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": sessionStatus.toJson(),
    "WebMethodOutputRows": webMethodOutputRows.toJson(),
  };
}

class CgOffersCitiesListSessionStatus {
  CgOffersCitiesListSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory CgOffersCitiesListSessionStatus.fromJson(Map<String, dynamic> json) => CgOffersCitiesListSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.cityOfCp,
  });

  String cityOfCp;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    cityOfCp: json["CityOfCP"],
  );

  Map<String, dynamic> toJson() => {
    "CityOfCP": cityOfCp,
  };
}
