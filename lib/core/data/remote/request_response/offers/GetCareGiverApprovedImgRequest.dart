// To parse this JSON data, do
//
//     final getCareGiverApprovedImgRequest = getCareGiverApprovedImgRequestFromJson(jsonString);

import 'dart:convert';

List<GetCareGiverApprovedImgRequest> getCareGiverApprovedImgRequestFromJson(String str) => List<GetCareGiverApprovedImgRequest>.from(json.decode(str).map((x) => GetCareGiverApprovedImgRequest.fromJson(x)));

String getCareGiverApprovedImgRequestToJson(List<GetCareGiverApprovedImgRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetCareGiverApprovedImgRequest {
  GetCareGiverApprovedImgRequest({
    this.accessToken,
  });

  String accessToken;

  factory GetCareGiverApprovedImgRequest.fromJson(Map<String, dynamic> json) => GetCareGiverApprovedImgRequest(
    accessToken: json["AccessToken"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
  };
}
