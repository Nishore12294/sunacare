// To parse this JSON data, do
//
//     final offersDetailsRequest = offersDetailsRequestFromJson(jsonString);

import 'dart:convert';

List<OffersDetailsRequest> offersDetailsRequestFromJson(String str) => List<OffersDetailsRequest>.from(json.decode(str).map((x) => OffersDetailsRequest.fromJson(x)));

String offersDetailsRequestToJson(List<OffersDetailsRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OffersDetailsRequest {
  OffersDetailsRequest({
    this.offerGuid,
    this.accessToken,
  });

  String offerGuid;
  String accessToken;

  factory OffersDetailsRequest.fromJson(Map<String, dynamic> json) => OffersDetailsRequest(
    offerGuid: json["OfferGuid"],
    accessToken: json["AccessToken"],
  );

  Map<String, dynamic> toJson() => {
    "OfferGuid": offerGuid,
    "AccessToken": accessToken,
  };
}
