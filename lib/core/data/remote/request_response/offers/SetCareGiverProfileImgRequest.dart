// To parse this JSON data, do
//
//     final setCareGiverProfileImgRequest = setCareGiverProfileImgRequestFromJson(jsonString);

import 'dart:convert';

import 'dart:typed_data';

List<SetCareGiverProfileImgRequest> setCareGiverProfileImgRequestFromJson(String str) => List<SetCareGiverProfileImgRequest>.from(json.decode(str).map((x) => SetCareGiverProfileImgRequest.fromJson(x)));

String setCareGiverProfileImgRequestToJson(List<SetCareGiverProfileImgRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SetCareGiverProfileImgRequest {
  SetCareGiverProfileImgRequest({
    this.accessToken,
    this.imageByteArray,
  });

  String accessToken;
  String imageByteArray;
 // List<int> imageByteArray;

  factory SetCareGiverProfileImgRequest.fromJson(Map<String, dynamic> json) => SetCareGiverProfileImgRequest(
    accessToken: json["AccessToken"],
    imageByteArray: json["ImageByteArray"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "ImageByteArray": imageByteArray,
  };
}
