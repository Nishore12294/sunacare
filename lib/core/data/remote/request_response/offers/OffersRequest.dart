// To parse this JSON data, do
//
//     final offersRequest = offersRequestFromJson(jsonString);

/*
import 'dart:convert';

List<OffersRequest> offersRequestFromJson(String str) => List<OffersRequest>.from(json.decode(str).map((x) => OffersRequest.fromJson(x)));

String offersRequestToJson(List<OffersRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OffersRequest {
  OffersRequest({
    this.accessToken,
  });

  String accessToken;

  factory OffersRequest.fromJson(Map<String, dynamic> json) => OffersRequest(
    accessToken: json["AccessToken"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
  };
}
*/

// To parse this JSON data, do
//
//     final offersRequest = offersRequestFromJson(jsonString);

import 'dart:convert';

List<OffersRequest> offersRequestFromJson(String str) => List<OffersRequest>.from(json.decode(str).map((x) => OffersRequest.fromJson(x)));

String offersRequestToJson(List<OffersRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OffersRequest {
  OffersRequest({
    this.accessToken,
    this.durationCode,
    this.cityOfCp,
    this.germanLevelCode,
  });

  String accessToken;
  String durationCode;
  String cityOfCp;
  String germanLevelCode;

  factory OffersRequest.fromJson(Map<String, dynamic> json) => OffersRequest(
    accessToken: json["AccessToken"],
    durationCode: json["DurationCode"],
    cityOfCp: json["CityOfCP"],
    germanLevelCode: json["GermanLevelCode"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "DurationCode": durationCode,
    "CityOfCP": cityOfCp,
    "GermanLevelCode": germanLevelCode,
  };
}
