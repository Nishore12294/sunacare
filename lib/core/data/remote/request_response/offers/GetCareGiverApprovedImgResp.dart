// To parse this JSON data, do
//
//     final getCareGiverApprovedImgResp = getCareGiverApprovedImgRespFromJson(jsonString);

// import 'dart:convert';
//
// GetCareGiverApprovedImgResp getCareGiverApprovedImgRespFromJson(String str) => GetCareGiverApprovedImgResp.fromJson(json.decode(str));
//
// String getCareGiverApprovedImgRespToJson(GetCareGiverApprovedImgResp data) => json.encode(data.toJson());
//
// class GetCareGiverApprovedImgResp {
//   GetCareGiverApprovedImgResp({
//     this.sessionStatus,
//     this.outputRowErrors,
//     this.webMethodOutputRows,
//   });
//
//   GetCareGiverApprovedImgRespSessionStatus sessionStatus;
//   OutputRowErrors outputRowErrors;
//   WebMethodOutputRows webMethodOutputRows;
//
//   factory GetCareGiverApprovedImgResp.fromJson(Map<String, dynamic> json) => GetCareGiverApprovedImgResp(
//     sessionStatus: json.containsKey("SessionStatus")?GetCareGiverApprovedImgRespSessionStatus.fromJson(json["SessionStatus"]):null,
//     outputRowErrors: json.containsKey("OutputRowErrors")?OutputRowErrors.fromJson(json["OutputRowErrors"]):null,
//     webMethodOutputRows: json.containsKey("WebMethodOutputRows")?WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]):null,
//   );
//
//   Map<String, dynamic> toJson() => {
//     "SessionStatus": (sessionStatus!=null)?sessionStatus.toJson():null,
//     "OutputRowErrors":(outputRowErrors!=null)? outputRowErrors.toJson():null,
//     "WebMethodOutputRows": (webMethodOutputRows!=null)?webMethodOutputRows.toJson():null,
//   };
// }
//
// class OutputRowErrors {
//   OutputRowErrors({
//     this.outputRowErrors,
//   });
//
//   List<OutputRowError> outputRowErrors;
//
//   factory OutputRowErrors.fromJson(Map<String, dynamic> json) => OutputRowErrors(
//     outputRowErrors: List<OutputRowError>.from(json["OutputRowErrors"].map((x) => OutputRowError.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "OutputRowErrors": List<dynamic>.from(outputRowErrors.map((x) => x.toJson())),
//   };
// }
//
// class OutputRowError {
//   OutputRowError({
//     this.rowNo,
//     this.typeErrorOrWarning,
//     this.textMessage,
//     this.verbalText,
//     this.columnName,
//   });
//
//   String rowNo;
//   String typeErrorOrWarning;
//   String textMessage;
//   String verbalText;
//   String columnName;
//
//   factory OutputRowError.fromJson(Map<String, dynamic> json) => OutputRowError(
//     rowNo: json["RowNo"],
//     typeErrorOrWarning: json["TypeErrorOrWarning"],
//     textMessage: json["TextMessage"],
//     verbalText: json["VerbalText"],
//     columnName: json["ColumnName"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "RowNo": rowNo,
//     "TypeErrorOrWarning": typeErrorOrWarning,
//     "TextMessage": textMessage,
//     "VerbalText": verbalText,
//     "ColumnName": columnName,
//   };
// }
//
// class GetCareGiverApprovedImgRespSessionStatus {
//   GetCareGiverApprovedImgRespSessionStatus({
//     this.sessionStatus,
//   });
//
//   List<SessionStatusElement> sessionStatus;
//
//   factory GetCareGiverApprovedImgRespSessionStatus.fromJson(Map<String, dynamic> json) => GetCareGiverApprovedImgRespSessionStatus(
//     sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
//   };
// }
//
// class SessionStatusElement {
//   SessionStatusElement({
//     this.runStatus,
//     this.sessionId,
//   });
//
//   String runStatus;
//   String sessionId;
//
//   factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
//     runStatus: json["RunStatus"],
//     sessionId: json["SessionID"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "RunStatus": runStatus,
//     "SessionID": sessionId,
//   };
// }
//
// class WebMethodOutputRows {
//   WebMethodOutputRows({
//     this.webMethodOutputRows,
//   });
//
//   List<WebMethodOutputRow> webMethodOutputRows;
//
//   factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
//     webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
//   };
// }
//
// class WebMethodOutputRow {
//   WebMethodOutputRow({
//     this.imgPath,
//     this.imgDownloadStatus,
//   });
//
//   String imgPath;
//   String imgDownloadStatus;
//
//   factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
//     imgPath: json["ImgPath"],
//     imgDownloadStatus: json["ImgDownloadStatus"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "ImgPath": imgPath,
//     "ImgDownloadStatus": imgDownloadStatus,
//   };
// }


// To parse this JSON data, do
//
//     final getCareGiverApprovedImgResp = getCareGiverApprovedImgRespFromJson(jsonString);

import 'dart:convert';

GetCareGiverApprovedImgResp getCareGiverApprovedImgRespFromJson(String str) => GetCareGiverApprovedImgResp.fromJson(json.decode(str));

String getCareGiverApprovedImgRespToJson(GetCareGiverApprovedImgResp data) => json.encode(data.toJson());

class GetCareGiverApprovedImgResp {
  GetCareGiverApprovedImgResp({
    this.sessionStatus,
    this.webMethodOutputRows,
    this.outputRowErrors,
  });

  GetCareGiverApprovedImgRespSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;
  OutputRowErrors outputRowErrors;

  factory GetCareGiverApprovedImgResp.fromJson(Map<String, dynamic> json) => GetCareGiverApprovedImgResp(
    sessionStatus: json.containsKey("SessionStatus")?GetCareGiverApprovedImgRespSessionStatus.fromJson(json["SessionStatus"]):null,
    webMethodOutputRows: json.containsKey("WebMethodOutputRows")?WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]):null,
    outputRowErrors: json.containsKey("OutputRowErrors")?OutputRowErrors.fromJson(json["OutputRowErrors"]):null,
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": (sessionStatus != null)?sessionStatus.toJson():null,
    "WebMethodOutputRows": (webMethodOutputRows != null)?webMethodOutputRows.toJson():null,
    "OutputRowErrors": (outputRowErrors != null)?outputRowErrors.toJson():null,
  };
}

class OutputRowErrors {
  OutputRowErrors({
    this.outputRowErrors,
  });

  List<OutputRowError> outputRowErrors;

  factory OutputRowErrors.fromJson(Map<String, dynamic> json) => OutputRowErrors(
    outputRowErrors: List<OutputRowError>.from(json["OutputRowErrors"].map((x) => OutputRowError.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "OutputRowErrors": List<dynamic>.from(outputRowErrors.map((x) => x.toJson())),
  };
}

class OutputRowError {
  OutputRowError({
    this.rowNo,
    this.typeErrorOrWarning,
    this.textMessage,
    this.verbalText,
    this.columnName,
  });

  String rowNo;
  String typeErrorOrWarning;
  String textMessage;
  String verbalText;
  String columnName;

  factory OutputRowError.fromJson(Map<String, dynamic> json) => OutputRowError(
    rowNo: json["RowNo"],
    typeErrorOrWarning: json["TypeErrorOrWarning"],
    textMessage: json["TextMessage"],
    verbalText: json["VerbalText"],
    columnName: json["ColumnName"],
  );

  Map<String, dynamic> toJson() => {
    "RowNo": rowNo,
    "TypeErrorOrWarning": typeErrorOrWarning,
    "TextMessage": textMessage,
    "VerbalText": verbalText,
    "ColumnName": columnName,
  };
}

class GetCareGiverApprovedImgRespSessionStatus {
  GetCareGiverApprovedImgRespSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory GetCareGiverApprovedImgRespSessionStatus.fromJson(Map<String, dynamic> json) => GetCareGiverApprovedImgRespSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.imgUrl,
    this.imgDownloadStatus,
  });

  String imgUrl;
  String imgDownloadStatus;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    imgUrl: json["ImgUrl"],
    imgDownloadStatus: json["ImgDownloadStatus"],
  );

  Map<String, dynamic> toJson() => {
    "ImgUrl": imgUrl,
    "ImgDownloadStatus": imgDownloadStatus,
  };
}
