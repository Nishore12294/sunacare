// To parse this JSON data, do
//
//     final offersListResponse = offersListResponseFromJson(jsonString);

import 'dart:convert';

OffersListResponse offersListResponseFromJson(String str) => OffersListResponse.fromJson(json.decode(str));

String offersListResponseToJson(OffersListResponse data) => json.encode(data.toJson());

class OffersListResponse {
  OffersListResponse({
    this.sessionStatus,
    this.webMethodOutputRows,
    this.outputRowErrors,
  });

  OffersListResponseSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;
  OutputRowErrors outputRowErrors;

  factory OffersListResponse.fromJson(Map<String, dynamic> json) => OffersListResponse(
    sessionStatus: json.containsKey("SessionStatus")?OffersListResponseSessionStatus.fromJson(json["SessionStatus"]):null,
    webMethodOutputRows: json.containsKey("WebMethodOutputRows")?WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]):null,
    outputRowErrors: json.containsKey("OutputRowErrors")?OutputRowErrors.fromJson(json["OutputRowErrors"]):null,
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": (sessionStatus != null)?sessionStatus.toJson():null,
    "WebMethodOutputRows": (webMethodOutputRows != null)?webMethodOutputRows.toJson():null,
    "OutputRowErrors":  (outputRowErrors != null)?outputRowErrors.toJson():null,
  };
}

class OutputRowErrors {
  OutputRowErrors({
    this.outputRowErrors,
  });

  List<OutputRowError> outputRowErrors;

  factory OutputRowErrors.fromJson(Map<String, dynamic> json) => OutputRowErrors(
    outputRowErrors: List<OutputRowError>.from(json["OutputRowErrors"].map((x) => OutputRowError.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "OutputRowErrors": List<dynamic>.from(outputRowErrors.map((x) => x.toJson())),
  };
}

class OutputRowError {
  OutputRowError({
    this.rowNo,
    this.typeErrorOrWarning,
    this.textMessage,
    this.verbalText,
    this.columnName,
  });

  String rowNo;
  String typeErrorOrWarning;
  String textMessage;
  String verbalText;
  String columnName;

  factory OutputRowError.fromJson(Map<String, dynamic> json) => OutputRowError(
    rowNo: json["RowNo"],
    typeErrorOrWarning: json["TypeErrorOrWarning"],
    textMessage: json["TextMessage"],
    verbalText: json["VerbalText"],
    columnName: json["ColumnName"],
  );

  Map<String, dynamic> toJson() => {
    "RowNo": rowNo,
    "TypeErrorOrWarning": typeErrorOrWarning,
    "TextMessage": textMessage,
    "VerbalText": verbalText,
    "ColumnName": columnName,
  };
}

class OffersListResponseSessionStatus {
  OffersListResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory OffersListResponseSessionStatus.fromJson(Map<String, dynamic> json) => OffersListResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.address,
    this.city,
    this.postalCode,
    this.earliestArrivalDate,
    this.duration,
    this.nettoPayment,
    this.germanLevel,
    this.offerGuid,
  });

  String address;
  String city;
  String postalCode;
  String earliestArrivalDate;
  String duration;
  String nettoPayment;
  String germanLevel;
  String offerGuid;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    address: json["Address"],
    city: json["City"],
    postalCode: json["PostalCode"],
    earliestArrivalDate: json["EarliestArrivalDate"],
    duration: json["Duration"],
    nettoPayment: json["NettoPayment"],
    germanLevel: json["GermanLevel"],
    offerGuid: json["OfferGuid"],
  );

  Map<String, dynamic> toJson() => {
    "Address": address,
    "City": city,
    "PostalCode": postalCode,
    "EarliestArrivalDate": earliestArrivalDate,
    "Duration": duration,
    "NettoPayment": nettoPayment,
    "GermanLevel": germanLevel,
    "OfferGuid": offerGuid,
  };
}
