class LoginRequest {
  String emailAddress;
  String password;
  String deviceID;
  String deviceToken;
  String deviceType;

  LoginRequest(
      {this.emailAddress,
        this.password,
        this.deviceID,
        this.deviceToken,
        this.deviceType});

  LoginRequest.fromJson(Map<String, dynamic> json) {
    emailAddress = json['EmailAddress'];
    password = json['Password'];
    deviceID = json['DeviceID'];
    deviceToken = json['DeviceToken'];
    deviceType = json['DeviceType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EmailAddress'] = this.emailAddress;
    data['Password'] = this.password;
    data['DeviceID'] = this.deviceID;
    data['DeviceToken'] = this.deviceToken;
    data['DeviceType'] = this.deviceType;
    return data;
  }
}