// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

// import 'dart:convert';
//
// LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));
//
// String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());
//
// class LoginResponse {
//   LoginResponse({
//     this.sessionStatus,
//     this.webMethodOutputRows,
//   });
//
//   LoginResponseSessionStatus sessionStatus;
//   WebMethodOutputRows webMethodOutputRows;
//
//   factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
//     sessionStatus: LoginResponseSessionStatus.fromJson(json["SessionStatus"]),
//     webMethodOutputRows: WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "SessionStatus": (sessionStatus!=null)?sessionStatus.toJson():null,
//     "WebMethodOutputRows": (webMethodOutputRows!=null)?webMethodOutputRows.toJson():null,
//   };
// }
//
// class LoginResponseSessionStatus {
//   LoginResponseSessionStatus({
//     this.sessionStatus,
//   });
//
//   List<SessionStatusElement> sessionStatus;
//
//   factory LoginResponseSessionStatus.fromJson(Map<String, dynamic> json) => LoginResponseSessionStatus(
//     sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
//   };
// }
//
// class SessionStatusElement {
//   SessionStatusElement({
//     this.runStatus,
//     this.sessionId,
//   });
//
//   String runStatus;
//   String sessionId;
//
//   factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
//     runStatus: json["RunStatus"],
//     sessionId: json["SessionID"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "RunStatus": runStatus,
//     "SessionID": sessionId,
//   };
// }
//
// class WebMethodOutputRows {
//   WebMethodOutputRows({
//     this.webMethodOutputRows,
//   });
//
//   List<WebMethodOutputRow> webMethodOutputRows;
//
//   factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
//     webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
//   };
// }
//
// class WebMethodOutputRow {
//   WebMethodOutputRow({
//     this.cgStatus,
//     this.accessToken,
//   });
//
//   String cgStatus;
//   String accessToken;
//
//   factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
//     cgStatus: json["CG_Status"],
//     accessToken: json["AccessToken"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "CG_Status": cgStatus,
//     "AccessToken": accessToken,
//   };
// }



// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.sessionStatus,
    this.webMethodOutputRows,
  });

  LoginResponseSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    sessionStatus: LoginResponseSessionStatus.fromJson(json["SessionStatus"]),
    webMethodOutputRows: WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": sessionStatus.toJson(),
    "WebMethodOutputRows": webMethodOutputRows.toJson(),
  };
}

class LoginResponseSessionStatus {
  LoginResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory LoginResponseSessionStatus.fromJson(Map<String, dynamic> json) => LoginResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.cgStatus,
    this.accessToken,
    this.isFirstTimeLogin,
  });

  String cgStatus;
  String accessToken;
  String isFirstTimeLogin;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    cgStatus: json["CG_Status"],
    accessToken: json["AccessToken"],
    isFirstTimeLogin: json["IsFirstTimeLogin"],
  );

  Map<String, dynamic> toJson() => {
    "CG_Status": cgStatus,
    "AccessToken": accessToken,
    "IsFirstTimeLogin": isFirstTimeLogin,
  };
}
