// To parse this JSON data, do
//
//     final changePasswordRequest = changePasswordRequestFromJson(jsonString);

import 'dart:convert';

List<ChangePasswordRequest> changePasswordRequestFromJson(String str) => List<ChangePasswordRequest>.from(json.decode(str).map((x) => ChangePasswordRequest.fromJson(x)));

String changePasswordRequestToJson(List<ChangePasswordRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ChangePasswordRequest {
  ChangePasswordRequest({
    this.accessToken,
    this.oldAppPassword,
    this.newAppPassword,
  });

  String accessToken;
  String oldAppPassword;
  String newAppPassword;

  factory ChangePasswordRequest.fromJson(Map<String, dynamic> json) => ChangePasswordRequest(
    accessToken: json["AccessToken"],
    oldAppPassword: json["OldAppPassword"],
    newAppPassword: json["NewAppPassword"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "OldAppPassword": oldAppPassword,
    "NewAppPassword": newAppPassword,
  };
}
