
import 'dart:convert';

VerifyAccessTokenResponse verifyAccessTokenResponseFromJson(String str) => VerifyAccessTokenResponse.fromJson(json.decode(str));

String verifyAccessTokenResponseToJson(VerifyAccessTokenResponse data) => json.encode(data.toJson());

class VerifyAccessTokenResponse {
  VerifyAccessTokenResponse({
    this.sessionStatus,
    this.webMethodOutputRows,
  });

  VerifyAccessTokenResponseSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;

  factory VerifyAccessTokenResponse.fromJson(Map<String, dynamic> json) => VerifyAccessTokenResponse(
    sessionStatus: VerifyAccessTokenResponseSessionStatus.fromJson(json["SessionStatus"]),
    webMethodOutputRows: WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": (sessionStatus != null)?sessionStatus.toJson():null,
    "WebMethodOutputRows": (webMethodOutputRows != null)?webMethodOutputRows.toJson():null,
  };
}

class VerifyAccessTokenResponseSessionStatus {
  VerifyAccessTokenResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory VerifyAccessTokenResponseSessionStatus.fromJson(Map<String, dynamic> json) => VerifyAccessTokenResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.cgStatus,
  });

  String cgStatus;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    cgStatus: json["CG_Status"],
  );

  Map<String, dynamic> toJson() => {
    "CG_Status": cgStatus,
  };
}