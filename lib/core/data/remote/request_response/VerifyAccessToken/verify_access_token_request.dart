
import 'dart:convert';

List<VerifyAccessTokenRequest> verifyAccessTokenRequestFromJson(String str) => List<VerifyAccessTokenRequest>.from(json.decode(str).map((x) => VerifyAccessTokenRequest.fromJson(x)));

String verifyAccessTokenRequestToJson(List<VerifyAccessTokenRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class VerifyAccessTokenRequest {
  VerifyAccessTokenRequest({
    this.accessToken,
    this.deviceId,
    this.deviceToken,
    this.deviceType,
  });

  String accessToken;
  String deviceId;
  String deviceToken;
  String deviceType;

  factory VerifyAccessTokenRequest.fromJson(Map<String, dynamic> json) => VerifyAccessTokenRequest(
    accessToken: json["AccessToken"],
    deviceId: json["DeviceID"],
    deviceToken: json["DeviceToken"],
    deviceType: json["DeviceType"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "DeviceID": deviceId,
    "DeviceToken": deviceToken,
    "DeviceType": deviceType,
  };
}