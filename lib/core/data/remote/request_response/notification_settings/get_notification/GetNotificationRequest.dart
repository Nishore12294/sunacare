// To parse this JSON data, do
//
//     final getNotificationRequest = getNotificationRequestFromJson(jsonString);

import 'dart:convert';

List<GetNotificationRequest> getNotificationRequestFromJson(String str) => List<GetNotificationRequest>.from(json.decode(str).map((x) => GetNotificationRequest.fromJson(x)));

String getNotificationRequestToJson(List<GetNotificationRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetNotificationRequest {
  GetNotificationRequest({
    this.accessToken,
  });

  String accessToken;

  factory GetNotificationRequest.fromJson(Map<String, dynamic> json) => GetNotificationRequest(
    accessToken: json["AccessToken"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
  };
}
