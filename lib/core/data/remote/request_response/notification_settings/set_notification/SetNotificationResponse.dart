// To parse this JSON data, do
//
//     final setNotificationResponse = setNotificationResponseFromJson(jsonString);

import 'dart:convert';

SetNotificationResponse setNotificationResponseFromJson(String str) => SetNotificationResponse.fromJson(json.decode(str));

String setNotificationResponseToJson(SetNotificationResponse data) => json.encode(data.toJson());

class SetNotificationResponse {
  SetNotificationResponse({
    this.sessionStatus,
    this.webMethodOutputRows,
    this.outputRowErrors,
  });

  SetNotificationResponseSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;
  OutputRowErrors outputRowErrors;

  factory SetNotificationResponse.fromJson(Map<String, dynamic> json) => SetNotificationResponse(
    sessionStatus:json.containsKey("SessionStatus")? SetNotificationResponseSessionStatus.fromJson(json["SessionStatus"]):null,
    webMethodOutputRows: json.containsKey("WebMethodOutputRows")?WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]):null,
    outputRowErrors:json.containsKey("OutputRowErrors")? OutputRowErrors.fromJson(json["OutputRowErrors"]):null,
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": (sessionStatus!=null)?sessionStatus.toJson():null,
    "WebMethodOutputRows":(webMethodOutputRows!=null)? webMethodOutputRows.toJson():null,
    "OutputRowErrors": (outputRowErrors!=null)? outputRowErrors.toJson():null,
  };
}

class OutputRowErrors {
  OutputRowErrors({
    this.outputRowErrors,
  });

  List<OutputRowError> outputRowErrors;

  factory OutputRowErrors.fromJson(Map<String, dynamic> json) => OutputRowErrors(
    outputRowErrors: List<OutputRowError>.from(json["OutputRowErrors"].map((x) => OutputRowError.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "OutputRowErrors": List<dynamic>.from(outputRowErrors.map((x) => x.toJson())),
  };
}

class OutputRowError {
  OutputRowError({
    this.rowNo,
    this.typeErrorOrWarning,
    this.textMessage,
    this.verbalText,
    this.columnName,
  });

  String rowNo;
  String typeErrorOrWarning;
  String textMessage;
  String verbalText;
  String columnName;

  factory OutputRowError.fromJson(Map<String, dynamic> json) => OutputRowError(
    rowNo: json["RowNo"],
    typeErrorOrWarning: json["TypeErrorOrWarning"],
    textMessage: json["TextMessage"],
    verbalText: json["VerbalText"],
    columnName: json["ColumnName"],
  );

  Map<String, dynamic> toJson() => {
    "RowNo": rowNo,
    "TypeErrorOrWarning": typeErrorOrWarning,
    "TextMessage": textMessage,
    "VerbalText": verbalText,
    "ColumnName": columnName,
  };
}

class SetNotificationResponseSessionStatus {
  SetNotificationResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory SetNotificationResponseSessionStatus.fromJson(Map<String, dynamic> json) => SetNotificationResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.setCgNotificationStatus,
  });

  String setCgNotificationStatus;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    setCgNotificationStatus: json["SetCGNotificationStatus"],
  );

  Map<String, dynamic> toJson() => {
    "SetCGNotificationStatus": setCgNotificationStatus,
  };
}
