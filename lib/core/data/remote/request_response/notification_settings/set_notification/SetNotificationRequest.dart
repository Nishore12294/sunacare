// To parse this JSON data, do
//
//     final setNotificationRequest = setNotificationRequestFromJson(jsonString);

import 'dart:convert';

List<SetNotificationRequest> setNotificationRequestFromJson(String str) => List<SetNotificationRequest>.from(json.decode(str).map((x) => SetNotificationRequest.fromJson(x)));

String setNotificationRequestToJson(List<SetNotificationRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SetNotificationRequest {
  SetNotificationRequest({
    this.accessToken,
    this.notificationName,
    this.notificationValue,
  });

  String accessToken;
  String notificationName;
  String notificationValue;

  factory SetNotificationRequest.fromJson(Map<String, dynamic> json) => SetNotificationRequest(
    accessToken: json["AccessToken"],
    notificationName: json["NotificationName"],
    notificationValue: json["NotificationValue"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "NotificationName": notificationName,
    "NotificationValue": notificationValue,
  };
}
