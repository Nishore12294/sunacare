class SetupTransactionPinRequest {
  String email;
  String phoneNumber;
  String otp;
  String transactionPin;
  String sessionInfo;

  SetupTransactionPinRequest({this.email, this.phoneNumber, this.otp, this.transactionPin, this.sessionInfo});

  SetupTransactionPinRequest.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    phoneNumber = json['phone'];
    otp = json['otp'];
    transactionPin = json['transation_pin'];
    sessionInfo = json['session_info'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['phone'] = this.phoneNumber;
    data['otp'] = this.otp;
    data['transation_pin'] = this.transactionPin;
    data['session_info'] = this.sessionInfo;
    return data;
  }
}