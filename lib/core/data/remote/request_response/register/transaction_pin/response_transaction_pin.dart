class SetupTransactionPinResponse {
  String status;
  String message;

  SetupTransactionPinResponse({this.status, this.message});

  SetupTransactionPinResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'] != null ? json['status'] : null ;
    message = json['message'] != null ? json['message'] : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}