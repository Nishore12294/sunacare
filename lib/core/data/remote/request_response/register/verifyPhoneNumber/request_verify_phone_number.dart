class VerifyPhoneNumberRequest {
  String phoneNumber;
  String countryCode;

  VerifyPhoneNumberRequest({this.phoneNumber, this.countryCode});

  VerifyPhoneNumberRequest.fromJson(Map<String, dynamic> json) {
    phoneNumber = json['phone_number'];
    countryCode = json['country_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone_number'] = this.phoneNumber;
    data['country_code'] = this.countryCode;
    return data;
  }
}