class VerifyPhoneNumberResponse {
  String status;
  String message;
  Error error;

  VerifyPhoneNumberResponse({this.status, this.message, this.error});

  VerifyPhoneNumberResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'] != null ? json['message'] : null;
    error = json['error'] != null ? new Error.fromJson(json['error']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.error != null) {
      data['error'] = this.error.toJson();
    }
    return data;
  }
}

class Error {
  String message;

  Error({this.message});

  Error.fromJson(Map<String, dynamic> json) {
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    return data;
  }
}