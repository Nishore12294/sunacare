class VerifyEmailOtpResponse {
  String message;
  String status;
  //Error error;

  VerifyEmailOtpResponse({ this.status, this.message});

  VerifyEmailOtpResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'] != null ? json['message'] : null;
    status = json['status'] != null ? json['status'] : null;
  //  error = json['error'] != null ? new Error.fromJson(json['error']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    //data['status'] = this.status;
    data['message'] = this.message;
    data['status'] = this.status;
//    if (this.error != null) {
//      data['error'] = this.error.toJson();
//    }
    return data;
  }
}