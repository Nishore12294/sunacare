class VerifyEmailOtpRequest {
  String email;
  String otp;
  String flag;

  VerifyEmailOtpRequest({this.email, this.otp, this.flag});

  VerifyEmailOtpRequest.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    otp = json['otp'];
    flag = json['flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['otp'] = this.otp;
    data['flag'] = this.flag;
    return data;
  }
}