// To parse this JSON data, do
//
//     final signUpRequest = signUpRequestFromJson(jsonString);

import 'dart:convert';

SignUpRequest signUpRequestFromJson(String str) => SignUpRequest.fromJson(json.decode(str));

String signUpRequestToJson(SignUpRequest data) => json.encode(data.toJson());

class SignUpRequest {
  SignUpRequest({
    this.firstName,
    this.lastName,
    this.email,
    this.mobileNumber,
    this.gender,
    this.driverLicense,
    this.smoking,
    this.speakingGermanLanguage,
    this.experienceInCare,
    this.willingToDoTransfer,
    this.willingtodoNightShifts,
    this.abletotakecareof2People,
  });

  String firstName;
  String lastName;
  String email;
  String mobileNumber;
  String gender;
  String driverLicense;
  String smoking;
  String speakingGermanLanguage;
  String experienceInCare;
  String willingToDoTransfer;
  String willingtodoNightShifts;
  String abletotakecareof2People;

  factory SignUpRequest.fromJson(Map<String, dynamic> json) => SignUpRequest(
    firstName: json["FirstName"],
    lastName: json["LastName"],
    email: json["Email"],
    mobileNumber: json["MobileNumber"],
    gender: json["Gender"],
    driverLicense: json["DriverLicense"],
    smoking: json["Smoking"],
    speakingGermanLanguage: json["SpeakingGermanLanguage"],
    experienceInCare: json["ExperienceInCare"],
    willingToDoTransfer: json["WillingToDoTransfer"],
    willingtodoNightShifts: json["WillingtodoNightShifts"],
    abletotakecareof2People: json["abletotakecareof2people"],
  );

  Map<String, dynamic> toJson() => {
    "FirstName": firstName,
    "LastName": lastName,
    "Email": email,
    "MobileNumber": mobileNumber,
    "Gender": gender,
    "DriverLicense": driverLicense,
    "Smoking": smoking,
    "SpeakingGermanLanguage": speakingGermanLanguage,
    "ExperienceInCare": experienceInCare,
    "WillingToDoTransfer": willingToDoTransfer,
    "WillingtodoNightShifts": willingtodoNightShifts,
    "abletotakecareof2people": abletotakecareof2People,
  };
}
