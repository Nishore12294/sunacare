class SendOtpToEmailResponse {
  String status;
  String message;

  SendOtpToEmailResponse({this.status, this.message});

  SendOtpToEmailResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'] != null ? json['status'] : "";
    message = json['message'] != null ?  json['message'] : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}