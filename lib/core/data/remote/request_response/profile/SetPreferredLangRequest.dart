// To parse this JSON data, do
//
//     final setPreferredLangRequest = setPreferredLangRequestFromJson(jsonString);

import 'dart:convert';

List<SetPreferredLangRequest> setPreferredLangRequestFromJson(String str) => List<SetPreferredLangRequest>.from(json.decode(str).map((x) => SetPreferredLangRequest.fromJson(x)));

String setPreferredLangRequestToJson(List<SetPreferredLangRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SetPreferredLangRequest {
  SetPreferredLangRequest({
    this.accessToken,
    this.preferredLanguage,
  });

  String accessToken;
  String preferredLanguage;

  factory SetPreferredLangRequest.fromJson(Map<String, dynamic> json) => SetPreferredLangRequest(
    accessToken: json["AccessToken"],
    preferredLanguage: json["PreferredLanguage"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "PreferredLanguage": preferredLanguage,
  };
}
