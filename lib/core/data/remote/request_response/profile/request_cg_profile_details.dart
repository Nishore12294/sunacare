// To parse this JSON data, do
//
//     final cgProfileDetailsRequest = cgProfileDetailsRequestFromJson(jsonString);

import 'dart:convert';

List<CgProfileDetailsRequest> cgProfileDetailsRequestFromJson(String str) => List<CgProfileDetailsRequest>.from(json.decode(str).map((x) => CgProfileDetailsRequest.fromJson(x)));

String cgProfileDetailsRequestToJson(List<CgProfileDetailsRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CgProfileDetailsRequest {
  CgProfileDetailsRequest({
    this.accessToken,
  });

  String accessToken;

  factory CgProfileDetailsRequest.fromJson(Map<String, dynamic> json) => CgProfileDetailsRequest(
    accessToken: json["AccessToken"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
  };
}
