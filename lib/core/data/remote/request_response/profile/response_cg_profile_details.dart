// To parse this JSON data, do
//
//     final cgProfileDetailsResponse = cgProfileDetailsResponseFromJson(jsonString);

import 'dart:convert';

CgProfileDetailsResponse cgProfileDetailsResponseFromJson(String str) => CgProfileDetailsResponse.fromJson(json.decode(str));

String cgProfileDetailsResponseToJson(CgProfileDetailsResponse data) => json.encode(data.toJson());

class CgProfileDetailsResponse {
  CgProfileDetailsResponse({
    this.sessionStatus,
    this.webMethodOutputRows,
  });

  CgProfileDetailsResponseSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;

  factory CgProfileDetailsResponse.fromJson(Map<String, dynamic> json) => CgProfileDetailsResponse(
    sessionStatus: CgProfileDetailsResponseSessionStatus.fromJson(json["SessionStatus"]),
    webMethodOutputRows: WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": sessionStatus.toJson(),
    "WebMethodOutputRows": webMethodOutputRows.toJson(),
  };
}

class CgProfileDetailsResponseSessionStatus {
  CgProfileDetailsResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory CgProfileDetailsResponseSessionStatus.fromJson(Map<String, dynamic> json) => CgProfileDetailsResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.firstName,
    this.lastName,
    this.gender,
    this.email,
    this.mobilePhone,
    this.address,
    this.maxStayPerAssignment,
    this.smoking,
    this.drivingLicense,
    this.experienceInGermany,
    this.transferOfPatients,
    this.nightshifts,
    this.takingCareOfTwoPatients,
    this.experienceInCare,
    this.allergies,
    this.germanLevel,
  });

  String firstName;
  String lastName;
  String gender;
  String email;
  String mobilePhone;
  String address;
  String maxStayPerAssignment;
  String smoking;
  String drivingLicense;
  String experienceInGermany;
  String transferOfPatients;
  String nightshifts;
  String takingCareOfTwoPatients;
  String experienceInCare;
  String allergies;
  String germanLevel;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    firstName: json["FirstName"],
    lastName: json["LastName"],
    gender: json["Gender"],
    email: json["Email"],
    mobilePhone: json["MobilePhone"],
    address: json["Address"],
    maxStayPerAssignment: json["MaxStayPerAssignment"],
    smoking: json["Smoking"],
    drivingLicense: json["DrivingLicense"],
    experienceInGermany: json["ExperienceInGermany"],
    transferOfPatients: json["TransferOfPatients"],
    nightshifts: json["Nightshifts"],
    takingCareOfTwoPatients: json["TakingCareOfTwoPatients"],
    experienceInCare: json["ExperienceInCare"],
    allergies: json["Allergies"],
    germanLevel: json["GermanLevel"],
  );

  Map<String, dynamic> toJson() => {
    "FirstName": firstName,
    "LastName": lastName,
    "Gender": gender,
    "Email": email,
    "MobilePhone": mobilePhone,
    "Address": address,
    "MaxStayPerAssignment": maxStayPerAssignment,
    "Smoking": smoking,
    "DrivingLicense": drivingLicense,
    "ExperienceInGermany": experienceInGermany,
    "TransferOfPatients": transferOfPatients,
    "Nightshifts": nightshifts,
    "TakingCareOfTwoPatients": takingCareOfTwoPatients,
    "ExperienceInCare": experienceInCare,
    "Allergies": allergies,
    "GermanLevel": germanLevel,
  };
}
