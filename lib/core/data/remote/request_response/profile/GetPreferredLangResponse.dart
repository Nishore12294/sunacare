// To parse this JSON data, do
//
//     final getPreferredLangResponse = getPreferredLangResponseFromJson(jsonString);

import 'dart:convert';

GetPreferredLangResponse getPreferredLangResponseFromJson(String str) => GetPreferredLangResponse.fromJson(json.decode(str));

String getPreferredLangResponseToJson(GetPreferredLangResponse data) => json.encode(data.toJson());

class GetPreferredLangResponse {
  GetPreferredLangResponse({
    this.sessionStatus,
    this.webMethodOutputRows,
    this.outputRowErrors,
  });

  GetPreferredLangResponseSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;
  OutputRowErrors outputRowErrors;

  factory GetPreferredLangResponse.fromJson(Map<String, dynamic> json) => GetPreferredLangResponse(
    sessionStatus: json.containsKey("SessionStatus")?GetPreferredLangResponseSessionStatus.fromJson(json["SessionStatus"]):null,
    webMethodOutputRows: json.containsKey("WebMethodOutputRows")?WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]):null,
    outputRowErrors: json.containsKey("OutputRowErrors")?OutputRowErrors.fromJson(json["OutputRowErrors"]):null,
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": (sessionStatus != null)?sessionStatus.toJson():null,
     "WebMethodOutputRows":(webMethodOutputRows != null)? webMethodOutputRows.toJson():null,
    "OutputRowErrors": (outputRowErrors != null)? outputRowErrors.toJson():null,
  };
}

class OutputRowErrors {
  OutputRowErrors({
    this.outputRowErrors,
  });

  List<OutputRowError> outputRowErrors;

  factory OutputRowErrors.fromJson(Map<String, dynamic> json) => OutputRowErrors(
    outputRowErrors: List<OutputRowError>.from(json["OutputRowErrors"].map((x) => OutputRowError.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "OutputRowErrors": List<dynamic>.from(outputRowErrors.map((x) => x.toJson())),
  };
}

class OutputRowError {
  OutputRowError({
    this.rowNo,
    this.typeErrorOrWarning,
    this.textMessage,
    this.verbalText,
    this.columnName,
  });

  String rowNo;
  String typeErrorOrWarning;
  String textMessage;
  String verbalText;
  String columnName;

  factory OutputRowError.fromJson(Map<String, dynamic> json) => OutputRowError(
    rowNo: json["RowNo"],
    typeErrorOrWarning: json["TypeErrorOrWarning"],
    textMessage: json["TextMessage"],
    verbalText: json["VerbalText"],
    columnName: json["ColumnName"],
  );

  Map<String, dynamic> toJson() => {
    "RowNo": rowNo,
    "TypeErrorOrWarning": typeErrorOrWarning,
    "TextMessage": textMessage,
    "VerbalText": verbalText,
    "ColumnName": columnName,
  };
}

class GetPreferredLangResponseSessionStatus {
  GetPreferredLangResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory GetPreferredLangResponseSessionStatus.fromJson(Map<String, dynamic> json) => GetPreferredLangResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.preferredLanguage,
    this.cgGuid,
    this.cgPreferredLanguageStatus,
  });

  String preferredLanguage;
  String cgGuid;
  String cgPreferredLanguageStatus;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    preferredLanguage: json["PreferredLanguage"],
    cgGuid: json["CGGuid"],
    cgPreferredLanguageStatus: json["CGPreferredLanguageStatus"],
  );

  Map<String, dynamic> toJson() => {
    "PreferredLanguage": preferredLanguage,
    "CGGuid": cgGuid,
    "CGPreferredLanguageStatus": cgPreferredLanguageStatus,
  };
}
