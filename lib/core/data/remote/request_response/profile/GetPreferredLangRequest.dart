// To parse this JSON data, do
//
//     final getPreferredLangRequest = getPreferredLangRequestFromJson(jsonString);

import 'dart:convert';

List<GetPreferredLangRequest> getPreferredLangRequestFromJson(String str) => List<GetPreferredLangRequest>.from(json.decode(str).map((x) => GetPreferredLangRequest.fromJson(x)));

String getPreferredLangRequestToJson(List<GetPreferredLangRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetPreferredLangRequest {
  GetPreferredLangRequest({
    this.accessToken,
  });

  String accessToken;

  factory GetPreferredLangRequest.fromJson(Map<String, dynamic> json) => GetPreferredLangRequest(
    accessToken: json["AccessToken"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
  };
}
