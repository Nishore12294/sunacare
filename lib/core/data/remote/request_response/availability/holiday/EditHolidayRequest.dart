// To parse this JSON data, do
//
//     final editHolidayRequest = editHolidayRequestFromJson(jsonString);

import 'dart:convert';

List<EditHolidayRequest> editHolidayRequestFromJson(String str) => List<EditHolidayRequest>.from(json.decode(str).map((x) => EditHolidayRequest.fromJson(x)));

String editHolidayRequestToJson(List<EditHolidayRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EditHolidayRequest {
  EditHolidayRequest({
    this.holidayCgScheduleGuid,
    this.accessToken,
    this.startDate,
    this.endDate,
  });

  String holidayCgScheduleGuid;
  String accessToken;
  String startDate;
  String endDate;

  factory EditHolidayRequest.fromJson(Map<String, dynamic> json) => EditHolidayRequest(
    holidayCgScheduleGuid: json["HolidayCGScheduleGuid"],
    accessToken: json["AccessToken"],
    startDate: json["StartDate"],
    endDate: json["EndDate"],
  );

  Map<String, dynamic> toJson() => {
    "HolidayCGScheduleGuid": holidayCgScheduleGuid,
    "AccessToken": accessToken,
    "StartDate": startDate,
    "EndDate": endDate,
  };
}
