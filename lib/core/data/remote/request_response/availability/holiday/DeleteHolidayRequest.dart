// To parse this JSON data, do
//
//     final deleteHolidayRequest = deleteHolidayRequestFromJson(jsonString);

import 'dart:convert';

List<DeleteHolidayRequest> deleteHolidayRequestFromJson(String str) => List<DeleteHolidayRequest>.from(json.decode(str).map((x) => DeleteHolidayRequest.fromJson(x)));

String deleteHolidayRequestToJson(List<DeleteHolidayRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DeleteHolidayRequest {
  DeleteHolidayRequest({
    this.holidayCgScheduleGuid,
    this.accessToken,
  });

  String holidayCgScheduleGuid;
  String accessToken;

  factory DeleteHolidayRequest.fromJson(Map<String, dynamic> json) => DeleteHolidayRequest(
    holidayCgScheduleGuid: json["HolidayCGScheduleGuid"],
    accessToken: json["AccessToken"],
  );

  Map<String, dynamic> toJson() => {
    "HolidayCGScheduleGuid": holidayCgScheduleGuid,
    "AccessToken": accessToken,
  };
}
