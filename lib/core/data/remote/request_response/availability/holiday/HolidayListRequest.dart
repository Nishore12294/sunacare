// To parse this JSON data, do
//
//     final holidayListRequest = holidayListRequestFromJson(jsonString);

import 'dart:convert';

List<HolidayListRequest> holidayListRequestFromJson(String str) => List<HolidayListRequest>.from(json.decode(str).map((x) => HolidayListRequest.fromJson(x)));

String holidayListRequestToJson(List<HolidayListRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HolidayListRequest {
  HolidayListRequest({
    this.accessToken,
    this.month,
    this.year,
  });

  String accessToken;
  String month;
  String year;

  factory HolidayListRequest.fromJson(Map<String, dynamic> json) => HolidayListRequest(
    accessToken: json["AccessToken"],
    month: json["Month"],
    year: json["Year"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "Month": month,
    "Year": year,
  };
}
