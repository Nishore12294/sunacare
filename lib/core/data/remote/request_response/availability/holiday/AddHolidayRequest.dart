// To parse this JSON data, do
//
//     final addHolidayRequest = addHolidayRequestFromJson(jsonString);

import 'dart:convert';

List<AddHolidayRequest> addHolidayRequestFromJson(String str) => List<AddHolidayRequest>.from(json.decode(str).map((x) => AddHolidayRequest.fromJson(x)));

String addHolidayRequestToJson(List<AddHolidayRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AddHolidayRequest {
  AddHolidayRequest({
    this.accessToken,
    this.startDate,
    this.endDate,
  });

  String accessToken;
  String startDate;
  String endDate;

  factory AddHolidayRequest.fromJson(Map<String, dynamic> json) => AddHolidayRequest(
    accessToken: json["AccessToken"],
    startDate: json["StartDate"],
    endDate: json["EndDate"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "StartDate": startDate,
    "EndDate": endDate,
  };
}
