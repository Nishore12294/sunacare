// To parse this JSON data, do
//
//     final holidayListResponse = holidayListResponseFromJson(jsonString);

import 'dart:convert';

HolidayListResponse holidayListResponseFromJson(String str) => HolidayListResponse.fromJson(json.decode(str));

String holidayListResponseToJson(HolidayListResponse data) => json.encode(data.toJson());

class HolidayListResponse {
  HolidayListResponse({
    this.sessionStatus,
    this.outputRowErrors,
    this.webMethodOutputRows,
  });

  HolidayListResponseSessionStatus sessionStatus;
  OutputRowErrors outputRowErrors;
  WebMethodOutputRows webMethodOutputRows;

  factory HolidayListResponse.fromJson(Map<String, dynamic> json) => HolidayListResponse(
    sessionStatus: json.containsKey("SessionStatus")?HolidayListResponseSessionStatus.fromJson(json["SessionStatus"]):null,
    outputRowErrors: json.containsKey("OutputRowErrors")?OutputRowErrors.fromJson(json["OutputRowErrors"]):null,
    webMethodOutputRows:json.containsKey("WebMethodOutputRows")? WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]):null,
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": (sessionStatus!=null)?sessionStatus.toJson():null,
    "OutputRowErrors": (outputRowErrors!=null)?outputRowErrors.toJson():null,
    "WebMethodOutputRows": (webMethodOutputRows!=null)?webMethodOutputRows.toJson():null,
  };
}

class OutputRowErrors {
  OutputRowErrors({
    this.outputRowErrors,
  });

  List<OutputRowError> outputRowErrors;

  factory OutputRowErrors.fromJson(Map<String, dynamic> json) => OutputRowErrors(
    outputRowErrors: List<OutputRowError>.from(json["OutputRowErrors"].map((x) => OutputRowError.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "OutputRowErrors": List<dynamic>.from(outputRowErrors.map((x) => x.toJson())),
  };
}

class OutputRowError {
  OutputRowError({
    this.rowNo,
    this.typeErrorOrWarning,
    this.textMessage,
    this.verbalText,
    this.columnName,
  });

  String rowNo;
  String typeErrorOrWarning;
  String textMessage;
  String verbalText;
  String columnName;

  factory OutputRowError.fromJson(Map<String, dynamic> json) => OutputRowError(
    rowNo: json["RowNo"],
    typeErrorOrWarning: json["TypeErrorOrWarning"],
    textMessage: json["TextMessage"],
    verbalText: json["VerbalText"],
    columnName: json["ColumnName"],
  );

  Map<String, dynamic> toJson() => {
    "RowNo": rowNo,
    "TypeErrorOrWarning": typeErrorOrWarning,
    "TextMessage": textMessage,
    "VerbalText": verbalText,
    "ColumnName": columnName,
  };
}

class HolidayListResponseSessionStatus {
  HolidayListResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory HolidayListResponseSessionStatus.fromJson(Map<String, dynamic> json) => HolidayListResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.cgScheduleGuid,
    this.cgScheduleNumber,
    this.fromDate,
    this.toDate,
  });

  String cgScheduleGuid;
  String cgScheduleNumber;
  DateTime fromDate;
  DateTime toDate;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    cgScheduleGuid: json["CGScheduleGuid"],
    cgScheduleNumber: json["CGScheduleNumber"],
    fromDate: DateTime.parse(json["FromDate"]),
    toDate: DateTime.parse(json["ToDate"]),
  );

  Map<String, dynamic> toJson() => {
    "CGScheduleGuid": cgScheduleGuid,
    "CGScheduleNumber": cgScheduleNumber,
    "FromDate": "${fromDate.year.toString().padLeft(4, '0')}-${fromDate.month.toString().padLeft(2, '0')}-${fromDate.day.toString().padLeft(2, '0')}",
    "ToDate": "${toDate.year.toString().padLeft(4, '0')}-${toDate.month.toString().padLeft(2, '0')}-${toDate.day.toString().padLeft(2, '0')}",
  };
}
