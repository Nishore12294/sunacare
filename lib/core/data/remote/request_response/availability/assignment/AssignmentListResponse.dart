// To parse this JSON data, do
//
//     final assignmentListResponse = assignmentListResponseFromJson(jsonString);

import 'dart:convert';

AssignmentListResponse assignmentListResponseFromJson(String str) => AssignmentListResponse.fromJson(json.decode(str));

String assignmentListResponseToJson(AssignmentListResponse data) => json.encode(data.toJson());

class AssignmentListResponse {
  AssignmentListResponse({
    this.sessionStatus,
    this.outputRowErrors,
    this.webMethodOutputRows,
  });

  AssignmentListResponseSessionStatus sessionStatus;
  OutputRowErrors outputRowErrors;
  WebMethodOutputRows webMethodOutputRows;

  factory AssignmentListResponse.fromJson(Map<String, dynamic> json) => AssignmentListResponse(
    sessionStatus: json.containsKey("SessionStatus")?AssignmentListResponseSessionStatus.fromJson(json["SessionStatus"]):null,
    outputRowErrors: json.containsKey("OutputRowErrors")?OutputRowErrors.fromJson(json["OutputRowErrors"]):null,
    webMethodOutputRows: json.containsKey("WebMethodOutputRows")?WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]):null,
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": (sessionStatus != null) ? sessionStatus.toJson():null,
    "OutputRowErrors": (outputRowErrors != null)? outputRowErrors.toJson():null,
    "WebMethodOutputRows":  (webMethodOutputRows != null) ?webMethodOutputRows.toJson():null,
  };
}

class OutputRowErrors {
  OutputRowErrors({
    this.outputRowErrors,
  });

  List<OutputRowError> outputRowErrors;

  factory OutputRowErrors.fromJson(Map<String, dynamic> json) => OutputRowErrors(
    outputRowErrors: List<OutputRowError>.from(json["OutputRowErrors"].map((x) => OutputRowError.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "OutputRowErrors": List<dynamic>.from(outputRowErrors.map((x) => x.toJson())),
  };
}

class OutputRowError {
  OutputRowError({
    this.rowNo,
    this.typeErrorOrWarning,
    this.textMessage,
    this.verbalText,
    this.columnName,
  });

  String rowNo;
  String typeErrorOrWarning;
  String textMessage;
  String verbalText;
  String columnName;

  factory OutputRowError.fromJson(Map<String, dynamic> json) => OutputRowError(
    rowNo: json["RowNo"],
    typeErrorOrWarning: json["TypeErrorOrWarning"],
    textMessage: json["TextMessage"],
    verbalText: json["VerbalText"],
    columnName: json["ColumnName"],
  );

  Map<String, dynamic> toJson() => {
    "RowNo": rowNo,
    "TypeErrorOrWarning": typeErrorOrWarning,
    "TextMessage": textMessage,
    "VerbalText": verbalText,
    "ColumnName": columnName,
  };
}

class AssignmentListResponseSessionStatus {
  AssignmentListResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory AssignmentListResponseSessionStatus.fromJson(Map<String, dynamic> json) => AssignmentListResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.customerName,
    this.offerNumber,
    this.arrivalDate,
    this.plannedEndDate,
    this.netToSalary,
  });

  String customerName;
  String offerNumber;
  DateTime arrivalDate;
  DateTime plannedEndDate;
  String netToSalary;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    customerName: json["CustomerName"],
    offerNumber: json["OfferNumber"],
    arrivalDate: DateTime.parse(json["ArrivalDate"]),
    plannedEndDate: DateTime.parse(json["PlannedEndDate"]),
    netToSalary: json["NetToSalary"],
  );

  Map<String, dynamic> toJson() => {
    "CustomerName": customerName,
    "OfferNumber": offerNumber,
    "ArrivalDate": "${arrivalDate.year.toString().padLeft(4, '0')}-${arrivalDate.month.toString().padLeft(2, '0')}-${arrivalDate.day.toString().padLeft(2, '0')}",
    "PlannedEndDate": "${plannedEndDate.year.toString().padLeft(4, '0')}-${plannedEndDate.month.toString().padLeft(2, '0')}-${plannedEndDate.day.toString().padLeft(2, '0')}",
    "NetToSalary": netToSalary,
  };
}
