// To parse this JSON data, do
//
//     final assignmentListRequest = assignmentListRequestFromJson(jsonString);

import 'dart:convert';

List<AssignmentListRequest> assignmentListRequestFromJson(String str) => List<AssignmentListRequest>.from(json.decode(str).map((x) => AssignmentListRequest.fromJson(x)));

String assignmentListRequestToJson(List<AssignmentListRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AssignmentListRequest {
  AssignmentListRequest({
    this.accessToken,
    this.month,
    this.year,
  });

  String accessToken;
  String month;
  String year;

  factory AssignmentListRequest.fromJson(Map<String, dynamic> json) => AssignmentListRequest(
    accessToken: json["AccessToken"],
    month: json["Month"],
    year: json["Year"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "Month": month,
    "Year": year,
  };
}