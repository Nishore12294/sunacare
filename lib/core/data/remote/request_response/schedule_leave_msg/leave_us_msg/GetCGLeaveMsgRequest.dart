import 'dart:convert';

List<GetCgLeaveMsgRequest> getCgLeaveMsgRequestFromJson(String str) => List<GetCgLeaveMsgRequest>.from(json.decode(str).map((x) => GetCgLeaveMsgRequest.fromJson(x)));

String getCgLeaveMsgRequestToJson(List<GetCgLeaveMsgRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetCgLeaveMsgRequest {
  GetCgLeaveMsgRequest({
    this.accessToken,
    this.message,
    this.offerNumber,
  });

  String accessToken;
  String message;
  String offerNumber;

  factory GetCgLeaveMsgRequest.fromJson(Map<String, dynamic> json) => GetCgLeaveMsgRequest(
    accessToken: json["AccessToken"],
    message: json["Message"],
    offerNumber: json["OfferNumber"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "Message": message,
    "OfferNumber": offerNumber,
  };
}


// To parse this JSON data, do
//
//     final getCgLeaveMsgRequest = getCgLeaveMsgRequestFromJson(jsonString);

/*import 'dart:convert';

List<GetCgLeaveMsgRequest> getCgLeaveMsgRequestFromJson(String str) => List<GetCgLeaveMsgRequest>.from(json.decode(str).map((x) => GetCgLeaveMsgRequest.fromJson(x)));

String getCgLeaveMsgRequestToJson(List<GetCgLeaveMsgRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetCgLeaveMsgRequest {
  GetCgLeaveMsgRequest({
    this.accessToken,
    this.message,
  });

  String accessToken;
  String message;

  factory GetCgLeaveMsgRequest.fromJson(Map<String, dynamic> json) => GetCgLeaveMsgRequest(
    accessToken: json["AccessToken"],
    message: json["Message"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "Message": message,
  };
}*/

// To parse this JSON data, do
//
//     final getCgLeaveMsgRequest = getCgLeaveMsgRequestFromJson(jsonString);

