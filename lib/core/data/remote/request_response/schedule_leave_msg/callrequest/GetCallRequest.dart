// To parse this JSON data, do
//
//     final getCallRequest = getCallRequestFromJson(jsonString);

import 'dart:convert';

List<GetCallRequest> getCallRequestFromJson(String str) => List<GetCallRequest>.from(json.decode(str).map((x) => GetCallRequest.fromJson(x)));

String getCallRequestToJson(List<GetCallRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetCallRequest {
  GetCallRequest({
    this.currentTimeZone,
  });

  String currentTimeZone;

  factory GetCallRequest.fromJson(Map<String, dynamic> json) => GetCallRequest(
    currentTimeZone: json["CurrentTimeZone"],
  );

  Map<String, dynamic> toJson() => {
    "CurrentTimeZone": currentTimeZone,
  };
}
