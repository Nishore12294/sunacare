// To parse this JSON data, do
//
//     final getCallResponse = getCallResponseFromJson(jsonString);

import 'dart:convert';

GetCallResponse getCallResponseFromJson(String str) => GetCallResponse.fromJson(json.decode(str));

String getCallResponseToJson(GetCallResponse data) => json.encode(data.toJson());

class GetCallResponse {
  GetCallResponse({
    this.sessionStatus,
    this.webMethodOutputRows,
    this.outputRowErrors,
  });

  GetCallResponseSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;
  OutputRowErrors outputRowErrors;

  factory GetCallResponse.fromJson(Map<String, dynamic> json) => GetCallResponse(
    sessionStatus:json.containsKey("SessionStatus")? GetCallResponseSessionStatus.fromJson(json["SessionStatus"]):null,
    webMethodOutputRows: json.containsKey("WebMethodOutputRows")?WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]):null,
    outputRowErrors:json.containsKey("OutputRowErrors")? OutputRowErrors.fromJson(json["OutputRowErrors"]):null,
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": (sessionStatus!= null)?sessionStatus.toJson():null,
    "WebMethodOutputRows": (webMethodOutputRows!= null)?webMethodOutputRows.toJson():null,
    "OutputRowErrors": (outputRowErrors!= null)?outputRowErrors.toJson():null,
  };
}

class OutputRowErrors {
  OutputRowErrors({
    this.outputRowErrors,
  });

  List<OutputRowError> outputRowErrors;

  factory OutputRowErrors.fromJson(Map<String, dynamic> json) => OutputRowErrors(
    outputRowErrors: List<OutputRowError>.from(json["OutputRowErrors"].map((x) => OutputRowError.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "OutputRowErrors": List<dynamic>.from(outputRowErrors.map((x) => x.toJson())),
  };
}

class OutputRowError {
  OutputRowError({
    this.rowNo,
    this.typeErrorOrWarning,
    this.textMessage,
    this.verbalText,
    this.columnName,
  });

  String rowNo;
  String typeErrorOrWarning;
  String textMessage;
  String verbalText;
  String columnName;

  factory OutputRowError.fromJson(Map<String, dynamic> json) => OutputRowError(
    rowNo: json["RowNo"],
    typeErrorOrWarning: json["TypeErrorOrWarning"],
    textMessage: json["TextMessage"],
    verbalText: json["VerbalText"],
    columnName: json["ColumnName"],
  );

  Map<String, dynamic> toJson() => {
    "RowNo": rowNo,
    "TypeErrorOrWarning": typeErrorOrWarning,
    "TextMessage": textMessage,
    "VerbalText": verbalText,
    "ColumnName": columnName,
  };
}

class GetCallResponseSessionStatus {
  GetCallResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory GetCallResponseSessionStatus.fromJson(Map<String, dynamic> json) => GetCallResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.contactNumber,
    this.currentTimeZoneStatus,
  });

  String contactNumber;
  String currentTimeZoneStatus;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    contactNumber: json["ContactNumber"],
    currentTimeZoneStatus: json["CurrentTimeZoneStatus"],
  );

  Map<String, dynamic> toJson() => {
    "ContactNumber": contactNumber,
    "CurrentTimeZoneStatus": currentTimeZoneStatus,
  };
}
