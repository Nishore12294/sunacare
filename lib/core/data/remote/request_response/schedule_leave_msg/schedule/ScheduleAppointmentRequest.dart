// To parse this JSON data, do
//
//     final scheduleAppointmentRequest = scheduleAppointmentRequestFromJson(jsonString);

import 'dart:convert';

List<ScheduleAppointmentRequest> scheduleAppointmentRequestFromJson(String str) => List<ScheduleAppointmentRequest>.from(json.decode(str).map((x) => ScheduleAppointmentRequest.fromJson(x)));

String scheduleAppointmentRequestToJson(List<ScheduleAppointmentRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ScheduleAppointmentRequest {
  ScheduleAppointmentRequest({
    this.accessToken,
    this.appointmentDate,
    this.appointmentStartTime,
    this.appointmentEndTime,
    this.offerNumber,
  });

  String accessToken;
  String appointmentDate;
  String appointmentStartTime;
  String appointmentEndTime;
  String offerNumber;

  factory ScheduleAppointmentRequest.fromJson(Map<String, dynamic> json) => ScheduleAppointmentRequest(
    accessToken: json["AccessToken"],
    appointmentDate: json["AppointmentDate"],
    appointmentStartTime: json["AppointmentStartTime"],
    appointmentEndTime: json["AppointmentEndTime"],
    offerNumber: json["OfferNumber"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "AppointmentDate": appointmentDate,
    "AppointmentStartTime": appointmentStartTime,
    "AppointmentEndTime": appointmentEndTime,
    "OfferNumber": offerNumber,
  };
}


// To parse this JSON data, do
//
//     final scheduleAppointmentRequest = scheduleAppointmentRequestFromJson(jsonString);

/*
import 'dart:convert';

List<ScheduleAppointmentRequest> scheduleAppointmentRequestFromJson(String str) => List<ScheduleAppointmentRequest>.from(json.decode(str).map((x) => ScheduleAppointmentRequest.fromJson(x)));

String scheduleAppointmentRequestToJson(List<ScheduleAppointmentRequest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ScheduleAppointmentRequest {
  ScheduleAppointmentRequest({
    this.accessToken,
    this.appointmentDate,
    this.appointmentStartTime,
    this.appointmentEndTime,
    this.appointmentMessage,
  });

  String accessToken;
  String appointmentDate;
  String appointmentStartTime;
  String appointmentEndTime;
  String appointmentMessage;

  factory ScheduleAppointmentRequest.fromJson(Map<String, dynamic> json) => ScheduleAppointmentRequest(
    accessToken: json["AccessToken"],
    appointmentDate: json["AppointmentDate"],
    appointmentStartTime: json["AppointmentStartTime"],
    appointmentEndTime: json["AppointmentEndTime"],
    appointmentMessage: json["AppointmentMessage"],
  );

  Map<String, dynamic> toJson() => {
    "AccessToken": accessToken,
    "AppointmentDate": appointmentDate,
    "AppointmentStartTime": appointmentStartTime,
    "AppointmentEndTime": appointmentEndTime,
    "AppointmentMessage": appointmentMessage,
  };
}
*/
