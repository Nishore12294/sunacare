// To parse this JSON data, do
//
//     final createPasswordResponse = createPasswordResponseFromJson(jsonString);

import 'dart:convert';

CreatePasswordResponse createPasswordResponseFromJson(String str) => CreatePasswordResponse.fromJson(json.decode(str));

String createPasswordResponseToJson(CreatePasswordResponse data) => json.encode(data.toJson());

class CreatePasswordResponse {
  CreatePasswordResponse({
    this.sessionStatus,
    this.webMethodOutputRows,
  });

  CreatePasswordResponseSessionStatus sessionStatus;
  WebMethodOutputRows webMethodOutputRows;

  factory CreatePasswordResponse.fromJson(Map<String, dynamic> json) => CreatePasswordResponse(
    sessionStatus: CreatePasswordResponseSessionStatus.fromJson(json["SessionStatus"]),
    webMethodOutputRows: WebMethodOutputRows.fromJson(json["WebMethodOutputRows"]),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": sessionStatus.toJson(),
    "WebMethodOutputRows": webMethodOutputRows.toJson(),
  };
}

class CreatePasswordResponseSessionStatus {
  CreatePasswordResponseSessionStatus({
    this.sessionStatus,
  });

  List<SessionStatusElement> sessionStatus;

  factory CreatePasswordResponseSessionStatus.fromJson(Map<String, dynamic> json) => CreatePasswordResponseSessionStatus(
    sessionStatus: List<SessionStatusElement>.from(json["SessionStatus"].map((x) => SessionStatusElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "SessionStatus": List<dynamic>.from(sessionStatus.map((x) => x.toJson())),
  };
}

class SessionStatusElement {
  SessionStatusElement({
    this.runStatus,
    this.sessionId,
  });

  String runStatus;
  String sessionId;

  factory SessionStatusElement.fromJson(Map<String, dynamic> json) => SessionStatusElement(
    runStatus: json["RunStatus"],
    sessionId: json["SessionID"],
  );

  Map<String, dynamic> toJson() => {
    "RunStatus": runStatus,
    "SessionID": sessionId,
  };
}

class WebMethodOutputRows {
  WebMethodOutputRows({
    this.webMethodOutputRows,
  });

  List<WebMethodOutputRow> webMethodOutputRows;

  factory WebMethodOutputRows.fromJson(Map<String, dynamic> json) => WebMethodOutputRows(
    webMethodOutputRows: List<WebMethodOutputRow>.from(json["WebMethodOutputRows"].map((x) => WebMethodOutputRow.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WebMethodOutputRows": List<dynamic>.from(webMethodOutputRows.map((x) => x.toJson())),
  };
}

class WebMethodOutputRow {
  WebMethodOutputRow({
    this.status,
  });

  String status;

  factory WebMethodOutputRow.fromJson(Map<String, dynamic> json) => WebMethodOutputRow(
    status: json["Status"],
  );

  Map<String, dynamic> toJson() => {
    "Status": status,
  };
}
