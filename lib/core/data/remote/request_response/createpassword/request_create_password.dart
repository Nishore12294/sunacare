// To parse this JSON data, do
//
//     final createPasswordResquest = createPasswordResquestFromJson(jsonString);

import 'dart:convert';

List<CreatePasswordResquest> createPasswordResquestFromJson(String str) => List<CreatePasswordResquest>.from(json.decode(str).map((x) => CreatePasswordResquest.fromJson(x)));

String createPasswordResquestToJson(List<CreatePasswordResquest> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CreatePasswordResquest {
  CreatePasswordResquest({
    this.forgotPasswordToken,
    this.emailAddress,
    this.newPassword,
  });

  String forgotPasswordToken;
  String emailAddress;
  String newPassword;

  factory CreatePasswordResquest.fromJson(Map<String, dynamic> json) => CreatePasswordResquest(
    forgotPasswordToken: json["ForgotPasswordToken"],
    emailAddress: json["EmailAddress"],
    newPassword: json["NewPassword"],
  );

  Map<String, dynamic> toJson() => {
    "ForgotPasswordToken": forgotPasswordToken,
    "EmailAddress": emailAddress,
    "NewPassword": newPassword,
  };
}
