import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:suna_care/core/data/remote/network/app_url.dart';
import 'package:suna_care/core/data/remote/network/method.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/CGOffersCitiesList.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgResp.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersListResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgResponse.dart';
import 'package:suna_care/core/data/remote/request_response/VerifyAccessToken/Verify_Access_Token_Resp.dart';
import 'package:suna_care/core/data/remote/request_response/VerifyAccessToken/verify_access_token_request.dart';
import 'package:suna_care/core/data/remote/request_response/availability/assignment/AssignmentListRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/assignment/AssignmentListResponse.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/AddHolidayRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/AddHolidayResponse.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/DeleteHolidayRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/DeleteHolidayResponse.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/EditHolidayRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/EditHolidayResponse.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/HolidayListRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/HolidayListResponse.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/request_change_password.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart';
import 'package:suna_care/core/data/remote/request_response/createpassword/request_create_password.dart';
import 'package:suna_care/core/data/remote/request_response/createpassword/response_create_password.dart';
import 'package:suna_care/core/data/remote/request_response/forgotpassword/request_forgot_password.dart';
import 'package:suna_care/core/data/remote/request_response/forgotpassword/response_forgot_password.dart';
import 'package:suna_care/core/data/remote/request_response/login/response_login.dart';
import 'package:suna_care/core/data/remote/request_response/login/request_login.dart';
import 'package:suna_care/core/data/remote/request_response/notification_settings/get_notification/GetNotificationRequest.dart';
import 'package:suna_care/core/data/remote/request_response/notification_settings/get_notification/GetNotificationResponse.dart';
import 'package:suna_care/core/data/remote/request_response/notification_settings/set_notification/SetNotificationRequest.dart';
import 'package:suna_care/core/data/remote/request_response/notification_settings/set_notification/SetNotificationResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangRequest.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangRequest.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/request_cg_profile_details.dart';
import 'package:suna_care/core/data/remote/request_response/profile/response_cg_profile_details.dart';
import 'package:suna_care/core/data/remote/request_response/register/signup/request_singup.dart';
import 'package:suna_care/core/data/remote/request_response/register/signup/response_signup.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/callrequest/GetCallRequest.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/callrequest/GetCallResponse.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/leave_us_msg/GetCGLeaveMsgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/leave_us_msg/GetCGLeaveMsgResponse.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/schedule/ScheduleAppointmentRequest.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/schedule/ScheduleAppointmentResponse.dart';
import 'package:suna_care/utils/app_constants.dart';

import '../../../../router.dart';
import 'base/base_repository.dart';

class AuthRepository extends BaseRepository {
  AuthRepository._internal();

  static final AuthRepository _singleInstance = AuthRepository._internal();

  factory AuthRepository() => _singleInstance;

  //api: Login
  Future<LoginResponse> apiUserLogin(List<LoginRequest> loginRequest) async {
    var inputJson = jsonEncode(loginRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathLogin,
        body: body,
        headers:  headerXFromUrl
        );

      try {
        if (response.statusCode == HttpStatus.ok) {
          return loginResponseFromJson(json.decode(response.body));
        } else {
          return null;
        }
      } catch (e) {
        return null;
      }
  }


  //api: Verify Access token
  Future<VerifyAccessTokenResponse> apiVerifyAccessToken(List<VerifyAccessTokenRequest> verifyAccessTokenRequest) async {
    var inputJson = jsonEncode(verifyAccessTokenRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathVerifyAccessToken,
        body: body,
        headers:  headerXFromUrl
        );

      try {
        if (response.statusCode == HttpStatus.ok) {
          return verifyAccessTokenResponseFromJson(json.decode(response.body));
        } else {
          return null;
        }
      } catch (e) {
        return null;
      }
  }

  //api: SignUp
  Future<SignUpResponse> apiSignUp(SignUpRequest signUpRequest) async {
    var inputJson = jsonEncode(signUpRequest);
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathSignUp,
        body: body,
        headers:  headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        debugPrint("apiUserSignUp :==>");
       String jsonRes = json.decode(response.body);
        debugPrint("apiUserSignUp jsonRes :==>"+jsonRes);
       if(jsonRes.contains("WebMethodOutputRows")){
          debugPrint("WebMethodOutputRows contain :==>");

        }else{
          debugPrint("WebMethodOutputRows not contain :==>");

        }
        if(jsonRes.contains("OutputRowErrors")){
          debugPrint("OutputRowErrors contain :==>");

        }else{
          debugPrint(" OutputRowErrors not contain :==>");

        }

        return signUpResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<ForgotPasswordResponse> apiForgotPassword(ForgotPasswordRequest forgotPasswordRequest) async {
    var inputJson = jsonEncode(forgotPasswordRequest);
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathForgotPassword,
        body: body,
        headers:  headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        debugPrint("apiForgotPassword :==>");
        return forgotPasswordResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<ChangePasswordResponse> apiChangePassword( {@required List<ChangePasswordRequest> listChangePasswordRequest}) async {
    var inputJson = jsonEncode(listChangePasswordRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathChangePassword,
        body: body,
        headers:  headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        debugPrint("apiChangePassword :==>");
        return  changePasswordResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  //api: GetAssignmentList
  Future<AssignmentListResponse> apiGetAssignmentList(List<AssignmentListRequest> assignmentListRequest) async {
    var inputJson = jsonEncode(assignmentListRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };

    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathGetAssignmentList,
        body: body,
        headers:  headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return assignmentListResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  //Holiday List
  Future<HolidayListResponse> apiGetHolidayList(List<HolidayListRequest> holidayListRequest) async {
      var inputJson = jsonEncode(holidayListRequest.map((e) => e.toJson()).toList());
      Map<String, String> body = {
        inputJsonDocument: inputJson.toString()
      };
      var response = await networkProvider.call(
          method: Method.POST,
          pathUrl: AppUrl.pathHolidayList,
          body: body,
          headers:  headerXFromUrl
      );

      try {
        if (response.statusCode == HttpStatus.ok) {
          return holidayListResponseFromJson(json.decode(response.body));

        } else {
          return null;
        }
      } catch (e) {
        return null;
      }
    }


  //Add Holiday List
  Future<AddHolidayResponse> apiAddHoliday(List<AddHolidayRequest> addHolidayRequest) async {
    String successResponse = "";
       var inputJson = jsonEncode(
        addHolidayRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathAddHoliday,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return addHolidayResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }


  //Delete Holiday List
  Future<DeleteHolidayResponse> apiDeleteHoliday(List<DeleteHolidayRequest> deleteHolidayRequest) async {
      var inputJson = jsonEncode(
          deleteHolidayRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathDeletHoliday,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return deleteHolidayResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }


  //Edit Holiday List
  Future<EditHolidayResponse> apiEditHoliday(List<EditHolidayRequest> editHolidayRequest) async {
     var inputJson = jsonEncode(
         editHolidayRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathEditHoliday,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return editHolidayResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

  //Offers List
  Future<OffersListResponse> apiOffersList(List<OffersRequest> offersRequest) async {
     var inputJson = jsonEncode(
         offersRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathOffersList,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return offersListResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

  //Offers Details
  Future<OffersDetailsResponse> apiOffersDetails(List<OffersDetailsRequest> offersDetailsRequest) async {
     var inputJson = jsonEncode(
         offersDetailsRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathOfferDetails,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return offersDetailsResponseFromJson(json.decode(response.body));
        //return OffersDetailsResponse.fromJson(json.decode(response.body));
       // return OffersDetailsResponse.fromJson(response.body);
       // return OffersDetailsResponse.fromJson(json.decode(utf8.decode(response.bodyBytes)));

      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }



  //Get CG Profile details
  Future<CgProfileDetailsResponse> apiGetCGProfileDetails(List<CgProfileDetailsRequest> cgProfileDetailsRequest) async {
    var inputJson = jsonEncode(
        cgProfileDetailsRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathCGProfile,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        //return cgProfileDetailsResponseFromJson getCareGiverApprovedImgRespFromJson(json.decode(response.body));
        return cgProfileDetailsResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

  //Get Offers Image Details
  Future<GetCareGiverApprovedImgResp> apiGetCGProfileImage(List<GetCareGiverApprovedImgRequest> careGiverApprovedImgRequest) async {
     var inputJson = jsonEncode(
         careGiverApprovedImgRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathCGApprovedImage,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return getCareGiverApprovedImgRespFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

  //Set Offers Image Details
  Future<SetCareGiverProfileImgResponse> apiSetOrUploadCGProfileImage(List<SetCareGiverProfileImgRequest> careGiverApprovedImgRequest) async {
     var inputJson = jsonEncode(
         careGiverApprovedImgRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathUploadCGApprovedImage,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return setCareGiverProfileImgResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }


  //Set language
  Future<SetPreferredLangResponse> apiSetPreferredLang(List<SetPreferredLangRequest> setPreferredLangRequest) async {
     var inputJson = jsonEncode(
         setPreferredLangRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathSetLanguage,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return setPreferredLangResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

  //get language
  Future<GetPreferredLangResponse> apiGetPreferredLang(List<GetPreferredLangRequest> getPreferredLangRequest) async {
     var inputJson = jsonEncode(
         getPreferredLangRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathGetLanguage,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return getPreferredLangResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

  //get language
  Future<CgOffersCitiesList> apiGetCGOffersCities() async {

    var response = await networkProvider.call(
        method: Method.GET,
        pathUrl: AppUrl.pathGetCGOffersCities,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return cgOffersCitiesListFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

  //set notification
  Future<CgOffersCitiesList> apiSetCGNotification() async {

    var response = await networkProvider.call(
        method: Method.GET,
        pathUrl: AppUrl.pathGetCGOffersCities,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return cgOffersCitiesListFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }


  //Set notifications
  Future<SetNotificationResponse> apiSetNotification(List<SetNotificationRequest> setNotificationRequest) async {
    var inputJson = jsonEncode(
        setNotificationRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathSetCGNotification,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return setNotificationResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }


  //Get notifications
  Future<GetNotificationResponse> apiGetNotification(List<GetNotificationRequest> getNotificationRequest) async {
    var inputJson = jsonEncode(
        getNotificationRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathGetCGNotification,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return getNotificationResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

  //Call Request
  Future<GetCallResponse> apiCallRequest(List<GetCallRequest> getCallRequest) async {
    var inputJson = jsonEncode(
        getCallRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathGetCallRequest,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return getCallResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }


  //Leave us message
  Future<GetCgLeaveMsgResponse> apiLeaveUsMessage(List<GetCgLeaveMsgRequest> getCgLeaveMsgRequest) async {
    var inputJson = jsonEncode(
        getCgLeaveMsgRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathLeaveUsMessage,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return getCgLeaveMsgResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

  //Schedule meeting / appointment
  Future<ScheduleAppointmentResponse> apiScheduleAppointment(List<ScheduleAppointmentRequest> scheduleAppointmentRequest) async {
    var inputJson = jsonEncode(
        scheduleAppointmentRequest.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathSchdeuleAppointment,
        body: body,
        headers: headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        return scheduleAppointmentResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;


    }
  }

//ForgotSetPassword or create password
  Future<CreatePasswordResponse> apiForgotSetPassword( {@required List<CreatePasswordResquest> listCreatePassword}) async {
    var inputJson = jsonEncode(listCreatePassword.map((e) => e.toJson()).toList());
    Map<String, String> body = {
      inputJsonDocument: inputJson.toString()
    };
    var response = await networkProvider.call(
        method: Method.POST,
        pathUrl: AppUrl.pathForgotSetPassword,
        body: body,
        headers:  headerXFromUrl
    );

    try {
      if (response.statusCode == HttpStatus.ok) {
        debugPrint("apiForgotSetPassword :==>");
        return  createPasswordResponseFromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
