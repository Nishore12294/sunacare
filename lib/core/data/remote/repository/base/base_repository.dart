import 'dart:io';

import 'package:suna_care/core/data/remote/network/network_provider.dart';

class BaseRepository {
  final networkProvider = NetworkProvider();

  //Common text
  final String inputJsonDocument = "inputJsonDocument";

  final headerContentType = {
    HttpHeaders.contentTypeHeader: "application/json",
  };

  final headerAccept = {
    HttpHeaders.contentTypeHeader: "application/json",
  };

  final headerContentTypeAndAccept = {
    HttpHeaders.contentTypeHeader: "application/json",
    HttpHeaders.acceptHeader: "application/json"
  };

  final headerXFromUrl = {
    HttpHeaders.contentTypeHeader: "application/x-www-form-urlencoded"
  };

  final Map<String, String> mapAuthHeader = {
    HttpHeaders.authorizationHeader: 'Bearer nhl8gwc8vtee0bookm2vbkw31enngxhj'
  };

  Map<String, String> buildHeaderWithUserToken(String token) {
    Map<String, String> header = headerContentTypeAndAccept;
    header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent(
        HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    return header;
  }

  Map<String, String> buildHeaderWithDeviceToken({String deviceId, String deviceToken, String deviceOsVersion, String deviceType}) {
    Map<String, String> header = headerContentTypeAndAccept;
    header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent("device_id", () => deviceId);
    header.putIfAbsent("device_token", () => deviceToken);
    header.putIfAbsent("device_os_version", () => deviceOsVersion);
    header.putIfAbsent("device_type", () => deviceType);
    return header;
  }

  Map<String, String> buildHeaderWithEmailOtp(String email, String otp) {
    Map<String, String> header = headerContentType;
    header.remove("email");
    header.remove("otp");
    header.putIfAbsent("email", () => email);
    header.putIfAbsent("otp", () => otp);
    return header;
  }

  Map<String, String> buildHeaderWithRefreshTokenAndUid({String refreshToken, String uid, String token}) {
    Map<String, String> header = headerContentTypeAndAccept;
    header.remove("refreshToken");
    header.remove("uid");
    header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent("refreshToken", () => refreshToken);
    header.putIfAbsent(
        HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    header.putIfAbsent("uid", () => uid);
    return header;
  }

  Map<String, String> buildTransactionPinHeaderWithRefreshTokenAndUid({String token, String encryptedPin, String sessionInfo}) {
    Map<String, String> header = headerContentTypeAndAccept;
    header.remove(HttpHeaders.authorizationHeader);
    header.remove("transation_pin");
    header.remove("session_info");

    header.putIfAbsent(
        HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    header.putIfAbsent("transation_pin", () => encryptedPin);
    header.putIfAbsent("session_info", () => sessionInfo);

    return header;
  }

  Map<String, String> buildCheckTransactionPinHeader({String token, String encryptedPin}) {
    Map<String, String> header = headerContentTypeAndAccept;
    header.remove(HttpHeaders.authorizationHeader);
    header.remove("encrypted-pin");

    header.putIfAbsent(
        HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    header.putIfAbsent("encrypted-pin", () => encryptedPin);

    return header;
  }

  Map<String, String> buildHeaderUserToken(String token) {
    Map<String, String> header = Map();
    //header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent(
        HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    return header;
  }

  Map<String, String> buildContentTypeWithUserToken(String token) {
    Map<String, String> header = headerContentType;
    header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent(
        HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    return header;
  }

  Map<String, String> buildAcceptWithUserToken(String token) {
    Map<String, String> header = headerAccept;
    header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent(
        HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    return header;
  }

  String getFormattedToken(String token) {
    return 'Bearer $token';
  }
}
