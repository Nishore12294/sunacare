import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/network/app_url.dart';
import 'package:suna_care/core/data/remote/network/method.dart';
import 'package:suna_care/utils/app_log_helper.dart';

import 'base/base_repository.dart';

class ServiceRepository extends BaseRepository {
  ServiceRepository._internal();
  final log = getLogger('ServiceRepository');

  static final ServiceRepository _singleInstance =
      ServiceRepository._internal();

  factory ServiceRepository() => _singleInstance;



}
