import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';

class AppSharedPreference {
  final log = getLogger('App SP');

  // singleton boilerplate
  AppSharedPreference._internal();

  static final AppSharedPreference _singleInstance = AppSharedPreference._internal();

  factory AppSharedPreference() => _singleInstance;

  //save: token of user after successful login
  Future<bool> saveUserToken(String token) async {
    log.i('called: saveUserToken');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(AppConstants.KEY_TOKEN_USER, token??"");
  }

  Future saveStringValue(String keyName, String value) async {
    log.i('called { ' + keyName + " " + value + "}");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(keyName, value);
  }

  Future saveIntValue(String keyName, int value) async {
    log.i('called { ' + keyName + " " + value.toString() + "}");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt(keyName, value);
  }

  Future saveBooleanValue(String keyName, bool value) async {
    log.i('called { ' + keyName + " " + value.toString() + "}");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(keyName, value);
  }

  //get: token of user
  Future<String> getUserToken() async {
    log.i('called: getUserToken');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(AppConstants.KEY_TOKEN_USER);
  }

  Future<String> getStringValue(String keyName) async {
    log.i('getStringValue' + keyName);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(keyName);
  }


  Future<int> getIntValue(String keyName) async {
    log.i('getIntValue' + keyName);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(keyName);
  }

  Future<bool> getBoolValue(String keyName) async {
    log.i('getIntValue' + keyName);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(keyName);
  }
}
