class Countries {
  List<CountriesList> countriesList;

  Countries({this.countriesList});

  Countries.fromJson(Map<String, dynamic> json) {
    if (json['countries'] != null) {
      countriesList = new List<CountriesList>();
      json['countries'].forEach((v) {
        countriesList.add(new CountriesList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.countriesList != null) {
      data['countries'] = this.countriesList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CountriesList {
  String name;
  String flag;
  String code;
  String dialCode;

  CountriesList({this.name, this.flag, this.code, this.dialCode});

  CountriesList.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    flag = json['flag'];
    code = json['code'];
    dialCode = json['dial_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['flag'] = this.flag;
    data['code'] = this.code;
    data['dial_code'] = this.dialCode;
    return data;
  }
}