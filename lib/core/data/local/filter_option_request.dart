// To parse this JSON data, do
//
//     final filterOptionRequest = filterOptionRequestFromJson(jsonString);

import 'dart:convert';

FilterOptionRequest filterOptionRequestFromJson(String str) => FilterOptionRequest.fromJson(json.decode(str));

String filterOptionRequestToJson(FilterOptionRequest data) => json.encode(data.toJson());

class FilterOptionRequest {
  FilterOptionRequest({
    this.durationCode,
    this.cityOfCp,
    this.germanLevelCode,
  });

  String durationCode;
  String cityOfCp;
  String germanLevelCode;

  factory FilterOptionRequest.fromJson(Map<String, dynamic> json) => FilterOptionRequest(
    durationCode: json["DurationCode"],
    cityOfCp: json["CityOfCP"],
    germanLevelCode: json["GermanLevelCode"],
  );

  Map<String, dynamic> toJson() => {
    "DurationCode": durationCode,
    "CityOfCP": cityOfCp,
    "GermanLevelCode": germanLevelCode,
  };
}
