import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/login/response_login.dart';
import 'package:suna_care/core/data/remote/request_response/login/request_login.dart';
import 'package:suna_care/utils/AppLanguage.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_network_check.dart';

import 'base/base_notifier.dart';

class WelcomeNotifier extends BaseNotifier {
  final log = getLogger('LoginNotifier');
  bool _isPasswordVisible = false;
  TextEditingController _textEditConEmail = TextEditingController();
  TextEditingController _textEditConPassword = TextEditingController();
  FocusNode _emailFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();
  bool _isEmailFocusing = false;
  bool _isPasswordFocusing = false;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  String deviceId, deviceToken, deviceType, deviceOsVersion;
  bool _isBioAuthAvailable = false;
  final LocalAuthentication auth = LocalAuthentication();
  final _repository = AuthRepository();

  WelcomeNotifier(BuildContext context) {
    setInitialData(context);
  }

  void setInitialData(BuildContext context) async {
    emailFocus.addListener(_onFocusChangeEmail);
    passwordFocus.addListener(_onFocusChangePassword);
    await getHardwareDeviceInfo();
    _checkBiometrics();

    ////Check Language code start ////
   /* var appLanguage = Provider.of<AppLanguage>(context);
    try{
      bool isEngLanguage = await AppSharedPreference().getBoolValue(AppConstants.KEY_LANG_ENGLISH);

     if(isEngLanguage != null){
       debugPrint("isEngLanguage :==>"+isEngLanguage.toString());
       if(!isEngLanguage){
         appLanguage.changeLanguage(Locale("pl"));
       }else{
         appLanguage.changeLanguage(Locale("en"));
       }
      }else{
       appLanguage.changeLanguage(Locale("en"));
      }

    }catch(ex){
      debugPrint("exception :==>"+ex.toString());
      appLanguage.changeLanguage(Locale("en"));
    }*/

    ////Check Language code end ////
  }


  bool get isBioAuthAvailable => _isBioAuthAvailable;

  set isBioAuthAvailable(bool value) {
    _isBioAuthAvailable = value;
    notifyListeners();

  }


  //Check Biometrics availables
  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
      log.e(e.toString());
    }
    isBioAuthAvailable = canCheckBiometrics;
  }
  Future<void> getHardwareDeviceInfo() async {
    //Map<String, dynamic> deviceData;

    try {
      if (Platform.isAndroid) {
        deviceType = "android";
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        deviceToken = androidInfo.androidId;
        deviceId = androidInfo.androidId;
        deviceOsVersion = androidInfo.version.release;
      } else if (Platform.isIOS) {

        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        deviceToken = iosInfo.identifierForVendor;
        deviceId = iosInfo.identifierForVendor;
        deviceOsVersion = iosInfo.systemVersion;
        deviceType = iosInfo.systemName;
      }

    } on PlatformException {
      deviceInfo = null;
    }
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  bool get isPasswordVisible => _isPasswordVisible;

  set isPasswordVisible(bool value) {
    _isPasswordVisible = value;
    notifyListeners();
  }

  TextEditingController get textEditConEmail => _textEditConEmail;

  set textEditConEmail(TextEditingController value) {
    _textEditConEmail = value;
  }

  TextEditingController get textEditConPassword => _textEditConPassword;

  set textEditConPassword(TextEditingController value) {
    _textEditConPassword = value;
  }

  bool get isEmailFocusing => _isEmailFocusing;

  set isEmailFocusing(bool value) {
    _isEmailFocusing = value;
    notifyListeners();
  }

  bool get isPasswordFocusing => _isPasswordFocusing;

  set isPasswordFocusing(bool value) {
    _isPasswordFocusing = value;
    notifyListeners();
  }


  FocusNode get emailFocus => _emailFocus;

  set emailFocus(FocusNode value) {
    _emailFocus = value;
  }

  FocusNode get passwordFocus => _passwordFocus;

  set passwordFocus(FocusNode value) {
    _passwordFocus = value;
  }

  void saveUserToken(String token) async {
    bool isTokenSaved = await AppSharedPreference().saveUserToken(token);
    log.i('user token saved satus: $isTokenSaved');
    log.i('test saved value from SP: ${await AppSharedPreference().getUserToken()}');
  }

  void saveUserCredential(String token) async {
      saveUserToken(token);
      AppSharedPreference().saveStringValue(AppConstants.KEY_USER_EMAIL_ID, _textEditConEmail.text.trim());
      AppSharedPreference().saveStringValue(AppConstants.KEY_USER_PASSWORD, _textEditConPassword.text.trim());
  }

  void _onFocusChangeEmail() {
    log.d('Email ' + _emailFocus.hasFocus.toString());
    if(_emailFocus.hasFocus) {
      isEmailFocusing = true;
    } else {
      isEmailFocusing = false;
    }
  }

  void _onFocusChangePassword() {
    log.d('Password ' + _passwordFocus.hasFocus.toString());
    if(_passwordFocus.hasFocus) {
      isPasswordFocusing = true;
    } else {
      isPasswordFocusing = false;
    }
  }



}
