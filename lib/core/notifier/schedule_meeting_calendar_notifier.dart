import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/request_change_password.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart' as changePassword;
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/schedule/ScheduleAppointmentRequest.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/schedule/ScheduleAppointmentResponse.dart';
import 'package:suna_care/ui/login_screen.dart';
import 'package:suna_care/ui/schedulemeeting/ScheduleMeetingTime.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_network_check.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';

import '../base_notifier.dart';


class ScheduleMeetingCalendarNotifier extends BaseNotifier {
  final log = getLogger('ScheduleMeetingCalendarNotifier');
  String token = '';
  DateTime _masterDate = DateTime.now();
  ScheduleAppointmentResponse _scheduleAppointmentResponse;
  final _repository = AuthRepository();
  String _languageCode = "en";
  List<ScheduleMeetingTime> _meetingTiming = new List();

  ScheduleMeetingCalendarNotifier() {
    setUpInitalPrefData();
    setInitialData();

  }
  set masterDate(value) {
    _masterDate = value;
    notifyListeners();
  }

  void setInitialData() async {


    meetingTiming.add(ScheduleMeetingTime(title: "08:00 am - 10:00 am", isSelected: false));
    meetingTiming.add(ScheduleMeetingTime(title: "10:00 am - 12:00 pm", isSelected: true));
    meetingTiming.add(ScheduleMeetingTime(title: "12:00 pm - 02:00 pm", isSelected: false));
    meetingTiming.add(ScheduleMeetingTime(title: "02:00 pm - 04:00 pm", isSelected: false));
    meetingTiming.add(ScheduleMeetingTime(title: "04:00 pm - 06:00 pm", isSelected: false));
  }

  setUpInitalPrefData() async{
    var prefs = await SharedPreferences.getInstance();
    if(prefs.getString('language_code') != null){
      debugPrint("ScheduleMeetingCalendar Screen language_code :==>"+prefs.getString('language_code')??"");
      languageCode = prefs.getString('language_code')??"";
    }else{
      debugPrint("ScheduleMeetingCalendar Screen language_code is null :==>");
      languageCode = "en";
    }
    await Jiffy.locale(languageCode);
  }


  // getter and setter
  String get languageCode => _languageCode;

  set languageCode(String value) {
    _languageCode = value;
    notifyListeners();
  }

  List<ScheduleMeetingTime>  get meetingTiming => _meetingTiming;

  set meetingTiming(List<ScheduleMeetingTime>  value)  {
    _meetingTiming = value;
    notifyListeners();
  }

  ScheduleAppointmentResponse get scheduleAppointmentResponse => _scheduleAppointmentResponse;

  set scheduleAppointmentResponse(ScheduleAppointmentResponse value)  {
    _scheduleAppointmentResponse = value;
    notifyListeners();
  }


  // call Api callApiScheduleAppointment
  Future<ScheduleAppointmentResponse> callApiScheduleAppointment(String accessToken,String appointmentDate,String appointmentStartTime,String appointmentEndTime,String offerNumber) async {
    log.i('api ::: callApiScheduleAppointment called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      ScheduleAppointmentResponse response;
      try {
        List<ScheduleAppointmentRequest> listScheduleAppointmentRequest = new List<ScheduleAppointmentRequest>();
        ScheduleAppointmentRequest scheduleAppointmentRequest = ScheduleAppointmentRequest(
            accessToken:accessToken,
            appointmentDate:appointmentDate,
            appointmentStartTime:appointmentStartTime,
            appointmentEndTime:appointmentEndTime,
            offerNumber: offerNumber

        );
        listScheduleAppointmentRequest.add(scheduleAppointmentRequest);
        response = await _repository.apiScheduleAppointment(listScheduleAppointmentRequest);

        scheduleAppointmentResponse = response;
        debugPrint("callApiScheduleAppointment :==>");
        debugPrint("callApiScheduleAppointment :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }

}
