import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/local/countries.dart';
import 'package:suna_care/core/data/remote/network/app_url.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/login/request_login.dart';
import 'package:suna_care/core/data/remote/request_response/login/response_login.dart';
import 'package:suna_care/core/data/remote/request_response/register/signup/request_singup.dart';
import 'package:suna_care/core/data/remote/request_response/register/signup/response_signup.dart';
import 'package:suna_care/core/data/remote/request_response/register/signup/response_signup.dart' as signUpResponse;
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_network_check.dart';
import 'base/base_notifier.dart';

class SignUpNotifier extends BaseNotifier {
  final log = getLogger('SignUpNotifier');
  bool _isPasswordVisible = false;
  PageController _registerFormsPageViewController = PageController();
  int _currentPageIndex = 0;
  final _repository = AuthRepository();
  String _formTitle = AppConstants.PERSONAL_INFORMATION;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  String deviceId, deviceToken, deviceType, deviceOsVersion;
  String _userToken = '';

  ////////////////////////
  // slide :3   Personal Detail
  TextEditingController _textEditFirstName = TextEditingController();
  TextEditingController _textEditLastName = TextEditingController();
  TextEditingController _textEditEmailId = TextEditingController();
  TextEditingController _textEditPhoneNo = TextEditingController();
  TextEditingController _textEditGender = TextEditingController();
  bool _gender = true;
  String genderStr, experienceInCareStr;

  FocusNode _firstNameFocus = FocusNode();
  FocusNode _lastNameFocus = FocusNode();
  FocusNode _phoneNoFocus = FocusNode();
  FocusNode _emailIdFocus = FocusNode();
  FocusNode _genderFocus = FocusNode();

  bool _driverLicSwitch = false,_smokingEnableSwitch = false,_speakingGermanLangSwitch = false,_expInCareSwitch = false,_transfWheelchairSwitch = false,_nightShiftSwitch = false,_ableTcTwoPeopleSwitch = false;
  /*
  {
  "FirstName": "",
  "LastName": "rane",
  "Email": "vrane@gmail.com",
  "MobileNumber": "8562586256",
  "Gender": "20",
  "DriverLicense": "0",
  "Smoking": "1",
  "SpeakingGermanLanguage": "0",
  "ExperienceInCare": "30",
  "WillingToDoTransfer": "1",
  "WillingtodoNightShifts": "1",
  "abletotakecareof2people": "0"
}
  * */

  /////////////////////////

   TextEditingController textEditTransactionPin = TextEditingController();

  SignUpNotifier() {
    setInitialData();
  }

  void setInitialData() async {
    await getHardwareDeviceInfo();
  }

  TextEditingController get textEditFirstName => _textEditFirstName;

  set textEditFirstName(TextEditingController value) {
    _textEditFirstName = value;
    notifyListeners();
  }

  FocusNode get firstNameFocus => _firstNameFocus;

  set firstNameFocus(FocusNode value) {
    _firstNameFocus = value;
    notifyListeners();
  }

  TextEditingController get textEditLastName => _textEditLastName;

  set textEditLastName(TextEditingController value) {
    _textEditLastName = value;
    notifyListeners();
  }

  FocusNode get lastNameFocus => _lastNameFocus;

  set lastNameFocus(FocusNode value) {
    _lastNameFocus = value;
    notifyListeners();
  }


  TextEditingController get textEditEmailId => _textEditEmailId;

  set textEditEmailId(TextEditingController value) {
    _textEditEmailId = value;
    notifyListeners();
  }

  FocusNode get emailIdFocus => _emailIdFocus;

  set emailIdFocus(FocusNode value) {
    _emailIdFocus = value;
    notifyListeners();
  }


  TextEditingController get textEditPhoneNo => _textEditPhoneNo;

  set textEditPhoneNo(TextEditingController value) {
    _textEditPhoneNo = value;
    notifyListeners();
  }
  FocusNode get phoneNoFocus => _phoneNoFocus;

  set phoneNoFocus(FocusNode value) {
    _phoneNoFocus = value;
    notifyListeners();
  }


  TextEditingController get textEditGender => _textEditGender;

  set textEditGender(TextEditingController value) {
    _textEditGender = value;
    notifyListeners();
  }
  FocusNode get genderFocus => _genderFocus;

  set genderFocus(FocusNode value) {
    _genderFocus = value;
    notifyListeners();
  }


  String get formTitle => _formTitle;

  set formTitle(String value) {
    _formTitle = value;
    notifyListeners();
  }

  ///////////////

  get registerFormsPageViewController => _registerFormsPageViewController;

  set registerFormsPageViewController(value) {
    _registerFormsPageViewController = value;
    notifyListeners();
  }

  int get currentPageIndex => _currentPageIndex;

  set currentPageIndex(int value) {
    _currentPageIndex = value;
    notifyListeners();
  }



  bool get gender => _gender;

  set gender(bool value) {
    _gender = value;
    notifyListeners();
  }

  bool get driverLicSwitch => _driverLicSwitch;

  set driverLicSwitch(bool value) {
    _driverLicSwitch = value;
    notifyListeners();
  }

  bool get smokingEnableSwitch => _smokingEnableSwitch;

  set smokingEnableSwitch(bool value) {
    _smokingEnableSwitch = value;
    notifyListeners();
  }
 bool get speakingGermanLangSwitch => _speakingGermanLangSwitch;

  set speakingGermanLangSwitch(bool value) {
    _speakingGermanLangSwitch = value;
    notifyListeners();
  }
 bool get expInCareSwitch => _expInCareSwitch;

  set expInCareSwitch(bool value) {
    _expInCareSwitch = value;
    notifyListeners();
  }
 bool get transfWheelchairSwitch => _transfWheelchairSwitch;

  set transfWheelchairSwitch(bool value) {
    _transfWheelchairSwitch = value;
    notifyListeners();
  }
 bool get nightShiftSwitch => _nightShiftSwitch;

  set nightShiftSwitch(bool value) {
    _nightShiftSwitch = value;
    notifyListeners();
  }
 bool get ableTcTwoPeopleSwitch => _ableTcTwoPeopleSwitch;

  set ableTcTwoPeopleSwitch(bool value) {
    _ableTcTwoPeopleSwitch = value;
    notifyListeners();
  }



  //api register
  Future<SignUpResponse> callApiSignUp(
      SignUpRequest signUpRequest) async {
    log.i('api ::: apiRegister called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      try {
        SignUpResponse response = await _repository.apiSignUp(signUpRequest);
        super.isLoading = false;
        return response;
      } catch (e) {
        super.isLoading = false;
        log.e("Err: " + e.toString());
        return null;
      }
    } else {
     // return SignUpResponse(message: AppConstants.ERROR_INTERNET_CONNECTION);

      //No internet connection
      signUpResponse.SignUpResponse response = new signUpResponse.SignUpResponse();

      signUpResponse.OutputRowErrors outputRowErrors = signUpResponse.OutputRowErrors();

      signUpResponse.OutputRowError outputRowError = signUpResponse.OutputRowError();
      outputRowError.textMessage = AppConstants.NO_INTERNET_CONNECTION;

      List<signUpResponse.OutputRowError> listOutputRowError = new  List<signUpResponse.OutputRowError>();
      listOutputRowError.add(outputRowError);

      outputRowErrors.outputRowErrors = listOutputRowError;
      response.outputRowErrors = outputRowErrors;
      return response;
    }
  }




  Future removeUserCred() async {
    AppSharedPreference().saveStringValue(AppConstants.KEY_USER_EMAIL_ID, '');
    AppSharedPreference().saveStringValue(AppConstants.KEY_USER_PASSWORD, '');
  }

  // Param Builder
  SignUpRequest getSignUpRequest() {
    return SignUpRequest(
        firstName: textEditFirstName.text.trim(),
        lastName: textEditLastName.text.trim(),
        email: textEditEmailId.text.trim(),
        mobileNumber: textEditPhoneNo.text.trim(),
        gender: genderStr == "Male" ? "10" : "20",
        driverLicense :driverLicSwitch ? "1" : "0",
        smoking:smokingEnableSwitch ? "1" : "0",
        speakingGermanLanguage:speakingGermanLangSwitch ? "1" : "0",
        //experienceInCare:expInCareSwitch ? "30" : "30",
//      No – 10
//Max 6 Months – 20
//6 Months - 1 Year – 30
//1 Year - 2 Years – 40
//Over 2 years – 50

    ////New Values = OPtions
      //     // 10     = No
      //     // 20     = Under 1 Year
      //     // 30     = 1 - 3 Years
      //     // 40     = 3 - 5 Years
      //     // 50     = 5 - 10 Years
      //     // 60     = Over 10 Years
        experienceInCare:
        (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "No") ? "10"
        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "Under 1 Year") ? "20"
        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "1 - 3 Years") ? "30"
        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "3 - 5 Years") ? "40"
        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "5 - 10 Years") ? "50"
        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "Over 10 Years") ? "60"
        : "10",
        willingToDoTransfer:transfWheelchairSwitch ? "1" : "0",
        willingtodoNightShifts:nightShiftSwitch ? "1" : "0",
        abletotakecareof2People:ableTcTwoPeopleSwitch ? "1" : "0",
    );
  }

  void saveUserCredential(String token) async {
    saveUserToken(token);
    AppSharedPreference().saveStringValue(AppConstants.KEY_USER_EMAIL_ID, textEditEmailId.text.trim());
    AppSharedPreference().saveStringValue(AppConstants.KEY_USER_PASSWORD, textEditEmailId.text.trim());

  }

  void saveUserToken(String token) async {
    bool isTokenSaved = await AppSharedPreference().saveUserToken(token);
    log.i('user token saved satus: $isTokenSaved');
    log.i('test saved value from SP: ${await AppSharedPreference().getUserToken()}');
  }

  @override
  void dispose() {
    try {

    } catch (e) {
      log.e(e.toString());
    }
    super.dispose();
  }

  Future<void> getHardwareDeviceInfo() async {
    //Map<String, dynamic> deviceData;

    try {
      if (Platform.isAndroid) {
        deviceType = "android";
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        deviceToken = androidInfo.androidId;
        deviceId = androidInfo.androidId;
        deviceOsVersion = androidInfo.version.release;
      } else if (Platform.isIOS) {

        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        deviceToken = iosInfo.identifierForVendor;
        deviceId = iosInfo.identifierForVendor;
        deviceOsVersion = iosInfo.systemVersion;
        deviceType = iosInfo.systemName;
      }

    } on PlatformException {
      deviceInfo = null;
    }
  }
}
