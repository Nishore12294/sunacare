import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgResp.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersListResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangRequest.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangRequest.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/request_cg_profile_details.dart';
import 'package:suna_care/core/data/remote/request_response/profile/response_cg_profile_details.dart';
import 'package:suna_care/core/data/local/cg_giver_needed_full_list.dart';
import 'package:suna_care/core/data/local/cg_giver_expandedhdr_items.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_network_check.dart';

import '../base_notifier.dart';

class ProfileNotifier extends BaseNotifier {
  final _repository = AuthRepository();
  String locationStr,germanSkillsStr,contractDaysStr;
  OffersListResponse _offersListResp;
  OffersDetailsResponse _offersDetailsResponse;
  GetCareGiverApprovedImgResp _getCareGiverApprovedImgResp;
  CgProfileDetailsResponse _cgProfileDetailsResponse;

  bool _updateOnAvailabilitySwitch = false,_newOffersToggleSwitch = false,_availabilitySetSwitch = false,_profileUploadSwitch = false,_ableTcTwoPeopleSwitch = false;
  bool _faceIdToggleSwitch = false,_fingerPrintToggleSwitch = false;
  bool _isEnglishLangSelected = false;
  File _imageLocalFile = null;
  bool _isBioAuthAvailable = false;
  final LocalAuthentication auth = LocalAuthentication();
  BuildContext mContext;
  ProfileNotifier(BuildContext context,{@required String isFromWhichScreen,@required String offerGuid})  {
    setUpInitialData(context,isFromWhichScreen:isFromWhichScreen,offerGuid: offerGuid);
  }



  void setUpInitialData(BuildContext context,{@required String isFromWhichScreen,@required String offerGuid})async{

    //Check finger print enable or not
    checkFingerPrintEnabledOrNot();
    mContext = context;
    String accessToken = await AppSharedPreference().getUserToken();
    debugPrint("accessToken:==>"+accessToken);

    if(isFromWhichScreen == "profileScreen"){
      callApiGetProfileImage(accessToken);
    }else if(isFromWhichScreen == "viewProfileScreen" ){
      callApiGetProfileDetails(accessToken);
    }else if(isFromWhichScreen == "changeLanguageScreen"){
      //callApiGetPreferredLang(accessToken);
      bool isEngLanguage = await AppSharedPreference().getBoolValue(AppConstants.KEY_LANG_ENGLISH)??true;
      debugPrint("isEngLanguage :==>"+isEngLanguage.toString());
      isEnglishLangSelected = isEngLanguage;

    }
  }

  bool get isBioAuthAvailable => _isBioAuthAvailable;

  set isBioAuthAvailable(bool value) {
    _isBioAuthAvailable = value;
    notifyListeners();
  }


  //Check Biometrics availables
  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
      log.e(e.toString());
    }
    isBioAuthAvailable = canCheckBiometrics;
  }

  void checkFingerPrintEnabledOrNot() async {
    debugPrint("profileNotifier.fingerPrintToggleSwitch:==>" +
       fingerPrintToggleSwitch.toString());
    print('profile isBioAuthAvailable:==>' +
        isBioAuthAvailable.toString());


    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isPrefFingerPrintEnabled = prefs.getBool(
        AppConstants.KEY_FINGER_PRINT_ENABLED) ?? false;
    print('isPrefFingerPrintEnabled:==>' + isPrefFingerPrintEnabled.toString());
    if(isPrefFingerPrintEnabled){
      fingerPrintToggleSwitch = true;
    }else{
      fingerPrintToggleSwitch = false;
    }

  }

  File get imageLocalFile => _imageLocalFile;

  set imageLocalFile(File value) {
    _imageLocalFile = value;
    notifyListeners();
  }


  bool get isEnglishLangSelected => _isEnglishLangSelected;

  set isEnglishLangSelected(bool value)  {
    // bool isEngLanguage = await AppSharedPreference().getBoolValue(AppConstants.KEY_LANG_ENGLISH);
    // debugPrint("isEngLanguage :==>"+isEngLanguage.toString());
    _isEnglishLangSelected = value;
    notifyListeners();
  }


  bool get updateOnAvailabilitySwitch => _updateOnAvailabilitySwitch;

  set updateOnAvailabilitySwitch(bool value) {
    _updateOnAvailabilitySwitch = value;
    notifyListeners();
  }
  bool get newOffersToggleSwitch => _newOffersToggleSwitch;

  set newOffersToggleSwitch(bool value) {
    _newOffersToggleSwitch = value;
    notifyListeners();
  }

  /////
  bool get availabilitySetSwitch => _availabilitySetSwitch;


  set availabilitySetSwitch(bool value) {
    _availabilitySetSwitch = value;
    notifyListeners();
  }
  bool get profileUploadSwitch => _profileUploadSwitch;

  set profileUploadSwitch(bool value) {
    _profileUploadSwitch = value;
    notifyListeners();
  }
  bool get ableTcTwoPeopleSwitch => _ableTcTwoPeopleSwitch;

  set ableTcTwoPeopleSwitch(bool value) {
    _ableTcTwoPeopleSwitch = value;
    notifyListeners();
  }


  bool get faceIdToggleSwitch => _faceIdToggleSwitch;

  set faceIdToggleSwitch(bool value) {
    _faceIdToggleSwitch = value;
    notifyListeners();
  }
  bool get fingerPrintToggleSwitch => _fingerPrintToggleSwitch;

  set fingerPrintToggleSwitch(bool value) {
    _fingerPrintToggleSwitch = value;
    notifyListeners();
  }


  //getCareGiverApprovedImgResp
  GetCareGiverApprovedImgResp get getCareGiverApprovedImgResp => _getCareGiverApprovedImgResp;

  set getCareGiverApprovedImgResp(GetCareGiverApprovedImgResp value) {

    _getCareGiverApprovedImgResp = value;
    notifyListeners();
  }

  //CgProfileDetailsResponse
  CgProfileDetailsResponse get cgProfileDetailsResponse => _cgProfileDetailsResponse;

  set cgProfileDetailsResponse(CgProfileDetailsResponse value) {

    _cgProfileDetailsResponse = value;
    notifyListeners();
  }


  // callApi CG profile details
  Future<CgProfileDetailsResponse> callApiGetProfileDetails(String accessToken) async {
    log.i('api ::: callApiOffersList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      CgProfileDetailsResponse response;
      try {
        List<CgProfileDetailsRequest> listCgProfileDetailsRequest = new List<CgProfileDetailsRequest>();
        CgProfileDetailsRequest cgProfileDetailsRequest = CgProfileDetailsRequest(accessToken:accessToken );
        listCgProfileDetailsRequest.add(cgProfileDetailsRequest);
        response = await _repository.apiGetCGProfileDetails(listCgProfileDetailsRequest);

        cgProfileDetailsResponse = response;
        debugPrint("callApiGetProfileDetails :==>");
        debugPrint("callApiGetProfileDetails :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }


  // callApiGetProfile
  Future<GetCareGiverApprovedImgResp> callApiGetProfileImage(String accessToken) async {
    log.i('api ::: callApiOffersList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      GetCareGiverApprovedImgResp response;
      try {
        List<GetCareGiverApprovedImgRequest> listCareGiverApprovedImgRequest = new List<GetCareGiverApprovedImgRequest>();
        GetCareGiverApprovedImgRequest listCareGiverApprovedImageReq = GetCareGiverApprovedImgRequest(accessToken:accessToken );
        listCareGiverApprovedImgRequest.add(listCareGiverApprovedImageReq);
        response = await _repository.apiGetCGProfileImage(listCareGiverApprovedImgRequest);

        // if(response != null && response.outputRowErrors != null
        //     && response.outputRowErrors.outputRowErrors[0] != null
        // &&response.outputRowErrors.outputRowErrors[0].textMessage != "" ){
        //   if(response.outputRowErrors.outputRowErrors[0].textMessage != "No image"){
        //     showDialog(
        //       context: mContext,
        //       builder: (_) => AlertOverlay(
        //           AppConstants.PROFILE_ALERT,
        //           response.outputRowErrors.outputRowErrors[0].textMessage,
        //           AppConstants.OKAY),
        //     );
        //   }
        //
        //
        // }

        getCareGiverApprovedImgResp = response;
        debugPrint("callApiGetProfile :==>");
        debugPrint("callApiGetProfile :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }

  //api Set/Upload Profile image
  Future<SetCareGiverProfileImgResponse> callApiSetOrUploadProfile(String accessToken,String base64Str) async {
    log.i('api ::: callApiOffersList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      SetCareGiverProfileImgResponse response;
      try {
        List<SetCareGiverProfileImgRequest> listSetCareGiverProfileImgRequest = new List<SetCareGiverProfileImgRequest>();
        SetCareGiverProfileImgRequest listCareGiverApprovedImageReq = SetCareGiverProfileImgRequest(accessToken:accessToken,imageByteArray: base64Str );
        listSetCareGiverProfileImgRequest.add(listCareGiverApprovedImageReq);
        response = await _repository.apiSetOrUploadCGProfileImage(listSetCareGiverProfileImgRequest);


        debugPrint("callApiGetProfile :==>");
        debugPrint("callApiGetProfile :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }


  //api set language
  Future<SetPreferredLangResponse> callApiSetPreferredLang(String accessToken,String prefferedLanguage) async {
    log.i('api ::: callApiOffersList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      SetPreferredLangResponse response;
      try {
        List<SetPreferredLangRequest> listSetPreferredLangRequest = new List<SetPreferredLangRequest>();
        SetPreferredLangRequest setPreferredLangRequest = SetPreferredLangRequest(accessToken:accessToken,preferredLanguage: prefferedLanguage );
        listSetPreferredLangRequest.add(setPreferredLangRequest);
        response = await _repository.apiSetPreferredLang(listSetPreferredLangRequest);


        debugPrint("callApiSetPreferredLang :==>");
        debugPrint("callApiSetPreferredLang :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }

  //api get language
  Future<GetPreferredLangResponse> callApiGetPreferredLang(String accessToken) async {
    log.i('api ::: callApiGetPreferredLang called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      GetPreferredLangResponse response;
      try {
        List<GetPreferredLangRequest> listGetPreferredLangRequest = new List<GetPreferredLangRequest>();
        GetPreferredLangRequest getPreferredLangRequest = GetPreferredLangRequest(accessToken:accessToken );
        listGetPreferredLangRequest.add(getPreferredLangRequest);
        response = await _repository.apiGetPreferredLang(listGetPreferredLangRequest);


        debugPrint("callApiSetPreferredLang :==>");
        debugPrint("callApiSetPreferredLang :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }

}