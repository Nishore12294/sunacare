import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgResp.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersListResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgResponse.dart';
import 'package:suna_care/core/data/remote/request_response/notification_settings/get_notification/GetNotificationRequest.dart';
import 'package:suna_care/core/data/remote/request_response/notification_settings/get_notification/GetNotificationResponse.dart';
import 'package:suna_care/core/data/remote/request_response/notification_settings/set_notification/SetNotificationRequest.dart';
import 'package:suna_care/core/data/remote/request_response/notification_settings/set_notification/SetNotificationResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangRequest.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangRequest.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/request_cg_profile_details.dart';
import 'package:suna_care/core/data/remote/request_response/profile/response_cg_profile_details.dart';
import 'package:suna_care/core/data/local/cg_giver_needed_full_list.dart';
import 'package:suna_care/core/data/local/cg_giver_expandedhdr_items.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_network_check.dart';

import '../base_notifier.dart';

class NotificationsNotifier extends BaseNotifier {
  final _repository = AuthRepository();
  String locationStr,germanSkillsStr,contractDaysStr;
  OffersListResponse _offersListResp;
  OffersDetailsResponse _offersDetailsResponse;
  GetCareGiverApprovedImgResp _getCareGiverApprovedImgResp;
  CgProfileDetailsResponse _cgProfileDetailsResponse;

  bool _updateOnAvailabilitySwitch = false,_newOffersToggleSwitch = false,_availabilitySetSwitch = false,_profileUploadSwitch = false,_ableTcTwoPeopleSwitch = false;
  bool _faceIdToggleSwitch = false,_fingerPrintToggleSwitch = false;
  bool _isEnglishLangSelected = false;
  File _imageLocalFile = null;

  NotificationsNotifier(BuildContext context,{@required String isFromWhichScreen,@required String offerGuid})  {
    setUpInitialData(context,isFromWhichScreen:isFromWhichScreen,offerGuid: offerGuid);
  }



  void setUpInitialData(BuildContext context,{@required String isFromWhichScreen,@required String offerGuid})async{
    String accessToken = await AppSharedPreference().getUserToken();
    debugPrint("accessToken:==>"+accessToken);
    callApiNotificationSettings(accessToken);


  }



  File get imageLocalFile => _imageLocalFile;

  set imageLocalFile(File value) {
    _imageLocalFile = value;
    notifyListeners();
  }


  bool get isEnglishLangSelected => _isEnglishLangSelected;

  set isEnglishLangSelected(bool value)  {
    // bool isEngLanguage = await AppSharedPreference().getBoolValue(AppConstants.KEY_LANG_ENGLISH);
    // debugPrint("isEngLanguage :==>"+isEngLanguage.toString());
    _isEnglishLangSelected = value;
    notifyListeners();
  }


  bool get updateOnAvailabilitySwitch => _updateOnAvailabilitySwitch;

  set updateOnAvailabilitySwitch(bool value) {
    _updateOnAvailabilitySwitch = value;
    notifyListeners();
  }
  bool get newOffersToggleSwitch => _newOffersToggleSwitch;

  set newOffersToggleSwitch(bool value) {
    _newOffersToggleSwitch = value;
    notifyListeners();
  }

  /////
  bool get availabilitySetSwitch => _availabilitySetSwitch;


  set availabilitySetSwitch(bool value) {
    _availabilitySetSwitch = value;
    notifyListeners();
  }
  bool get profileUploadSwitch => _profileUploadSwitch;

  set profileUploadSwitch(bool value) {
    _profileUploadSwitch = value;
    notifyListeners();
  }
  bool get ableTcTwoPeopleSwitch => _ableTcTwoPeopleSwitch;

  set ableTcTwoPeopleSwitch(bool value) {
    _ableTcTwoPeopleSwitch = value;
    notifyListeners();
  }


  bool get faceIdToggleSwitch => _faceIdToggleSwitch;

  set faceIdToggleSwitch(bool value) {
    _faceIdToggleSwitch = value;
    notifyListeners();
  }
  bool get fingerPrintToggleSwitch => _fingerPrintToggleSwitch;

  set fingerPrintToggleSwitch(bool value) {
    _fingerPrintToggleSwitch = value;
    notifyListeners();
  }


  //getCareGiverApprovedImgResp
  GetCareGiverApprovedImgResp get getCareGiverApprovedImgResp => _getCareGiverApprovedImgResp;

  set getCareGiverApprovedImgResp(GetCareGiverApprovedImgResp value) {

    _getCareGiverApprovedImgResp = value;
    notifyListeners();
  }

  //CgProfileDetailsResponse
  CgProfileDetailsResponse get cgProfileDetailsResponse => _cgProfileDetailsResponse;

  set cgProfileDetailsResponse(CgProfileDetailsResponse value) {

    _cgProfileDetailsResponse = value;
    notifyListeners();
  }





  //api Set Notification
  Future<SetNotificationResponse> callApiSetNotification(String accessToken,String notificationName,String notificationValue) async {
    log.i('api ::: callApiSetNotification called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      SetNotificationResponse response;
      try {
        List<SetNotificationRequest> listSetPreferredLangRequest = new List<SetNotificationRequest>();
        SetNotificationRequest setNotificationRequest = SetNotificationRequest(accessToken:accessToken,notificationName: notificationName,notificationValue: notificationValue );
        listSetPreferredLangRequest.add(setNotificationRequest);
        response = await _repository.apiSetNotification(listSetPreferredLangRequest);


        debugPrint("callApiSetNotification :==>");
        debugPrint("callApiSetNotification :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }



  //api Get Notification Response
  Future<GetNotificationResponse> callApiNotificationSettings(String accessToken) async {
    log.i('api ::: callApiNotificationSettings called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      GetNotificationResponse response;
      try {
        List<GetNotificationRequest> listGetNotificationRequest = new List<GetNotificationRequest>();
        GetNotificationRequest getPreferredLangRequest = GetNotificationRequest(accessToken:accessToken );
        listGetNotificationRequest.add(getPreferredLangRequest);
        response = await _repository.apiGetNotification(listGetNotificationRequest);
        if(response != null && response.webMethodOutputRows != null  && response.webMethodOutputRows.webMethodOutputRows[0] != null){

          //Availability set
          if(response.webMethodOutputRows.webMethodOutputRows[0].availabilitySet == "On"){
            availabilitySetSwitch = true;
          }else{
            availabilitySetSwitch = false;
          }

          //ProfileUpload set
          if(response.webMethodOutputRows.webMethodOutputRows[0].profileUpload == "On"){
            profileUploadSwitch = true;
          }else{
            profileUploadSwitch = false;
          }

          //updateOnAvailability set
          if(response.webMethodOutputRows.webMethodOutputRows[0].updateOnAvailability == "On"){
            updateOnAvailabilitySwitch = true;
          }else{
            updateOnAvailabilitySwitch = false;
          }

          //newOffers set
          if(response.webMethodOutputRows.webMethodOutputRows[0].newOffers == "On"){
            newOffersToggleSwitch = true;
          }else{
            newOffersToggleSwitch = false;
          }


        }


        debugPrint("callApiNotificationSettings :==>");
        debugPrint("callApiNotificationSettings :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }

}