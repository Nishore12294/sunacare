import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgResp.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersListResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangRequest.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangRequest.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangResponse.dart';
import 'package:suna_care/core/data/remote/request_response/profile/request_cg_profile_details.dart';
import 'package:suna_care/core/data/remote/request_response/profile/response_cg_profile_details.dart';
import 'package:suna_care/core/data/local/cg_giver_needed_full_list.dart';
import 'package:suna_care/core/data/local/cg_giver_expandedhdr_items.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/leave_us_msg/GetCGLeaveMsgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/leave_us_msg/GetCGLeaveMsgResponse.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/leave_us_msg/GetCGLeaveMsgResponse.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_network_check.dart';

import '../base_notifier.dart';

class CallRequestNotifier extends BaseNotifier {
  final _repository = AuthRepository();
  bool _isLeaveUsMsgSelected = false;
  bool _isInitalPageLeaveUsMsgSelected = true;


  CallRequestNotifier(BuildContext context,{@required String isFromWhichScreen})  {
    setUpInitialData(context,isFromWhichScreen:isFromWhichScreen);
  }



  void setUpInitialData(BuildContext context,{@required String isFromWhichScreen,@required String offerGuid})async{
    String accessToken = await AppSharedPreference().getUserToken();
    debugPrint("accessToken:==>"+accessToken);

   
  }


  bool get isLeaveUsMsgSelected => _isLeaveUsMsgSelected;

  set isLeaveUsMsgSelected(bool value)  {
    _isLeaveUsMsgSelected = value;
    notifyListeners();
  }

 bool get isInitalPageLeaveUsMsgSelected => _isInitalPageLeaveUsMsgSelected;

  set isInitalPageLeaveUsMsgSelected(bool value)  {
    _isInitalPageLeaveUsMsgSelected = value;
    notifyListeners();
  }







}