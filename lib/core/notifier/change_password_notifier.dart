import 'package:flutter/material.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/request_change_password.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart' as changePassword;
import 'package:suna_care/ui/login_screen.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_network_check.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';

import 'base/base_notifier.dart';

class ChangePasswordNotifier extends BaseNotifier {
  final log = getLogger('ChangePasswordNotifier');
  bool _isPasswordVisible = false;

  TextEditingController _textEditOldPwd = TextEditingController();
  TextEditingController _textEditNewPwd = TextEditingController();
  TextEditingController _textEditConfirmPwd = TextEditingController();
  FocusNode _oldPwdFocus = FocusNode();
  FocusNode _newPwdFocus = FocusNode();
  FocusNode _confirmPwdFocus = FocusNode();
  bool _isOldPwdFocusing = false;
  bool _isNewPwdFocusing = false;
  bool _isConfirmPwdFocusing = false;
  String token = '';

  final _repository = AuthRepository();

  ChangePasswordNotifier() {
    setInitialData();
  }

  TextEditingController get textEditOldPwd => _textEditOldPwd;

  set textEditOldPwd(TextEditingController value) {
    _textEditOldPwd = value;
    notifyListeners();
  }
  TextEditingController get textEditNewPwd => _textEditNewPwd;

  set textEditNewPwd(TextEditingController value) {
    _textEditNewPwd = value;
    notifyListeners();
  }
  TextEditingController get textEditConfirmPwd => _textEditConfirmPwd;

  set textEditConfirmPwd(TextEditingController value) {
    _textEditConfirmPwd = value;
    notifyListeners();
  }


  void setInitialData() async {
//    token = await AppSharedPreference().getUserToken();
//    oldPwdFocus.addListener(_onFocusChangeEmail);
//    newPwdFocus.addListener(_onFocusChangePassword);
  }

  bool get isPasswordVisible => _isPasswordVisible;

  set isPasswordVisible(bool value) {
    _isPasswordVisible = value;
    notifyListeners();
  }

  bool get isOldPwdFocusing => _isOldPwdFocusing;

  set isOldPwdFocusing(bool value) {
    _isOldPwdFocusing = value;
    notifyListeners();
  }

  bool get isNewPwdFocusing => _isNewPwdFocusing;

  set isNewPwdFocusing(bool value) {
    _isNewPwdFocusing = value;
    notifyListeners();
  }

  FocusNode get oldPwdFocus => _oldPwdFocus;

  set oldPwdFocus(FocusNode value) {
    _oldPwdFocus = value;
    notifyListeners();
  }

  FocusNode get newPwdFocus => _newPwdFocus;

  set newPwdFocus(FocusNode value) {
    _newPwdFocus = value;
    notifyListeners();
  }


  FocusNode get confirmPwdFocus => _confirmPwdFocus;

  set confirmPwdFocus(FocusNode value) {
    _confirmPwdFocus = value;
    notifyListeners();
  }

  bool get isConfirmPwdFocusing => _isConfirmPwdFocusing;

  set isConfirmPwdFocusing(bool value) {
    _isConfirmPwdFocusing = value;
    notifyListeners();
  }


  //api ChangePassword
  Future<ChangePasswordResponse> callApiChangePassword() async {
    log.i('api ::: apiCallApiChangePassword called');
    String accessToken = await AppSharedPreference().getUserToken();
    log.d('saved accessToken value from SP :==> '+accessToken);

    String storedEmailId = await AppSharedPreference().getStringValue(AppConstants.KEY_USER_EMAIL_ID);
    String storedPassword = await AppSharedPreference().getStringValue(AppConstants.KEY_USER_PASSWORD);
    log.i('api ::: storedEmailId:==>'+storedEmailId);
    log.i('api ::: storedPassword:==>'+storedPassword);

    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      ChangePasswordResponse response;
      try {
        List<ChangePasswordRequest> listChangePasswordRequest = new List<ChangePasswordRequest>();
        ChangePasswordRequest changePasswordRequest = new  ChangePasswordRequest(
            accessToken: accessToken,
            oldAppPassword: storedPassword,
            newAppPassword: textEditConfirmPwd.text
        );
        listChangePasswordRequest.add(changePasswordRequest);
        response = await _repository.apiChangePassword(listChangePasswordRequest:listChangePasswordRequest);


        super.isLoading = false;
      } catch (e) {
        log.e("Err : " + e.toString());
        super.isLoading = false;
        return null;
      }
      return response;
    } else {

      //No internet connection
      changePassword.ChangePasswordResponse response = new changePassword.ChangePasswordResponse();

      changePassword.OutputRowErrors outputRowErrors = changePassword.OutputRowErrors();

      changePassword.OutputRowError outputRowError = changePassword.OutputRowError();
      outputRowError.textMessage = AppConstants.NO_INTERNET_CONNECTION;

      List<changePassword.OutputRowError> listOutputRowError = new  List<changePassword.OutputRowError>();
      listOutputRowError.add(outputRowError);

      outputRowErrors.outputRowErrors = listOutputRowError;
      response.outputRowErrors = outputRowErrors;
      return response;
    }
  }

  void _onFocusChangeEmail() {
    log.d('Email ' + _oldPwdFocus.hasFocus.toString());
    if(_oldPwdFocus.hasFocus) {
      isOldPwdFocusing = true;
    } else {
      isOldPwdFocusing = false;
    }
  }

  void _onFocusChangePassword() {
    log.d('Password ' + _newPwdFocus.hasFocus.toString());
    if(_newPwdFocus.hasFocus) {
      isNewPwdFocusing = true;
    } else {
      isNewPwdFocusing = false;
    }
  }





}
