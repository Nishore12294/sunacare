import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/VerifyAccessToken/Verify_Access_Token_Resp.dart';
import 'package:suna_care/core/data/remote/request_response/VerifyAccessToken/verify_access_token_request.dart';
import 'package:suna_care/core/data/remote/request_response/login/response_login.dart' as loginresponse;
import 'package:suna_care/core/data/remote/request_response/login/request_login.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangRequest.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangResponse.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_network_check.dart';

import 'base/base_notifier.dart';

class LoginNotifier extends BaseNotifier {
  final log = getLogger('LoginNotifier');
  bool _isPasswordVisible = false;
  // TextEditingController _textEditEmailId = TextEditingController(text: "vijaymoby+14@gmail.com");
  // TextEditingController _textEditPassword = TextEditingController(text: "test123");
 TextEditingController _textEditEmailId = TextEditingController(text: "");
  TextEditingController _textEditPassword = TextEditingController(text: "");
  FocusNode _emailFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();
  bool _isEmailFocusing = false;
  bool _isPasswordFocusing = false;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  String deviceId, deviceToken, deviceType, deviceOsVersion;
  bool _isBioAuthAvailable = false;
  final LocalAuthentication auth = LocalAuthentication();
  int _pageView = 1;

  final _repository = AuthRepository();

  LoginNotifier() {
    setInitialData();
  }

  void setInitialData() async {
   // emailFocus.addListener(_onFocusChangeEmail);
   // passwordFocus.addListener(_onFocusChangePassword);
    await getHardwareDeviceInfo();
    //AppLanguage appLanguage = AppLanguage();
    await fetchLocale();
    _checkBiometrics();
  }


  bool get isBioAuthAvailable => _isBioAuthAvailable;

  set isBioAuthAvailable(bool value) {
    _isBioAuthAvailable = value;
    notifyListeners();
  }


  //Check Biometrics availables
  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
      log.e(e.toString());
    }
    isBioAuthAvailable = canCheckBiometrics;
  }
  Future<void> getHardwareDeviceInfo() async {
    //Map<String, dynamic> deviceData;

    try {
      if (Platform.isAndroid) {
        deviceType = "android";
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        deviceToken = androidInfo.androidId;
        deviceId = androidInfo.androidId;
        deviceOsVersion = androidInfo.version.release;
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        deviceToken = iosInfo.identifierForVendor;
        deviceId = iosInfo.identifierForVendor;
        deviceOsVersion = iosInfo.systemVersion;
        deviceType = "ios"; //iosInfo.systemName;
      }
    } on PlatformException {
      deviceInfo = null;
    }
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  bool get isPasswordVisible => _isPasswordVisible;

  set isPasswordVisible(bool value) {
    _isPasswordVisible = value;
    notifyListeners();
  }

  TextEditingController get textEditEmailId => _textEditEmailId;

  set textEditEmailId(TextEditingController value) {
    _textEditEmailId = value;
  }

  TextEditingController get textEditPassword => _textEditPassword;

  set textEditPassword(TextEditingController value) {
    _textEditPassword = value;
  }

  bool get isEmailFocusing => _isEmailFocusing;

  set isEmailFocusing(bool value) {
    _isEmailFocusing = value;
    notifyListeners();
  }

  bool get isPasswordFocusing => _isPasswordFocusing;

  set isPasswordFocusing(bool value) {
    _isPasswordFocusing = value;
    notifyListeners();
  }

  FocusNode get emailFocus => _emailFocus;

  set emailFocus(FocusNode value) {
    _emailFocus = value;
  }

  FocusNode get passwordFocus => _passwordFocus;

  set passwordFocus(FocusNode value) {
    _passwordFocus = value;
  }

  //api Login
  Future<loginresponse.LoginResponse> callApiLoginUser() async {
    log.i('api ::: apiLoginUser called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      loginresponse.LoginResponse response;
      try {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String fcmToken = prefs.getString("FCM_TOKEN")??"";
        List<String> deviceInfo =  await getDeviceInfo();
        List<LoginRequest> listLoginRequest = new List<LoginRequest>();
         LoginRequest loginRequest = LoginRequest(
          emailAddress: _textEditEmailId.text.trim(),
          password: _textEditPassword.text.trim(),
          deviceID:deviceInfo[1],
          deviceToken: fcmToken,
         // deviceToken: deviceInfo[1],
          deviceType: deviceInfo[0],);
        listLoginRequest.add(loginRequest);

        response = await _repository.apiUserLogin(listLoginRequest);
        debugPrint("callApiLoginUser :==>");
        debugPrint("callApiLoginUser1 :==>"+response.toJson().toString());
        debugPrint("callApiLoginUser1 :==>"+json.encode(response.toJson()));
        debugPrint("callApiLoginUser  response:==>"+response.webMethodOutputRows.webMethodOutputRows[0].cgStatus.toString());
        //log.d('Response notifier:==>: $response');
       //  response = await _repository.apiCall(listLoginRequest);
        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
        return null;
      }

      return response;
    } else {

      //No Internet connection
      loginresponse.LoginResponse response = new loginresponse.LoginResponse();

      loginresponse.WebMethodOutputRows webMethodOPRows = loginresponse.WebMethodOutputRows();

      loginresponse.WebMethodOutputRow webMethodOutputRow = loginresponse.WebMethodOutputRow();
      webMethodOutputRow.cgStatus = AppConstants.NO_INTERNET_CONNECTION;

      List<loginresponse.WebMethodOutputRow> listWebMethodOutput = new  List<loginresponse.WebMethodOutputRow>();
      listWebMethodOutput.add(webMethodOutputRow);

      webMethodOPRows.webMethodOutputRows = listWebMethodOutput;
      response.webMethodOutputRows = webMethodOPRows;

      return response;
    }

  void saveUserToken(String token) async {
    bool isTokenSaved = await AppSharedPreference().saveUserToken(token);
    log.d('user token saved satus: $isTokenSaved');
    log.d('saved value from SP: ${await AppSharedPreference().getUserToken()}');
  }

  void saveUserCredential(String token) async {
    saveUserToken(token);
    AppSharedPreference().saveStringValue(
        AppConstants.KEY_USER_EMAIL_ID, _textEditEmailId.text.trim());
    AppSharedPreference().saveStringValue(
        AppConstants.KEY_USER_PASSWORD, _textEditPassword.text.trim());
  }

  void _onFocusChangeEmail() {
    log.d('Email ' + _emailFocus.hasFocus.toString());
    if (_emailFocus.hasFocus) {
      isEmailFocusing = true;
    } else {
      isEmailFocusing = false;
    }
  }

  void _onFocusChangePassword() {
    log.d('Password ' + _passwordFocus.hasFocus.toString());
    if (_passwordFocus.hasFocus) {
      isPasswordFocusing = true;
    } else {
      isPasswordFocusing = false;
    }
  }
}

  //api Verify token
  Future<VerifyAccessTokenResponse> callApiVerifyAccessToken(String accessToken) async {
    log.i('api ::: apiLoginUser called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      VerifyAccessTokenResponse response;
      try {
        List<String> deviceInfo =  await getDeviceInfo();
        List<VerifyAccessTokenRequest> listVerifyAccessTokenRequest = new List<VerifyAccessTokenRequest>();
        VerifyAccessTokenRequest verifyAccessTokenRequest = VerifyAccessTokenRequest(
           accessToken:accessToken ,
          deviceId:deviceInfo[1],
          deviceToken: deviceInfo[1],
          deviceType: deviceInfo[0],);
        listVerifyAccessTokenRequest.add(verifyAccessTokenRequest);

        response = await _repository.apiVerifyAccessToken(listVerifyAccessTokenRequest);
        debugPrint("callApiVerifyAccessToken :==>");
        debugPrint("callApiVerifyAccessToken 1:==>"+response.toJson().toString());
        debugPrint("callApiVerifyAccessToken 2 :==>"+json.encode(response.toJson()));
        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
    }


  }

  void saveUserCredential(String token) async {
    saveUserToken(token);
    AppSharedPreference().saveStringValue(
        AppConstants.KEY_USER_EMAIL_ID, _textEditEmailId.text.trim());
    AppSharedPreference().saveStringValue(
        AppConstants.KEY_USER_PASSWORD, _textEditPassword.text.trim());
  }


  void saveUserToken(String token) async {
    bool isTokenSaved = await AppSharedPreference().saveUserToken(token);
    log.d('user token saved satus: $isTokenSaved');
    log.d('saved value from SP: ${await AppSharedPreference().getUserToken()}');
  }


  void _onFocusChangeEmail() {
    log.d('Email ' + _emailFocus.hasFocus.toString());
    if (_emailFocus.hasFocus) {
      isEmailFocusing = true;
    } else {
      isEmailFocusing = false;
    }
  }

  void _onFocusChangePassword() {
    log.d('Password ' + _passwordFocus.hasFocus.toString());
    if (_passwordFocus.hasFocus) {
      isPasswordFocusing = true;
    } else {
      isPasswordFocusing = false;
    }
  }

  static Future<List<String>> getDeviceInfo() async {
    String deviceName,deviceVersion,deviceType, identifier;
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        deviceType = "android";
        var build = await deviceInfoPlugin.androidInfo;
        //deviceName = build.model;
        //deviceVersion = build.version.toString();
        identifier = build.androidId ??"";  //UUID for Android
      } else if (Platform.isIOS) {
        deviceType = "ios";
        var data = await deviceInfoPlugin.iosInfo;
        // deviceName = data.name;
        // deviceVersion = data.systemVersion;
        identifier = data.identifierForVendor ?? "";  //UUID for iOS
      }
    } on PlatformException {
      print('Failed to get platform version');
      identifier = "";
    }

    return  [deviceType,identifier];
  }


  //api get language
  Future<GetPreferredLangResponse> callApiGetPreferredLang(String accessToken) async {
    log.i('api ::: callApiGetPreferredLang called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      GetPreferredLangResponse response;
      try {
        List<GetPreferredLangRequest> listGetPreferredLangRequest = new List<GetPreferredLangRequest>();
        GetPreferredLangRequest getPreferredLangRequest = GetPreferredLangRequest(accessToken:accessToken );
        listGetPreferredLangRequest.add(getPreferredLangRequest);
        response = await _repository.apiGetPreferredLang(listGetPreferredLangRequest);
        debugPrint("callApiGetPreferredLang :==>");
        debugPrint("callApiGetPreferredLang :==>"+json.encode(response.toJson()));



        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }

  }

}
