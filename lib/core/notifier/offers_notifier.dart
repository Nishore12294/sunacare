import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/CGOffersCitiesList.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgResp.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersDetailsResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersListResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgRequest.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgResponse.dart';
import 'package:suna_care/core/data/local/cg_giver_needed_full_list.dart';
import 'package:suna_care/core/data/local/cg_giver_expandedhdr_items.dart';
import 'package:suna_care/core/data/local/cg_giver_expandedhdr_items.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/callrequest/GetCallRequest.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/callrequest/GetCallResponse.dart';
import 'package:suna_care/utils/app_network_check.dart';

import '../base_notifier.dart';

class OffersNotifier extends BaseNotifier {
  final _repository = AuthRepository();
  String locationStr,germanSkillsStr,contractDaysStr;
  OffersListResponse _offersListResp;
  OffersDetailsResponse _offersDetailsResponse;
  GetCareGiverApprovedImgResp _getCareGiverApprovedImgResp;

  List<CGGiverNeededFullList> cGGiverNeededFullLists;
  List<CGGiverNeededItem> cgGiverNeededListItems1,cgGiverNeededListItems2,cgGiverNeededListItems3,cgGiverNeededListItems4;
  List<String> _cgOfferCitiesList = new List<String>();
  List<ExpandedHeaderItems> cgGiverNeededItems;
  List<ExpandedHeaderItems> expandHeaderItemsList;
  String _languageCode = "en";



  OffersNotifier(BuildContext context,{@required String isFromWhichScreen,@required String offerGuid})  {
    setUpInitialData(context,isFromWhichScreen:isFromWhichScreen,offerGuid: offerGuid);

  }
  void setUpInitialData(BuildContext context,{@required String isFromWhichScreen,@required String offerGuid})async{
    String accessToken = await AppSharedPreference().getUserToken();
    debugPrint("accessToken:==>"+accessToken);

    var prefs = await SharedPreferences.getInstance();
    if(prefs.getString('language_code') != null){
      debugPrint("language_code in offer notifier :==>"+prefs.getString('language_code')??"");
      languageCode = prefs.getString('language_code')??"";
    }else{
      debugPrint("language_code in offer notifier  is null :==>");
      languageCode = "en";
    }


    if(isFromWhichScreen == "offerListScreen"){
      //callApiOffersList(accessToken,"10","Mumbai","20");
      callApiOffersList(accessToken,"","","");
    }else if(isFromWhichScreen == "filterOptionScreen"){
      callApiGetCitiesList();
    }else{
      if(isFromWhichScreen == "offerDetailsScreen" && offerGuid != "")
        callApiOffersDetails(accessToken,offerGuid);
      // if(isFromWhichScreen == "cgProfileUpload")
      //   callApiGetProfile(accessToken);
    }


  }

// getter and setter
  String get languageCode => _languageCode;

  set languageCode(String value) {
    _languageCode = value;
    notifyListeners();
  }
  //Cities list
  List<String> get cgOfferCitiesList => _cgOfferCitiesList;
  set cgOfferCitiesList(List<String> value) {
    _cgOfferCitiesList = value;
    notifyListeners();
  }

//Offers list
  OffersListResponse get offersListResp => _offersListResp;

  set offersListResp(OffersListResponse value) {

    _offersListResp = value;
    notifyListeners();
  }

 //offersDetailsResp
  OffersDetailsResponse get offersDetailsResp => _offersDetailsResponse;

  set offersDetailsResp(OffersDetailsResponse value) {

    _offersDetailsResponse = value;
    notifyListeners();
  }

 //getCareGiverApprovedImgResp
  GetCareGiverApprovedImgResp get getCareGiverApprovedImgResp => _getCareGiverApprovedImgResp;

  set getCareGiverApprovedImgResp(GetCareGiverApprovedImgResp value) {

    _getCareGiverApprovedImgResp = value;
    notifyListeners();
  }

  //Api Offers List
  //German language.
  //   'Good', //       'Middle',//       'Low', //       'No Preference'

  // DurationCode or contract days
  // 'Unlimited', //    '4-6 weeks',    // 'Longer than 6 weeks',
  Future<OffersListResponse> callApiOffersList(String accessToken,String durationCode,String location,String germanCode) async {
    log.i('api ::: callApiOffersList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      OffersListResponse response;
      try {
        List<OffersRequest> offersRequestList = new List<OffersRequest>();
        OffersRequest offersRequest = OffersRequest(
            accessToken:accessToken,
            durationCode: durationCode,
            cityOfCp: location,
            germanLevelCode: germanCode

          // durationCode: "10",
            // cityOfCp: "Mumbai",
            // germanLevelCode: "20"


          //   durationCode  : (contractDaysStr != null&&contractDaysStr == "Unlimited")? "10"
          //                 : (contractDaysStr != null && contractDaysStr == "4-6 weeks")?"20"
          //                 :"30"
          // ,
          //   cityOfCp  : (locationStr != null)?locationStr:"",
          //   germanLevelCode :  (germanSkillsStr != null && germanSkillsStr == "Good")? "10"
          //                 : (germanSkillsStr != null && germanSkillsStr == "Middle")?"20"
          //                 : (germanSkillsStr != null && germanSkillsStr == "Low")?"30"
          //                 :"40",
        );
        offersRequestList.add(offersRequest);
        response = await _repository.apiOffersList(offersRequestList);
        offersListResp = response;
        debugPrint("callApiOffersList :==>");
        debugPrint("callApiOffersList :==>"+json.encode(response.toJson()));
        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }

  // //api Offers Details
  Future<OffersDetailsResponse> callApiOffersDetails(String accessToken,String offerGuid) async {
    log.i('api ::: callApiOffersList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      OffersDetailsResponse response;
      try {
        List<OffersDetailsRequest> offerDetailsRequest = new List<OffersDetailsRequest>();
        OffersDetailsRequest offersDetailsRequest = OffersDetailsRequest(accessToken:accessToken,offerGuid:offerGuid );
        offerDetailsRequest.add(offersDetailsRequest);
        response = await _repository.apiOffersDetails(offerDetailsRequest);




        offersDetailsResp = response;


        //Expanded view
        cGGiverNeededFullLists =  List<CGGiverNeededFullList>();


        //First expanded item
        expandHeaderItemsList = new List<ExpandedHeaderItems>();
        ExpandedHeaderItems expandHeaderReqItems = new ExpandedHeaderItems();
        expandHeaderReqItems.orgId = "1";
        expandHeaderReqItems.resourceMatchCount= "";
        expandHeaderReqItems.title = "CG Requirements";

        ExpandedHeaderItems expandHeaderAccItems = new ExpandedHeaderItems();
        expandHeaderAccItems.orgId = "1";
        expandHeaderAccItems.resourceMatchCount= "";
        expandHeaderAccItems.title = "Accommodations";

        ExpandedHeaderItems expandHeaderEqupItems = new ExpandedHeaderItems();
        expandHeaderEqupItems.orgId = "1";
        expandHeaderEqupItems.resourceMatchCount= "";
        expandHeaderEqupItems.title = "Equipments";

        expandHeaderItemsList.add(expandHeaderReqItems);
        expandHeaderItemsList.add(expandHeaderAccItems);
        expandHeaderItemsList.add(expandHeaderEqupItems);






        if(offersDetailsResp != null
            && offersDetailsResp.webMethodOutputRows != null
            &&offersDetailsResp.webMethodOutputRows.webMethodOutputRows != null) {

          debugPrint("callApiOffersDetails respon innner :==>"+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Gen_NettoPayment']);
          debugPrint("callApiOffersDetails respon innner :==>"+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_UrineIncontinence']);

          //////CG Requirements Start ////
          cgGiverNeededListItems1 = new List<CGGiverNeededItem>();

          CGGiverNeededItem cgGiverNeededCGReqItem1 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededCGReqItem2 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededCGReqItem3 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededCGReqItem4 = new CGGiverNeededItem();
          cgGiverNeededCGReqItem1.item = "CGGender : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CGReq_CGGender'];
          cgGiverNeededListItems1.add(cgGiverNeededCGReqItem1);
          cgGiverNeededCGReqItem2.item = "Required German skills : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CGReq_RequiredGermanSkills'];
          cgGiverNeededListItems1.add(cgGiverNeededCGReqItem2);
          cgGiverNeededCGReqItem3.item = "Driver license requirement : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CGReq_DriversLicenceRequirements'];
          cgGiverNeededListItems1.add(cgGiverNeededCGReqItem3);
         // bool smokingValue = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CGReq_SmokingPremittion'] == "True";
          //cgGiverNeededList4.item = "Smoking permission : "+(smokingValue?"Yes":"No");
          cgGiverNeededCGReqItem4.item = "Smoking permission : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CGReq_SmokingPremittion'];
          cgGiverNeededListItems1.add(cgGiverNeededCGReqItem4);

          CGGiverNeededFullList cgGiverNeededFullList1 =  CGGiverNeededFullList();
          cgGiverNeededFullList1.cGGiverNeededItem = cgGiverNeededListItems1;

          //////CG Requirements End ////

          // CG Accommodation Start ////
          cgGiverNeededListItems2 = new List<CGGiverNeededItem>();

          CGGiverNeededItem cgGiverNeededAccomList1 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededAccomList2 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededAccomList3 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededAccomList4 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededAccomList5 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededAccomList6 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededAccomList7 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededAccomList8 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededAccomList9 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededAccomList10 = new CGGiverNeededItem();
          cgGiverNeededAccomList1.item = "HouseOrApartment : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_HouseOrApartment'];
          cgGiverNeededListItems2.add(cgGiverNeededAccomList1);

          bool ownBathRoomValue = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_OwnBathroomForCG'] == "True";
          cgGiverNeededAccomList2.item = "Own Bathroom : "+(ownBathRoomValue?"Yes":"No");
          cgGiverNeededListItems2.add(cgGiverNeededAccomList2);

          bool ownKitchenValue = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_OwnKitchenForCG'] == "True";
          cgGiverNeededAccomList3.item = "Own Kitchen : "+(ownKitchenValue?"Yes":"No");
          cgGiverNeededListItems2.add(cgGiverNeededAccomList3);

         bool ownTVValue = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_OwnTv'] == "True";
          cgGiverNeededAccomList4.item = "Own TV : "+(ownTVValue?"Yes":"No");
          cgGiverNeededListItems2.add(cgGiverNeededAccomList4);

          bool animalInHouseHoldValue = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_AnimalsInTheHousehold'] == "True";
          cgGiverNeededAccomList5.item = "Animal in house hold : "+(animalInHouseHoldValue?"Yes":"No");
          cgGiverNeededListItems2.add(cgGiverNeededAccomList5);

          cgGiverNeededAccomList6.item = "Kind of animal : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_KindOfAnimal'];
          cgGiverNeededListItems2.add(cgGiverNeededAccomList6);



          cgGiverNeededAccomList7.item = "Further people living : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_FurtherPeopleLivingInTheSameHousehold'];
          cgGiverNeededListItems2.add(cgGiverNeededAccomList7);

          cgGiverNeededAccomList8.item = "Family living nearby : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_FamilyLivingNearby'];
          cgGiverNeededListItems2.add(cgGiverNeededAccomList8);

          cgGiverNeededAccomList9.item = "Distance from care person : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_DistanceFromCareperson'];
          cgGiverNeededListItems2.add(cgGiverNeededAccomList9);

          cgGiverNeededAccomList10.item = "State of relation : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_StateOfRelation'];
          cgGiverNeededListItems2.add(cgGiverNeededAccomList10);

          CGGiverNeededFullList cgGiverNeededFullList2 =  CGGiverNeededFullList();
          cgGiverNeededFullList2.cGGiverNeededItem = cgGiverNeededListItems2;

          //////CG Accommodation End ////



          // CG Equipment Start ////
          cgGiverNeededListItems3 = new List<CGGiverNeededItem>();

          CGGiverNeededItem cgGiverNeededEuqList1 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededEuqList2 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededEuqList3 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededEuqList4 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededEuqList5 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededEuqList6 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededEuqList7 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededEuqList8 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededEuqList9 = new CGGiverNeededItem();
          CGGiverNeededItem cgGiverNeededEuqList10 = new CGGiverNeededItem();
          bool hospBed = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Equip_HospitalBed'] == "True";

          cgGiverNeededEuqList1.item =  "Hospital Bed : "+(hospBed?"Yes":"No");
          cgGiverNeededListItems3.add(cgGiverNeededEuqList1);

          bool showerChair = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Equip_ShowerChair'] == "True";
          cgGiverNeededEuqList2.item = "Shower chair: "+(showerChair?"Yes":"No");
          cgGiverNeededListItems3.add(cgGiverNeededEuqList2);

          bool rollator = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Equip_Rollator'] == "True";
          cgGiverNeededEuqList3.item = "Rollator : "+(rollator?"Yes":"No");
          cgGiverNeededListItems3.add(cgGiverNeededEuqList3);

          bool bedLift = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Equip_Bedlift'] == "True";
          cgGiverNeededEuqList4.item = "Bed list : "+(bedLift?"Yes":"No");
          cgGiverNeededListItems3.add(cgGiverNeededEuqList4);

          bool stairLift = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Equip_StairsLift'] == "True";
          cgGiverNeededEuqList5.item = "Stairs lift : "+(stairLift?"Yes":"No");
          cgGiverNeededListItems3.add(cgGiverNeededEuqList5);


          bool toiletBoosterSeat = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Equip_ToiletBoosterSeat'] == "True";
          cgGiverNeededEuqList6.item = "Toliet booster seat : "+(toiletBoosterSeat?"Yes":"No");
          cgGiverNeededListItems3.add(cgGiverNeededEuqList6);




          bool bathTubLift = offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Equip_BathtubLift'] == "True";
          cgGiverNeededEuqList7.item = "Bath tub lift : "+(bathTubLift?"Yes":"No");
          cgGiverNeededListItems3.add(cgGiverNeededEuqList7);


          cgGiverNeededEuqList8.item =   cgGiverNeededAccomList10.item = "Nursing service : "+offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Equip_NursingServiceAssistance'];
          cgGiverNeededListItems3.add(cgGiverNeededEuqList8);

          CGGiverNeededFullList cgGiverNeededFullList3 =  CGGiverNeededFullList();
          cgGiverNeededFullList3.cGGiverNeededItem = cgGiverNeededListItems3;

          //////CG Equipment End ////

          //Finally all the datas add in full list
          cGGiverNeededFullLists.add( cgGiverNeededFullList1);
          cGGiverNeededFullLists.add( cgGiverNeededFullList2);
          cGGiverNeededFullLists.add( cgGiverNeededFullList3);


        }
        debugPrint("callApiOffersDetails :==>");
        debugPrint("callApiOffersDetails :==>"+json.encode(response.toJson()));
        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }



  // //api Get profile
  Future<GetCareGiverApprovedImgResp> callApiGetProfileImage(String accessToken) async {
    log.i('api ::: callApiOffersList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      GetCareGiverApprovedImgResp response;
      try {
        List<GetCareGiverApprovedImgRequest> listCareGiverApprovedImgRequest = new List<GetCareGiverApprovedImgRequest>();
        GetCareGiverApprovedImgRequest listCareGiverApprovedImageReq = GetCareGiverApprovedImgRequest(accessToken:accessToken );
        listCareGiverApprovedImgRequest.add(listCareGiverApprovedImageReq);
        response = await _repository.apiGetCGProfileImage(listCareGiverApprovedImgRequest);


        debugPrint("callApiGetProfile :==>");
        debugPrint("callApiGetProfile :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }

  //api Set/Upload Profile image
  Future<SetCareGiverProfileImgResponse> callApiSetOrUploadProfileImage(String accessToken,String base64Str) async {
    log.i('api ::: callApiOffersList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      SetCareGiverProfileImgResponse response;
      try {
        List<SetCareGiverProfileImgRequest> listSetCareGiverProfileImgRequest = new List<SetCareGiverProfileImgRequest>();
        //SetCareGiverProfileImgRequest listCareGiverApprovedImageReq = SetCareGiverProfileImgRequest(accessToken:accessToken,imageByteArray: base64Str );
       // listSetCareGiverProfileImgRequest.add(listCareGiverApprovedImageReq);
        //response = await _repository.apiSetOrUploadCGProfileImage(listSetCareGiverProfileImgRequest);


        debugPrint("callApiGetProfile :==>");
        debugPrint("callApiGetProfile :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }
  }

  //api Get cities List
  Future<CgOffersCitiesList> callApiGetCitiesList() async {
    log.i('api ::: callApiGetCitiesList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      CgOffersCitiesList response;
      try {
        response = await _repository.apiGetCGOffersCities();

        if(response != null && response.webMethodOutputRows != null
          && response.webMethodOutputRows.webMethodOutputRows != null && response.webMethodOutputRows.webMethodOutputRows.length > 0){

          for(int arrIndex =0 ;arrIndex <response.webMethodOutputRows.webMethodOutputRows.length;arrIndex++ ){
            debugPrint("callApiGetCitiesList value :==>"+response.webMethodOutputRows.webMethodOutputRows[arrIndex].cityOfCp);
            cgOfferCitiesList.add(response.webMethodOutputRows.webMethodOutputRows[arrIndex].cityOfCp);

          }

        }

        debugPrint("callApiGetCitiesList :==>");
        debugPrint("callApiGetCitiesList :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }
  }


  //api Get call request
  Future<GetCallResponse> callApiCallRequest(String timeZone) async {
    log.i('api ::: callApiCallRequest called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      GetCallResponse response;
      try {
        List<GetCallRequest> listCallRequest = new List<GetCallRequest>();
        GetCallRequest callRequest = GetCallRequest(currentTimeZone:timeZone );
        listCallRequest.add(callRequest);
        response = await _repository.apiCallRequest(listCallRequest);
        debugPrint("callApiCallRequest :==>");
        debugPrint("callApiCallRequest :==>"+json.encode(response.toJson()));

        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
      return null;
    }


  }

}