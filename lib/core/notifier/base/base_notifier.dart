import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';

class BaseNotifier extends ChangeNotifier {
  final log = getLogger('BaseNotifier');
  bool _isLoading = false;
  var context;

  //getter
  get isLoading => _isLoading;

  //setter
  set isLoading(value) {
    _isLoading = value;
    notifyListeners();
  }

  void showSnackBarMessageWithContext(String message) {
    final snackBar = SnackBar(content: Text(message), duration: Duration(seconds: AppConstants.TIME_SHOW_SNACK_BAR),);
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    if (context != null) {
      Scaffold.of(context).showSnackBar(snackBar);
    } else {
      log.e('Context is Null, coudn\'t show snackbar toast');
    }
  }

  void showSnackBarMessage({ @required BuildContext ctx, @required String message}) {
    final snackBar = SnackBar(content: Text(message), duration: Duration(seconds: AppConstants.TIME_SHOW_SNACK_BAR),);
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    if (ctx != null) {
      Scaffold.of(ctx).showSnackBar(snackBar);
    } else {
      log.e('Context is Null, coudn\'t show snackbar toast');
    }
  }

  void showCustomSnackBarMessageWithContext(String message,{ Color bgColor, Color txtColor}) {
    final snackBar = SnackBar(
      content: Text(message, style: getStyleButtonText(context).copyWith(color: txtColor),),
      duration: Duration(seconds: AppConstants.TIME_SHOW_SNACK_BAR),
      backgroundColor: bgColor,
    );
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    if (context != null) {
      Scaffold.of(context).showSnackBar(snackBar);
    } else {
      log.e('Context is Null, coudn\'t show snackbar toast');
    }
  }

  Locale _appLocale = Locale('en');

  Locale get appLocal => _appLocale ?? Locale("en");
  fetchLocale() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString('language_code') == null) {
      _appLocale = Locale('en');
      return Null;
    }
    _appLocale = Locale(prefs.getString('language_code'));
    return Null;
  }


  void changeLanguage(Locale type) async {
    var prefs = await SharedPreferences.getInstance();
    if (_appLocale == type) {
      return;
    }
    if (type == Locale("ar")) {
      _appLocale = Locale("ar");
      await prefs.setString('language_code', 'ar');
      await prefs.setString('countryCode', '');
    } else {
      _appLocale = Locale("en");
      await prefs.setString('language_code', 'en');
      await prefs.setString('countryCode', 'US');
    }
    notifyListeners();
  }
}
