import 'package:flutter/material.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart';
import 'package:suna_care/core/data/remote/request_response/createpassword/request_create_password.dart';
import 'package:suna_care/core/data/remote/request_response/createpassword/response_create_password.dart';
import 'package:suna_care/ui/login_screen.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_network_check.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';

import 'base/base_notifier.dart';

class CreatePasswordNotifier extends BaseNotifier {
  final log = getLogger('CreatePasswordNotifier');
  bool _isPasswordVisible = false;

  TextEditingController _textEditNewPwd = TextEditingController();
  TextEditingController _textEditConfirmPwd = TextEditingController();
  FocusNode _newPwdFocus = FocusNode();
  FocusNode _confirmPwdFocus = FocusNode();
  bool _isNewPwdFocusing = false;
  bool _isConfirmPwdFocusing = false;
  String verficationCode = '';

  final _repository = AuthRepository();

  CreatePasswordNotifier() {
    setInitialData();
  }


  TextEditingController get textEditNewPwd => _textEditNewPwd;

  set textEditNewPwd(TextEditingController value) {
    _textEditNewPwd = value;
    notifyListeners();
  }
  TextEditingController get textEditConfirmPwd => _textEditConfirmPwd;

  set textEditConfirmPwd(TextEditingController value) {
    _textEditConfirmPwd = value;
    notifyListeners();
  }


  void setInitialData() async {
//    token = await AppSharedPreference().getUserToken();
//    oldPwdFocus.addListener(_onFocusChangeEmail);
//    newPwdFocus.addListener(_onFocusChangePassword);
  }

  bool get isPasswordVisible => _isPasswordVisible;

  set isPasswordVisible(bool value) {
    _isPasswordVisible = value;
    notifyListeners();
  }



  bool get isNewPwdFocusing => _isNewPwdFocusing;

  set isNewPwdFocusing(bool value) {
    _isNewPwdFocusing = value;
    notifyListeners();
  }



  FocusNode get newPwdFocus => _newPwdFocus;

  set newPwdFocus(FocusNode value) {
    _newPwdFocus = value;
    notifyListeners();
  }


  FocusNode get confirmPwdFocus => _confirmPwdFocus;

  set confirmPwdFocus(FocusNode value) {
    _confirmPwdFocus = value;
    notifyListeners();
  }

  bool get isConfirmPwdFocusing => _isConfirmPwdFocusing;

  set isConfirmPwdFocusing(bool value) {
    _isConfirmPwdFocusing = value;
    notifyListeners();
  }


  //api create password or forgot set password
  Future<CreatePasswordResponse> callApiForgotSetOrCreatePassword(String emailId,String verificationCode) async {
    log.i('api ::: apiCallApiChangePassword called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      CreatePasswordResponse response;
      try {

        List<CreatePasswordResquest> listCreatePasswordRequest = new List<CreatePasswordResquest>();
        CreatePasswordResquest createPasswordResquest = CreatePasswordResquest(
          forgotPasswordToken: verificationCode,
          emailAddress: emailId,
          newPassword: textEditConfirmPwd.text.toString(),
        );
        listCreatePasswordRequest.add(createPasswordResquest);

        response = await _repository.apiForgotSetPassword(listCreatePassword:listCreatePasswordRequest);


        super.isLoading = false;
      } catch (e) {
        log.e("Err : " + e.toString());
        super.isLoading = false;
      }
      return response;
    } else {
      return null;
    }
  }





}
