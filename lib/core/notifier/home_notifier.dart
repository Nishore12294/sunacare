import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:jiffy/jiffy.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/network/app_url.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/repository/service_repository.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersListResponse.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/OffersRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/assignment/AssignmentListRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/assignment/AssignmentListResponse.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/AddHolidayRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/AddHolidayResponse.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/DeleteHolidayRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/DeleteHolidayResponse.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/EditHolidayRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/EditHolidayResponse.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/HolidayListRequest.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/HolidayListResponse.dart';
import 'package:suna_care/core/notifier/base/base_notifier.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_network_check.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:intl/intl.dart';
import 'package:suna_care/ui/home/availability/calendarData.dart';


class HomeNotifier extends BaseNotifier {
  final log = getLogger('HomeNotifier');

  //double headerHighSize = 200.0;
  final LocalAuthentication auth = LocalAuthentication();
  double _homeTabTopTabSize = AppConstants.AVAILABILITY_TAB;
  var context;
  final scaffoldKey = GlobalKey<ScaffoldState>(debugLabel: "scaffoldHome");
  PageController _dashboardPageViewController = PageController();
  String _formTitle = AppConstants.HOME;
  int _currentPageNo = 0;
  String token = '';
  final _repository = AuthRepository();
  String _shortLetterOfName = '';
  int _carouselSideCurrentPosition = 0;
  AssignmentListResponse _assignmentListResponse;
  HolidayListResponse _holidayListResponse;
  AddHolidayResponse _addHolidayResponse;
  DeleteHolidayResponse _deleteHolidayResponse;
  EditHolidayResponse _editHolidayResponse;
  CalendarData calendarData = new CalendarData();
  List<CalendarData>  initialIntervals = [];
  //String _fromDate =  "DD/MM/YYYY",_toDate = "DD/MM/YYYY",_scheduleFormatedFromDate = "DD/MM/YYYY", _scheduleFormatedToDate =  "DD/MM/YYYY";
  DateTime _fromDate  =  null,_toDate =  null,_scheduleFormatedFromDate  =  DateTime.now() , _scheduleFormatedToDate =  DateTime.now();
  TextEditingController _textFromField = TextEditingController(text: "");
  TextEditingController get textFromField => _textFromField;
  DateTime _masterDate = DateTime.now();
  bool _isScheduleDetailsPage = false;
  String _languageCode = "en";



  HomeNotifier(BuildContext context,bool isScheduleDetailsPage)  {

    setUpPrefInitalData();

    if(!isScheduleDetailsPage) {
      getInitialData();
    }
  }

  void getInitialData()async{
    String accessToken = await AppSharedPreference().getUserToken();
    debugPrint("accessToken:==>"+accessToken);
    var todayDate = new DateTime.now();
    var monthFormatter = new DateFormat('MM');
    var yearFormatter = new DateFormat('yyyy');
    String month = monthFormatter.format(todayDate);
    String year = yearFormatter.format(todayDate);
    debugPrint("todayDate:==>"+todayDate.toString());
    debugPrint("Month:==>"+month.toString());
    debugPrint("year :==>"+year.toString());

    clearInitialIntervals();
    AssignmentListResponse assignmentListResponse = await callApiGetAssignmentList(accessToken, month, year);
    if(assignmentListResponse != null){

      _assignmentListResponse = assignmentListResponse;
    }
    HolidayListResponse holidayListResponse = await callApiGetHolidayList(accessToken, month, year);
    if(holidayListResponse != null){
      _holidayListResponse = holidayListResponse;
    }

  }


  // getter and setter
  String get languageCode => _languageCode;

  set languageCode(String value) {
    _languageCode = value;
    notifyListeners();
  }
  setUpPrefInitalData() async{
    var prefs = await SharedPreferences.getInstance();
    if(prefs.getString('language_code') != null){
      debugPrint("language_code :==>"+prefs.getString('language_code')??"");
      languageCode = prefs.getString('language_code')??"";
    }else{
      debugPrint("language_code is null :==>");
      languageCode = "en";
    }
    await Jiffy.locale(languageCode);

    // monthFormatter = new DateFormat('MM',languageCode);
    // yearFormatter = new DateFormat('yyyy',languageCode);
    // dateFormatter = new DateFormat('dd',languageCode);
  }

  DateTime get fromDate => _fromDate;

   set fromDate(value) {

    _fromDate = value;
    notifyListeners();
  }

  DateTime  get toDate => _toDate;

   set toDate(value) {
    _toDate = value;
    notifyListeners();
  }

  DateTime  get masterDate => _masterDate;

  set masterDate(value) {
    _masterDate = value;
    notifyListeners();
  }

  DateTime get scheduleFormatedFromDate => _scheduleFormatedFromDate;

  set scheduleFormatedFromDate(value) {
    _scheduleFormatedFromDate = value;
    notifyListeners();
  }

  DateTime  get scheduleFormatedToDate => _scheduleFormatedToDate;

  set scheduleFormatedToDate(value) {
    _scheduleFormatedToDate = value;
    notifyListeners();
  }

  set textFromField(TextEditingController value) {
    _textFromField = value;
    notifyListeners();
  }

  void clearInitialIntervals(){
    if(initialIntervals.length >0)
      initialIntervals.clear();
    //notifyListeners();
  }
  //slide : 4
  bool _autoVerifyTransactionPin = false;
  bool _isBioAuthAvailable = false;

  // getter and setter
  double get homeTabTopTabSize => _homeTabTopTabSize;

  set homeTabTopTabSize(double value) {
    _homeTabTopTabSize = value;
    notifyListeners();
  }




  //Assignment list
  AssignmentListResponse get assignmentListResp => _assignmentListResponse;

  set assignmentListResp(AssignmentListResponse value) {

    _assignmentListResponse = value;
    notifyListeners();
  }

  //Holiday List
  HolidayListResponse get holidayListResponse => _holidayListResponse;

  set holidayListResponse(HolidayListResponse value) {
    _holidayListResponse = value;
    notifyListeners();
  }
  //Add Holiday List
  AddHolidayResponse get addHolidayResponse => _addHolidayResponse;

  set addHolidayResponse(AddHolidayResponse value) {
    _addHolidayResponse = value;
    notifyListeners();
  }

  //Delete Holiday List
  DeleteHolidayResponse get deleteHolidayResponse => _deleteHolidayResponse;

  set deleteHolidayResponse(DeleteHolidayResponse value) {
    _deleteHolidayResponse = value;
    notifyListeners();
  }


//Edit Holiday List
  EditHolidayResponse get editHolidayResponse => _editHolidayResponse;

  set editHolidayResponse(EditHolidayResponse value) {
    _editHolidayResponse = value;
    notifyListeners();
  }

  bool get isScheduleDetailsPage => _isScheduleDetailsPage;

  set isScheduleDetailsPage(bool value) {
    _isScheduleDetailsPage = value;
    notifyListeners();
  }



  String get formTitle => _formTitle;

  set formTitle(String value) {
    _formTitle = value;
    notifyListeners();
  }

  PageController get dashboardPageViewController =>
      _dashboardPageViewController;

  set dashboardPageViewController(PageController value) {
    _dashboardPageViewController = value;
    notifyListeners();
  }

  int get currentPageNo => _currentPageNo;

  set currentPageNo(int value) {
    _currentPageNo = value;
    notifyListeners();
  }

  int get carouselSideCurrentPosition => _carouselSideCurrentPosition;

  set carouselSideCurrentPosition(int value) {
    _carouselSideCurrentPosition = value;
    notifyListeners();
  }




  String get shortLetterOfName => _shortLetterOfName;

  set shortLetterOfName(String value) {
    _shortLetterOfName = value;
    notifyListeners();
  }



  bool get autoVerifyTransactionPin => _autoVerifyTransactionPin;

  set autoVerifyTransactionPin(bool value) {
    _autoVerifyTransactionPin = value;
    notifyListeners();
  }

  bool get isBioAuthAvailable => _isBioAuthAvailable;


  reloadApis() async {}

  Future removeUserCred() async {
    AppSharedPreference().saveStringValue(AppConstants.KEY_USER_EMAIL_ID, '');
    AppSharedPreference().saveStringValue(AppConstants.KEY_USER_PASSWORD, '');
    AppSharedPreference().saveUserToken('');
  }


  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
      log.e(e.toString());
    }
  }






  void newDashboardDetailResponse() {
    //showMessage(dashboardResponse.message);
  }

  showMessage(String content) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(content),
    ));
  }

  @override
  void dispose() {
    super.dispose();
  }



//api GetAssignmentList
  Future<AssignmentListResponse> callApiGetAssignmentList(String accessToken,String month,String year) async {
    log.i('api ::: callApiGetAssignmentList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      AssignmentListResponse response;
      try {
        List<AssignmentListRequest> assignmentListRequest = new List<AssignmentListRequest>();
        AssignmentListRequest assignmentRequest = AssignmentListRequest(
          accessToken:accessToken ,
          month:month,
          year: year,);
        assignmentListRequest.add(assignmentRequest);

        response = await _repository.apiGetAssignmentList(assignmentListRequest);
        //response = null;
        debugPrint("callApiGetAssignmentList :==>");
        debugPrint("callApiGetAssignmentList 1:==>"+response.toJson().toString());
        debugPrint("callApiGetAssignmentList 2 :==>"+json.encode(response.toJson()));
        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
    }


  }

//api GetHolidayList
  Future<HolidayListResponse> callApiGetHolidayList(String accessToken,String month,String year) async {
    log.i('api ::: callApiGetHolidayList called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      HolidayListResponse response;
      try {
        List<HolidayListRequest> holidayListRequest = new List<HolidayListRequest>();
        HolidayListRequest assignmentRequest = HolidayListRequest(
          accessToken:accessToken ,
          month:month,
          year: year,);
        holidayListRequest.add(assignmentRequest);

        response = await _repository.apiGetHolidayList(holidayListRequest);
        debugPrint("callApiGetHolidayList :==>");
        debugPrint("callApiGetHolidayList 2 :==>"+json.encode(response.toJson()));
        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
    }


  }



  //api ADD Holiday
  Future<AddHolidayResponse> callApiAddHoliday(String accessToken,String startDate,String endDate) async {
    log.i('api ::: callApiAddHoliday called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      AddHolidayResponse response;
      try {
        List<AddHolidayRequest> addHolidayListRequest = new List<AddHolidayRequest>();
        AddHolidayRequest addHolidayRequest = AddHolidayRequest(
          accessToken:accessToken ,
          startDate:startDate,
          endDate: endDate,);
        addHolidayListRequest.add(addHolidayRequest);

        response = await _repository.apiAddHoliday(addHolidayListRequest);
        debugPrint("callApiAddHolidayList :==>");
        debugPrint("callApiAddHolidayList 2 :==>"+json.encode(response.toJson()));
        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
    }


  }

  //api DELETE Holiday
  Future<DeleteHolidayResponse> callApiDeleteHoliday(String accessToken,String cgScheduleGuid) async {
    log.i('api ::: callApiDeleteHoliday called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      DeleteHolidayResponse response;
      try {
        List<DeleteHolidayRequest> deleteHolidayRequestRequest = new List<DeleteHolidayRequest>();
        DeleteHolidayRequest deleteHolidayRequest = DeleteHolidayRequest(
          accessToken:accessToken,
          holidayCgScheduleGuid: cgScheduleGuid,
          );
        deleteHolidayRequestRequest.add(deleteHolidayRequest);

        response = await _repository.apiDeleteHoliday(deleteHolidayRequestRequest);
        debugPrint("callApiDeleteHoliday :==>");
        debugPrint("callApiDeleteHoliday  :==>"+json.encode(response.toJson()));
        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
    }


  }


  // //api EDIT Holiday
  Future<EditHolidayResponse> callApiEditHoliday(String accessToken,String cgScheduleGuid,String startDate,String endDate) async {
    log.i('api ::: callApiAddHoliday called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      EditHolidayResponse response;
      try {
        List<EditHolidayRequest> editHolidayRequestList = new List<EditHolidayRequest>();
        EditHolidayRequest editHolidayRequest = EditHolidayRequest(
          accessToken:accessToken ,
          holidayCgScheduleGuid: cgScheduleGuid,
          startDate:startDate,
          endDate: endDate,);
        editHolidayRequestList.add(editHolidayRequest);

        response = await _repository.apiEditHoliday(editHolidayRequestList);
        debugPrint("callApiEditHolidayList :==>");
        debugPrint("callApiEditHolidayList 2 :==>"+json.encode(response.toJson()));
        super.isLoading = false;
      } catch (e) {
        super.isLoading = false;
      }

      return response;
    } else {
    }


  }




 }
