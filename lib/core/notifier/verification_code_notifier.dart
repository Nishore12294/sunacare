import 'package:flutter/material.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart';
import 'package:suna_care/core/data/remote/request_response/forgotpassword/request_forgot_password.dart';
import 'package:suna_care/core/data/remote/request_response/forgotpassword/response_forgot_password.dart';
import 'package:suna_care/ui/login_screen.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_network_check.dart';
import 'package:suna_care/core/data/remote/repository/auth_repository.dart';

import 'base/base_notifier.dart';

class VerficationCodeNotifier extends BaseNotifier {
  final log = getLogger('VerficationCodeNotifier');
  bool _isPasswordVisible = false;

  TextEditingController _textEditingController = TextEditingController();
  FocusNode _emailFocus = FocusNode();
  bool _isOldPwdFocusing = false;
  bool _isNewPwdFocusing = false;
  bool _isConfirmPwdFocusing = false;
  String token = '',emailId = '';

  final _repository = AuthRepository();

  VerficationCodeNotifier() {
    setInitialData();
  }


  TextEditingController get textEditingController => _textEditingController;

  set textEditingController(TextEditingController value) {
    _textEditingController = value;
    notifyListeners();
  }


  FocusNode get emailFocus => _emailFocus;

  set emailFocus(FocusNode value) {
    _emailFocus = value;
    notifyListeners();
  }
  void setInitialData() async {
//    token = await AppSharedPreference().getUserToken();
//    oldPwdFocus.addListener(_onFocusChangeEmail);
//    newPwdFocus.addListener(_onFocusChangePassword);
  }

  bool get isPasswordVisible => _isPasswordVisible;

  set isPasswordVisible(bool value) {
    _isPasswordVisible = value;
    notifyListeners();
  }



  Future<ForgotPasswordResponse> callApiForgotPassword() async {
    log.i('api ::: apiCallApiChangePassword called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      ForgotPasswordResponse response;
      try {
        response = await _repository.apiForgotPassword(ForgotPasswordRequest(
            emailAddress: emailId.trim()
        ));


        super.isLoading = false;
      } catch (e) {
        log.e("Err : " + e.toString());
        super.isLoading = false;
      }
      return response;
    } else {
      return null;
    }
  }

  //api ChangePassword
  Future<ChangePasswordResponse> callApiChangePassword() async {
    log.i('api ::: apiCallApiChangePassword called');
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      super.isLoading = true;
      ChangePasswordResponse response;
      try {
//        response = await _repository.apiChangePassword(changePasswordRequest: ChangePasswordRequest(
//          oldPassword: textEditOldPwd.text,
//          password: textEditNewPwd.text,
//          passwordConfirmation: textEditConfirmPwd.text
//        ), token: token);


        super.isLoading = false;
      } catch (e) {
        log.e("Err : " + e.toString());
        super.isLoading = false;
      }
      return response;
    } else {
      //return ChangePasswordResponse(message: AppConstants.ERROR_INTERNET_CONNECTION);
    }
  }





}
