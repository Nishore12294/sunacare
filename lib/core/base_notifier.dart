import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';

class BaseNotifier extends ChangeNotifier {
  final log = getLogger('BaseNotifier');
  bool _isLoading = false;
  var context;

  //getter
  get isLoading => _isLoading;

  //setter
  set isLoading(value) {
    _isLoading = value;
    notifyListeners();
  }

  void showSnackBarMessageWithContext(String message) {
    final snackBar = SnackBar(content: Text(message), duration: Duration(seconds: AppConstants.TIME_SHOW_SNACK_BAR),);
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    if (context != null) {
      Scaffold.of(context).showSnackBar(snackBar);
    } else {
      log.e('Context is Null, coudn\'t show snackbar toast');
    }
  }

  void showSnackBarMessage(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message), duration: Duration(seconds: AppConstants.TIME_SHOW_SNACK_BAR),);
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    if (context != null) {
      Scaffold.of(context).showSnackBar(snackBar);
    } else {
      log.e('Context is Null, coudn\'t show snackbar toast');
    }
  }

  void showCustomSnackBarMessageWithContext(String message,{ Color bgColor, Color txtColor}) {
    final snackBar = SnackBar(
      content: Text(message, style: getStyleButtonText(context).copyWith(color: txtColor),),
      duration: Duration(seconds: AppConstants.TIME_SHOW_SNACK_BAR),
      backgroundColor: bgColor,
    );
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    if (context != null) {
      Scaffold.of(context).showSnackBar(snackBar);
    } else {
      log.e('Context is Null, coudn\'t show snackbar toast');
    }
  }

}
