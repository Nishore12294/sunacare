import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/register/signup/response_signup.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/core/notifier/signup_notifier.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_custom_icon.dart';
import 'package:suna_care/utils/app_date_picker.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:provider/provider.dart';

class FilterOptionsScreen extends StatefulWidget {
  BuildContext context;
  static const routeName = '/filter_options_screen';
  FilterOptionsScreen({this.context});

  @override
  FilterOptionsScreenState createState() =>
      FilterOptionsScreenState();
}

class FilterOptionsScreenState
    extends State<FilterOptionsScreen> {
  var log = getLogger("FilterOptionsScreenState");
  final _keyValidationForm = GlobalKey<FormState>();
  //String genderDropDownValue;

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider<OffersNotifier>(
      builder: (context) => OffersNotifier(context,isFromWhichScreen:"filterOptionScreen"),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: colorLitePinkFilterBG,
//          image: DecorationImage(
//              image: AssetImage(
//                AppImages.IMAGE_LOGIN_BG,
//              ),
//              fit: BoxFit.fill),
        ),
        child: WillPopScope(
            onWillPop: () {
              backButtonPressed();
            },
            child: Scaffold(
          backgroundColor: colorLitePinkFilterBG,
         // key: _scaffoldKey,
          body: Consumer<OffersNotifier>(
            builder: (context, offerNotifier, _) => Stack(
              fit: StackFit.expand,
              children: <Widget>[
                ModalProgressHUD(
                    color: Colors.transparent,
                    inAsyncCall: offerNotifier.isLoading,
                    child: GestureDetector(
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());

                        },
                        child: Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[

                              SizedBox(
                                height: AppConstants.SIDE_MARGIN * 2,
                              ),
                              topHeader(offerNotifier),
                              Expanded(
                                flex: 2 ,
                                child:  _buildWidgetFiltersCard(offerNotifier,context),

                              ),



                            ],
                          ),
                        )

                    )),
              ],
            ),
          ),
        )),
      ),
    );

  }

  /////////////////////
  /*  build helpers */
  ////////////////////

  Widget topHeader(OffersNotifier offersNotifier) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      //mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Expanded(
          flex: 1,

            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    backButtonPressed();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    child: Image.asset(
                      AppImages.IMAGE_BACK,
                      height: 25,
                    ),
                  ),
                ),


              ],
            ),
        ),
        Expanded(
          flex: 2,
          child: Text(
            //AppConstants.FILTERS_OPTIONS,
            AppLocalizations.of(context).translate('filtersOptions'),
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            alignment: Alignment.center,
            child: Image.asset(
              AppImages.IMAGE_APP_LOGO,
              height: 20,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildWidgetFiltersCard(
      OffersNotifier offersNotifier, BuildContext context) {
    List<String> locationsList = [
      'Berlin',
      'Hamburg',
    ];
    List<String> germanSkillsList = [
      'Good',
      'Middle',
      'Low',
      'No Preference'
    ];

    // 0 - 2 Weeks, 2 - 4 Weeks4 - 6 Weeks, 6 - 8 Weeks, 8 - 10 Weeks
 // List<String> contractDayList = [
 //      '0 - 2 Weeks',
 //      '2 - 4 Weeks',
 //      '4 - 6 Weeks',
 //      '8 - 10 Weeks',
 //    ];
 //Unlimited
    // 4-6 weeks
    // Longer than 6 weeks
 List<String> contractDayList = [
      'Unlimited',
      '4-6 weeks',
      'Longer than 6 weeks',
    ];

    return Column(
      children: <Widget>[
        Container(
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 16.0, left: 30.0, right: 30.0, bottom: 8.0),
              child: Theme(
                data: ThemeData(
                    hintColor: colorBlack,
                    primaryColor: colorAccentMild,
                    primaryColorDark: colorLiteOrange),
                child:GestureDetector(
                  onTap: (){
                    FocusScope.of(context).requestFocus(FocusNode());

                  },
                  child: Container(
                    alignment: Alignment.center,
                    child: SingleChildScrollView(

                      physics: BouncingScrollPhysics(),
                      child: Form(

                        key: _keyValidationForm,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN,
                            ),

                            ///Locations////
                            Text(
                              //AppConstants.LOCATIONS,
                              AppLocalizations.of(context).translate('locations'),
                              textAlign: TextAlign.left,
                              style: getStyleBody1(context).copyWith(
                                  color: colorPencilBlack,
                                  fontWeight: AppFont.fontWeightRegular,
                                  fontSize: 17),
                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN/1.5,
                            ),
                            GestureDetector(
                              onTap: (){
                                FocusScope.of(context).requestFocus(FocusNode());

                              },
                              child:  SizedBox(
                                height: AppConstants.SIDE_MARGIN*2.5,
                                child: DropdownButtonFormField(
                                 icon: Image.asset(
                                        AppImages.IMAGE_DROPDOWN_ARROW,
                                    width: 15,
                                    height: 15,
                                   ),
                                  style: getFormStyleText(context),

                                  decoration:
                                  _buildTextDecoration(
                                     // AppConstants.SELECT_YOUR_LOCATIONS,
                                       AppLocalizations.of(context).translate('selectYourLocation'),
                                  ).copyWith(
                                    isDense: true,
                                    hintStyle: TextStyle(color: colorDarkGrey),
                                    fillColor: colorWhite,
                                    focusedBorder: new OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: colorWhite,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
                                      ),
                                    ),
                                    enabledBorder: new OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: colorWhite,
                                        //color: Colors.transparent,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
                                      ),
                                    ),
                                  ),
                                  value: offersNotifier.locationStr,
                                  onChanged: (String Value) {
                                    FocusScope.of(context).requestFocus(new FocusNode());
                                    setState(() {
                                      offersNotifier.locationStr = Value;

                                    });
                                  },
                                  // items: locationsList
                                  //     .map((locationItem) => DropdownMenuItem(
                                  //     value: locationItem,
                                  //     child: Text("$locationItem")
                                  //     // child:  Row(
                                  //     //   children: <Widget>[
                                  //     //     SingleChildScrollView(
                                  //     //       child:  Column(
                                  //     //         children: <Widget>[
                                  //     //           SizedBox(height: 30,
                                  //     //           child: Padding(
                                  //     //             padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                                  //     //             child:  Text(
                                  //     //
                                  //     //
                                  //     //               "$locationItem",
                                  //     //               style:  TextStyle(color: Colors.black),
                                  //     //             ),
                                  //     //           ),
                                  //     //
                                  //     //
                                  //     //           ),
                                  //     //
                                  //     //         ]),
                                  //     //     )
                                  //     //   ],),
                                  // )
                                  // ).toList(),

                                  items: offersNotifier.cgOfferCitiesList
                                      .map((locationItem) => DropdownMenuItem(
                                      value: locationItem,
                                      child: Text("$locationItem")
                                  )
                                  ).toList(),
                                ),
                              ),

                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN,
                            ),
                            Divider(
                              thickness: 0.25,
                              color:  Colors.black,
                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN,
                            ),


                            ///German skills////
                            Text(
                              //AppConstants.GERMAN_SKILLS,
                              AppLocalizations.of(context).translate('germanSkills'),
                              textAlign: TextAlign.left,
                              style: getStyleBody1(context).copyWith(
                                  color: colorPencilBlack,
                                  fontWeight: AppFont.fontWeightRegular,
                                  fontSize: 17),
                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN/1.5,
                            ),
                            GestureDetector(
                              onTap: (){
                                FocusScope.of(context).requestFocus(FocusNode());

                              },
                              child:  SizedBox(
                                height: AppConstants.SIDE_MARGIN*2.5,
                                child: DropdownButtonFormField(
                                  icon: Image.asset(
                                    AppImages.IMAGE_DROPDOWN_ARROW,
                                    width: 15,
                                    height: 15,
                                  ),
                                 style: getFormStyleText(context),
                                  decoration:
                                  _buildTextDecoration(
                                    //AppConstants.SELECT_GERMAN_SKILLS,
                                    AppLocalizations.of(context).translate('selectYourGermanSkills'),
                                  ).copyWith(
                                    isDense: true,
                                    hintStyle: TextStyle(color: colorDarkGrey),
                                    fillColor: colorWhite,
                                    focusedBorder: new OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: colorWhite,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
                                      ),
                                    ),
                                    enabledBorder: new OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: colorWhite,
                                        //color: Colors.transparent,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
                                      ),
                                    ),
                                  ),
                                  value: offersNotifier.germanSkillsStr,
                                  onChanged: (String Value) {
                                    FocusScope.of(context).requestFocus(new FocusNode());
                                    setState(() {
                                      offersNotifier.germanSkillsStr = Value;

                                    });
                                  },
                                  items: germanSkillsList
                                      .map((Item) => DropdownMenuItem(
                                      value: Item,
                                      child: Text("$Item")
                                    // child:  Row(
                                    //   children: <Widget>[
                                    //     SingleChildScrollView(
                                    //       child:  Column(
                                    //         children: <Widget>[
                                    //           SizedBox(height: 30,
                                    //           child: Padding(
                                    //             padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                                    //             child:  Text(
                                    //
                                    //
                                    //               "$Item",
                                    //               style:  TextStyle(color: Colors.black),
                                    //             ),
                                    //           ),
                                    //
                                    //
                                    //           ),
                                    //
                                    //         ]),
                                    //     )
                                    //   ],),
                                  )
                                  ).toList(),
                                ),
                              ),

                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN,
                            ),
                            Divider(
                              thickness: 0.25,
                              color:  Colors.black,
                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN,
                            ),


                            ///Contract days////
                            Text(
                              //AppConstants.CONTRACT_DAYS,
                              AppLocalizations.of(context).translate('contractDays'),
                              textAlign: TextAlign.left,
                              style: getStyleBody1(context).copyWith(
                                  color: colorPencilBlack,
                                  fontWeight: AppFont.fontWeightRegular,
                                  fontSize: 17),
                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN/1.5,
                            ),
                            GestureDetector(
                              onTap: (){
                                FocusScope.of(context).requestFocus(FocusNode());

                              },
                              child:  SizedBox(
                                height: AppConstants.SIDE_MARGIN*2.5,
                                child: DropdownButtonFormField(
                                  icon: Image.asset(
                                    AppImages.IMAGE_DROPDOWN_ARROW,
                                    width: 15,
                                    height: 15,
                                  ),
                                  style: getFormStyleText(context),
                                  decoration:
                                  _buildTextDecoration(
                                    //  AppConstants.SELECT_YOUR_SLAB,
                                    AppLocalizations.of(context).translate('selectYourSlab'),
                                  ) .copyWith(
                                    isDense: true,
                                    hintStyle: TextStyle(color: colorDarkGrey),
                                    fillColor: colorWhite,
                                    focusedBorder: new OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: colorWhite,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
                                      ),
                                    ),
                                    enabledBorder: new OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: colorWhite,
                                        //color: Colors.transparent,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
                                      ),
                                    ),
                                  ),
                                  value: offersNotifier.contractDaysStr,
                                  onChanged: (String Value) {
                                    FocusScope.of(context).requestFocus(new FocusNode());
                                    setState(() {
                                      offersNotifier.contractDaysStr = Value;

                                    });
                                  },
                                  items: contractDayList
                                      .map((Item) => DropdownMenuItem(
                                      value: Item,
                                      child: Text("$Item")
                                    // child:  Row(
                                    //   children: <Widget>[
                                    //     SingleChildScrollView(
                                    //       child:  Column(
                                    //         children: <Widget>[
                                    //           SizedBox(height: 30,
                                    //           child: Padding(
                                    //             padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                                    //             child:  Text(
                                    //
                                    //
                                    //               "$Item",
                                    //               style:  TextStyle(color: Colors.black),
                                    //             ),
                                    //           ),
                                    //
                                    //
                                    //           ),
                                    //
                                    //         ]),
                                    //     )
                                    //   ],),
                                  )
                                  ).toList(),
                                ),
                              ),

                            ),


                            SizedBox(
                              height: AppConstants.SIDE_MARGIN*3,
                            ),

                            //APPLY BUTTON
                            ButtonTheme(
                              minWidth: double.infinity,
                              child: RaisedButton(
                                highlightElevation: 5.0,
                                onPressed: () {


                                  // if ( offersNotifier.locationStr == null ||  offersNotifier.locationStr == "") {
                                  //   showDialog(
                                  //     context: context,
                                  //     builder: (_) => AlertOverlay(
                                  //         AppConstants.OPEN_OFFERS_ALERT,
                                  //         AppConstants.PLEASE_SELECT_LOCATION,
                                  //         AppConstants.OKAY),
                                  //   );
                                  // }else if (offersNotifier.germanSkillsStr == null ||  offersNotifier.germanSkillsStr == "") {
                                  //   showDialog(
                                  //     context: context,
                                  //     builder: (_) => AlertOverlay(
                                  //         AppConstants.OPEN_OFFERS_ALERT,
                                  //         AppConstants.PLEASE_SELECT_GERMAN_SKILLS,
                                  //         AppConstants.OKAY),
                                  //   );
                                  // }else if (offersNotifier.contractDaysStr == null ||  offersNotifier.contractDaysStr == "") {
                                  //   showDialog(
                                  //     context: context,
                                  //     builder: (_) => AlertOverlay(
                                  //         AppConstants.OPEN_OFFERS_ALERT,
                                  //         AppConstants.PLEASE_SELECT_CONTRACT_DAYS,
                                  //         AppConstants.OKAY),
                                  //   );
                                  // }
                                  // else {
                                    //Navigator.pop(context,true);
                                    String germanSkill = offersNotifier.germanSkillsStr??"";
                                    String location = offersNotifier.locationStr??"";
                                    String contractDays = offersNotifier.contractDaysStr??"";
                                    String selectedFilterValues = '{"DurationCode":"$contractDays" ,"CityOfCP":"$location","GermanLevelCode":"$germanSkill"}';
                                    Navigator.pop(context,selectedFilterValues);
                                  //}
                                },
                                color: colorLiteOrange,
                                textColor: colorWhite,
                                elevation: 1,
                                padding: EdgeInsets.only(
                                    top: AppConstants.SIDE_MARGIN / 1.5,
                                    bottom: AppConstants.SIDE_MARGIN / 1.5),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(AppConstants.SIDE_MARGIN*1.5))),
                                child: Text(
                                  //AppConstants.APPLY,
                                  AppLocalizations.of(context).translate('apply'),
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorWhite,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightBold),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN,
                            ),

                          ],
                        ),
                      ),
                    ),
                  ),
                ),

              ),
            )),
      ],
    );
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  InputDecoration _buildTextDecoration(String value) {

    return InputDecoration(
        isDense: true,
        hintText: value,
        // labelText: value,
        //alignLabelWithHint: true,
        filled: true,
        //hintStyle: TextStyle(color: colorDarkGrey,),
        hintStyle:  getFormStyleText(context).copyWith(
            color: colorPencilBlack,
            fontWeight: AppFont.fontWeightSemiBold,
            fontSize: 16),
        contentPadding: EdgeInsets.all(16.7));
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  void saveUserCredential(String emailId, String password) async {
    AppSharedPreference()
        .saveStringValue(AppConstants.KEY_USER_EMAIL_ID, emailId);
    AppSharedPreference()
        .saveStringValue(AppConstants.KEY_USER_PASSWORD, password);
  }

  String _validateNameField(String value) {
    return value.trim().length < 1 ? 'Field can\'t be empty' : null;
  }

  String _validatePassword(String value) {
    if (value.trim().length < 1) {
      return 'Field can\'t be empty';
    } else if (value.trim().length < 8 || value.trim().length > 15) {
      return 'Please use 8 to 15 characters password';
    } else {
      return null;
    }
  }
}

