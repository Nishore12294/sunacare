import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/callrequest/GetCallResponse.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/ui/schedulemeeting/call_request_screen.dart';
import 'package:suna_care/ui/schedulemeeting/leave_us_msg_intial_screen.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_meeting_calendar_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_globals.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';

//import 'package:ishare/screens/custom_expansion_tile.dart' as custom;
import 'package:suna_care/ui/offers/custom_expansion_tile.dart' as custom;
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

import '../../core/data/local/cg_giver_needed_full_list.dart';
import '../../core/data/local/cg_giver_expandedhdr_items.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz;

class OffersDetailsScreen extends StatefulWidget {
  static const routeName = '/offersList';
  String offerGuid = "", germanSkillLevel = "";

  OffersDetailsScreen(this.offerGuid, this.germanSkillLevel);

  @override
  _OffersDetailsScreenState createState() => _OffersDetailsScreenState();
}

class _OffersDetailsScreenState extends State<OffersDetailsScreen> {
  final log = getLogger('OffersDetailsScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  String offerGuid, germanSkillLevel;
  OffersNotifier offersScreenNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  var _keyValidationForm = GlobalKey<FormState>();
   DateFormat dateMonthFormatter;
  List<Entry> data = <Entry>[];

  List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];

  // List cardBGColors = colorsOffer;
  String languageCode = "en";
  _OffersDetailsScreenState();

  @override
  void initState() {
    super.initState();
    debugPrint("globalLanguageCode in OffersDetails screen :==>"+globalLanguageCode);
    setInitialData();
  }

  setInitialData() async {
    if (widget != null) {
      offerGuid = widget.offerGuid;
      germanSkillLevel = widget.germanSkillLevel;
      log.d('offerGuid: ==>' + offerGuid);
      log.d('germanSkillLevel: ==>' + germanSkillLevel);
      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>" + accessToken);
    }

    var prefs = await SharedPreferences.getInstance();
    if(prefs.getString('language_code') != null){
      debugPrint("language_code :==>"+prefs.getString('language_code')??"");
      languageCode = prefs.getString('language_code')??"";
    }else{
      debugPrint("language_code is null :==>");
      languageCode = "en";
    }
  }

  @override
  Widget build(BuildContext context) {
    dateMonthFormatter = DateFormat('dd MMM, yyyy',globalLanguageCode);
    return ChangeNotifierProvider<OffersNotifier>(
      builder: (context) => OffersNotifier(context,
          isFromWhichScreen: "offerDetailsScreen", offerGuid: offerGuid),
      child: Scaffold(
        backgroundColor: colorWhite,
        key: _scaffoldKey,
        body: Consumer<OffersNotifier>(
          builder: (context, offersNotifier, _) => ModalProgressHUD(
            inAsyncCall: offersNotifier.isLoading,
            child: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: Container(
                  color: colorWhite,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          //color: colorWhite,
                          color: ((germanSkillLevel == "Good"))
                              ? cardBGColors[0]
                              : ((germanSkillLevel == "Middle"))
                                  ? cardBGColors[1]
                                  : cardBGColors[2],
                          child: Container(
                            height: 290,
                            //color: colorLitePink,
                            child: Column(
                                //mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN * 2,
                                    child: Container(
                                      //color:colorLitePink,
                                      color: ((germanSkillLevel == "Good"))
                                          ? cardBGColors[0]
                                          : ((germanSkillLevel == "Middle"))
                                              ? cardBGColors[1]
                                              : cardBGColors[2],
                                    ),
                                  ),
                                  topHeader(offersNotifier),
                                  _buildWidgetMainContent(
                                      context, offersNotifier),
                                ]),
                          )),
                      SizedBox(
                        height: AppConstants.SIDE_MARGIN,
                      ),
                      Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                              child: Text(
                                (offersScreenNotifier != null &&
                                        offersScreenNotifier
                                                .offersDetailsResp !=
                                            null &&
                                        offersScreenNotifier.offersDetailsResp
                                                .webMethodOutputRows !=
                                            null &&
                                        offersScreenNotifier
                                                .offersDetailsResp
                                                .webMethodOutputRows
                                                .webMethodOutputRows[0] !=
                                            null)
                                    ? offersScreenNotifier
                                                .offersDetailsResp
                                                .webMethodOutputRows
                                                .webMethodOutputRows[0]
                                            ['CP_NoOfPeopleNeedingCare'] +
                                        " " +
                                       // AppConstants.CAREGIVER_NEEDED
                                       AppLocalizations.of(context).translate('peopleNeedingCare')
                                    : "",
                                textAlign: TextAlign.left,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 22),
                              ),
                            )
                          ]),
                      Expanded(
                          flex: 1,
                          child: Container(
                            color: colorWhite,
                            // height: 300,
                            child: Column(children: <Widget>[
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.only(bottom: 50.0),

                                  // child : ListView(
                                  //
                                  //   key: PageStorageKey('myScrollable'),
                                  //   children: <Widget>[
                                  //     if(offersScreenNotifier.cgGiverNeededListItems1 != null && offersScreenNotifier.cGGiverNeededFullLists != null)
                                  //     for(int arrIndex =0 ;arrIndex <=offersScreenNotifier.cGGiverNeededFullLists.length;arrIndex++)
                                  //       if(arrIndex == (offersScreenNotifier.cGGiverNeededFullLists.length))
                                  //         ListItem(null, null,arrIndex)
                                  //       else
                                  //         ListItem(offersScreenNotifier.expandHeaderItemsList[arrIndex],offersScreenNotifier.cGGiverNeededFullLists[arrIndex],arrIndex)
                                  //
                                  //   ],
                                  // )

                                  child: ListView.builder(
                                    itemBuilder:
                                        (BuildContext context, int index) =>
                                            EntryItem(data[index]),
                                    itemCount: data.length,
                                  ),
                                ),
                              )
                            ]),
                          ))
                    ],
                  ),
                )),
          ),
        ),
      ),
    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////

  Widget topHeader(OffersNotifier offersNotifier) {
    return Container(
      height: 50,
      //color: colorLitePink,
      color: ((germanSkillLevel == "Good"))
          ? cardBGColors[0]
          : ((germanSkillLevel == "Middle"))
              ? cardBGColors[1]
              : cardBGColors[2],
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            //mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: InkWell(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          backButtonPressed();
                        },
                        child: Container(
                          color: colorTransparent,
                          alignment: Alignment.center,
                          child: Image.asset(
                            AppImages.IMAGE_BACK,
                            height: 25,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  //AppConstants.OFFERS_DETAILS,
                  AppLocalizations.of(context).translate('offerDetails'),
                  textAlign: TextAlign.center,
                  style: getStyleBody1(context).copyWith(
                    color: colorPencilBlack,
                    fontWeight: AppFont.fontWeightMedium,
                    fontSize: 23,
                    fontFamily: 'NPSunaGrotesk',
                  ),
                ),
              ),
              Expanded(
                  flex: 1,
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () async {
                            debugPrint("offerNumberValue :==>"+offerNumberValue);
                            Navigator.pushNamed(context, LeaveUsMessageInitialScreen.routeName);
                          },
                          child: Container(
                            color: colorTransparent,
                            alignment: Alignment.center,
                            child: Icon(
                              Icons.message_outlined,
                              color: colorLiteOrange,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: AppConstants.SIDE_MARGIN / 1.5,
                        ),
                        InkWell(
                          onTap: () async {
                            // showDialog(
                            //   context: context,
                            //   builder: (_) => AlertOverlay(
                            //       AppConstants.OPEN_OFFERS_ALERT,
                            //       AppConstants.CURRENTLY_NO_AGENTS_AVAILABLE,
                            //       AppConstants.OKAY),
                            // );

                            tz.initializeTimeZones();
                            var locations = tz.timeZoneDatabase.locations;
                            print("location lengt:==>" +
                                locations.length.toString()); // => 429
                            print("location first:==>" +
                                locations.keys.first); // => "Africa/Abidjan"
                            print("location last:==>" + locations.keys.last);

                            DateTime dateTime = DateTime.now();

                            print(
                                "formatISOTime:==> " + formatISOTime(dateTime));
                            print("timeZoneName:==> " + dateTime.timeZoneName);
                            print("dateTime:==> " +
                                dateTime.timeZoneOffset.toString());
                            print("toIso8601String:==> " +
                                dateTime.toIso8601String());

                            GetCallResponse getCallRequestResp =
                                await offersScreenNotifier.callApiCallRequest("Central European Standard Time");
                            if (getCallRequestResp != null &&
                                getCallRequestResp.webMethodOutputRows !=
                                    null &&
                                getCallRequestResp.webMethodOutputRows
                                        .webMethodOutputRows[0] !=
                                    null &&
                                getCallRequestResp
                                        .webMethodOutputRows
                                        .webMethodOutputRows[0]
                                        .currentTimeZoneStatus ==
                                    "Yes") {
                              UrlLauncher.launch('tel:' +
                                  getCallRequestResp.webMethodOutputRows
                                      .webMethodOutputRows[0].contactNumber);
                            } else {
                              Navigator.pushNamed(context, CallRequestScreen.routeName);
                            }
                          },
                          child: Container(
                            color: colorTransparent,
                            alignment: Alignment.center,
                            child: Image.asset(
                              AppImages.IMAGE_CALL,
                              height: 23,
                            ),
                          ),
                        ),
                      ])),
            ],
          ),
        ],
      ),
    );
  }

  static String formatISOTime(DateTime date) {
    var duration = date.timeZoneOffset;
    if (duration.isNegative)
      return (date.toIso8601String() +
          "-${duration.inHours.toString().padLeft(2, '0')}:${(duration.inMinutes - (duration.inHours * 60)).toString().padLeft(2, '0')}");
    else
      return (date.toIso8601String() +
          "+${duration.inHours.toString().padLeft(2, '0')}:${(duration.inMinutes - (duration.inHours * 60)).toString().padLeft(2, '0')}");
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  Widget _buildWidgetMainContent(
      BuildContext context, OffersNotifier offersNotifier) {
    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);
    offersScreenNotifier = offersNotifier;

    return buildWidgetOffersItem(context, imageWidth, cardRadio);
  }

  static Future<void> openMap(double latitude, double longitude) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  Widget buildWidgetOffersItem(
      BuildContext context, double imageWidth, Radius cardRadio) {
    if (offersScreenNotifier != null &&
        offersScreenNotifier.offersDetailsResp != null &&
        offersScreenNotifier.offersDetailsResp.webMethodOutputRows != null &&
        offersScreenNotifier
                .offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0] !=
            null) {

      //OfferNumber
      offerNumberValue =  offersScreenNotifier.offersDetailsResp.webMethodOutputRows
          .webMethodOutputRows[0]['OfferNumber'];

      //Accommodation
      bool ownBathRoomValue = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['Accomo_OwnBathroomForCG'] ==
          "True";
      bool ownKitchenValue = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['Accomo_OwnKitchenForCG'] ==
          "True";
      bool ownTVValue = offersScreenNotifier.offersDetailsResp
              .webMethodOutputRows.webMethodOutputRows[0]['Accomo_OwnTv'] ==
          "True";
      bool animalInHouseHoldValue = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['Accomo_AnimalsInTheHousehold'] ==
          "True";

      //Equipments
      bool hospBed = offersScreenNotifier.offersDetailsResp.webMethodOutputRows
              .webMethodOutputRows[0]['Equip_HospitalBed'] ==
          "True";
      bool showerChair = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['Equip_ShowerChair'] ==
          "True";
      bool rollator = offersScreenNotifier.offersDetailsResp.webMethodOutputRows
              .webMethodOutputRows[0]['Equip_Rollator'] ==
          "True";
      bool bedLift = offersScreenNotifier.offersDetailsResp.webMethodOutputRows
              .webMethodOutputRows[0]['Equip_Bedlift'] ==
          "True";
      bool stairLift = offersScreenNotifier.offersDetailsResp
              .webMethodOutputRows.webMethodOutputRows[0]['Equip_StairsLift'] ==
          "True";
      bool toiletBoosterSeat = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['Equip_ToiletBoosterSeat'] ==
          "True";
      bool bathTubLift = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['Equip_BathtubLift'] ==
          "True";

      String noOfPersonNeedingToCare = offersScreenNotifier
          .offersDetailsResp
          .webMethodOutputRows
          .webMethodOutputRows[0]['CP_NoOfPeopleNeedingCare'];
      //Care person 1
      bool cp1UrineIncontinence = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['CP1_UrineIncontinence'] ==
          "True";
      bool cp1StoolIncontinence = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['CP1_StoolIncontinence'] ==
          "True";
      bool cp1Diaper = offersScreenNotifier.offersDetailsResp
              .webMethodOutputRows.webMethodOutputRows[0]['CP1_Diaper'] ==
          "True";
      bool cp1Catheter = offersScreenNotifier.offersDetailsResp
              .webMethodOutputRows.webMethodOutputRows[0]['CP1_Catheter'] ==
          "True";

      //Care person 1
      bool cp2Diaper = offersScreenNotifier.offersDetailsResp
              .webMethodOutputRows.webMethodOutputRows[0]['CP2_Diaper'] ==
          "True";
      bool cp2Catheter = offersScreenNotifier.offersDetailsResp
              .webMethodOutputRows.webMethodOutputRows[0]['CP2_Catheter'] ==
          "True";
      bool cp2UrineIncontinence = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['CP2_UrineIncontinence'] ==
          "True";
      bool cp2StoolIncontinence = offersScreenNotifier
              .offersDetailsResp
              .webMethodOutputRows
              .webMethodOutputRows[0]['CP2_StoolIncontinence'] ==
          "True";

      data = <Entry>[

        if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
            .webMethodOutputRows[0]['CGReq_CGGender'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows
            .webMethodOutputRows[0]['CGReq_RequiredGermanSkills'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows
            .webMethodOutputRows[0]['CGReq_DriversLicenceRequirements'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows
            .webMethodOutputRows[0]['CGReq_SmokingPremittion'] != "" )
        //'CG Requirements',
        Entry(

          (languageCode!= null && languageCode!= ""&& languageCode=="en")?"CG Requirements":"Wymagania CG",
          <Entry>[
            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['CGReq_CGGender'] != "") Entry(AppLocalizations.of(context).translate('cGGender')+" : " +
                offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                    .webMethodOutputRows[0]['CGReq_CGGender']),

            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['CGReq_RequiredGermanSkills'] != "") Entry(AppLocalizations.of(context).translate('requiredGermanSkill')+" : " +
                offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                    .webMethodOutputRows[0]['CGReq_RequiredGermanSkills']),

            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['CGReq_DriversLicenceRequirements'] == "Yes")Entry(AppLocalizations.of(context).translate('driverLicenseRequirement')+" : " +
                offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]
                    ['CGReq_DriversLicenceRequirements']),

            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['CGReq_SmokingPremittion'] != "") Entry(AppLocalizations.of(context).translate('smokingPermission')+" : " +
                offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                    .webMethodOutputRows[0]['CGReq_SmokingPremittion']),
          ],
        ),

        //CarPersons

        if(
         offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_CareDegree'] != ""|| offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_CareDegree'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Age'] != ""|| offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Age'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Weight'] != ""|| offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Weight'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Mobility'] != ""|| offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Mobility'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Movability'] != ""|| offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Movability'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_ClimbingSteps'] != ""|| offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_ClimbingSteps'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_TransferBedToWheelchair'] != ""|| offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_TransferBedToWheelchair'] != ""
        || cp1UrineIncontinence|| cp2UrineIncontinence
        || cp1StoolIncontinence|| cp2StoolIncontinence
        || cp1Diaper|| cp2Diaper
        || cp1Catheter|| cp2Catheter
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Allergies'] != ""|| offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Allergies'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_FoodIncompability'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_FoodIncompability'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_DifficultiesWithSpeech'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_DifficultiesWithSpeech'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_DifficultiesWithHearing'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_DifficultiesWithHearing'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_DifficultiesWithSight'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_DifficultiesWithSight'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Dementia'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Dementia'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_TemporalDisorientation'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_TemporalDisorientation'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_LocationDisorientation'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_LocationDisorientation'] != ""
        || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_SituationalDisorientation'] != "" || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_SituationalDisorientation'] != ""
        )

          Entry(
          (languageCode!= null &&languageCode!= ""&&languageCode=="en")?'Care persons':'Opiekuńcze osoby',
          <Entry>[


            Entry(
              (languageCode!= null &&languageCode!= ""&&languageCode=="en")?'Care person 1':'Osoba opiekująca się 1',

              <Entry>[
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_CareDegree'] != "") Entry(AppLocalizations.of(context).translate('careDegree')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_CareDegree']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Age'] != "") Entry(AppLocalizations.of(context).translate('age')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_Age']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Weight'] != "")  Entry(AppLocalizations.of(context).translate('weight')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_Weight']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Mobility'] != "") Entry(AppLocalizations.of(context).translate('mobility')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_Mobility']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Movability'] != "") Entry(AppLocalizations.of(context).translate('moveAbility')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_Movability']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_ClimbingSteps'] != "") Entry(AppLocalizations.of(context).translate('climbingSteps')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_ClimbingSteps']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_TransferBedToWheelchair'] != "") Entry(AppLocalizations.of(context).translate('transferBedToWheelchair')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_TransferBedToWheelchair']),
                if(cp1UrineIncontinence)Entry(AppLocalizations.of(context).translate('urineInContinence')+" : " +
                    (cp1UrineIncontinence  ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
                if(cp1StoolIncontinence) Entry(AppLocalizations.of(context).translate('stoolInContinence')+" : " +
                    (cp1StoolIncontinence  ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),

                if(cp1Diaper) Entry(AppLocalizations.of(context).translate('diaper')+" : " + (cp1Diaper  ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
                if(cp1Catheter) Entry(AppLocalizations.of(context).translate('catheter')+" : " + (cp1Catheter  ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Allergies'] != "") Entry(AppLocalizations.of(context).translate('allergies')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_Allergies']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_FoodIncompability'] != "")  Entry(AppLocalizations.of(context).translate('foodIncompatibility')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_FoodIncompability']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_DifficultiesWithSpeech'] != "") Entry(AppLocalizations.of(context).translate('difficultiesWithSpeech')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_DifficultiesWithSpeech']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_DifficultiesWithHearing'] != "") Entry(AppLocalizations.of(context).translate('difficultiesWithHearing')+"  : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_DifficultiesWithHearing']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_DifficultiesWithSight'] != "") Entry(AppLocalizations.of(context).translate('difficultiesWithSight')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_DifficultiesWithSight']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_Dementia'] != "") Entry(AppLocalizations.of(context).translate('dementia')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_Dementia']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_TemporalDisorientation'] != "") Entry(AppLocalizations.of(context).translate('temporalDisorientation')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_TemporalDisorientation']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_LocationDisorientation'] != "") Entry(AppLocalizations.of(context).translate('locationDisorientation')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP1_LocationDisorientation']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP1_SituationalDisorientation'] != "") Entry(AppLocalizations.of(context).translate('situationalDisorientation')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                            .webMethodOutputRows[0]
                        ['CP1_SituationalDisorientation']),
              ],
            ),

            if(int.parse(noOfPersonNeedingToCare) > 1)
            Entry(
              //'Care person 2',
              (languageCode!= null &&languageCode!= ""&&languageCode=="en")?'Care person 2':'Osoba opiekująca się 2',
              <Entry>[
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_CareDegree'] != "") Entry(AppLocalizations.of(context).translate('careDegree')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_CareDegree']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Age'] != "")  Entry(AppLocalizations.of(context).translate('age')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_Age']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Weight'] != "") Entry(AppLocalizations.of(context).translate('weight')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_Weight']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Mobility'] != "")  Entry(AppLocalizations.of(context).translate('mobility')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_Mobility']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Movability'] != "")  Entry(AppLocalizations.of(context).translate('moveAbility')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_Movability']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_ClimbingSteps'] != "") Entry(AppLocalizations.of(context).translate('climbingSteps')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_ClimbingSteps']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_TransferBedToWheelchair'] != "") Entry(AppLocalizations.of(context).translate('transferBedToWheelchair')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_TransferBedToWheelchair']),
               if(cp2UrineIncontinence)Entry(AppLocalizations.of(context).translate('urineInContinence')+" : " +
                    (cp2UrineIncontinence  ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
                if(cp2StoolIncontinence) Entry(AppLocalizations.of(context).translate('stoolInContinence')+" : " +
                    (cp2StoolIncontinence  ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
                 if(cp2Diaper) Entry(AppLocalizations.of(context).translate('diaper')+" : " + (cp2Diaper  ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
                if(cp2Catheter) Entry(AppLocalizations.of(context).translate('catheter')+" : " + (cp2Catheter  ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Allergies'] != "") Entry(AppLocalizations.of(context).translate('allergies')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_Allergies']),

                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_FoodIncompability'] != "") Entry(AppLocalizations.of(context).translate('foodIncompatibility')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_FoodIncompability']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_DifficultiesWithSpeech'] != "") Entry(AppLocalizations.of(context).translate('difficultiesWithSpeech')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_DifficultiesWithSpeech']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_DifficultiesWithHearing'] != "") Entry(AppLocalizations.of(context).translate('difficultiesWithHearing')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_DifficultiesWithHearing']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_DifficultiesWithSight'] != "") Entry(AppLocalizations.of(context).translate('difficultiesWithSight')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_DifficultiesWithSight']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_Dementia'] != "") Entry(AppLocalizations.of(context).translate('dementia')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_Dementia']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_TemporalDisorientation'] != "") Entry(AppLocalizations.of(context).translate('temporalDisorientation')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_TemporalDisorientation']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_LocationDisorientation'] != "") Entry(AppLocalizations.of(context).translate('locationDisorientation')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['CP2_LocationDisorientation']),
                if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CP2_SituationalDisorientation'] != "") Entry(AppLocalizations.of(context).translate('situationalDisorientation')+" : " +
                    offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                            .webMethodOutputRows[0]
                        ['CP2_SituationalDisorientation']),
              ],
            ),
          ],
        ),

        //  'Equipments',
        if(hospBed ||showerChair||rollator||bedLift||stairLift||toiletBoosterSeat||bathTubLift||offersScreenNotifier.offersDetailsResp.webMethodOutputRows
            .webMethodOutputRows[0]['Equip_NursingServiceAssistance'] !="")
          Entry(
         (languageCode!= null &&languageCode!= ""&&languageCode=="en")?"Equipments":"Wyposażenie",
          <Entry>[
            if(hospBed) Entry(AppLocalizations.of(context).translate('hospitalBed')+" : " + (hospBed ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(showerChair) Entry(AppLocalizations.of(context).translate('showerChair')+" : " + (showerChair ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(rollator) Entry(AppLocalizations.of(context).translate('rollator') + " : " + (rollator ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(bedLift) Entry(AppLocalizations.of(context).translate('bedList')+" : " + (bedLift ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(stairLift) Entry(AppLocalizations.of(context).translate('stairsLift')+" : " + (stairLift ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(toiletBoosterSeat)  Entry(AppLocalizations.of(context).translate('tolietBoosterSeat')+" : " + (toiletBoosterSeat ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(bathTubLift) Entry(AppLocalizations.of(context).translate('bathTubLift')+" : " + (bathTubLift ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                 .webMethodOutputRows[0]['Equip_NursingServiceAssistance'] =="Yes") Entry(AppLocalizations.of(context).translate('nursingService')+" : " +
                // offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Equip_NursingServiceAssistance']
                   AppLocalizations.of(context).translate('yes')
              ),
          ],
        ),

        //'Accommodations',
    if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_HouseOrApartment'] != ""
      || ownBathRoomValue||ownKitchenValue||ownTVValue||animalInHouseHoldValue
      || offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_KindOfAnimal'] != ""
      ||offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_FurtherPeopleLivingInTheSameHousehold'] != ""
      ||offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_FamilyLivingNearby'] != ""
      ||offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_DistanceFromCareperson'] != ""
      ||offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Accomo_StateOfRelation'] != ""
    )
        Entry(

          (languageCode!= null &&languageCode!= ""&&languageCode=="en")?"Accommodations":"Zakwaterowanie",
          <Entry>[
            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['Accomo_HouseOrApartment'] == "House")Entry(AppLocalizations.of(context).translate('houseOrApartment')+" : "+AppLocalizations.of(context).translate('house')),

          if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['Accomo_HouseOrApartment'] == "Apartment")Entry(AppLocalizations.of(context).translate('houseOrApartment')+" : " +AppLocalizations.of(context).translate('apartment')),

            if(ownBathRoomValue)  Entry(AppLocalizations.of(context).translate('ownBathRoom')+" : " + (ownBathRoomValue ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(ownKitchenValue)  Entry(AppLocalizations.of(context).translate('ownKitchen')+" : " + (ownKitchenValue ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(ownTVValue) Entry(AppLocalizations.of(context).translate('ownTV')+" : " + (ownTVValue ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),
            if(animalInHouseHoldValue) Entry(AppLocalizations.of(context).translate('animalInHouseHold')+" : " + (animalInHouseHoldValue ? AppLocalizations.of(context).translate('yes') : AppLocalizations.of(context).translate('no'))),

            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['Accomo_KindOfAnimal'] != "") Entry(AppLocalizations.of(context).translate('kindOfAnimal')+" : " +
                offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                    .webMethodOutputRows[0]['Accomo_KindOfAnimal']),
            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['Accomo_FurtherPeopleLivingInTheSameHousehold'] != "")Entry(AppLocalizations.of(context).translate('furtherPeopleLiving')+" : " +
                offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                        .webMethodOutputRows[0]['Accomo_FurtherPeopleLivingInTheSameHousehold']),
            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['Accomo_FamilyLivingNearby'] != "")Entry(AppLocalizations.of(context).translate('familyLivingNearby')+" : " +
                offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                    .webMethodOutputRows[0]['Accomo_FamilyLivingNearby']),
            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['Accomo_DistanceFromCareperson'] != "")Entry(AppLocalizations.of(context).translate('distanceFromCarePerson')+" : " +
                offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                    .webMethodOutputRows[0]['Accomo_DistanceFromCareperson']),
            if(offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                .webMethodOutputRows[0]['Accomo_StateOfRelation'] != "")Entry(AppLocalizations.of(context).translate('stateOfRelation')+" : " +
                offersScreenNotifier.offersDetailsResp.webMethodOutputRows
                    .webMethodOutputRows[0]['Accomo_StateOfRelation']),
          ],
        ),
      ];

      return GestureDetector(
        onTap: () {
          log.d('onPress: Offers details');
          //Navigator.pushNamed(context, OffersProfileUploadScreen.routeName);
          //

          //UrlLauncher.launch("tel://<phone_number>");
        },
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: AppConstants.SIDE_MARGIN / 2,
              ),
              Container(
                // color: (position%3 == 0) ? cardBGColors[0]: ((position%3 == 1))? cardBGColors[1]: cardBGColors[2],
                // color: cardBGColors[2],

                decoration: BoxDecoration(
                    //color: colorLitePink,
                    // color: ( (offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
                    //     && offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0] != null
                    //     &&offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CGReq_RequiredGermanSkills'] == "Good") )
                    //     ? cardBGColors[0]:
                    // ((offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
                    //     && offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[0] != null
                    //     &&offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['CGReq_RequiredGermanSkills'] == "Middle") )
                    //     ? cardBGColors[1]
                    //     : cardBGColors[2],

                    color: ((germanSkillLevel == "Good"))
                        ? cardBGColors[0]
                        : ((germanSkillLevel == "Middle"))
                            ? cardBGColors[1]
                            : cardBGColors[2],
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                child: Container(
                  padding: EdgeInsets.all(AppConstants.SIDE_MARGIN / 1.5),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: AppConstants.SIDE_MARGIN / 2,
                      ),
                      Flexible(
                          fit: FlexFit.tight,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN / 4,
                                ),
                                //Address
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      AppImages.IMAGE_ADDRESS,
                                      height: 20,
                                    ),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN / 1.5,
                                    ),
                                    Flexible(
                                      fit: FlexFit.tight,
                                      child: Text(
                                        offersScreenNotifier
                                                    .offersDetailsResp
                                                    .webMethodOutputRows
                                                    .webMethodOutputRows[0]
                                                ['Gen_Address'] +
                                            ", " +
                                            offersScreenNotifier
                                                    .offersDetailsResp
                                                    .webMethodOutputRows
                                                    .webMethodOutputRows[0]
                                                ['Gen_City'] +
                                            ", " +
                                            offersScreenNotifier
                                                    .offersDetailsResp
                                                    .webMethodOutputRows
                                                    .webMethodOutputRows[0]
                                                ['Gen_PostalCode'],
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        style: getFormTitleStyle(context)
                                            .copyWith(
                                                color: colorBlack,
                                                fontWeight:
                                                    AppFont.fontWeightSemiBold,
                                                fontSize: 13),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () async {
                                        log.d('onPress: Map');

                                        // From a query
                                        //final query = "1600 Amphiteatre Parkway, Mountain View";
                                        final query = offersScreenNotifier
                                                    .offersDetailsResp
                                                    .webMethodOutputRows
                                                    .webMethodOutputRows[0]
                                                ['Gen_Address'] +
                                            ", " +
                                            offersScreenNotifier
                                                    .offersDetailsResp
                                                    .webMethodOutputRows
                                                    .webMethodOutputRows[0]
                                                ['Gen_City'] +
                                            ", " +
                                            offersScreenNotifier
                                                    .offersDetailsResp
                                                    .webMethodOutputRows
                                                    .webMethodOutputRows[0]
                                                ['Gen_PostalCode'];
                                        if (query != "") {
                                          print("Query  :==>" + query);
                                         var addresses = await Geocoder.local.findAddressesFromQuery(query);
                                          //var addresses = await Geocoder.google(AppConstants.GOOGLE_API_KEY).findAddressesFromQuery(query);
                                          if (addresses != null &&
                                              addresses.isNotEmpty) {
                                            var first = addresses.first;
                                            print("LatLng :==>" +
                                                "${first.featureName} : ${first.coordinates}");
                                            openMap(first.coordinates.latitude,
                                                first.coordinates.longitude);
                                          } else {
                                            print("addresses is empty :==>");
                                          }
                                        }
                                      },
                                      child: Image.asset(
                                        AppImages.IMAGE_FORWARD,
                                        height: 20,
                                      ),
                                    ),
                                  ],
                                ),

                                _buildDivider(),
                                // //Date / Duration
                                // Row(
                                //   mainAxisAlignment: MainAxisAlignment.start,
                                //   crossAxisAlignment: CrossAxisAlignment.center,
                                //   children: [
                                //
                                //     Flexible(
                                //     fit: FlexFit.tight,
                                //      child: Row(
                                //           mainAxisAlignment: MainAxisAlignment.start,
                                //           crossAxisAlignment: CrossAxisAlignment.center,
                                //           children: [
                                //             Image.asset(
                                //               AppImages.IMAGE_DATE,
                                //               height: 20,
                                //             ),
                                //             SizedBox(
                                //               width: AppConstants.SIDE_MARGIN / 1.5,
                                //             ),
                                //             Flexible(
                                //               fit: FlexFit.tight,
                                //               child:Text(
                                //                 ""+offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Gen_LatestArrivalDate'],
                                //                 // dateMonthFormatter.format(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0].genLatestArrivalDate)
                                //                 // +" - "+dateMonthFormatter.format(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0].genLatestArrivalDate),
                                //                 maxLines: 1,
                                //                 overflow: TextOverflow.ellipsis,
                                //                 softWrap: false,
                                //                 style: getFormTitleStyle(context).copyWith(
                                //                     color: colorBlack,
                                //                     fontWeight: AppFont.fontWeightSemiBold,
                                //                     fontSize: 13
                                //                 ),
                                //               ),
                                //             ),
                                //
                                //           ]),
                                //
                                //     ),
                                //
                                //
                                //
                                //   ],
                                // ),

                                //Date / Duration
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            AppImages.IMAGE_DATE,
                                            height: 20,
                                          ),
                                          SizedBox(
                                            width:
                                                AppConstants.SIDE_MARGIN / 1.5,
                                          ),
                                          SizedBox(
                                            child: Text(
                                                dateMonthFormatter.format(new DateFormat("yyyy-MM-dd","en").parse(offersScreenNotifier
                                                    .offersDetailsResp
                                                    .webMethodOutputRows
                                                    .webMethodOutputRows[0]
                                                ['Gen_LatestArrivalDate'])) ,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              softWrap: false,
                                              textAlign: TextAlign.left,
                                              style: getFormTitleStyle(context)
                                                  .copyWith(
                                                      color: colorBlack,
                                                      fontWeight: AppFont
                                                          .fontWeightSemiBold,
                                                      fontSize: 13),
                                            ),
                                          ),
                                        ]),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN,
                                    ),
                                    Expanded(
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Image.asset(
                                              AppImages.IMAGE_DURATION,
                                              height: 18,
                                            ),
                                            SizedBox(
                                              width:
                                                  AppConstants.SIDE_MARGIN / 2,
                                            ),
                                            Expanded(
                                              child: Text(
                                                // offersScreenNotifier
                                                //         .offersDetailsResp
                                                //         .webMethodOutputRows
                                                //         .webMethodOutputRows[0]
                                                //     ['Gen_Duration'],

                                                (offersScreenNotifier.offersDetailsResp != null && offersScreenNotifier.offersDetailsResp.webMethodOutputRows != null
                                                    && offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Gen_Duration'] != null)
                                                    ?(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Gen_Duration']  == "Longer than 6 weeks")
                                                    ?AppLocalizations.of(context).translate('longerThanSixWeeks')
                                                    :(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Gen_Duration']  == "Unlimited")
                                                    ?AppLocalizations.of(context).translate('unlimited')
                                                    :(offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Gen_Duration']  == "4-6 weeks")
                                                    ?AppLocalizations.of(context).translate('fourToSixWeeks')
                                                    :offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0]['Gen_Duration']
                                                    :"",
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                softWrap: false,
                                                textAlign: TextAlign.left,
                                                style: getFormTitleStyle(
                                                        context)
                                                    .copyWith(
                                                        color: colorBlack,
                                                        fontWeight: AppFont
                                                            .fontWeightSemiBold,
                                                        fontSize: 13),
                                              ),
                                            )
                                          ]),
                                    ),
                                  ],
                                ),

                                _buildDivider(),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    //Address
                                    Image.asset(
                                      AppImages.IMAGE_EURO,
                                      height: 20,
                                    ),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN / 1.5,
                                    ),
                                    Flexible(
                                      fit: FlexFit.tight,
                                      child: Text(
                                        //"Netto payment : \€ "+offersScreenNotifier.offersDetailsResp.webMethodOutputRows.webMethodOutputRows[0].genNettoPayment,
                                        AppLocalizations.of(context).translate('nettoPayment')+" : \€ " +
                                            offersScreenNotifier
                                                    .offersDetailsResp
                                                    .webMethodOutputRows
                                                    .webMethodOutputRows[0]
                                                ['Gen_NettoPayment'],
                                        maxLines: 1,
                                        style: getFormTitleStyle(context)
                                            .copyWith(
                                                color: colorBlack,
                                                fontWeight:
                                                    AppFont.fontWeightSemiBold,
                                                fontSize: 13),
                                      ),
                                    ),
                                  ],
                                ),

                                _buildDivider(),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN / 4,
                                ),
                              ],
                            ),
                          ))
                    ],
                  ),
                ),
              ),
            ]),
      );
    } else {
      return Container();
    }
  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}

//
// class ListItem extends StatefulWidget{
//   String sharedDetails;
//   CGGiverNeededFullList cgGiverNeededListItems;
//   ExpandedHeaderItems cgGiverNeededItem;
//   String searchItemTitle;
//   int itemIndex;
//   ListItem(this.cgGiverNeededItem,this.cgGiverNeededListItems,this.itemIndex);
//
//   @override
//   State<StatefulWidget>createState()
//   {
//     return ListItemState();
//   }
// }
// class ListItemState extends State<ListItem>
// {
//   bool isExpand=false;
//   RichText richText = null;
//
//
//   @override
//   void initState() {
//     super.initState();
//     if(this.widget.itemIndex != null && this.widget.itemIndex == 0){
//       setState(() {
//         isExpand=true;
//       });
//     }else{
//       isExpand=false;
//     }
//     //isExpand=false;
//   }
//   @override
//   Widget build(BuildContext context) {
//     if(this.widget.cgGiverNeededListItems != null) {
//       CGGiverNeededFullList listItem = this.widget.cgGiverNeededListItems;
//       return  Padding(
//         padding: (isExpand==true)?const EdgeInsets.all(0.0):const EdgeInsets.all(0.0),
//         child: Container(
//           decoration:BoxDecoration(
//               color: Colors.transparent,
//               borderRadius: (isExpand!=true)?BorderRadius.all(Radius.circular(8)):BorderRadius.all(Radius.circular(8)),
//               border: Border.all(color: Colors.transparent)
//           ),
//
//           child: custom.ExpansionTile(
//             trailing: (isExpand==true)?Icon(Icons.keyboard_arrow_up):Icon(Icons.keyboard_arrow_down),
//             headerBackgroundColor: colorWhite,
//             //initiallyExpanded: true,
//             initiallyExpanded: (widget.itemIndex == 0),
//             key: PageStorageKey<String>(this.widget.cgGiverNeededItem.title+"_"+this.widget.cgGiverNeededItem.orgId.toString()),
//             title: Container(
//
//                 color: colorWhite,
//                 /*decoration:BoxDecoration(
//                     color: AppColors.APP_LIGHT_GREY_PROFILE,
//                     borderRadius:BorderRadius.all(Radius.circular(8)),
//                     border: Border.all(color: Colors.transparent)
//
//                 ),*/
//
//                 width: double.infinity,
//
//                 child: Row(
//
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   children: <Widget>[
//
//                     Container(
//
//                       transform: Matrix4.translationValues(5.0, 0.0, 0.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.start,
//                         crossAxisAlignment: CrossAxisAlignment.center,
//                         // mainAxisSize: MainAxisSize.max,
//                         children: <Widget>[
//                         ],
//                       ),
//                     ),
//
//                     Container(
//                       transform: Matrix4.translationValues(0.0, 0.0, 0.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.start,
//                         crossAxisAlignment: CrossAxisAlignment.center,
//                         mainAxisSize: MainAxisSize.max,
//                         children: <Widget>[
//                           Padding(
//                             padding: EdgeInsets.only(top: 3, bottom: 3,left: 5.0),
//                             child: Container(
//                                 padding: const EdgeInsets.only(
//                                   left: 10,
//                                   top: 7,
//                                   right: 15,
//                                   bottom: 7,
//                                 ),
//                                 decoration: BoxDecoration(
//                                   // color: AppColors.APP_LIGHT_GREY_PROFILE,
//                                     color: Colors.transparent,
//                                     borderRadius: BorderRadius.only(
//                                         bottomRight: Radius.circular(2.0),
//                                         topRight: Radius.circular(2.0))),
//                                 child: Text(
//                                   this.widget.cgGiverNeededItem.title,
//                                   style: TextStyle(
//
//                                       fontWeight: FontWeight.w500, fontSize: 16,color: colorPencilBlack),
//                                 )
//
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//
//                   ],
//                 )),
//             onExpansionChanged: (value){
//
//               setState(() {
//                 isExpand=value;
//               });
//             },
//             children: [
//
//
//               for(final resourceItem in listItem.cGGiverNeededItem)
//                   Padding(
//                     padding: const EdgeInsets.fromLTRB(8.0,0.0,8.0,0.0),
//                     child: InkWell(
//                       onTap: (){
//                         //Scaffold.of(context).showSnackBar(SnackBar(backgroundColor: Colors.pink,duration:Duration(microseconds: 2000),content: Text("Selected Item "+this.widget.searchItemTitle+":==>"+resourceItem.firstName )));
//
//                       },
//
//                       child:  Container(
//                         width: getScreenWidth(context),
//                         height: 30,
//
//
//                         child: GestureDetector(
//                           child: Padding(
//                             padding: EdgeInsets.fromLTRB(23.0, 0.0, 23.0, 0.0),
//                             // padding:  const EdgeInsets.all(1.0),
//
//                             child:Column(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 crossAxisAlignment: CrossAxisAlignment.stretch,
//                                 mainAxisSize: MainAxisSize.min,
//                                 children: <Widget>[
//                                   Row(
//                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                     //crossAxisAlignment: CrossAxisAlignment.center,
//
//                                     children: <Widget>[
//                                       Row(
//                                         children: <Widget>[
//
//                                           Container(
//                                             width: 10,
//                                             height: 10,
//                                             decoration: BoxDecoration(
//                                                 shape: BoxShape.circle,
//                                                 color: colorLiteOrange),
//                                           ),
//
//                                           Padding(
//                                             padding: EdgeInsets.only(left: 10),
//
//                                             child: Container(
//                                               width: getScreenWidth(context)/1.5,
//                                               color: Colors.transparent,
//                                               //constraints: BoxConstraints(maxWidth: 280),
//                                               child: Column(
//                                                 mainAxisAlignment: MainAxisAlignment.start,
//                                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                                 children: <Widget>[
//                                                   Text(
//                                                     (resourceItem.item != null)?resourceItem.item: "",
//                                                     overflow: TextOverflow.ellipsis,
//                                                     softWrap: false,
//                                                     maxLines: 1,
//                                                     style: TextStyle(
//                                                         fontSize: 14, fontWeight: FontWeight.normal,color: colorBlack),
//                                                   ),
//
//                                                   // Text("test Tamilnadu")
//                                                 ],
//                                               ),
//                                             ),
//                                           )
//                                         ],
//                                       ),
//
//
//
//
//                                     ],
//
//                                   ),
//                                   SizedBox(height: 1,),
//
//
//                                 ]),),
//
//                           onTap: () {
//
//                           },
//                         ),
//
//                       ),
//
//
//                     ),
//
//
//                   )
//
//
//             ],
//
//           ),
//         ),
//       );
//     }else{
//       return  Container(
//         height: 60,
//       );
//     }
//
//   }
//
//
// }

//////////// New list item ////

class Entry {
  Entry(this.title, [this.children = const <Entry>[]]);

  final String title;
  final List<Entry> children;
}

class EntryItem extends StatelessWidget {
  BuildContext mContext;

  EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty)
      return ListTile(
          title: Container(
        width: getScreenWidth(mContext) / 1.5,
        child: Row(children: <Widget>[
          SizedBox(
            width: 10,
          ),
          Container(
            width: 10,
            height: 10,
            decoration:
                BoxDecoration(shape: BoxShape.circle, color: colorLiteOrange),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
              child: Text(
            root.title,
            style: TextStyle(
              color: colorPencilBlack,
              fontWeight: AppFont.fontWeightRegular,
              fontStyle: FontStyle.normal,
              fontSize: 14,
            ),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            softWrap: false,
          ))
        ]),
      ));
    return custom.ExpansionTile(
      key: PageStorageKey<Entry>(root),
      title: Text(
        root.title,
        style: TextStyle(
            color: colorPencilBlack,
            fontWeight: AppFont.fontWeightSemiBold,
            fontStyle: FontStyle.normal,
            fontSize: 15),
      ),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mContext = context;
    return _buildTiles(entry);
  }
}
