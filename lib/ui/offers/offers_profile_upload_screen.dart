import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgResponse.dart';
import 'package:suna_care/core/data/remote/request_response/register/signup/response_signup.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/core/notifier/signup_notifier.dart';
import 'package:suna_care/ui/home/slides/tab3_profile_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_custom_icon.dart';
import 'package:suna_care/utils/app_date_picker.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:provider/provider.dart';

import 'offers_details_screen.dart';

class OffersProfileUploadScreen extends StatefulWidget {
  BuildContext context;
  static const routeName = '/offers_profile_upload_screen';
  String offerGuid = "",germanSkillLevel = "";
  OffersProfileUploadScreen(this.offerGuid,this.germanSkillLevel);

  @override
  OffersProfileUploadScreenState createState() =>
      OffersProfileUploadScreenState();
}



class OffersProfileUploadScreenState
    extends State<OffersProfileUploadScreen> {
  var log = getLogger("OffersProfileUploadScreenState");
  final _keyValidationForm = GlobalKey<FormState>();
  String offerGuid,germanSkillLevel;
  @override
  void initState() {
    super.initState();
    setInitialData();
  }

  setInitialData() async {

    if (widget != null) {
      offerGuid = widget.offerGuid;
      germanSkillLevel = widget.germanSkillLevel;
      log.d('offerGuid: ==>'+offerGuid);
      log.d('germanSkillLevel: ==>'+germanSkillLevel);
    }
  }

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider<OffersNotifier>(
      builder: (context) => OffersNotifier(context,isFromWhichScreen: "cgProfileUpload",),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: colorLitePinkBG,
        ),
        child: Scaffold(
          backgroundColor: colorLitePinkBG,
         // key: _scaffoldKey,
          body: Consumer<OffersNotifier>(
            builder: (context, offerNotifier, _) => Stack(
              fit: StackFit.expand,
              children: <Widget>[
                ModalProgressHUD(
                    color: Colors.transparent,
                    inAsyncCall: offerNotifier.isLoading,
                    child: GestureDetector(
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());

                        },
                        child:Center(
                          child: Container(
                            child: Column(
                              //mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[

                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN * 2,
                                ),
                                topHeader(offerNotifier),
                                Expanded(
                                  flex: 1 ,
                                  child:  _buildWidget(offerNotifier,context),

                                ),



                              ],
                            ),
                          ),
                        ),


                    )),
              ],
            ),
          ),
        ),
      ),
    );


  }

  /////////////////////
  /*  build helpers */
  ////////////////////

  Widget topHeader(OffersNotifier offersNotifier) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      //mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Expanded(
          flex: 1,

            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    backButtonPressed();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    child: Image.asset(
                      AppImages.IMAGE_BACK,
                      height: 25,
                    ),
                  ),
                ),


              ],
            ),
        ),
        Expanded(
          flex: 2,
          child: Text(
            //AppConstants.OFFERS_DETAILS,
            AppLocalizations.of(context).translate('offerDetails'),
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            alignment: Alignment.center,
            child: Image.asset(
              AppImages.IMAGE_APP_LOGO,
              height: 20,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildWidget(
      OffersNotifier offersNotifier, BuildContext context) {


    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 2.0, left: 30.0, right: 30.0, bottom: 2.0),
              child: Theme(
                data: ThemeData(
                    hintColor: colorBlack,
                    primaryColor: colorAccentMild,
                    primaryColorDark: colorLiteOrange),
                child:GestureDetector(
                  onTap: (){
                    FocusScope.of(context).requestFocus(FocusNode());

                  },
                  child: Container(
                    alignment: Alignment.center,
                    child: SingleChildScrollView(

                      physics: BouncingScrollPhysics(),
                      child: Form(

                        key: _keyValidationForm,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[


                            Image.asset(
                              AppImages.IMAGE_UPLOAD,
                              width: getScreenWidth(context) / 2,
                              height: 100,
                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN,
                            ),
                            ///Profile picture upload info////
                            Text(
                             // AppConstants.YOU_CANNOT_VIEW_THE_OFFERS_UNTIL_PROF_UPLOAD,
                              AppLocalizations.of(context).translate('youCannotViewOffersUntilUploadYourProfilePic'),
                              textAlign: TextAlign.center,
                              style: getStyleBody1(context).copyWith(
                                  color: colorPencilBlack,
                                  fontWeight: AppFont.fontWeightSemiBold,
                                  fontSize: 17),
                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN*2,
                            ),


                            //UPLOAD BUTTON
                            ButtonTheme(
                              minWidth: double.infinity,
                              child: RaisedButton(
                                highlightElevation: 5.0,
                                onPressed: () {

                                  Navigator.pushNamed(context, Tab3ProfileScreen.routeName,
                                  arguments: true
                                  );

                                  // showDialog(
                                  //     context: context,
                                  //     builder: (BuildContext context) =>
                                  //         _buildPopUpDialogForImageSelection(
                                  //             context, offersNotifier));
                                },
                                color: colorLiteOrange,
                                textColor: colorWhite,
                                elevation: 1,
                                padding: EdgeInsets.only(
                                    top: AppConstants.SIDE_MARGIN / 1.5,
                                    bottom: AppConstants.SIDE_MARGIN / 1.5),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(AppConstants.SIDE_MARGIN))),
                                child: Text(
                                  //AppConstants.UPLOAD_PROFILE_PHOTO,
                                  AppLocalizations.of(context).translate('uploadProfilePhoto'),
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorWhite,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightBold),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: AppConstants.SIDE_MARGIN,
                            ),

                          ],
                        ),
                      ),
                    ),
                  ),
                ),

              ),
            )),
      ],
    );
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  InputDecoration _buildTextDecoration(String value) {
    return InputDecoration(
        isDense: true,
        hintText: value,
        // labelText: value,
        //alignLabelWithHint: true,
        filled: true,
        hintStyle: TextStyle(color: colorDarkGrey),
        contentPadding: EdgeInsets.all(16));
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  void saveUserCredential(String emailId, String password) async {
    AppSharedPreference()
        .saveStringValue(AppConstants.KEY_USER_EMAIL_ID, emailId);
    AppSharedPreference()
        .saveStringValue(AppConstants.KEY_USER_PASSWORD, password);
  }

  String _validateNameField(String value) {
    return value.trim().length < 1 ? 'Field can\'t be empty' : null;
  }

  String _validatePassword(String value) {
    if (value.trim().length < 1) {
      return 'Field can\'t be empty';
    } else if (value.trim().length < 8 || value.trim().length > 15) {
      return 'Please use 8 to 15 characters password';
    } else {
      return null;
    }
  }

  Widget _buildPopUpDialogForImageSelection(BuildContext context,
      OffersNotifier stateEditProfile) {
    double widget = getScreenWidth(context);

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(16),
          child: Center(
            child: SizedBox(
              //height: height / 3.0,
              width: widget - 32,
              child: Card(
                color: colorWhite,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6),
                ),
                elevation: 10.0,
                child: Padding(
                    padding: const EdgeInsets.only(
                        top: 24.0, bottom: 32.0, left: 24.0, right: 24.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Choose one option ',
                            textAlign: TextAlign.center,
                            style: getStyleSubHeading(context).copyWith(
                                color: Colors.black,
                                fontWeight: AppFont.fontWeightSemiBold)),
                        verticalSpace(8),
                        Divider(
                          height: 1.0,
                          color: colorDividerGrey,
                        ),
                        verticalSpace(16),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Icon(
                              Icons.photo_library_sharp,
                              color: colorPrimary,
                            ),
                            RaisedButton(
                              color: colorWhite,
                              textColor: colorBlack,
                              padding: EdgeInsets.only(left: 16),
                              elevation: 0.0,
                              child: Text(
                                AppConstants.GALLERY,
                                style: getStyleButtonText(context)
                                    .copyWith(color: colorBlack),
                                overflow: TextOverflow.ellipsis,
                                softWrap: true,
                              ),
                              onPressed: () {
                                try {
                                  openGallery(stateEditProfile, context);
                                } catch (e) {
                                  log.e('Error Camera : ' + e.toString());
                                }
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Icon(
                              Icons.camera_alt_sharp,
                              color: colorPrimary,
                            ),
                            RaisedButton(
                              color: colorWhite,
                              textColor: colorBlack,
                              padding: EdgeInsets.only(left: 16),
                              elevation: 0.0,
                              child: Text(
                                AppConstants.CAMERA,
                                style: getStyleButtonText(context)
                                    .copyWith(color: colorBlack),
                                overflow: TextOverflow.ellipsis,
                                softWrap: true,
                              ),
                              onPressed: () {
                                try {
                                  openCamera(stateEditProfile, context);
                                } catch (e) {
                                  log.e('Error Camera : ' + e.toString());
                                }
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      ],
                    )),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget verticalSpace(double height) {
    return SizedBox(
      height: height,
    );
  }

  Future openGallery(OffersNotifier stateEditProfile,
      BuildContext context) async {
    await ImagePicker.pickImage(source: ImageSource.gallery).then((value) {
      if (value != null) {
        //  stateEditProfile.imageLocalFile = value;
        _validateCompanyImage(value,stateEditProfile);

      }
    });
  }

  Future openCamera(OffersNotifier stateEditProfile,
      BuildContext context) async {
    await ImagePicker.pickImage(source: ImageSource.camera).then((value) {
      if (value != null) {
        // stateEditProfile.imageLocalFile = value;
        _validateCompanyImage(value,stateEditProfile);
      }
    });
  }
  Future _validateCompanyImage(File file, OffersNotifier offersNotifier) async {
    if(file != null) {
      var decodedImage = await decodeImageFromList(file.readAsBytesSync());

      List<int> imageBytes = file.readAsBytesSync();
      print(imageBytes);
      String base64Image = base64Encode(imageBytes);
      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>"+accessToken);

      SetCareGiverProfileImgResponse response =  await offersNotifier.callApiSetOrUploadProfileImage(accessToken, base64Image);
      if(response != null && response.webMethodOutputRows != null
          && response.webMethodOutputRows.webMethodOutputRows[0] != null
          && response.webMethodOutputRows.webMethodOutputRows[0].cgProfilePictureStatus == "Added"
      ){




        showDialog(
            context: context,
            builder: (_) => AlertOverlayWithRefersh(
                AppConstants.OPEN_OFFERS_ALERT,
               // "Image Uploaded successfully",
                AppConstants.PROFILE_IMAGE_UPLOADED,
                AppConstants.OKAY,offerGuid,germanSkillLevel));
      }else{
        if(response != null && response.outputRowErrors != null
            && response.outputRowErrors.outputRowErrors[0] != null
            &&response.outputRowErrors.outputRowErrors[0].textMessage != ""
        ){
          // showDialog(
          //   context: context,
          //   builder: (_) => AlertOverlay(AppConstants.ALERT,response.outputRowErrors.outputRowErrors[0].textMessage,AppConstants.OKAY),
          // );



        }

      }
      if((decodedImage.width >= 150 && decodedImage.width <= 256) && (decodedImage.height >= 110 && decodedImage.height <= 256)){
        //profileEditNotifier.imageLocalFile = file;
        log.d(file.path.toString()+ " " + decodedImage.width.toString() + " x " + decodedImage.height.toString());
      } else{
       // profileEditNotifier.imageLocalFile = file;
        log.d("erooorr"+file.path.toString()+ " " + decodedImage.width.toString() + " x " + decodedImage.height.toString());
        // profileEditNotifier.showCustomSnackBarMessageWithContext(AppConstants.IMAGE_PROFILE_PROTOCOL, bgColor: colorGMailRed, txtColor: colorWhite, ctx: _scfoldContext);
      }
    }
  }
}



class AlertOverlayWithRefersh extends StatefulWidget {
  String title, subTitle, buttonTitle,offerGuid,germanSkillLevel;

  AlertOverlayWithRefersh(this.title, this.subTitle, this.buttonTitle,this.offerGuid,this.germanSkillLevel);

  @override
  State<StatefulWidget> createState() =>
      AlertOverlayState(this.title, this.subTitle, this.buttonTitle,this.offerGuid,this.germanSkillLevel);
}

class AlertOverlayState extends State<AlertOverlayWithRefersh>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  String title, subTitle, buttonTitle,offerGuid,germanSkillLevel;

  AlertOverlayState(this.title, this.subTitle, this.buttonTitle,this.offerGuid,this.germanSkillLevel);

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: 200.0,
              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10.0, left: 20.0, right: 20.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  widget.title,
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightExtraBold,
                                      fontSize: 17),
                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN / 3.5,
                                ),
                                Text(
                                  widget.subTitle != null
                                      ? widget.subTitle
                                      : "",
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      fontWeight: AppFont.fontWeightRegular,
                                      fontSize: (widget.subTitle != null &&
                                          widget.subTitle.length > 10)
                                          ? 14
                                          : 16),
                                ),
                              ]))),
                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN * 4,
                            height: 45,
                            child: RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context)
                                    ? Navigator.pop(context)
                                    : '';
                                Navigator.of(context)
                                    .push(
                                  new MaterialPageRoute(
                                    builder: (_) =>
                                    new OffersDetailsScreen(widget.offerGuid,widget.germanSkillLevel),
                                  ),
                                );

                              },
                              color: colorDarkRed,
                              textColor: colorWhite,
                              elevation: 1,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonTitle,
                                style: getStyleButtonText(context).copyWith(
                                    color: colorWhite,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              )),
        ),
      ),
    );
  }
}

