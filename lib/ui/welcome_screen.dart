import 'dart:async';
import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:key_guardmanager/key_guardmanager.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/ui/login_screen.dart';
import 'package:suna_care/ui/register/pending_approval_screen.dart';
import 'package:suna_care/utils/app_globals.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/core/data/remote/request_response/login/response_login.dart';
import 'package:suna_care/core/notifier/welcome_notifier.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';

import 'home/home_screen.dart';


class WelcomeScreen extends StatefulWidget {
  static const routeName = '/welcome_screen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> with SingleTickerProviderStateMixin {
  var _iconAnimationController;
  var _iconAnimation;
  final log = getLogger('WelcomeScreen');
  final _keyValidationForm = GlobalKey<FormState>(debugLabel: '_welcome_key');
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isBioAuthAvailable = false;
  bool isPageExecuted = false;
  final LocalAuthentication auth = LocalAuthentication();

  @override
  void initState() {
    super.initState();
    setInitialData();

    // WelcomeNotifier welcomeNotifierNotfInitalState = Provider.of<WelcomeNotifier>(context, listen: false);

  }

  setInitialData() async {
    bool isEngLanguage = await AppSharedPreference().getBoolValue(AppConstants.KEY_LANG_ENGLISH);
    debugPrint("isEngLanguage :==>"+isEngLanguage.toString());
    if(isEngLanguage != null){
      globalLanguageCode = isEngLanguage?"en":"pl";
    }else{
      globalLanguageCode = "en";
    }

    debugPrint("globalLanguageCode welcome screen :==>"+globalLanguageCode);
    if(!isPageExecuted){
      isPageExecuted = true;
      Timer(
          Duration(milliseconds: 2500),
              () =>  {
            redirect()
          }

      );
    }


//    _iconAnimationController = AnimationController(duration: Duration(milliseconds: 100),vsync: this);
//    _iconAnimation = CurvedAnimation(parent: _iconAnimationController,curve: Curves.fastOutSlowIn);
//    _iconAnimation.addListener(() => this.setState(() {}));
//    _iconAnimationController.forward();
  }

  redirect() async {
    if(!bioMetricCondChecked) {
      bioMetricCondChecked = true;
      //get finger print enabled
      SharedPreferences prefs = await SharedPreferences.getInstance();
      bool isPrefFingerPrintEnabled = prefs.getBool(
          AppConstants.KEY_FINGER_PRINT_ENABLED) ?? false;
      print('isPrefFingerPrintEnabled:==>' + isPrefFingerPrintEnabled.toString());
      print('Welcome screen : isBioAuthAvailable:==>' +
          isBioAuthAvailable.toString());


     if (isPrefFingerPrintEnabled) {
        _checkBiometrics();
      } else {
        openNewPage();
      }
    }



  }



  //Check Biometrics availables
  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
      log.e(e.toString());
    }
    isBioAuthAvailable = canCheckBiometrics;

    if(isBioAuthAvailable){
      _authenticateWithBiometrics();
    }else{
      openKeyGuard();
    }
  }


  //Authenticate with bio metrics.
  Future<void> _authenticateWithBiometrics() async {
    bool authenticated = false;
    try {
      const androidStrings = const AndroidAuthMessages(
          cancelButton: 'cancel',
          goToSettingsButton: 'settings',
          goToSettingsDescription: 'Please set up your Touch ID.',
         );
      authenticated = await auth.authenticateWithBiometrics(
          //localizedReason:'Confirm your fingerprint so Sunacare can verify it’s you.',
          localizedReason:'Scan your fingerprint to authenticate',
          useErrorDialogs: false,
          stickyAuth: true,);

    } on PlatformException catch (e) {
      print("PlatformException Message :==>"+e.message);
      setState(()  {

        if(e.message == "No Biometrics enrolled on this device."){

            openKeyGuard();
           // AppSettings.openSecuritySettings();
        }else if(e.message == "The operation was canceled because the API is locked out due to too many attempts. This occurs after 5 failed attempts, and lasts for 30 seconds."){

          openKeyGuard();

        }
        else if(e.message == "NotEnrolled, No Biometrics enrolled on this device."){
          openKeyGuard();
         // AppSettings.openSecuritySettings();
        }else if(e.message == "PasscodeNotSet, Phone not secured by PIN, pattern or password, or SIM is currently locked."){
          openKeyGuard();
        }else if(e.message == "Fingerprint operation canceled by user."){
          _authenticateWithBiometrics();
        }else if(e.message == "Cancel"){
          if(Platform.isAndroid){
            SystemNavigator.pop();
          }else{
            exit(0);
          }

        }

      });

      return;
    }
    if (!mounted) return;

    final String message = authenticated ? 'Authorized' : 'Not Authorized';

    if(message =='Authorized' ){
      openNewPage();
    }

  }

  //Open new page..
  void openNewPage() async {
    //Store first login local data
     SharedPreferences prefs = await SharedPreferences.getInstance();
    String firstLoginValue = prefs.getString(AppConstants.KEY_LANG_FIRST_LOGIN)??"";
    debugPrint("welcome screen firstLoginValue :==>"+firstLoginValue);
    if(firstLoginValue == "" ) {
      //prefs.setString("FIRST_LOGIN", "1");
      Navigator.pushNamedAndRemoveUntil(context, LoginScreen.routeName, (e) => false);
     // Navigator.pushNamedAndRemoveUntil(context, PendingApprovalScreen.routeName, (e) => false);
    }else{

      Navigator.pushNamedAndRemoveUntil(context, HomeScreen.routeName, (e) => false);
    }
  }

  //Open the keygurad.
  void openKeyGuard() async{


    String platformAuth = "false";
    try {
      platformAuth = await KeyGuardmanager.authStatus;
      print("platformAuth :==>"+platformAuth);
      openNewPage();
    } on PlatformException catch (e) {
      print("PlatformException keyguard :==>"+e.message);
      platformAuth = 'Failed to get platform auth.';
    }catch(e){
      print("Exception keyguard :==>"+e.message);
    }

  }


  @override
  Widget build(BuildContext context) {
    final bgColor = Colors.white;
    var width = MediaQuery.of(context).size.width;
    return ChangeNotifierProvider<WelcomeNotifier>(
      builder: (context) => WelcomeNotifier(context),
      child: DecoratedBox(

        decoration: BoxDecoration(
          color: bgColor
//          image: DecorationImage(
//              image: AssetImage(
//                AppImages.IMAGE_BG_SPLASH_SCREEN,
//              ),
//              fit: BoxFit.fill),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            key: _scaffoldKey,
            body: Consumer<WelcomeNotifier>(
              builder: (context, welcomeNotifier, _) => Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  ModalProgressHUD(
                    inAsyncCall: welcomeNotifier.isLoading,
                    child: new SafeArea(
                      child: new Column(
                        children: [
                          new Expanded(
                            flex: 2,
                            child: Center(
                              child: new Container(
                                width: width,
                                color: bgColor,
                                child: new Column(
                                  children: <Widget>[
                                    new Expanded(
                                      flex: 1,
                                      child: Center(

                                        child:  new Container(
                                            alignment: Alignment.center,

                                            child:new Row(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Image.asset(
                                                    AppImages.IMAGE_APP_LOGO_WITH_TEXT,
                                                    height: 30,
                                                  ),
                                                  // SizedBox(
                                                  //   width: AppConstants.SMALL_SPACE ,
                                                  // ),
                                                  // Text(
                                                  //   'SunaCare',
                                                  //   maxLines: 1,
                                                  //   textAlign: TextAlign.center,
                                                  //   style: getStyleTitle(context).copyWith( color: colorPencilBlack, fontWeight: AppFont.fontWeightSemiBold, fontSize: 35),
                                                  // )
                                                ])

                                        ),
                                      ),

                                    ),

                                  ],
                                ),
                              ),
                            ),

                          ),
                          new Expanded(
                            flex: 3,
                            child: new Container(
                              width: width,
                              color: bgColor,//
                              child: DecoratedBox(

                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(AppImages.IMAGE_BG_SPLASH_SCREEN),
                                      fit: BoxFit.fill),
                                ),),
                            ),

                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

      )),
    );
  }


}
