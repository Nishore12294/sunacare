import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/VerifyAccessToken/Verify_Access_Token_Resp.dart';
import 'package:suna_care/core/data/remote/request_response/profile/GetPreferredLangResponse.dart';
import 'package:suna_care/ui/changepassword/change_password_screen.dart';
import 'package:suna_care/ui/home/home_screen.dart';
import 'package:suna_care/ui/register/pending_approval_screen.dart';
import 'package:suna_care/ui/register/signup_screen.dart';
import 'package:suna_care/utils/AppLanguage.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_globals.dart';
import 'package:suna_care/utils/app_localizations.dart';

import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/core/data/remote/request_response/login/response_login.dart';
import 'package:suna_care/core/notifier/login_notifier.dart';

import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';

import '../utils/app_custom_icon.dart';
import 'forgotpassword/forgot_password_screen.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = '/login';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final log = getLogger('LoginScreen');
  final _keyValidationForm = GlobalKey<FormState>(debugLabel: '_loginkey');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  BuildContext loginContext;

  @override
  void initState() {
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: colorBlack, // navigation bar color
      statusBarColor: colorLightGray, // status bar color
    ));

    //SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    setInitialData(); //
  }

  setInitialData() async {
    firebaseCloudMessaging_Listeners(context);
  }

  @override
  Widget build(BuildContext context) {
    loginContext = context;

    return ChangeNotifierProvider<LoginNotifier>(
      builder: (context) => LoginNotifier(),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: colorLitePinkBG,
         image: DecorationImage(
             image: AssetImage(
               AppImages.IMAGE_LOGIN_BG_WITH_CIRCLE,
             ),
             fit: BoxFit.fill),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            key: _scaffoldKey,
            body: Consumer<LoginNotifier>(
              builder: (context, loginNotifier, _) => Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  ModalProgressHUD(
                   // color: colorDarkRed,
                      color: Colors.transparent,
                    inAsyncCall: loginNotifier.isLoading,
                    child: GestureDetector(
                      onTap: () {
                        FocusScope.of(context).requestFocus(new FocusNode());

                      },
                      child: Container(
                          //alignment: Alignment.center,
                          child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[

                                    Expanded(
                                      flex: 2 ,
                                      child:  _buildWidgetImageLogo(),

                                    ),

                                    Expanded(
                                      flex: 4 ,
                                      child: SingleChildScrollView(

                                          physics: BouncingScrollPhysics(),


                                      child: _buildWidgetLoginCard(loginNotifier),
                                    ),
                                    ),

//                                    Expanded(
//                                      flex: 1 ,
//
//                                        child: Container(
//                                          width: getScreenWidth(context),
//                                          height: getScreenHeight(context)/3,
//                                          color: colorWhite,//
//                                          child: DecoratedBox(
//
//                                            decoration: BoxDecoration(
//                                              image: DecorationImage(
//                                                  image: AssetImage(AppImages.IMAGE_LOGIN_BG),
//                                                  fit: BoxFit.fill),
//                                            ),),
//                                        ),
//                                    ),


                                  ],
                                ),
                              )

                          )),
                ],
              ),
            ),
//            bottomNavigationBar: Consumer<LoginNotifier>(
//                builder: (context, loginNotifier, _) =>
//                    //_buildSignUpLabel(loginNotifier)
//            )

            //_buildSignUpLabel(),
            ),
      ),
    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////
  Widget _buildWidgetImageLogo() {
    return Container(
      height: getScreenHeight(context) / 6,
      //padding: EdgeInsets.only(top: 64, bottom: 32),
      alignment: Alignment.topCenter,
     child:new Column(
         mainAxisAlignment: MainAxisAlignment.center,
         crossAxisAlignment: CrossAxisAlignment.center,
         //mainAxisSize: MainAxisSize.min,
         children: <Widget>[
           topHeader(),
           SizedBox(
             height: AppConstants.SIDE_MARGIN*1.7,
           ),
           Text(
             //AppConstants.HELLO,
             AppLocalizations.of(context).translate('hello')+",",
             textAlign: TextAlign.center,
             style: getStyleBody1(context).copyWith(
                 color: colorPencilBlack,
                 fontWeight: AppFont.fontWeightSemiBold,
                 fontSize: 25
             ),

           ),
           SizedBox(
             height: AppConstants.SIDE_MARGIN/2,
           ),
           Text(
            // AppConstants.PLEASE_LOGIN,
             AppLocalizations.of(context).translate('pleaseLogIn'),
             textAlign: TextAlign.center,
             style: getStyleBody1(context).copyWith(
                 color: colorPencilBlack,
                 fontWeight: AppFont.fontWeightSemiBold,
                 fontSize: 25
             ),

           ),
         ]),

    );
  }

  Widget topHeader(){
    return  new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
            flex: 1,
            child:Container(
              height: 20,
            )
        ),
        Expanded(
          flex: 1,
          child:Text(
            //AppConstants.LOGIN,
            AppLocalizations.of(context).translate('login'),
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',
            ),

          ),
        ),

        Expanded(
          flex: 1,
          child: Container(

            child:Image.asset(
              AppImages.IMAGE_APP_LOGO,
              // width: getScreenWidth(context) / 4,
              height: 20,
            ),),

        ),
      ],
    );
  }

  Widget _buildWidgetLoginCard(LoginNotifier loginNotifier) {
    if (balanceHit == 1) {
      //_checkUserCred(loginNotifier);
      balanceHit = balanceHit + 1;
    }

    return Column(
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.only(
                top: 5.0, left: 30.0, right: 30.0, bottom: 8.0),
            child: Theme(
              data: ThemeData(
                  hintColor: colorBlack,
                  primaryColor: colorDarkRed,
                  primaryColorDark: colorLiteOrange),
              child: Form(
                key: _keyValidationForm,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                        controller: loginNotifier.textEditEmailId,
                        focusNode: loginNotifier.emailFocus,
                        keyboardType: TextInputType.emailAddress,
                        //textCapitalization: TextCapitalization.sentences,
                        textInputAction: TextInputAction.next,
                        validator: _validateNameField,
                        style: getFormStyleText(context),
                        onFieldSubmitted: (String value) {
                          FocusScope.of(context).requestFocus(loginNotifier.passwordFocus);
                        },
                        decoration: _buildTextDecoration(
                            //AppConstants.ENTER_EMAIL_ADDRESS
                            AppLocalizations.of(context).translate('enterEmailAddress')
                        ).copyWith(
                          fillColor: colorWhite,
                          prefixIcon: Image.asset(
                            AppImages.IMAGE_EMAIL,
                            // width: getScreenWidth(context) / 4,
                            width: 5,
                            height: 5,
                            ),
                              focusedBorder: _buildOutlineInputBorder(),
                              enabledBorder: _buildOutlineInputBorder(),
                        )
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    TextFormField(
                      controller: loginNotifier.textEditPassword,
                      focusNode: loginNotifier.passwordFocus,
                      obscureText: !isPasswordVisible,
                      keyboardType: TextInputType.text,
                      validator: _validateNameField,
                      textInputAction: TextInputAction.done,
                      style: getFormStyleText(context),

                      decoration:
                          _buildTextDecoration(
                             // AppConstants.ENTER_PASSWORD
                              AppLocalizations.of(context).translate('enterPassword'),
                          ).copyWith(
                        fillColor: colorWhite,
                        prefixIcon: Image.asset(
                        AppImages.IMAGE_PASSWORD,
                        // width: getScreenWidth(context) / 4,
                          width: 5,
                        height: 5,
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(isPasswordVisible
                              ? Icons.visibility
                              : Icons.visibility_off),
                          onPressed: () {
                            setState(() {
                              isPasswordVisible = !isPasswordVisible;
                            });
                          },
                        ),
                        focusedBorder: _buildOutlineInputBorder(),
                        enabledBorder: _buildOutlineInputBorder(),
                      ),
                      onFieldSubmitted: (String value) {
                        validationAndAPICall(loginNotifier);
                      },
                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        GestureDetector(

                          onTap:()=>_onClickTextForgotPassword(),
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(0.0, 0.0, 13.0, 0.0),
                            child:        Text(
                              //AppConstants.FORGOT_PASSWORD + '?',
                              AppLocalizations.of(context).translate('forgotPassword')+ '?',
                              style: getStyleBody1(context).copyWith(
                                  color: colorLiteOrange,
                                  fontWeight: AppFont.fontWeightSemiBold),
                              textAlign: TextAlign.end,
                            ),
                          )

                        ),

                      ]),

                    SizedBox(
                      height: AppConstants.SIDE_MARGIN*1.5,
                    ),
                    ButtonTheme(
                      minWidth: double.infinity,

                      child: RaisedButton(

                        highlightElevation: 8.0,
                        onPressed: () {
                          //Language
                          //var appLanguage = Provider.of<AppLanguage>(context);
                          //appLanguage.changeLanguage(Locale("en"));

                          //Homescreen
                          //Navigator.pushNamed(context, HomeScreen.routeName);

                         validationAndAPICall(loginNotifier);
                        },
                        color: colorLiteOrange,
                        textColor: colorWhite,
                        elevation: 1,
                        padding: EdgeInsets.only(
                            top: AppConstants.SIDE_MARGIN / 1.5,
                            bottom: AppConstants.SIDE_MARGIN / 1.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                        ),
                        child: Text(
                          //AppConstants.LOGIN,
                          AppLocalizations.of(context).translate('login'),
                          style: getStyleButtonText(context).copyWith(
                              color: colorWhite,
                              letterSpacing: 1,
                              fontWeight: AppFont.fontWeightBold),
                        ),
                      ),


                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),

                   Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                     // AppConstants.DONT_HAVE_AN_ACCOUNT,
                      AppLocalizations.of(context).translate('dontHaveAnAccount'),
                      style: getStyleBody1(context).copyWith(
                          color: colorPencilBlack,
                          fontWeight: AppFont.fontWeightSemiBold),
                      textAlign: TextAlign.end,
                    ),
                    GestureDetector(
                      onTap:()=>_onClickSignUpScreen(loginNotifier),
                      child:   Text(
                       // " "+AppConstants.SIGN_UP_HERE+".",
                        " "+AppLocalizations.of(context).translate('signUpHere')+".",
                        style: getStyleBody1(context).copyWith(
                            color: colorLiteOrange,
                            fontWeight: AppFont.fontWeightSemiBold),
                        textAlign: TextAlign.end,
                      ),
                    )

                  ]),

                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }



  void firebaseCloudMessaging_Listeners(BuildContext context) {
    if (Platform.isIOS) iOSPermission();

    try {
      _firebaseMessaging.getToken().then((token) async {
        print("token in login:==> ${token}");
        //saveToken(token);
        //Store local data for Auto login
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String fcmToken = prefs.getString("FCM_TOKEN")??"";

        if(token != "" ) {
          prefs.setString("FCM_TOKEN", token);
        }

        debugPrint("FCM TOKEN in pref :==>"+fcmToken);
      });


    } on Exception catch (e) {
      print("token:==> ${e.toString()}");
      // TODO
    }
  }

  void iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      //print("Settings registered : $settings");
    });
  }


  validationAndAPICall(LoginNotifier loginNotifier){
    if(loginNotifier.textEditEmailId.text.toString() == "" && loginNotifier.textEditPassword.text.toString() == ""){
      showDialog(
        context: context,
        builder: (_) => AlertOverlay( AppLocalizations.of(context).translate('signIn'),
           // AppConstants.ENTER_EMAIL_PASSWORD,
            AppLocalizations.of(context).translate('pleaseEnterEmailAndPassword'),
            AppLocalizations.of(context).translate('okay')),
      );

    }else if(loginNotifier.textEditEmailId.text.toString() == "" ){
      showDialog(
        context: context,
        builder: (_) => AlertOverlay( AppLocalizations.of(context).translate('signIn'),
           // AppConstants.ENTER_EMAILID,
            AppLocalizations.of(context).translate('pleaseEnterYourEmailAddress'),
            AppLocalizations.of(context).translate('okay')),
      );

    }else if(loginNotifier.textEditPassword.text.toString() == ""){
      showDialog(
        context: context,
        builder: (_) => AlertOverlay( AppLocalizations.of(context).translate('signIn'),
            //AppConstants.VD_ENTER_PASSWORD,
            AppLocalizations.of(context).translate('pleaseEnterYourPassword'),
            AppLocalizations.of(context).translate('okay')),
      );

    }else{
      bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(loginNotifier.textEditEmailId.text.toString());
      if(!emailValid) {
        showDialog(
          context: context,
          builder: (_) =>
              AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                 // AppConstants.ENTER_INVALID_EMAIL_PASSWORD,
                  AppLocalizations.of(context).translate('youHaveEnteredInvalidEmailPassword'),
                   AppLocalizations.of(context).translate('okay')),
        );
      }else {
        FocusScope.of(context).requestFocus(FocusNode());
        _onClickButtonLogIn(loginNotifier);

      }
    }
  }

  OutlineInputBorder _buildOutlineInputBorder(){
  return  OutlineInputBorder(
  borderSide: BorderSide(
  color: colorWhite,
  //color: Colors.transparent,
  ),
  borderRadius: const BorderRadius.all(

  const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
  ),
  );
  }
  InputDecoration _buildTextDecoration(String email) {
    return InputDecoration(
        isDense: true,
        hintText: email,
        alignLabelWithHint: true,
        filled: true,
        contentPadding: EdgeInsets.all(16));
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////
  void _onClickButtonLogIn(LoginNotifier loginNotifier) async {
    print('Action Tap: LogIn button');

    print('Login screen: isBioAuthAvailable:==>'+loginNotifier.isBioAuthAvailable.toString());

    //Api: 1st
    LoginResponse loginResponse = await loginNotifier.callApiLoginUser();


    if (loginResponse != null) {
      debugPrint("loginResponse  response:==>"+loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus.toString());

      if(loginResponse.webMethodOutputRows != null && loginResponse.webMethodOutputRows.webMethodOutputRows[0] != null && loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus != null) {
        if(loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "Valid"){
          //Save user access token



          //Call Verify Token API
          VerifyAccessTokenResponse verifyAccessTokenResponse = await loginNotifier.callApiVerifyAccessToken(loginResponse.webMethodOutputRows.webMethodOutputRows[0].accessToken);
          if (verifyAccessTokenResponse != null) {
          if(verifyAccessTokenResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "Valid") {
            //Navigator.pushNamed(context, HomeScreen.routeName);

            //Store the credentials
            loginNotifier.saveUserCredential(
                loginResponse.webMethodOutputRows.webMethodOutputRows[0]
                    .accessToken);
            String accessToken = await AppSharedPreference().getUserToken();
            log.d('saved accessToken value from SP :==> ' + accessToken);

            //Get Preferred Language
            GetPreferredLangResponse getPreferredLangResponse = await loginNotifier
                .callApiGetPreferredLang(accessToken);
            if (getPreferredLangResponse != null
                && getPreferredLangResponse.webMethodOutputRows != null
                && getPreferredLangResponse.webMethodOutputRows
                    .webMethodOutputRows[0] != null
                && getPreferredLangResponse.webMethodOutputRows
                    .webMethodOutputRows[0].preferredLanguage != null) {
              var appLanguage = Provider.of<AppLanguage>(context);

              if (getPreferredLangResponse.webMethodOutputRows
                  .webMethodOutputRows[0].preferredLanguage == "English") {
                await AppSharedPreference().saveBooleanValue(
                    AppConstants.KEY_LANG_ENGLISH, true);
                appLanguage.changeLanguage(Locale("en"));
              } else {
                await AppSharedPreference().saveBooleanValue(
                    AppConstants.KEY_LANG_ENGLISH, false);
                appLanguage.changeLanguage(Locale("pl"));
              }


              bool isEngLanguage = await AppSharedPreference().getBoolValue(
                  AppConstants.KEY_LANG_ENGLISH);
              debugPrint("isEngLanguage :==>" + isEngLanguage.toString());

              globalLanguageCode = isEngLanguage?"en":"pl";
              debugPrint("globalLanguageCode in login screen :==>"+globalLanguageCode);
            }

            //Store local data for Auto login
            SharedPreferences prefs = await SharedPreferences.getInstance();
            String firstLoginValue = prefs.getString(
                AppConstants.KEY_LANG_FIRST_LOGIN) ?? "";
            debugPrint("firstLoginValue :==>"+firstLoginValue);
            if(firstLoginValue == "" ) {
              prefs.setString(AppConstants.KEY_LANG_FIRST_LOGIN, "1");
             }
            if(loginResponse.webMethodOutputRows.webMethodOutputRows[0].isFirstTimeLogin == "Yes" ) {

              Navigator.pushNamed(context, ChangePasswordScreen.routeName);
              //Navigator.pushNamed(context, HomeScreen.routeName);
            }else{
              Navigator.pushNamed(context, HomeScreen.routeName);
            }
          }else if(verifyAccessTokenResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "Blacklisted"){
             Navigator.pushNamed(context, PendingApprovalScreen.routeName);
          }else{
            if(verifyAccessTokenResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "InvalidToken"){
              showDialog(
                  context: context,
                  builder: (_) =>
                      AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                          //AppConstants.TOKEN_IS_INVALID,
                          AppLocalizations.of(context).translate('tokenIsInvalid'),
                           AppLocalizations.of(context).translate('okay')));
            }else if(verifyAccessTokenResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "MultipleTokenMatch"){
              showDialog(
                  context: context,
                  builder: (_) =>
                      AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                          //AppConstants.TOKEN_IS_VALID_MATCH_MULTI_CG_REC,
                          AppLocalizations.of(context).translate('tokenValidMatchMultipleCGRecords'),
                           AppLocalizations.of(context).translate('okay')));
            } else if(verifyAccessTokenResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "Blacklisted"){
              showDialog(
                  context: context,
                  builder: (_) =>
                      AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                         // AppConstants.TOKEN_VALID_CG_INACTIVE_BLACKLISTED,
                           AppLocalizations.of(context).translate('tokenValidCGBlocklistedInactive'),
                           AppLocalizations.of(context).translate('okay')
                      ));
            }else if(verifyAccessTokenResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "Resigned"){
              showDialog(
                  context: context,
                  builder: (_) =>
                      AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                          //AppConstants.TOKEN_VALID_CG_INACTIVE_RESIGNED,
                           AppLocalizations.of(context).translate('tokenValidCGResignedInactive'),
                           AppLocalizations.of(context).translate('okay')
                      ));
            }else if(verifyAccessTokenResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "Unexpected"){
              showDialog(
                  context: context,
                  builder: (_) =>
                      AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                         // AppConstants.UNEXPECTED_ERROR,
                           AppLocalizations.of(context).translate('unexpectedError'),
                           AppLocalizations.of(context).translate('okay')
                      ));
            }else if(verifyAccessTokenResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == AppConstants.NO_INTERNET_CONNECTION){
              showDialog(
                  context: context,
                  builder: (_) =>
                      AlertOverlay(
                           //AppConstants.NO_INTERNET_CONNECTION_TITLE,
                           //AppConstants.NO_INTERNET_CONNECTION,
                           AppLocalizations.of(context).translate('noNetworkConnection'),
                           AppLocalizations.of(context).translate('noInternetConnection'),
                           AppLocalizations.of(context).translate('okay')
                      ));
            }else{
              showDialog(
                  context: context,
                  builder: (_) =>
                      AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                          loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus,
                           AppLocalizations.of(context).translate('okay')));
            }
          }

          }
            //Navigator.pushNamed(context, HomeScreen.routeName);
        }else if(loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "InvalidEmail"){
            showDialog(
                context: context,
                builder: (_) =>
                    AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                       // AppConstants.ENTER_INVALID_EMAIL_PASSWORD,
                         AppLocalizations.of(context).translate('youHaveEnteredInvalidEmailPassword'),
                         AppLocalizations.of(context).translate('okay')
                    ));
          }else if(loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "MultipleEmailMatch"){
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                     // AppConstants.EMAIL_IS_VALID_MATCH_MULTI_CG_REC,
                       AppLocalizations.of(context).translate('emailValidButMatchToMultiCGRec'),
                       AppLocalizations.of(context).translate('okay')
                  ));
         } else if(loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "Resigned"){
            showDialog(
                context: context,
                builder: (_) =>
                    AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                       // AppConstants.EMAIL_VALID_CG_INACTIVE_RESIGNED,
                         AppLocalizations.of(context).translate('emailOrPasswordValidCGInActiveAndResigned'),
                         AppLocalizations.of(context).translate('okay')
                    ));
          }else if(loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "InvalidPassword"){
            showDialog(
                context: context,
                builder: (_) =>
                    AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                        //AppConstants.ENTER_INVALID_EMAIL_PASSWORD,
                         AppLocalizations.of(context).translate('pleaseEnterValidEmailAddress'),
                         AppLocalizations.of(context).translate('okay')
                    ));
          }else if(loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "Blacklisted"){
            showDialog(
                context: context,
                builder: (_) =>
                    AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                        //AppConstants.EMAIL_PASSWORD_VALID_CG_INACTIVE_BLACKLISTED,
                        AppLocalizations.of(context).translate('yourAccountBlockListedPleaseContactAdmin'),
                         AppLocalizations.of(context).translate('okay')));
          }else if(loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == "Unexpected"){
            showDialog(
                context: context,
                builder: (_) =>
                    AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                        loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus,
                         AppLocalizations.of(context).translate('okay')));
          }else if(loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus == AppConstants.NO_INTERNET_CONNECTION){
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(
                     // AppConstants.NO_INTERNET_CONNECTION_TITLE,
                      //AppConstants.NO_INTERNET_CONNECTION,
                      AppLocalizations.of(context).translate('noNetworkConnection'),
                      AppLocalizations.of(context).translate('noInternetConnection'),
                       AppLocalizations.of(context).translate('okay')));
         }else{
           showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                      loginResponse.webMethodOutputRows.webMethodOutputRows[0].cgStatus,
                       AppLocalizations.of(context).translate('okay')));
          }

      }else{

        showDialog(
            context: context,
            builder: (_) =>
                AlertOverlay( AppLocalizations.of(context).translate('signIn'),
                    //AppConstants.SOME_THING_WENT_WRONG,
                    AppLocalizations.of(context).translate('somethingWentWrongPleaseTryAgainLater'),
                     AppLocalizations.of(context).translate('okay'))
        );

      }


    }  else {
      //failure case: of login api
      _showSnackBarMessage(  AppLocalizations.of(context).translate('somethingWentWrongPleaseTryAgainLater'));
    }
  }

  void _onClickTextForgotPassword() {
    print('Action Tap: ForgotPassword');
    Navigator.pushNamed(context, ForgotPasswordScreen.routeName);

    //Language
    // var appLanguage = Provider.of<AppLanguage>(context);
    // appLanguage.changeLanguage(Locale("ar"));
  }

  void _onClickSignUpScreen(LoginNotifier loginNotifier) {
    print('Action Tap: _onClickSignUpScreen');

    Navigator.pushNamed(context, SignUpScreen.routeName);
  }

  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  String _validateNameField(String value) {
    return value.trim().length < 1 ? 'Field can\'t be empty' : null;
  }

  void _checkUserCred(LoginNotifier loginNotifier) async {
    String email = await AppSharedPreference()
        .getStringValue(AppConstants.KEY_USER_EMAIL_ID);
    String password = await AppSharedPreference()
        .getStringValue(AppConstants.KEY_USER_PASSWORD);
    if (email != null &&
        password != null &&
        email.isNotEmpty &&
        password.isNotEmpty) {
      loginNotifier.textEditEmailId.text = email;
      loginNotifier.textEditPassword.text = password;
      _onClickButtonLogIn(loginNotifier);
    }
  }


}
