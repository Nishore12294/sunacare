import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgResp.dart';
import 'package:suna_care/core/notifier/profile_notifier.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:intl/intl.dart';

import 'notification_settings_screen.dart';

class NotificationListScreen extends StatefulWidget {
  static const routeName = '/notification_list_screen';

  NotificationListScreen();

  @override
  _NotificationListScreenState createState() =>
      _NotificationListScreenState();
}

class _NotificationListScreenState extends State<NotificationListScreen> {
  final log = getLogger('NotificationListScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  final int subCategoryId;
  ProfileNotifier offersScreenNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  var _keyValidationForm = GlobalKey<FormState>();
  final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  _NotificationListScreenState({@required this.subCategoryId});

  @override
  void initState() {
    super.initState();
    setInitialData(); //aa
  }

  setInitialData() async {}

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ProfileNotifier>(
      builder: (context) => ProfileNotifier(context,isFromWhichScreen:"notificationListScreen"),
      child: Scaffold(
        backgroundColor: colorYoutubeGrey,
        key: _scaffoldKey,

        body: Consumer<ProfileNotifier>(
          builder: (context, profileNotifier, _) => ModalProgressHUD(
            color: colorTransparent,
            inAsyncCall: profileNotifier.isLoading,
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Container(
                color: colorLitePinkViewProfileBG,
                  child:  Column(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[

                        Container(
                        color: colorLitePinkViewProfileBG,
                        height: AppConstants.SIDE_MARGIN * 2.1,
                        ),

                        topHeader(profileNotifier),

                        _buildWidgetMainContent(context, profileNotifier),
                      ],
                    ),
                  )),
            ),
          ),
      ),
    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  Widget topHeader(ProfileNotifier profileNotifier) {
    return Container(
      height: 24,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: InkWell(

              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      backButtonPressed();
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Image.asset(
                        AppImages.IMAGE_BACK,
                        height: 25,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: AppConstants.SIDE_MARGIN / 1.5,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, NotificationSettingsScreen.routeName);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Image.asset(
                        AppImages.IMAGE_NOTIFICATION_SETTINGS,
                        height: 23,
                      ),
                    ),
                  ),
                ],
              ),

            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              AppConstants.NOTIFICATIONS,
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                height: 20,
              ),
            ),
          ),
        ],
      )
    );

  }

  Widget _buildWidgetMainContent(BuildContext context, ProfileNotifier profileNotifier) {
    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);
    offersScreenNotifier = profileNotifier;

      return    Container(

        child: Expanded(
          child:ListView.builder(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,

              //padding: EdgeInsets.all(AppConstants.SIDE_MARGIN / 2),
              itemCount: 10,
              itemBuilder: (context, position) {
                return buildWidgetNotificationsItem(
                    context,
                    position,
                    imageWidth,
                    cardRadio);
              }),
        ),
      );




  }

  Widget buildWidgetNotificationsItem(BuildContext context, int position,double imageWidth,  Radius cardRadio) {
    List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];



    return GestureDetector(
      onTap: () async {
        log.d('onPress: Offers Item');
      },
      child: Container(
        color: colorLitePinkViewProfileBG,
        padding: EdgeInsets.fromLTRB(10.0, 7.0, 7.0, 10.0),
        child:Card(

          elevation: 1.5,
          color: colorWhite,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.all(cardRadio)),
          child: Container(
            padding: EdgeInsets.all(AppConstants.SIDE_MARGIN / 1.5),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                SizedBox(
                  width: AppConstants.SIDE_MARGIN / 2,
                ),
                Flexible(
                    fit: FlexFit.tight,
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: AppConstants.SIDE_MARGIN / 4,
                          ),
                          //Notification title
                          Row(

                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [


                              Image.asset(
                                AppImages.IMAGE_HOME_SCREEN_PROFILE_GRAY,
                                height: 20,
                              ),
                              SizedBox(
                                width: AppConstants.SIDE_MARGIN/1.5 ,
                              ),
                              Expanded(
                                flex: 1,
                                child: Text(
                                "Availability",
                                overflow: TextOverflow.ellipsis,
                                softWrap: false,
                                maxLines: 2,
                                style: getFormTitleStyle(context).copyWith(
                                    color: colorBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 14
                                ),
                              ),),

                              Text(
                                "1 min",
                                overflow: TextOverflow.ellipsis,
                                softWrap: false,
                                maxLines: 1,
                                style: getFormTitleStyle(context).copyWith(
                                    color: colorBlack,
                                    fontWeight: AppFont.fontWeightRegular,
                                    fontSize: 13
                                ),
                              ),






                            ],
                          ),


                          //Description
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                flex:1,

                                child:  Padding(
                                  padding: EdgeInsets.fromLTRB(32.0, 5.0, 0.0, 2.0),
                                  child: Text(
                                    "Your haven't set your unavailable dates. Go to availability screen and set your unavilability screen and set your unavailable dates",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: false,
                                    style: getFormTitleStyle(context).copyWith(
                                        color: colorBlack,
                                        fontWeight: AppFont.fontWeightRegular,
                                        fontSize: 13
                                    ),
                                  ),
                                ),),

                              SizedBox(
                                width: AppConstants.SIDE_MARGIN / 1.5,
                              ),




                            ],
                          ),


                          SizedBox(
                            height: AppConstants.SIDE_MARGIN / 4,
                          ),

                        ],


                      ),
                    ))
              ],
            ),
          ),
        ),
      )

    );
  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }


}

