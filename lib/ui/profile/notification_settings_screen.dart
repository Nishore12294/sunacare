import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/notification_settings/set_notification/SetNotificationResponse.dart';
import 'package:suna_care/core/notifier/notifications_notifier.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/core/notifier/profile_notifier.dart';
import 'package:suna_care/ui/changepassword/change_password_screen.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';
//import 'package:ishare/screens/custom_expansion_tile.dart' as custom;
import 'package:suna_care/ui/offers/custom_expansion_tile.dart' as custom;
import 'package:suna_care/utils/widget_helper/custom_switch.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';


class NotificationSettingsScreen extends StatefulWidget {
  static const routeName = '/notification_settings_screen';
  String id = "";

  NotificationSettingsScreen({this.id});

  @override
  _NotificationSettingsScreenState createState() => _NotificationSettingsScreenState();
}

class _NotificationSettingsScreenState extends State<NotificationSettingsScreen> {
  final log = getLogger('NotificationSettingsScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  String offerGuid;
  NotificationsNotifier notificationsNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  _NotificationSettingsScreenState();

  @override
  void initState() {
    super.initState();
    setInitialData();
  }

  setInitialData() async {

    if (widget != null) {
      offerGuid = widget.id;
      log.d('id: ==>'+ widget.id.toString());
      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>"+accessToken);
    }




  }


  @override
  Widget build(BuildContext context) {

      return ChangeNotifierProvider<NotificationsNotifier>(
        builder: (context) => NotificationsNotifier(context,isFromWhichScreen:"notificationSettingsScreen",offerGuid:offerGuid),
        child: Scaffold(
          backgroundColor: colorLitePinkViewProfileBG,
          key: _scaffoldKey,

          body: Consumer<NotificationsNotifier>(
            builder: (context, notificationsNotifier, _) => ModalProgressHUD(
              inAsyncCall: notificationsNotifier.isLoading,
              child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Container(
                    //color: colorLitePinkViewProfileBG,
                   child:  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[


                        Expanded(
                          flex: 1,
                          child:  Column(
                              children: <Widget>[

                                SizedBox(

                                  height: AppConstants.SIDE_MARGIN * 2,
                                  child:Container(
                                    color: colorLitePinkBG,
                                  ),
                                ),
                                topHeader(notificationsNotifier),

                                SizedBox(

                                  height: AppConstants.SIDE_MARGIN ,
                                  child:Container(
                                    color: colorLitePinkBG,
                                  ),
                                ),

                          Expanded(
                              flex: 1,
                              child:SingleChildScrollView(
                                child: _buildWidgetMainContent(context, notificationsNotifier),
                              ),

                          ),

                              ]),
                        ),
                        SizedBox(

                          height: AppConstants.SIDE_MARGIN,
                        ),
                      ],
                    ),

                  )),
            ),
          ),
        ),
      );


  }

  /////////////////////
  /*  build helpers */
  ////////////////////


  Widget topHeader(NotificationsNotifier notificationsNotifier) {
    return Container(
      color: colorLitePinkBG,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child:  InkWell(
              onTap: () {
                Navigator.canPop(context) ? Navigator.pop(context) : '';
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child:Image.asset(
                  AppImages.IMAGE_BACK,
                  height: 25,
                ),
              ),
            ),


          ),
          Expanded(
            flex: 3,
            child: Text(
              //AppConstants.NOTIFICATIONS_SETTINGS,
              AppLocalizations.of(context).translate('notificationSettings'),
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                height: 20,
              ),
            ),
          ),
        ],
      ),

    );
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  Widget _buildWidgetMainContent(BuildContext context, NotificationsNotifier notificationsNotif) {
    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);
    notificationsNotifier = notificationsNotif;

       return buildWidgetViewNotifiSettings(
        context,
        imageWidth,
        cardRadio);
  }

  Widget buildWidgetViewNotifiSettings(BuildContext context,double imageWidth,  Radius cardRadio) {
    List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];

      return Stack(
              children: [
                Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 16.0, left: 30.0, right: 30.0, bottom: 8.0),
                          child: Theme(
                            data: ThemeData(
                                hintColor: colorBorderLine,
                                primaryColor: colorGradientPink,
                                primaryColorDark: colorGradientPink),
                            child: Form(


                              child:Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[



                                  //////////////// Notification settings ///////////////////
                                  Text(
                                    //AppConstants.PUSH_NOTIFICATIONS,
                                    AppLocalizations.of(context).translate('pushNotifications'),
                                    textAlign: TextAlign.center,
                                    style: getStyleBody1(context).copyWith(
                                        color: colorPencilBlack,
                                        fontWeight: AppFont.fontWeightSemiBold,
                                        fontSize: 21.5),
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN*1.5,
                                  ),

                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[

                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                //AppConstants.AVAILABILITY_SET,
                                                AppLocalizations.of(context).translate('availabilitySet'),
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightRegular,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap:()=> _availabilitySetToggle(notificationsNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched:notificationsNotifier.availabilitySetSwitch),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              //AppConstants.PROFILE_UPLOAD,
                                              AppLocalizations.of(context).translate('profileUpload'),
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap:()=> _profileUploadToggle(notificationsNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched: notificationsNotifier.profileUploadSwitch),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              AppLocalizations.of(context).translate('updateOnAvailability'),
                                             // AppConstants.UPDATE_ON_AVAILABILITY,
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                           onTap:()=> _updateOnAvailabilityToggle(notificationsNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched: notificationsNotifier.updateOnAvailabilitySwitch),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              //AppConstants.NEW_OFFERS,
                                              AppLocalizations.of(context).translate('newOffers'),
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap:()=> _newOffersToggle(notificationsNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched: notificationsNotifier.newOffersToggleSwitch),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),






                                ],
                              ),


                            ),
                          ),
                        ),
                      ),

                    ])
              ],
            );






  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void _availabilitySetToggle(NotificationsNotifier notificationsNotifier) {

    bool currentToggleValue = notificationsNotifier.availabilitySetSwitch;
    debugPrint("notificationsNotifier.availabilitySet:==>"+notificationsNotifier.availabilitySetSwitch.toString());
    setNotificationProcess("AvailabilitySet",!currentToggleValue?"On":"Off");
    
    

  }
  void _profileUploadToggle(NotificationsNotifier notificationsNotifier) {

    //notificationsNotifier.profileUploadSwitch = !notificationsNotifier.profileUploadSwitch;
    bool currentToggleValue = notificationsNotifier.profileUploadSwitch;
    debugPrint("notificationsNotifier.profileUploadSwitch:==>"+notificationsNotifier.profileUploadSwitch.toString());
    setNotificationProcess("ProfileUpload",!currentToggleValue?"On":"Off");

  }

  void _updateOnAvailabilityToggle(NotificationsNotifier notificationsNotifier) {

    //notificationsNotifier.updateOnAvailabilitySwitch = !notificationsNotifier.updateOnAvailabilitySwitch;
    bool currentToggleValue = notificationsNotifier.updateOnAvailabilitySwitch;
    debugPrint("notificationsNotifier.updateOnAvailabilitySwitch:==>"+notificationsNotifier.updateOnAvailabilitySwitch.toString());
    setNotificationProcess("UpdateOnAvailability",!currentToggleValue?"On":"Off");

  }
  void _newOffersToggle(NotificationsNotifier notificationsNotifier) {
    //notificationsNotifier.newOffersToggleSwitch = !notificationsNotifier.newOffersToggleSwitch;
    bool currentToggleValue = notificationsNotifier.newOffersToggleSwitch;
    debugPrint("notificationsNotifier.newOffersToggleSwitch:==>"+notificationsNotifier.newOffersToggleSwitch.toString());
    setNotificationProcess("NewOffers",!currentToggleValue?"On":"Off");
  }
  
  
  
Future<void> setNotificationProcess(String notificationName,String notificationValue) async {
  String accessToken = await AppSharedPreference().getUserToken();
  debugPrint("accessToken:==>"+accessToken);
  SetNotificationResponse response =  await notificationsNotifier.callApiSetNotification(accessToken,notificationName,notificationValue);


  if(response != null && response.webMethodOutputRows != null
      && response.webMethodOutputRows.webMethodOutputRows[0] != null){

    if(response != null && response.webMethodOutputRows != null
        && response.webMethodOutputRows.webMethodOutputRows[0] != null
        && response.webMethodOutputRows.webMethodOutputRows[0].setCgNotificationStatus == "Success"
    ){

      if(notificationName == "AvailabilitySet"){
        notificationsNotifier.availabilitySetSwitch = !notificationsNotifier.availabilitySetSwitch;
      }else if(notificationName == "ProfileUpload"){
        notificationsNotifier.profileUploadSwitch = !notificationsNotifier.profileUploadSwitch;
      }else if(notificationName == "UpdateOnAvailability"){
        notificationsNotifier.updateOnAvailabilitySwitch = !notificationsNotifier.updateOnAvailabilitySwitch;
      }else if(notificationName == "NewOffers"){
        notificationsNotifier.newOffersToggleSwitch = !notificationsNotifier.newOffersToggleSwitch;
      }

      /*showDialog(
        context: context,
        builder: (_) => AlertOverlay(
            AppLocalizations.of(context).translate('notificationSettings'),
            AppLocalizations.of(context).translate('youHaveSuccessfullyUpdatedYourNotificationSetting'),
           // AppConstants.NOTIFICATION_SETTINGS_UPDATED_SUCCESSFULLY,
            AppLocalizations.of(context).translate('okay')),
      );*/

    }else{
      showDialog(
        context: context,
        builder: (_) => AlertOverlay(
            AppLocalizations.of(context).translate('notificationSettings'),
            response.webMethodOutputRows.webMethodOutputRows[0].setCgNotificationStatus??"",
            AppLocalizations.of(context).translate('okay')),
      );
    }

  }else if(response != null && response.outputRowErrors != null
      && response.outputRowErrors.outputRowErrors[0] != null
      && response.outputRowErrors.outputRowErrors[0].textMessage != ""){

    showDialog(
      context: context,
      builder: (_) => AlertOverlay(
          AppLocalizations.of(context).translate('notificationSettings'),
          response.outputRowErrors.outputRowErrors[0].textMessage??"",
          AppLocalizations.of(context).translate('okay')),
    );

  }

}



}



