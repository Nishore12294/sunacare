import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangResponse.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/core/notifier/profile_notifier.dart';
import 'package:suna_care/ui/changepassword/change_password_screen.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/utils/AppLanguage.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/alert_overlay_with_refersh.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_globals.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';
//import 'package:ishare/screens/custom_expansion_tile.dart' as custom;
import 'package:suna_care/ui/offers/custom_expansion_tile.dart' as custom;
import 'package:suna_care/utils/widget_helper/custom_switch.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

import '../../main.dart';


class ChangeLanguageScreen extends StatefulWidget {
  static const routeName = '/change_language_screen';
  String id = "";

  ChangeLanguageScreen({this.id});

  @override
  _ChangeLanguageScreenState createState() => _ChangeLanguageScreenState();
}

class _ChangeLanguageScreenState extends State<ChangeLanguageScreen> {
  final log = getLogger('ChangeLanguageScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  String offerGuid;
  ProfileNotifier profileNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  bool isEngLanguage = false;
  _ChangeLanguageScreenState();

  @override
  void initState() {
    super.initState();
    setInitialData();
  }

  setInitialData() async {

    if (widget != null) {
      offerGuid = widget.id;
      log.d('id: ==>'+ widget.id.toString());
      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>"+accessToken);
    }







  }


  @override
  Widget build(BuildContext context) {

      return ChangeNotifierProvider<ProfileNotifier>(
        builder: (context) => ProfileNotifier(context,isFromWhichScreen:"changeLanguageScreen",offerGuid:offerGuid),
        child: Scaffold(
          backgroundColor: colorLitePinkViewProfileBG,
          key: _scaffoldKey,

          body: Consumer<ProfileNotifier>(
            builder: (context, profileNotifier, _) => ModalProgressHUD(
              inAsyncCall: profileNotifier.isLoading,
              child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Container(
                    color: colorLitePinkViewProfileBG,
                    child:  Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                          Container(
                            color: colorLitePinkViewProfileBG,

                            child:Container(
                              color: colorLitePinkViewProfileBG,
                              child: Column(
                                  children: <Widget>[

                                    SizedBox(

                                      height: AppConstants.SIDE_MARGIN * 2,
                                      child:Container(
                                        color: colorLitePinkBG,
                                      ),
                                    ),
                                    topHeader(profileNotifier),

                                    SizedBox(

                                      height: AppConstants.SIDE_MARGIN ,
                                      child:Container(
                                        color: colorLitePinkBG,
                                      ),
                                    ),

                                    _buildWidgetMainContent(context, profileNotifier),

                                  ]),
                            )

                          ),

                        SizedBox(

                          height: AppConstants.SIDE_MARGIN,
                        ),

                      ],
                    ),
                  )),
            ),
          ),
        ),
      );


  }

  /////////////////////
  /*  build helpers */
  ////////////////////


  Widget topHeader(ProfileNotifier profileNotifier) {
    return Container(
      color: colorLitePinkBG,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child:  InkWell(
              onTap: () {
                Navigator.canPop(context) ? Navigator.pop(context) : '';
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child:Image.asset(
                  AppImages.IMAGE_BACK,
                  height: 25,
                ),
              ),
            ),


          ),
          Expanded(
            flex: 3,
            child: Text(
              //AppConstants.PREFERRED_LANGUAGE,
              AppLocalizations.of(context).translate('preferredLanguage'),
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 22.5,
                fontFamily:'NPSunaGrotesk',),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                height: 20,
              ),
            ),
          ),
        ],
      ),

    );
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  Widget _buildWidgetMainContent(BuildContext context, ProfileNotifier profileNotif)  {
    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);
   profileNotifier = profileNotif;
   //profileNotifier.isEnglishLangSelected = isEngLanguage;


       return buildWidgetViewProfile(
        context,
        imageWidth,
        cardRadio);
  }

  Widget buildWidgetViewProfile(BuildContext context,double imageWidth,  Radius cardRadio) {
    List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];


      return GestureDetector(
        onTap: () {
          log.d('onPress:  changelanguage');

        },
        child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child:Container(
      child:Padding(
        padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
          child :Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                SizedBox(
                  height: AppConstants.SIDE_MARGIN,
                ),

                Text(
                  //AppConstants.CHOOSE_YOUR_PREF_LANGUAGE ,
                  AppLocalizations.of(context).translate('chooseYourPreferredLanguage'),
                  textAlign: TextAlign.center,
                  style: getStyleBody1(context).copyWith(
                      color: colorPencilBlack,
                      fontWeight: AppFont.fontWeightSemiBold,
                      fontSize: 24),
                ),


                SizedBox(
                  height: AppConstants.SIDE_MARGIN,
                ),


                ButtonTheme(
                  minWidth: double.infinity,

                  child: RaisedButton(

                    highlightElevation: 8.0,
                    onPressed: () async {
                      FocusScope.of(context).requestFocus(FocusNode());
                      profileNotifier.isEnglishLangSelected = false;


                      await AppSharedPreference().saveBooleanValue(AppConstants.KEY_LANG_ENGLISH,false);
                      bool isEngLanguage = await AppSharedPreference().getBoolValue(AppConstants.KEY_LANG_ENGLISH);
                      debugPrint("isEngLanguage :==>"+isEngLanguage.toString());
                    },
                    color: (profileNotifier.isEnglishLangSelected)?colorWhite:colorGreen,
                    elevation: 1,
                    padding: EdgeInsets.only(
                        top: AppConstants.SIDE_MARGIN / 1.5,
                        bottom: AppConstants.SIDE_MARGIN       / 1.5),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: colorGreen, width: 1),
                        borderRadius: BorderRadius.all( Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                    ),
                    child: Text(
                      //AppConstants.POLISH,
                      AppLocalizations.of(context).translate('polish'),
                      style: getStyleButtonText(context).copyWith(
                          color:(profileNotifier.isEnglishLangSelected)?colorGreen:colorWhite,
                          letterSpacing: 1,
                          fontWeight: AppFont.fontWeightBold),
                    ),
                  ),


                ),


                SizedBox(
                  height: AppConstants.SIDE_MARGIN/2,
                ),


                ButtonTheme(
                  minWidth: double.infinity,

                  child: RaisedButton(

                    highlightElevation: 8.0,
                    onPressed: () async {
                      FocusScope.of(context).requestFocus(FocusNode());
                      profileNotifier.isEnglishLangSelected = true;


                    },
                    color: (!profileNotifier.isEnglishLangSelected)?colorWhite:colorGreen,
                    elevation: 1,
                    padding: EdgeInsets.only(
                        top: AppConstants.SIDE_MARGIN / 1.5,
                        bottom: AppConstants.SIDE_MARGIN / 1.5),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: colorGreen, width: 1),
                        borderRadius: BorderRadius.all(Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                    ),
                    child: Text(
                      //AppConstants.ENGLISH,
                      AppLocalizations.of(context).translate('english'),
                      style: getStyleButtonText(context).copyWith(
                          color: (!profileNotifier.isEnglishLangSelected)?colorGreen:colorWhite,
                          letterSpacing: 1,
                          fontWeight: AppFont.fontWeightBold),
                    ),
                  ),


                ),



                SizedBox(
                  height: AppConstants.SIDE_MARGIN*2,
                ),


                ButtonTheme(
                  minWidth: double.infinity,

                  child: RaisedButton(

                    highlightElevation: 8.0,
                    onPressed: () async {
                      FocusScope.of(context).requestFocus(FocusNode());

                      //Set preferred language API call
                      setPreferredLanguage();

                    },
                    color: colorLiteOrange,
                    textColor: colorWhite,
                    elevation: 1,
                    padding: EdgeInsets.only(
                        top: AppConstants.SIDE_MARGIN / 1.5,
                        bottom: AppConstants.SIDE_MARGIN / 1.5),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                    ),
                    child: Text(
                      //AppConstants.SAVE,
                      AppLocalizations.of(context).translate('save'),
                      style: getStyleButtonText(context).copyWith(
                          color: colorWhite,
                          letterSpacing: 1,
                          fontWeight: AppFont.fontWeightBold),
                    ),
                  ),


                ),

              ])
      )
            ),
    ),



      );


  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

void setPreferredLanguage()async{



  String accessToken = await AppSharedPreference().getUserToken();
  debugPrint("accessToken:==>"+accessToken);
  debugPrint("profileNotifier.isEnglishLangSelected;:==>"+profileNotifier.isEnglishLangSelected.toString());


  SetPreferredLangResponse response =  await profileNotifier.callApiSetPreferredLang(accessToken, profileNotifier.isEnglishLangSelected?"English":"Polish");

  if(response != null && response.webMethodOutputRows != null
      && response.webMethodOutputRows.webMethodOutputRows[0] != null){

    if(response != null && response.webMethodOutputRows != null
        && response.webMethodOutputRows.webMethodOutputRows[0] != null
        && response.webMethodOutputRows.webMethodOutputRows[0].cgPreferredLanguageStatus == "Success"
    ){



      await AppSharedPreference().saveBooleanValue(AppConstants.KEY_LANG_ENGLISH,profileNotifier.isEnglishLangSelected?true:false);

      bool isEngLanguage = await AppSharedPreference().getBoolValue(AppConstants.KEY_LANG_ENGLISH);
      debugPrint("isEngLanguage :==>"+isEngLanguage.toString());
      //Language
      var appLanguage = Provider.of<AppLanguage>(context);
      appLanguage.changeLanguage(Locale(isEngLanguage?"en":"pl"));

      globalLanguageCode = isEngLanguage?"en":"pl";

      showDialog(
        context: context,
        builder: (_) => AlertOverlayWithRefresh(
            AppLocalizations.of(context).translate('preferredLanguage'),
            AppLocalizations.of(context).translate('youHaveSuccChangedPreferredLanguage'),
            //AppConstants.LANGUAGE_UPDATED_SUCCESSFULLY,
            AppLocalizations.of(context).translate('okay'),""),
      );

    }else{
      showDialog(
        context: context,
        builder: (_) => AlertOverlay(
            AppLocalizations.of(context).translate('preferredLanguage'),
            response.webMethodOutputRows.webMethodOutputRows[0].cgPreferredLanguageStatus??"",
            AppLocalizations.of(context).translate('okay')),
      );
    }

  }else if(response != null && response.outputRowErrors != null
      && response.outputRowErrors.outputRowErrors[0] != null
      && response.outputRowErrors.outputRowErrors[0].textMessage != ""){

    showDialog(
      context: context,
      builder: (_) => AlertOverlay(
          AppLocalizations.of(context).translate('preferredLanguage'),
          response.outputRowErrors.outputRowErrors[0].textMessage??"",
          AppLocalizations.of(context).translate('okay')),
    );

  }
}

 

}



