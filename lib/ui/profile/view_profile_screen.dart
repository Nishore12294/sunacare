import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/core/notifier/profile_notifier.dart';
import 'package:suna_care/ui/changepassword/change_password_screen.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';
//import 'package:ishare/screens/custom_expansion_tile.dart' as custom;
import 'package:suna_care/ui/offers/custom_expansion_tile.dart' as custom;
import 'package:suna_care/utils/widget_helper/custom_switch.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';


class ViewProfileScreen extends StatefulWidget {
  static const routeName = '/view_profile_screen';
  String id = "";

  ViewProfileScreen({this.id});

  @override
  _ViewProfileScreenState createState() => _ViewProfileScreenState();
}

class _ViewProfileScreenState extends State<ViewProfileScreen> {
  final log = getLogger('ViewProfileScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  String offerGuid;
  ProfileNotifier profileNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  _ViewProfileScreenState();

  @override
  void initState() {
    super.initState();
    setInitialData();
  }

  setInitialData() async {

    if (widget != null) {
      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>"+accessToken);
    }




  }


  @override
  Widget build(BuildContext context) {

      return ChangeNotifierProvider<ProfileNotifier>(
        builder: (context) => ProfileNotifier(context,isFromWhichScreen:"viewProfileScreen"),
        child: Scaffold(
          backgroundColor: colorLitePinkViewProfileBG,
          key: _scaffoldKey,

          body: Consumer<ProfileNotifier>(
            builder: (context, profileNotifier, _) => ModalProgressHUD(
              inAsyncCall: profileNotifier.isLoading,
              child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Container(
                    //color: colorLitePinkViewProfileBG,
                   child:  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[


                        Expanded(
                          flex: 1,
                          child:  Column(
                              children: <Widget>[

                                SizedBox(

                                  height: AppConstants.SIDE_MARGIN * 2,
                                  child:Container(
                                    color: colorLitePinkBG,
                                  ),
                                ),
                                topHeader(profileNotifier),

                                SizedBox(

                                  height: AppConstants.SIDE_MARGIN ,
                                  child:Container(
                                    color: colorLitePinkBG,
                                  ),
                                ),

                          Expanded(
                              flex: 1,
                              child:
                                _buildWidgetMainContent(context, profileNotifier),
                          ),

                              ]),
                        ),
                        SizedBox(

                          height: AppConstants.SIDE_MARGIN,
                        ),







                      ],
                    ),

                  )),
            ),
          ),
        ),
      );


  }

  /////////////////////
  /*  build helpers */
  ////////////////////


  Widget topHeader(ProfileNotifier profileNotifier) {
    return Container(
      color: colorLitePinkBG,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child:  InkWell(
              onTap: () {
                Navigator.canPop(context) ? Navigator.pop(context) : '';
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child:Image.asset(
                  AppImages.IMAGE_BACK,
                  height: 25,
                ),
              ),
            ),


          ),
          Expanded(
            flex: 2,
            child: Text(
             // AppConstants.VIEW_PROFILE_TITLE,
              AppLocalizations.of(context).translate('viewProfile'),
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                height: 20,
              ),
            ),
          ),
        ],
      ),

    );
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  Widget _buildWidgetMainContent(BuildContext context, ProfileNotifier profileNotif) {
    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);
    profileNotifier = profileNotif;

       return buildWidgetViewProfile(
        context,
        imageWidth,
        cardRadio);
  }

  Widget buildWidgetViewProfile(BuildContext context,double imageWidth,  Radius cardRadio) {
    List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];

  if(profileNotifier.cgProfileDetailsResponse != null
     && profileNotifier.cgProfileDetailsResponse.webMethodOutputRows != null
      && profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0] != null){
    var experienceInCareStr = profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].experienceInCare;
      return SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Stack(
              children: [
                Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 16.0, left: 30.0, right: 30.0, bottom: 8.0),
                          child: Theme(
                            data: ThemeData(
                                hintColor: colorBorderLine,
                                primaryColor: colorGradientPink,
                                primaryColorDark: colorGradientPink),
                            child: Form(


                              child:Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[

                                  //////////////// Primary details ///////////////////

                                  Text(
                                   // AppConstants.PROFILE_PRIMARY_DETAILS,
                                    AppLocalizations.of(context).translate('primaryDetails'),
                                    textAlign: TextAlign.center,
                                    style: getStyleBody1(context).copyWith(
                                        color: colorPencilBlack,
                                        fontWeight: AppFont.fontWeightSemiBold,
                                        fontSize: 22),
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN*1.5,
                                  ),

                                  //First Name

                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[

                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                //AppConstants.PROFILE_FIRST_NAME,
                                                AppLocalizations.of(context).translate('firstName'),
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightRegular,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                        Expanded(
                                          flex: 2,
                                          child:Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].firstName ??"",
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                softWrap: false,
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightSemiBold,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),

                                  //Second Name

                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[

                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                //AppConstants.PROFILE_SECOND_NAME,
                                                AppLocalizations.of(context).translate('secondName'),
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightRegular,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                        Expanded(
                                          flex: 2,
                                          child:Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].lastName ??"",
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                softWrap: false,
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightSemiBold,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),


                                  //Email

                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[

                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                //AppConstants.PROFILE_EMAIL,
                                                AppLocalizations.of(context).translate('email'),
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightRegular,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                        Expanded(
                                          flex: 2,
                                          child:Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].email ?? "",
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                softWrap: false,
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightSemiBold,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),

                                  //Mobile

                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[

                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                               // AppConstants.PROFILE_MOBILE,
                                                AppLocalizations.of(context).translate('mobile'),
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightRegular,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                        Expanded(
                                          flex: 2,
                                          child:Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].mobilePhone??"",
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                softWrap: false,
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightSemiBold,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),


                                  //Gender

                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[

                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                //AppConstants.PROFILE_GENDER,
                                                AppLocalizations.of(context).translate('gender'),
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightRegular,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                        Expanded(
                                          flex: 2,
                                          child:Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].gender??"",
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                softWrap: false,
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightSemiBold,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN*2,

                                  ),


                                  //////////////// Information ///////////////////
                                  Text(
                                    //AppConstants.PROFILE_INFORMATION,
                                    AppLocalizations.of(context).translate('information'),
                                    textAlign: TextAlign.center,
                                    style: getStyleBody1(context).copyWith(
                                        color: colorPencilBlack,
                                        fontWeight: AppFont.fontWeightSemiBold,
                                        fontSize: 22),
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN*1.5,
                                  ),

                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[

                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                //AppConstants.DRIVER_LICENCE,
                                                AppLocalizations.of(context).translate('driverLicence'),
                                                style: getStyleBody1(context).copyWith(
                                                    color: colorPencilBlack,
                                                    fontWeight: AppFont.fontWeightRegular,
                                                    fontSize: 16
                                                )
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          //onTap:()=> _driverLicenseToggle(profileNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched: (profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].drivingLicense =="True")? true:false),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              //AppConstants.SMOKING,
                                              AppLocalizations.of(context).translate('smoking'),
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          //onTap:()=> _smokingToggle(profileNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched: (profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].smoking =="True")? true:false),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              //AppConstants.SPEAKING_GERMAN_LANGUAGE,
                                              AppLocalizations.of(context).translate('speakingGermanLanguage'),
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          // onTap:()=> _speakingLangToggle(profileNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched: (profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].experienceInGermany =="True")? true:false),

                                                ],
                                              ),
                                            )),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),


                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                             // AppConstants.WILLING_TO_DO_TRANFER,
                                              AppLocalizations.of(context).translate('willingTransferwheelchair'),
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          //onTap:()=> _expInCareToggle(profileNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched: (profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].transferOfPatients =="True")? true:false),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),

                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              //AppConstants.WILLING_TO_DO_NIGHT_SHIFTS,
                                              AppLocalizations.of(context).translate('willingNightShifts'),
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          //onTap:()=> _expInCareToggle(profileNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched: (profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].nightshifts =="True")? true:false),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ), SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),

                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              //AppConstants.ABLE_TO_TAKE_CARE_2PEOPLE,
                                              AppLocalizations.of(context).translate('ableTakeCareTwoPeople'),
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          //onTap:()=> _expInCareToggle(profileNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  CustomSwitch(switched: (profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].takingCareOfTwoPatients =="True")? true:false),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              //AppConstants.EXPERIENCE_IN_CARE,
                                              AppLocalizations.of(context).translate('experienceInCare'),
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16),
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          //onTap:()=> _expInCareToggle(profileNotifier),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 5,),
                                                  // CustomSwitch(switched: (profileNotifier.cgProfileDetailsResponse.webMethodOutputRows.webMethodOutputRows[0].experienceInCare =="True")? true:false),
                                                  Text(
                                                    (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "10") ? "No"
                                                        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "20") ? "Under 1 Year"
                                                        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "30") ? "1 - 3 Years"
                                                        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "40") ? "3 - 5 Years"
                                                        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "50") ? "5 - 10 Years"
                                                        : (experienceInCareStr!=""&&experienceInCareStr!=null&&experienceInCareStr == "60") ? "Over 10 Years":"No",
                                                    maxLines: 2,
                                                    overflow: TextOverflow.ellipsis,
                                                    softWrap: false,
                                                    style: getStyleBody1(context).copyWith(
                                                        color: colorPencilBlack,
                                                        fontWeight: AppFont.fontWeightSemiBold,
                                                        fontSize: 16
                                                    ),),
                                                ],
                                              ),
                                            )),


                                      ]

                                  ),

                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),
                                  Divider(
                                    color: colorDividerGrey,
                                  ),
                                  SizedBox(
                                    height: AppConstants.SIDE_MARGIN/2.5,

                                  ),


                                ],
                              ),


                            ),
                          ),
                        ),
                      ),

                    ])
              ],
            ),

      );


    }else {
     return Container();
    }

  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }





}



