import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/filter_option_request.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/GetCareGiverApprovedImgResp.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/ui/schedulemeeting/call_request_screen.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_meeting_calendar_screen.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_globals.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import '../../offers/offers_details_screen.dart';
import 'package:intl/intl.dart';

class Tab2OffersListScreen extends StatefulWidget {
  static const routeName = '/offersList';

  Tab2OffersListScreen();

  @override
  _OffersListScreenState createState() =>  _OffersListScreenState();
}

class _OffersListScreenState extends State<Tab2OffersListScreen> {
  final log = getLogger('Tab2OffersListScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  final int subCategoryId;
  OffersNotifier offersScreenNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  var _keyValidationForm = GlobalKey<FormState>();
   DateFormat dateMonthFormatter;
  _OffersListScreenState({@required this.subCategoryId});
  @override
  void initState() {
    super.initState();

    debugPrint("globalLanguageCode in OffersList screen :==>"+globalLanguageCode);
  }


  @override
  Widget build(BuildContext context) {
    dateMonthFormatter = DateFormat('dd MMM, yyyy',globalLanguageCode);
    return ChangeNotifierProvider<OffersNotifier>(
      builder: (context) => OffersNotifier(context,isFromWhichScreen:"offerListScreen"),
      child: Scaffold(
        backgroundColor: colorYoutubeGrey,
        key: _scaffoldKey,

        body: Consumer<OffersNotifier>(
          builder: (context, offersNotifier, _) => ModalProgressHUD(
            inAsyncCall: offersNotifier.isLoading,
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Container(
                color: colorWhite,
                  child:  Column(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[

                        Container(
                        color: colorLitePinkBG,
                        height: AppConstants.SIDE_MARGIN * 2.1,
                        ),

                        topHeader(offersNotifier),

                        Container(
                          color: colorLitePinkBG,
                          height: AppConstants.SIDE_MARGIN/1.7,
                        ),
                        _buildWidgetMainContent(context, offersNotifier),
                      ],
                    ),
                  )),
            ),
          ),
      ),
    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////


  Widget topHeader(OffersNotifier offersNotifier) {
    return Container(
      color: colorLitePinkBG,
      height: 24,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: InkWell(
              onTap: () {
                // backButton(homeNotifier);
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {


                    },
                    child: Container(
                      alignment: Alignment.center,
                      // child: Image.asset(
                      //   AppImages.IMAGE_NOTIFICATION_BELL,
                      //   height: 25,
                      // ),
                    ),
                  ),
                  SizedBox(
                    width: AppConstants.SIDE_MARGIN / 1.5,
                  ),
                  InkWell(
                    onTap: () async {


                      //Navigator.pushNamed(context, CallRequestScreen.routeName);

                      Navigator.pushNamed(context, FilterOptionsScreen.routeName).then((value) =>{
                        if (value != null) {
                            //  stateEditProfile.imageLocalFile = value;
                            print('from filter page value: ' + value.toString()),
                          filterProcess(value)

                         }
                      });
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Image.asset(
                        AppImages.IMAGE_FILTERS,
                        height: 23,
                      ),
                    ),
                  ),
                ],
              ),

            ),
          ),
          Expanded(

            flex: 2,
            child: Text(
              //AppConstants.OPEN_OFFERS,
              AppLocalizations.of(context).translate('openOffers'),
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 22.5,
                fontFamily:'NPSunaGrotesk',
              ),
            ),
          ),
        Expanded(
          flex: 1,
          child:InkWell(
            onTap: () async {

            },
            child: Container(
            alignment: Alignment.center,
            child: Image.asset(
              AppImages.IMAGE_APP_LOGO,
              height: 20,
            ),
          ),
        )),


        ],
      )
    );

  }

  Future<void> filterProcess(String value) async {
    String accessToken = await AppSharedPreference().getUserToken();
    debugPrint("accessToken:==>"+accessToken);
    FilterOptionRequest filterOptionRequest = FilterOptionRequest.fromJson(jsonDecode(value));
    print(filterOptionRequest.toString());

     String durationCode  = (filterOptionRequest.durationCode != null&&filterOptionRequest.durationCode== "Unlimited")? "10"
                    : (filterOptionRequest.durationCode != null && filterOptionRequest.durationCode == "4-6 weeks")?"20"
                    : (filterOptionRequest.durationCode != null && filterOptionRequest.durationCode == "Longer than 6 weeks")?"30"
                    :"";

      String cityOfCp  =  (filterOptionRequest.cityOfCp != null)?filterOptionRequest.cityOfCp:"";
      String germanLevelCode =  (filterOptionRequest.germanLevelCode != null && filterOptionRequest.germanLevelCode == "Good")? "10"
                    : (filterOptionRequest.germanLevelCode != null && filterOptionRequest.germanLevelCode == "Middle")?"20"
                    : (filterOptionRequest.germanLevelCode != null && filterOptionRequest.germanLevelCode == "Low")?"30"
                    : (filterOptionRequest.germanLevelCode != null && filterOptionRequest.germanLevelCode == "No Preference")?"40"
                    :"";
    offersScreenNotifier.callApiOffersList(accessToken,durationCode,cityOfCp,germanLevelCode);
  }

  Widget _buildWidgetMainContent(BuildContext context, OffersNotifier offersNotifier) {
    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);
    offersScreenNotifier = offersNotifier;

    if((offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null)){
      return    Container(

        child: Expanded(
          child:ListView.builder(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,

              //padding: EdgeInsets.all(AppConstants.SIDE_MARGIN / 2),
              itemCount: (offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
              )? offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows.length:0,
              itemBuilder: (context, position) {
                return buildWidgetOffersItem(
                    context,
                    position,
                    imageWidth,
                    cardRadio);
              }),
        ),
      );
    }else if((offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.outputRowErrors != null
        && offersScreenNotifier.offersListResp.outputRowErrors.outputRowErrors[0] != null
        && offersScreenNotifier.offersListResp.outputRowErrors.outputRowErrors[0].textMessage =="NoOffers"
      )){
      return    Container(
        child: Expanded(
          flex: 1,
          child:  Center(
            child:  Text(
              //AppConstants.OFFERS_UNAVAILABLE,
              AppLocalizations.of(context).translate('offersNotAvailable'),
              maxLines: 2,
              style: getFormTitleStyle(context).copyWith(
                  color: colorBlack,
                  fontWeight: AppFont.fontWeightSemiBold,
                  fontSize: 13
              ),
            ),
          ),

        ),
      );
    }else{
      return    Container();
    }



  }

  Widget buildWidgetOffersItem(BuildContext context, int position,double imageWidth,  Radius cardRadio) {
    List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];
    //List cardBGColors = colorsOffer;

    //DateTime formatedArrivalDate = new DateFormat("yyyy-MM-dd").parse(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].earliestArrivalDate);


    return GestureDetector(
      onTap: () async {
        log.d('onPress: Offers Item');

          String accessToken = await AppSharedPreference().getUserToken();
          debugPrint("accessToken:==>"+accessToken);




          GetCareGiverApprovedImgResp getCareGiverApprovedImgResp =  await offersScreenNotifier.callApiGetProfileImage(accessToken);
          if(getCareGiverApprovedImgResp != null && getCareGiverApprovedImgResp.webMethodOutputRows != null
              && getCareGiverApprovedImgResp.webMethodOutputRows.webMethodOutputRows[0] != null
              && getCareGiverApprovedImgResp.webMethodOutputRows.webMethodOutputRows[0].imgDownloadStatus == "Success"){

            Navigator.of(context)
                .push(
              new MaterialPageRoute(
                builder: (_) =>
                new OffersDetailsScreen(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].offerGuid,
                    offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel),
              ),
            );

            // Navigator.of(context)
            //     .push(
            //   new MaterialPageRoute(
            //     builder: (_) =>
            //     new OffersProfileUploadScreen(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].offerGuid,
            //         offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel),
            //   ),
            // );
          }else{
            Navigator.of(context)
                .push(
              new MaterialPageRoute(
                builder: (_) =>
                new OffersProfileUploadScreen(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].offerGuid,
                    offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel),
              ),
            );
          }





      },
      child: Container(
        color: colorWhite,
        padding: EdgeInsets.fromLTRB(10.0, 7.0, 7.0, 10.0),
        child:Card(
          elevation: 1.5,
          // color: (position%3 == 0) ? cardBGColors[0]: ((position%3 == 1))? cardBGColors[1]: cardBGColors[2],
          color: ( (offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
              && offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position] != null
              &&offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel == "Good") )
              ? cardBGColors[0]:
          ((offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
              && offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position] != null
              &&offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel == "Middle") )
              ? cardBGColors[1]
              : cardBGColors[2],
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.all(cardRadio)),
          child: Container(
            padding: EdgeInsets.all(AppConstants.SIDE_MARGIN / 1.5),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                SizedBox(
                  width: AppConstants.SIDE_MARGIN / 2,
                ),
                Flexible(
                    fit: FlexFit.tight,
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: AppConstants.SIDE_MARGIN / 4,
                          ),
                          //Address
                          Row(

                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [


                              Image.asset(
                                AppImages.IMAGE_ADDRESS,
                                height: 20,
                              ),
                              SizedBox(
                                width: AppConstants.SIDE_MARGIN / 1.5,
                              ),
                              Expanded(child: Text(
                                (offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
                                    && offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position] != null)
                                    ? offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].address
                                      +", "+offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].city
                                      +", "+offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].postalCode
                                    :"",
                                // " "+offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].city??""
                                //       +" "+offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].postalCode??"" ,
                                overflow: TextOverflow.ellipsis,
                                softWrap: false,
                                maxLines: 2,
                                style: getFormTitleStyle(context).copyWith(
                                    color: colorBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 13
                                ),
                              ),)






                            ],
                          ),

                          _buildDivider(),
                          //Date / Duration
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      AppImages.IMAGE_DATE,
                                      height: 20,
                                    ),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN / 1.5,
                                    ),
                                    SizedBox(
                                      width: 80,
                                      child: Text(
                                        (offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
                                            && offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position] != null
                                            && offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].earliestArrivalDate != ""
                                        )
                                            ?dateMonthFormatter.format(new DateFormat("yyyy-MM-dd","en").parse(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].earliestArrivalDate))
                                            :"",

                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        textAlign: TextAlign.left,
                                        style: getFormTitleStyle(context).copyWith(
                                            color: colorBlack,
                                            fontWeight: AppFont.fontWeightSemiBold,
                                            fontSize: 13
                                        ),
                                      ),
                                    ),


                                  ]),

                              SizedBox(
                                width: AppConstants.SIDE_MARGIN ,
                              ),

                              Expanded(

                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        AppImages.IMAGE_DURATION,
                                        height: 18,
                                      ),
                                      SizedBox(
                                        width: AppConstants.SIDE_MARGIN / 2,
                                      ),
                                      Expanded(child: Text(
                                        (offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
                                            && offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position] != null)
                                            ?(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].duration == "Longer than 6 weeks")
                                                ?AppLocalizations.of(context).translate('longerThanSixWeeks')
                                                :(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].duration == "Unlimited")
                                                  ?AppLocalizations.of(context).translate('unlimited')
                                                  :(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].duration == "4-6 weeks")
                                                      ?AppLocalizations.of(context).translate('fourToSixWeeks')
                                                      :offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].duration.toString()
                                            :"",
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        textAlign: TextAlign.left,
                                        style: getFormTitleStyle(context).copyWith(
                                            color: colorBlack,
                                            fontWeight: AppFont.fontWeightSemiBold,
                                            fontSize: 13
                                        ),
                                      ),)

                                    ]),),


                            ],
                          ),

                          _buildDivider(),


                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [

                              //Address
                              Image.asset(
                                AppImages.IMAGE_EURO,
                                height: 20,
                              ),
                              SizedBox(
                                width: AppConstants.SIDE_MARGIN / 1.5,
                              ),
                              Expanded(child:  Text(
                                (offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
                                    && offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position] != null)
                                    ?AppLocalizations.of(context).translate('nettoPayment')+" : \€ "+offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].nettoPayment
                                    :"",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                softWrap: false,
                                style: getFormTitleStyle(context).copyWith(
                                    color: colorBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 13
                                ),
                              ),),


                            ],
                          ),

                          _buildDivider(),


                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              //Address
                              Image.asset(
                                AppImages.IMAGE_LANGUAGE,
                                height: 23,
                              ),
                              SizedBox(
                                width: AppConstants.SIDE_MARGIN / 1.5,
                              ),
                              Expanded(child:  Text(

                                (offersScreenNotifier.offersListResp != null && offersScreenNotifier.offersListResp.webMethodOutputRows != null
                                    && offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position] != null)
                                  ?(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel == "Good")
                                    ?AppLocalizations.of(context).translate('requiredGermanSkill')+" : "+AppLocalizations.of(context).translate('good')
                                    :(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel == "Middle")
                                    ?AppLocalizations.of(context).translate('requiredGermanSkill')+" : "+AppLocalizations.of(context).translate('middle')
                                    :(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel == "Low")
                                    ?AppLocalizations.of(context).translate('requiredGermanSkill')+" : "+AppLocalizations.of(context).translate('low')
                                    :(offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel == "No Preference")
                                    ?AppLocalizations.of(context).translate('requiredGermanSkill')+" : "+AppLocalizations.of(context).translate('noPreference')
                                    :offersScreenNotifier.offersListResp.webMethodOutputRows.webMethodOutputRows[position].germanLevel.toString()

                                  :"",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                softWrap: false,
                                style: getFormTitleStyle(context).copyWith(
                                    color: colorBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 13
                                ),
                              ),),


                            ],
                          ),

                          SizedBox(
                            height: AppConstants.SIDE_MARGIN / 4,
                          ),

                        ],


                      ),
                    ))
              ],
            ),
          ),
        ),
      )

    );
  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }


}

