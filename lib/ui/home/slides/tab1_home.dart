import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jiffy/jiffy.dart';
import 'package:quiver/iterables.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/availability/assignment/AssignmentListResponse.dart'
    as AssignmentList;
import 'package:suna_care/core/data/remote/request_response/availability/holiday/AddHolidayResponse.dart'
    as AddHolidayResp;
import 'package:suna_care/core/data/remote/request_response/availability/holiday/HolidayListResponse.dart'
    as HolidayList;
import 'package:suna_care/core/notifier/home_notifier.dart';
import 'package:suna_care/ui/home/availability/calendarData.dart';
import 'package:suna_care/ui/home/availability/day.dart';
import 'package:suna_care/ui/home/availability/schedule_details.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_globals.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:provider/provider.dart';
//import 'package:table_calendar/table_calendar.dart';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class Tab1HomeScreen extends StatefulWidget {
  BuildContext context;

  Tab1HomeScreen({this.context});

  @override
  Tab1HomeScreenState createState() => Tab1HomeScreenState(context);
}

class Tab1HomeScreenState extends State<Tab1HomeScreen> {
  BuildContext context;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Tab1HomeScreenState(this.context);

  //CalendarController _controller = CalendarController();

  var log = getLogger("Tab1HomeScreen");
  var LastDateOfMonthArr = const [
    31,
    28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31
  ];
  DateTime masterDate = DateTime.now();

  //Picker
  DateTime selectedStartDate = DateTime.now();
  String scheduleFormatedStartDate = "";
  DateTime selectedEndDate = DateTime.now();

  ///Multi date range picker
  Day start;
  List<List<Day>> intervals = [];
  bool deleteConfirm = false;
  Day deleteDay;
  List<Day> deleteInterval;
  bool onlyOne = false;
  Color selectionColor = Colors.red;
  Color buttonColor = Colors.lightGreenAccent;
  Color buttonTextColor = Colors.black;
  Color primaryTextColor = Colors.black;
  Color dateTextColor = Colors.black;
  Color ignoreTextColor = Colors.grey;
  Color selectedDateTextColor = Colors.black;
  Color selectedIgnoreTextColor = Colors.black;
  Color backgroundTextColor = Colors.white;
  //final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  final DateFormat dateMonthFormatter = DateFormat('ddo MMMM , yyyy');

  int assignmentListSize = 0, holidayListSize = 0;
  var monthFormatter ;
  var yearFormatter;
  var dateFormatter;

  @override
  void initState() {
    super.initState();
    //Get Assignment List and Holiday List data and load into the Intervals list
    // getInitialData(homeNotifier);

    //Prepare the list<Day> to load data into calendar
    // loadInitialData(homeNotifier);

    debugPrint("globalLanguageCode in Home screen :==>"+globalLanguageCode);
  }

  @override
  Widget build(BuildContext context) {

    return Consumer<HomeNotifier>(
        builder: (context, homeNotifier, _) => Theme(
            data: ThemeData(
                hintColor: colorDividerGrey,
                primaryColor: colorPrimary,
                accentColor: colorYellow,
                primaryColorDark: colorPrimary),
            child: _buildWidgetHomeCard(homeNotifier,
                context))); //_buildWidgetSendEmailIdCard(homeNotifier, context)));
  }

  /////////////////////
  /*  build helpers */
  ////////////////////

  Widget _buildWidgetHomeCard(HomeNotifier homeNotifier, BuildContext context) {

    //Formatter
    monthFormatter = new DateFormat('MM',globalLanguageCode);
    yearFormatter = new DateFormat('yyyy',globalLanguageCode);
    dateFormatter = new DateFormat('dd',globalLanguageCode);

    //Context
    homeNotifier.context = context;

    //Get Assignment List and Holiday List data and load into the Intervals list
    getInitialData(homeNotifier);

    //Prepare the list<Day> to load data into calendar
    loadInitialData(homeNotifier);

    //Get selected date
    var selectedDate = masterDate;

    String currentDateMonth = monthFormatter.format(DateTime.now());
    String currentDateYear = yearFormatter.format(DateTime.now());
    String selectedDateMonth = monthFormatter.format(selectedDate);
    String selectedDateYear = yearFormatter.format(selectedDate);
    debugPrint("selectedDateMonth :==>" + selectedDateMonth.toString());
    debugPrint("currentDateMonth:==>" + currentDateMonth.toString());
    debugPrint("selectedDateYear :==>" + selectedDateYear.toString());
    debugPrint("currentDateYear :==>" + currentDateYear.toString());

    var currentDate = new DateTime.now();
    debugPrint("CompareTo :==>" + masterDate.compareTo(currentDate).toString());

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: _scaffoldKey,
      backgroundColor: colorWhite,
      body: Container(
          child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Container(
                    color: colorLitePinkBG,
                    height: AppConstants.SIDE_MARGIN * 2,
                  ),

                topHeader(homeNotifier),

                Container(
                  color: colorLitePinkBG,
                  height: AppConstants.SIDE_MARGIN/ 2,
                ),


                SizedBox(height: AppConstants.SIDE_MARGIN/ 2 ,),
                // Select unavaliable date range information
                Visibility(
                  visible: (intervals != null &&
                          intervals.length == 0 &&
                          ((int.parse(selectedDateMonth) >=
                                  int.parse(currentDateMonth)) &&
                              (int.parse(selectedDateYear) >=
                                  int.parse(currentDateYear))))
                      ? true
                      : false,
                  child: Expanded(
                    flex: 3,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                      child: Text(
                        //AppConstants.SELECT_RANGE_INFO,
                        AppLocalizations.of(context).translate('selectDateRangeClickAddUnavailSetUnavail'),
                        textAlign: TextAlign.center,
                        style: getStyleBody2(context).copyWith(
                            height: 1.3,
                            fontWeight: AppFont.fontWeightSemiBold,
                            color: (homeNotifier.currentPageNo == 0)
                                ? colorBlack
                                : colorLightGrey,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
                // Select unavaliable date range
                Visibility(
                  visible: (intervals != null &&
                          intervals.length > 0 &&
                          ((int.parse(selectedDateMonth) >=
                                  int.parse(currentDateMonth)) &&
                              (int.parse(selectedDateYear) >=
                                  int.parse(currentDateYear))))
                      ? true
                      : false,
                  // visible: false,
                  child: Expanded(
                    flex: 4,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.fromLTRB(30.0, 0.0, 3.0, 0.0),
                          child: Text(
                           // AppConstants.SELECT_UNAVAILABLE_DATE_RANGE,
                            AppLocalizations.of(context).translate('selectUnavailableDateRange'),
                            textAlign: TextAlign.center,
                            style: getStyleBody2(context).copyWith(
                                //fontWeight: AppFont.fontWeightSemiBold,
                                color: (homeNotifier.currentPageNo == 0)
                                    ? colorBlack
                                    : colorLightGrey,
                                fontSize: 17),
                          ),
                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN / 3,
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(30.0, 0.0, 5.0, 0.0),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                //From

                                GestureDetector(
                                  onTap: () {
                                    selectStartEndDateProcess(
                                        homeNotifier, true);
                                  },
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          //AppConstants.FROM,
                                          AppLocalizations.of(context).translate('from'),
                                          textAlign: TextAlign.left,
                                          style: getStyleBody2(context)
                                              .copyWith(
                                                  fontWeight: AppFont
                                                      .fontWeightSemiBold,
                                                  color: (homeNotifier
                                                              .currentPageNo ==
                                                          0)
                                                      ? colorBlack
                                                      : colorLightGrey,
                                                  fontSize: 13.5),
                                        ),
                                        SizedBox(
                                          height: AppConstants.SIDE_MARGIN / 3.2,
                                        ),
                                        Text(
                                          // (homeNotifier.fromDate != null)
                                          //     ? dateMonthFormatter
                                          //         .format(homeNotifier.fromDate)
                                          //     : "DD/MM/YYYY",
                                          (homeNotifier.fromDate != null)
                                              ? Jiffy([homeNotifier.fromDate.year, homeNotifier.fromDate.month, homeNotifier.fromDate.day]).format("do MMM, yyyy")
                                              : "DD/MM/YYYY",
                                          textAlign: TextAlign.left,
                                          style: getStyleDisplay1(context)
                                              .copyWith(
                                                  color: colorBlack,
                                                  fontSize: 12),
                                        ),
                                      ]),
                                ),

                                SizedBox(
                                  width: AppConstants.SIDE_MARGIN * 3,
                                ),
                                //To

                                GestureDetector(
                                  onTap: () {
                                    selectStartEndDateProcess(
                                        homeNotifier, false);
                                  },
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          //AppConstants.TO,
                                          AppLocalizations.of(context).translate('to'),
                                          textAlign: TextAlign.left,
                                          style: getStyleBody2(context)
                                              .copyWith(
                                                  fontWeight: AppFont
                                                      .fontWeightSemiBold,
                                                  color: (homeNotifier
                                                              .currentPageNo ==
                                                          0)
                                                      ? colorBlack
                                                      : colorLightGrey,
                                                  fontSize: 13.5),
                                        ),
                                        SizedBox(
                                          height: AppConstants.SIDE_MARGIN / 3.2,
                                        ),
                                        Text(
                                          // (homeNotifier.toDate != null)
                                          //     ? dateMonthFormatter.format(homeNotifier.toDate)
                                          //     : "DD/MM/YYYY",
                                          (homeNotifier.toDate != null)
                                              ?  Jiffy([homeNotifier.toDate.year, homeNotifier.toDate.month, homeNotifier.toDate.day]).format("do MMM, yyyy")
                                              : "DD/MM/YYYY",
                                          textAlign: TextAlign.left,
                                          style: getStyleDisplay1(context)
                                              .copyWith(
                                                  color: colorBlack,
                                                  fontSize: 12),
                                        ),
                                      ]),
                                ),
                              ]),
                        )
                      ],
                    ),
                  ),
                ),

                Visibility(
                  visible: (intervals != null &&
                      intervals.length > 0 &&((int.parse(selectedDateMonth) >=
                              int.parse(currentDateMonth)) &&
                          (int.parse(selectedDateYear) >=
                              int.parse(currentDateYear))))
                      ? true
                      : false,
                  child: Container(

                    margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                    child: Divider(
                        height: .5,
                        color: Colors.black),
                  ),
                ),

                Expanded(
                  flex: 18,
                  //////////////////////////Customized CalendarPage  Design ////////////////////////

                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Column(
                      children: <Widget>[
                        getCalendarCard(homeNotifier, context),
                        Row(
                          children: <Widget>[],
                        )
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: AppConstants.SIDE_MARGIN / 12,
                ),

                Expanded(
                  flex: ((intervals != null && intervals.length > 0)&& ((int.parse(selectedDateMonth) <
                      int.parse(currentDateMonth)) &&
                      (int.parse(selectedDateYear) <=
                          int.parse(currentDateYear))))?11:3,
                  child: Visibility(
                    //visible: true,
                    visible: (intervals != null && intervals.length > 0)
                        ? true
                        : false,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 15,
                                      height: 15,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.green),
                                    ),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN / 3,
                                    ),
                                    Text(
                                        //AppConstants.CONTRACT_DATES,
                                        AppLocalizations.of(context).translate('contractDates'),
                                        textAlign: TextAlign.center,
                                        style: getStyleBody2(context).copyWith(
                                            fontWeight:
                                                AppFont.fontWeightSemiBold,
                                            color:
                                                (homeNotifier.currentPageNo ==
                                                        0)
                                                    ? colorBlack
                                                    : colorLightGrey)),
                                  ],
                                ),
                                SizedBox(
                                  //height: AppConstants.SIDE_MARGIN/6,
                                  width: AppConstants.SIDE_MARGIN,
                                ),
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        width: 15,
                                        height: 15,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.red),
                                      ),
                                      SizedBox(
                                        width: AppConstants.SIDE_MARGIN / 3,
                                      ),
                                      Text(
                                          // AppLocalizations.of(context).translate('unavailableDates'),
                                          AppLocalizations.of(context).translate('unavailableDates'),
                                          textAlign: TextAlign.center,
                                          style: getStyleBody2(context)
                                              .copyWith(
                                                  fontWeight: AppFont
                                                      .fontWeightSemiBold,
                                                  color: (homeNotifier
                                                              .currentPageNo ==
                                                          0)
                                                      ? colorBlack
                                                      : colorLightGrey)),
                                    ]),
                              ]),
                          SizedBox(
                            height: AppConstants.SIDE_MARGIN / 1.5,
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  width: 15,
                                  height: 15,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.brown),
                                ),
                                SizedBox(
                                  width: AppConstants.SIDE_MARGIN / 3,
                                ),
                                Text(
                                    //AppConstants.FUTURE_CONTRACT_DATES,
                                    AppLocalizations.of(context).translate('futureContractDates'),
                                    textAlign: TextAlign.center,
                                    style: getStyleBody2(context).copyWith(
                                        fontWeight: AppFont.fontWeightSemiBold,
                                        color: (homeNotifier.currentPageNo == 0)
                                            ? colorBlack
                                            : colorLightGrey)),
                              ]),
                        ]),
                  ),
                ),

                SizedBox(
                  height: AppConstants.SIDE_MARGIN / 4,
                ),
                SizedBox(
                  width: getScreenWidth(context) / 1.5,
                  height: 45,
                  child: ButtonTheme(
                    minWidth: double.infinity,
                    child: Visibility(
                      //visible: true,
                      visible: (intervals != null &&
                              intervals.length > 0 &&
                              ((int.parse(selectedDateMonth) >=
                                      int.parse(currentDateMonth)) &&
                                  (int.parse(selectedDateYear) >=
                                      int.parse(currentDateYear))))
                          ? true
                          : (intervals != null &&
                          intervals.length == 0 &&
                          ((int.parse(selectedDateMonth) >=
                              int.parse(currentDateMonth)) &&
                              (int.parse(selectedDateYear) >=
                                  int.parse(currentDateYear))))
                          ?true:false,
                      child: RaisedButton(
                        highlightElevation: 8.0,
                        onPressed: () async {
                          // 0 is equal / positive value greater / negative value being less


                          if (selectedStartDate.compareTo(selectedEndDate) <=
                              0) {
                            addHolidayAPI(homeNotifier);
                          } else {
                            showDialog(
                                context: context,
                                builder: (_) => AlertOverlay(
                                     AppLocalizations.of(context).translate('unavailableDates'),
                                    AppConstants
                                        .TO_DATE_SHOULD_BE_GRATER_THEN_FROM_DATE,
                                     AppLocalizations.of(context).translate('okay')));
                          }
                        },
                        color: colorLiteOrange,
                        textColor: colorWhite,
                        elevation: 1,
                        padding: EdgeInsets.only(
                            top: AppConstants.SIDE_MARGIN / 1.5,
                            bottom: AppConstants.SIDE_MARGIN / 1.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(AppConstants.SIDE_MARGIN * 2))),
                        child: Text(
                         //AppConstants.ADD_UNAVAILABLE_DATES,
                          AppLocalizations.of(context).translate('addUnavailableDates'),
                          style: getStyleButtonText(context).copyWith(
                              color: colorWhite,
                              letterSpacing: 1,
                              fontWeight: AppFont.fontWeightBold),
                        ),
                      ),
                    ),
                  ),
                ),

                SizedBox(
                  height: AppConstants.SIDE_MARGIN / 2,
                ),
              ],
            ),
          )
        ],
      )),
    );
  }

  Future<void> addHolidayAPI(HomeNotifier homeNotifier) async {
    try{

      log.d('assignmentListSize:==> $assignmentListSize');
      log.d('holidayListSize:==> $holidayListSize');
      //log.d('saved value from SP: ${await AppSharedPreference().getUserToken()}');
      String accessToken = await AppSharedPreference().getUserToken()??"";
      debugPrint("accessToken:==>" + accessToken);
      var todayDate = new DateTime.now();

      String month = monthFormatter.format(todayDate);
      String year = yearFormatter.format(todayDate);
      debugPrint("todayDate:==>" + todayDate.toString());
      debugPrint("Month:==>" + month.toString());
      debugPrint("year :==>" + year.toString());
      List<List<Day>> newlyAddedInterval = new List<List<Day>>();

      //Clear Already newly added date
      if (newlyAddedInterval.length > 0) {
        newlyAddedInterval.clear();
      }

      if (intervals != null && intervals.length > 0) {
        for (int arrIndex = 0; arrIndex < intervals.length; arrIndex++) {
          List<Day> interval = intervals[arrIndex];
          if (interval != null &&
              interval[0].isNewlyAddedOne != null &&
              interval[0].isNewlyAddedOne &&
              interval[1].isNewlyAddedOne != null &&
              interval[1].isNewlyAddedOne) {
            debugPrint("newlyAddedInterval index :==>" + arrIndex.toString());
            newlyAddedInterval.add(intervals[arrIndex]);
          }
        }

        //CHECK NEWLY ADD HOLIDAY OR UNAVAILABLE DAY
        if (newlyAddedInterval != null && newlyAddedInterval.length > 0) {
          List<Day> interval = newlyAddedInterval[0]; // get First index value
          debugPrint(
              "isNewlyAddedOne fromDate:==>" + interval[0].date.toString());
          debugPrint("isNewlyAddedOne toDate: ==>" + interval[1].date.toString());
          debugPrint(
              "isNewlyAddedOne :==>" + interval[0].isNewlyAddedOne.toString());

          String startDateMonth = "",
              endDateMonth = "",
              startDay = "",
              endDay = "";
          if (interval[0].date.month <= 9) {
            startDateMonth = "0" + interval[0].date.month.toString();
          } else {
            startDateMonth = interval[0].date.month.toString();
          }

          if (interval[1].date.month <= 9) {
            endDateMonth = "0" + interval[1].date.month.toString();
          } else {
            endDateMonth = interval[1].date.month.toString();
          }

          if (interval[0].date.day <= 9) {
            startDay = "0" + interval[0].date.day.toString();
          } else {
            startDay = interval[0].date.day.toString();
          }

          if (interval[1].date.day <= 9) {
            endDay = "0" + interval[1].date.day.toString();
          } else {
            endDay = interval[1].date.day.toString();
          }

          String startDate = interval[0].date.year.toString() +
              "" +
              startDateMonth +
              "" +
              startDay;
          String endDate =
              interval[1].date.year.toString() + "" + endDateMonth + "" + endDay;

          if (startDate != null && endDate != null) {
            AddHolidayResp.AddHolidayResponse addHolidayResponse =
                await homeNotifier.callApiAddHoliday(
                    accessToken, startDate, endDate);
            if (addHolidayResponse != null) {
              homeNotifier.addHolidayResponse = addHolidayResponse;
              if (homeNotifier.addHolidayResponse != null &&
                  homeNotifier.addHolidayResponse.webMethodOutputRows != null &&
                  homeNotifier.addHolidayResponse.webMethodOutputRows
                          .webMethodOutputRows !=
                      null) {
                if (homeNotifier.addHolidayResponse.webMethodOutputRows
                        .webMethodOutputRows[0].holidayCgScheduleStatus ==
                    "Added") {
                  showDialog(
                      context: context,
                      builder: (_) => AlertOverlay(
                           AppLocalizations.of(context).translate('unavailableDates'),
                           AppLocalizations.of(context).translate('youHaveSuccessfullySetYourUnavailableDates'),
                          //AppConstants.TOKEN_VALID_AND_HOLIDAY_SET_SUCCESSFULLY,
                           AppLocalizations.of(context).translate('okay')));

                  homeNotifier.fromDate = null;
                  homeNotifier.toDate = null;

                  getAssignmentAndHolidayList(homeNotifier);
                } else {
                  showDialog(
                      context: context,
                      builder: (_) => AlertOverlay(
                           AppLocalizations.of(context).translate('unavailableDates'),
                          homeNotifier
                                  .addHolidayResponse
                                  .webMethodOutputRows
                                  .webMethodOutputRows[0]
                                  .holidayCgScheduleStatus ??
                              "",
                           AppLocalizations.of(context).translate('okay')));
                }
              } else if (homeNotifier.addHolidayResponse != null &&
                  homeNotifier.addHolidayResponse.outputRowErrors != null &&
                  homeNotifier
                          .addHolidayResponse.outputRowErrors.outputRowErrors !=
                      null &&
                  homeNotifier.addHolidayResponse.outputRowErrors
                          .outputRowErrors[0] !=
                      null &&
                  homeNotifier.addHolidayResponse.outputRowErrors
                          .outputRowErrors[0].textMessage !=
                      null &&
                  homeNotifier.addHolidayResponse.outputRowErrors
                          .outputRowErrors[0].textMessage !=
                      "") {
                if (homeNotifier.addHolidayResponse.outputRowErrors
                        .outputRowErrors[0].textMessage ==
                    "Overlapped") {
                  showDialog(
                      context: context,
                      builder: (_) => AlertOverlay(
                           AppLocalizations.of(context).translate('unavailableDates'),
                           AppLocalizations.of(context).translate('tokenIsValidButHolidayIsOverlapped'),
                          // AppConstants.TOKEN_VALID_HOLIDAY_OVERLAPEED,
                           AppLocalizations.of(context).translate('okay')));
                } else if (homeNotifier.addHolidayResponse.outputRowErrors
                        .outputRowErrors[0].textMessage ==
                    "InvalidToken") {
                  showDialog(
                      context: context,
                      builder: (_) => AlertOverlay(
                          AppLocalizations.of(context).translate('unavailableDates'),
                          AppLocalizations.of(context).translate('tokenIsInvalid'),
                          //AppConstants.TOKEN_INVALID,
                          AppLocalizations.of(context).translate('okay')));
                } else if (homeNotifier.addHolidayResponse.outputRowErrors
                        .outputRowErrors[0].textMessage ==
                    "unexpected") {
                  showDialog(
                      context: context,
                      builder: (_) => AlertOverlay(
                          AppLocalizations.of(context).translate('unavailableDates'),
                          AppLocalizations.of(context).translate('unexpectedError'),
                          //AppConstants.UNEXPECTED_ERROR,
                          AppLocalizations.of(context).translate('okay')));
                } else {
                  showDialog(
                      context: context,
                      builder: (_) => AlertOverlay(
                           AppLocalizations.of(context).translate('unavailableDates'),
                          homeNotifier.addHolidayResponse.outputRowErrors
                              .outputRowErrors[0].textMessage,
                           AppLocalizations.of(context).translate('okay')));
                }
              }
            }
          }
        } else {
          showDialog(
              context: context,
              builder: (_) => AlertOverlay(
                  AppLocalizations.of(context).translate('unavailableDates'),
                  AppLocalizations.of(context).translate('pleaseSelectUnavailableDates'),
                 // AppConstants.PLEASE_ADD_UNAVAILABLE_DATES,
                  AppLocalizations.of(context).translate('okay')));
        }
      } else {
        showDialog(
            context: context,
            builder: (_) => AlertOverlay(
                AppLocalizations.of(context).translate('unavailableDates'),
                AppLocalizations.of(context).translate('pleaseSelectUnavailableDates'),
                //AppConstants.PLEASE_ADD_UNAVAILABLE_DATES,
                AppLocalizations.of(context).translate('okay')));
      }
    }catch(ex){
      log.d('Exception:==>'+ex.toString());
    }
  }

  Widget topHeader(HomeNotifier homeNotifier) {
    return Container(
      color: colorLitePinkBG,
      child:new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: InkWell(
              onTap: () {
                // backButton(homeNotifier);
              },
              child: Container(
                alignment: Alignment.center,
                // child: Image.asset(
                //   AppImages.IMAGE_NOTIFICATION_BELL,
                //   height: 25,
                // ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              //AppConstants.AVAILABILITY,
              AppLocalizations.of(context).translate('availability'),
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                height: 20,
              ),
            ),
          ),
        ],
      )
    ) ;
  }

  @override
  void dispose() {
    this.context = null;
    super.dispose();
  }

/////////////////////
/*On click-actions*/

  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

//////////////MultiRange picker page content///////////

  Widget getCalendarCard(HomeNotifier homeNotifier, BuildContext context) {
    DateFormat formatter = new DateFormat("MMMM yyyy",globalLanguageCode);

    return Card(
      elevation: 0.0,
      child: Container(
        color: backgroundTextColor,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(13.0, 2.0, 13.0, 2.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    padding: EdgeInsets.fromLTRB(0.0, 0.0, 55.0, 0.0),

                    elevation: 0.0,
                    child: Icon(
                      Icons.navigate_before,
                      color: colorLiteOrange,
                    ),
                    onPressed: () {
                      setState(() {
                        DateTime previousMonth =
                            DateTime(masterDate.year, masterDate.month - 1);
                        //masterDate = masterDate.add(Duration(days: -29));
                        masterDate = previousMonth;
                        homeNotifier.masterDate = masterDate;
                        intervals.clear();
                      });

                      //Call APIs
                      getAssignmentAndHolidayList(homeNotifier);
                    },
                    color: Colors.white,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                    formatter.format(masterDate),
                      textAlign: TextAlign.center,
                    style: TextStyle(
                      color: primaryTextColor,
                    ),
                  ),),

                  RaisedButton(
                    padding: EdgeInsets.fromLTRB(55.0, 0.0, 0.0, 0.0),
                    elevation: 0.0,
                    child: Icon(
                      Icons.navigate_next,
                      color: colorLiteOrange,
                    ),
                    onPressed: () async {
                      setState(() {
                        //masterDate = masterDate.add(Duration(days: 29));
                        DateTime futureMonth =
                            DateTime(masterDate.year, masterDate.month + 1);
                        masterDate = futureMonth;
                        homeNotifier.masterDate = futureMonth;
                        intervals.clear();
                      });

                      //Call APIs
                      getAssignmentAndHolidayList(homeNotifier);
                    },
                    color: Colors.white,
                  ),
                ],
              ),
              SizedBox(
                height: 2,
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        DateFormat("EEE", globalLanguageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    1))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",globalLanguageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    2))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",globalLanguageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    3))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",globalLanguageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    4))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",globalLanguageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    5))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",globalLanguageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    6))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE", globalLanguageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    7))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  Column(
                    children: buildCalendar(homeNotifier, context),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> getAssignmentAndHolidayList(HomeNotifier homeNotifier) async {
    //log.d('saved value from SP: ${await AppSharedPreference().getUserToken()}');
    String accessToken = await AppSharedPreference().getUserToken();
    //accessToken = "";
    // debugPrint("accessToken:==>"+accessToken);
    var selectedDate = masterDate;

    String month = monthFormatter.format(selectedDate);
    String year = yearFormatter.format(selectedDate);
    debugPrint("selectedDate:==>" + selectedDate.toString());
    debugPrint("Month:==>" + month.toString());
    debugPrint("year :==>" + year.toString());
    //
    //

    homeNotifier.clearInitialIntervals();
    if (intervals != null && intervals.length > 0) {
      intervals.clear();
    }
    AssignmentList.AssignmentListResponse assignmentListResponse =
        await homeNotifier.callApiGetAssignmentList(accessToken, month, year);
    if (assignmentListResponse != null) {
      homeNotifier.assignmentListResp = assignmentListResponse;
    }
    HolidayList.HolidayListResponse holidayListResponse =
        await homeNotifier.callApiGetHolidayList(accessToken, month, year);

    if (holidayListResponse != null) {
      homeNotifier.holidayListResponse = holidayListResponse;
    }
  }

  getInitialData(HomeNotifier homeNotifier) {
    /////ASSIGNMENTS//////
    if (homeNotifier.assignmentListResp != null &&
        homeNotifier.assignmentListResp.webMethodOutputRows != null &&
        homeNotifier
                .assignmentListResp.webMethodOutputRows.webMethodOutputRows !=
            null) {
      List<AssignmentList.WebMethodOutputRow> assignmentListData = homeNotifier
          .assignmentListResp.webMethodOutputRows.webMethodOutputRows;
      for (int arrIndex = 0; arrIndex < assignmentListData.length; arrIndex++) {
        var currentDate = new DateTime.now();
        var formatter = new DateFormat('yyyy-MM-dd');
        String currentFormatedDate = formatter.format(currentDate);
        print("Current date now :==>" + currentDate.toString());
        print("Current date formated:==>" + currentFormatedDate);

        CalendarData calendarData = new CalendarData(
          dates: [
            assignmentListData[arrIndex].arrivalDate,
            assignmentListData[arrIndex].plannedEndDate,
          ],
          isBlocked: true,
          isContractDate: false,
          isHolidayBlocked: false,
          CustomerName: assignmentListData[arrIndex].customerName,
          OfferNumber: assignmentListData[arrIndex].offerNumber,
          NetToSalary: assignmentListData[arrIndex].netToSalary,
        );
        homeNotifier.initialIntervals.add(calendarData);
      }
      assignmentListSize = assignmentListData.length;
      homeNotifier.assignmentListResp = null;
    } else if (homeNotifier.assignmentListResp != null &&
        homeNotifier.assignmentListResp.outputRowErrors != null &&
        homeNotifier.assignmentListResp.outputRowErrors.outputRowErrors !=
            null &&
        homeNotifier.assignmentListResp.outputRowErrors.outputRowErrors[0]
                .textMessage !=
            null &&
        homeNotifier.assignmentListResp.outputRowErrors.outputRowErrors[0]
                .textMessage !=
            "") {
      if (homeNotifier.assignmentListResp.outputRowErrors.outputRowErrors[0]
              .textMessage !=
          "NoAssignments") {
        /* Future.delayed(Duration.zero, () {
         // _showSnackBarMessage( homeNotifier.assignmentListResp.outputRowErrors.outputRowErrors[0].textMessage);
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay( AppLocalizations.of(context).translate('unavailableDates'),
                      AppConstants.TOKEN_VALID_NO_CG_ASSIGNMENTS,
                       AppLocalizations.of(context).translate('okay'))
          );

          Future.delayed(Duration(seconds: 2), () {
            homeNotifier.assignmentListResp = null;
          });
        });*/
      } else {
        /* Future.delayed(Duration.zero, () {
         // _showSnackBarMessage( homeNotifier.assignmentListResp.outputRowErrors.outputRowErrors[0].textMessage);
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay( AppLocalizations.of(context).translate('unavailableDates'),
                      homeNotifier.assignmentListResp.outputRowErrors.outputRowErrors[0].textMessage,
                       AppLocalizations.of(context).translate('okay'))
          );

          Future.delayed(Duration(seconds: 2), () {
            homeNotifier.assignmentListResp = null;
          });
        });*/
      }
    } else {
      /* Future.delayed(Duration.zero, () {
        showDialog(
            context: context,
            builder: (_) =>
                AlertOverlay( AppLocalizations.of(context).translate('unavailableDates'),
                    AppConstants.SOME_THING_WENT_WRONG,
                     AppLocalizations.of(context).translate('okay'))
        );

        Future.delayed(Duration(seconds: 2), () {
          homeNotifier.assignmentListResp = null;
        });
      });*/

    }

    //////HOLIDAY LIST/////

    if (homeNotifier.holidayListResponse != null &&
        homeNotifier.holidayListResponse.webMethodOutputRows != null &&
        homeNotifier
                .holidayListResponse.webMethodOutputRows.webMethodOutputRows !=
            null) {
      List<HolidayList.WebMethodOutputRow> holidayListData = homeNotifier
          .holidayListResponse.webMethodOutputRows.webMethodOutputRows;
      for (int arrIndex = 0; arrIndex < holidayListData.length; arrIndex++) {
        CalendarData calendarData = new CalendarData(
            dates: [
              holidayListData[arrIndex].fromDate,
              holidayListData[arrIndex].toDate,
            ],
            isBlocked: false,
            isContractDate: false,
            isHolidayBlocked: true,
            CGScheduleNumber: holidayListData[arrIndex].cgScheduleGuid);
        homeNotifier.initialIntervals.add(calendarData);
      }
      homeNotifier.holidayListResponse = null;
      holidayListSize = holidayListData.length;
    } else if (homeNotifier.holidayListResponse != null &&
        homeNotifier.holidayListResponse.outputRowErrors != null &&
        homeNotifier.holidayListResponse.outputRowErrors.outputRowErrors !=
            null &&
        homeNotifier.holidayListResponse.outputRowErrors.outputRowErrors[0] !=
            null &&
        homeNotifier.holidayListResponse.outputRowErrors.outputRowErrors[0]
                .textMessage !=
            null &&
        homeNotifier.holidayListResponse.outputRowErrors.outputRowErrors[0]
                .textMessage !=
            "") {
      /* //failure case
      Future.delayed(Duration.zero, () {
        //_showSnackBarMessage( homeNotifier.holidayListResponse.outputRowErrors.outputRowErrors[0].textMessage);
        if(homeNotifier.holidayListResponse.outputRowErrors.outputRowErrors[0]
            .textMessage !=
            "NoHolidays"){
              showDialog(
                context: context,
                builder: (_) =>
                    AlertOverlay( AppLocalizations.of(context).translate('unavailableDates'),
                        AppConstants.TOKEN_VALID_NO_CG_HOLIDAY,
                         AppLocalizations.of(context).translate('okay'))
            );
        }else{

            //_showSnackBarMessage( homeNotifier.holidayListResponse.outputRowErrors.outputRowErrors[0].textMessage);

            showDialog(
                context: context,
                builder: (_) =>
                    AlertOverlay( AppLocalizations.of(context).translate('unavailableDates'),
                        homeNotifier.holidayListResponse.outputRowErrors.outputRowErrors[0].textMessage,
                         AppLocalizations.of(context).translate('okay'))
            );

       }
        Future.delayed(Duration(seconds: 2), () {
          homeNotifier.holidayListResponse = null;
        });
      });*/
    } else {
/*
      Future.delayed(Duration.zero, () {
        //_showSnackBarMessage( homeNotifier.holidayListResponse.outputRowErrors.outputRowErrors[0].textMessage);

          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay( AppLocalizations.of(context).translate('unavailableDates'),
                      AppConstants.SOME_THING_WENT_WRONG,
                       AppLocalizations.of(context).translate('okay'))
          );

        Future.delayed(Duration(seconds: 2), () {
          homeNotifier.holidayListResponse = null;
        });
      });*/

    }
  }

  void loadInitialData(HomeNotifier homeNotifier) {
    debugPrint("initState:==> ");
    debugPrint(
        "initState :==> " + homeNotifier.initialIntervals.length.toString());
    for (int arrIndex = 0;
        arrIndex < homeNotifier.initialIntervals.length;
        arrIndex++) {
      var interval = homeNotifier.initialIntervals[arrIndex];
      debugPrint("Outter interval :==> " +
          arrIndex.toString() +
          "," +
          interval.toString());
      debugPrint(
          "Outter interval isBlocked:==> " + interval.isBlocked.toString());
      debugPrint(
          "Outter interval 0th position:==> " + interval.dates[0].toString());
      debugPrint(
          "Outter interval 1st position :==> " + interval.dates[1].toString());
      for (int InnerArrIndex = 0;
          InnerArrIndex < interval.dates.length;
          InnerArrIndex++) {
        debugPrint("InnerList value,InnerArrIndex :==> " +
            InnerArrIndex.toString() +
            "," +
            interval.dates[InnerArrIndex].toString());
        intervals.add([
          Day(
              date: DateTime(interval.dates[0].year, interval.dates[0].month,
                  interval.dates[0].day),
              ignore: null,
              inInterval: null,
              isEnd: null,
              isStart: null,
              isBlocked: interval.isBlocked,
              isHolidayBlocked: interval.isHolidayBlocked,
              isContractDate: interval.isContractDate,
              isNewlyAddedOne: null,

              //Assignment list
              customerName: interval.CustomerName,
              offerNumber: interval.OfferNumber,
              netToSalary: interval.NetToSalary,
              //Holiday list
              cgScheduleGuid: interval.CGScheduleNumber),
          Day(
              date: DateTime(interval.dates[1].year, interval.dates[1].month,
                  interval.dates[1].day),
              ignore: null,
              inInterval: null,
              isEnd: null,
              isStart: null,
              isBlocked: interval.isBlocked,
              isHolidayBlocked: interval.isHolidayBlocked,
              isContractDate: interval.isContractDate,
              isNewlyAddedOne: null,

              //Assignment list
              customerName: interval.CustomerName,
              offerNumber: interval.OfferNumber,
              netToSalary: interval.NetToSalary,
              //Holiday list
              cgScheduleGuid: interval.CGScheduleNumber)
        ]);

        intervals = mergeInterval(intervals);
      }
    }
  }

  List<Day> getDays() {
    final List<Day> days = [];

    DateTime start = DateTime(masterDate.year, masterDate.month);
    DateTime finish = DateTime(masterDate.year, masterDate.month + 1);

    start = start.add(Duration(days: -start.weekday));

    start = start.subtract(Duration(
        days:
            (7 - dateTimeSymbolMap()[Intl.getCurrentLocale()].FIRSTDAYOFWEEK) %
                7));

    finish = finish.add(Duration(days: 6 - finish.weekday));

    finish = finish.subtract(Duration(
        days:
            (7 - dateTimeSymbolMap()[Intl.getCurrentLocale()].FIRSTDAYOFWEEK) %
                7));

    for (var i = 0; i <= finish.difference(start).inDays; i++) {
      final date = start.add(Duration(days: i + 1));

      bool inInterval = false;
      bool isStart = false;
      bool isEnd = false;
      bool isBlocked = false;
      bool isHolidayBlocked = false;
      bool isContractDate = false;

      for (final interval in intervals) {
        inInterval = false;
        isStart = false;
        isEnd = false;
        isBlocked = false;
        isHolidayBlocked = false;
        isContractDate = false;
        if (interval[0].date == date) {
          isStart = true;
        }
        if (interval[1].date == date) {
          isEnd = true;
        }

        //Blocked Dated
        if (interval[0].isBlocked != null && interval[0].isBlocked) {
          isBlocked = true;
        }
        if (interval[1].isBlocked != null && interval[1].isBlocked) {
          isBlocked = true;
        }

        //isContractDate
        if (interval[0].isContractDate != null && interval[0].isContractDate) {
          isContractDate = true;
        }
        if (interval[1].isContractDate != null && interval[1].isContractDate) {
          isContractDate = true;
        }

        //isHolidayBlocked
        if (interval[0].isHolidayBlocked != null &&
            interval[0].isHolidayBlocked) {
          isHolidayBlocked = true;
        }
        if (interval[1].isHolidayBlocked != null &&
            interval[1].isHolidayBlocked) {
          isHolidayBlocked = true;
        }

        //SinceEpoch
        if (interval[0].date.millisecondsSinceEpoch <=
                date.millisecondsSinceEpoch &&
            date.millisecondsSinceEpoch <=
                interval[1].date.millisecondsSinceEpoch) {
          inInterval = true;
          break;
        }
      }

      days.add(
        Day(
          date: date,
          ignore: date.month == masterDate.month,
          inInterval: inInterval,
          isStart: isStart,
          isEnd: isEnd,
          isBlocked: isBlocked,
          isHolidayBlocked: isHolidayBlocked,
          isContractDate: isContractDate,
        ),
      );
    }

    return days;
  }

  List<List<Day>> mergeInterval(List<List<Day>> intervals) {
    debugPrint(" :==>");
    final List<List<Day>> result = [];

    intervals.sort((a, b) => a[0]
        .date
        .millisecondsSinceEpoch
        .compareTo(b[0].date.millisecondsSinceEpoch));

    List<Day> currentRange;

    for (final range in intervals) {
      if (range[0].date.millisecondsSinceEpoch >
          range[1].date.millisecondsSinceEpoch) continue;

      if (currentRange == null) {
        currentRange = range;
        continue;
      }

      if (currentRange[1].date.millisecondsSinceEpoch <
          range[0].date.millisecondsSinceEpoch) {
        result.add(currentRange);
        currentRange = range;
      } else if (currentRange[1].date.millisecondsSinceEpoch <
          range[1].date.millisecondsSinceEpoch) {
        currentRange[1] = range[1];
      }
    }

    if (currentRange != null) {
      result.add(currentRange);
    }

    return result;
  }

  void click(Day day, HomeNotifier homeNotifier, BuildContext context) {
    // if(day.inInterval == true){
    bool isDayBlocked = false;
    bool isHolidayBlocked = false;
    int selectedHolidayIndex = -1;
    int newAddHolidayIndex = -1;
    String cgScheduleGuid = "";
    String fromDate = "";
    String toDate = "";

    debugPrint("inInterval clicked :==>");
    debugPrint("inInterval clicked date :==>" + day.date.toString());
    for (int arrIndex = 0; arrIndex < intervals.length; arrIndex++) {
      var interval = intervals[arrIndex];
      for (int InnerArrIndex = 0;
          InnerArrIndex < interval.length - 1;
          InnerArrIndex++) {
        final startTime = interval[0].date;
        final endTime = interval[1].date;
        final isStartDateBlocked = interval[0].isBlocked;
        final isEndDateBlocked = interval[1].isBlocked;
        final isStartHolidayDateBlocked = interval[0].isHolidayBlocked;
        final isEndHolidayDateBlocked = interval[1].isHolidayBlocked;

        final currentTime = day.date;

        if (interval != null &&
            interval[0].isNewlyAddedOne != null &&
            interval[0].isNewlyAddedOne &&
            interval[1].isNewlyAddedOne != null &&
            interval[1].isNewlyAddedOne) {
          newAddHolidayIndex = arrIndex;
        }

        if (currentTime.isAtSameMomentAs(startTime)) {
          debugPrint("It's same as startDate:==> ");
          debugPrint("Range Index :==> " + arrIndex.toString());
          debugPrint("Range Start Date @@:==> " + startTime.toString());
          debugPrint("Range End Date @@:==> " + endTime.toString());

          fromDate = interval[0].date.toString();
          toDate = interval[1].date.toString();

          if (interval[0].cgScheduleGuid != null) {
            debugPrint("Range cgScheduleGuid @@:==> " +
                interval[0].cgScheduleGuid.toString());
            cgScheduleGuid = interval[0].cgScheduleGuid.toString();
          }
          selectedHolidayIndex = arrIndex;

          if (isStartDateBlocked != null &&
              isStartDateBlocked &&
              isEndDateBlocked != null &&
              isEndDateBlocked) {
            // showToastMessage("You Can't edit this range !!");
            isDayBlocked = true;
            break;
          }

          if (isStartHolidayDateBlocked != null &&
              isStartHolidayDateBlocked &&
              isEndHolidayDateBlocked != null &&
              isEndHolidayDateBlocked) {
            // showToastMessage("You Can't edit this range !!");
            isHolidayBlocked = true;
            break;
          }
        } else if (currentTime.isAtSameMomentAs(endTime)) {
          debugPrint("It's same as endDate :==> ");
          debugPrint("Range Index :==> " + arrIndex.toString());
          debugPrint("Range Start Date @@:==> " + startTime.toString());
          debugPrint("Range End Date @@:==> " + endTime.toString());

          fromDate = interval[0].date.toString();
          toDate = interval[1].date.toString();

          if (interval[0].cgScheduleGuid != null) {
            debugPrint("Range cgScheduleGuid @@:==> " +
                interval[0].cgScheduleGuid.toString());
            cgScheduleGuid = interval[0].cgScheduleGuid.toString();
          }
          selectedHolidayIndex = arrIndex;

          if (isStartDateBlocked != null &&
              isStartDateBlocked &&
              isEndDateBlocked != null &&
              isEndDateBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isDayBlocked = true;
            break;
          }

          if (isStartHolidayDateBlocked != null &&
              isStartHolidayDateBlocked &&
              isEndHolidayDateBlocked != null &&
              isEndHolidayDateBlocked) {
            // showToastMessage("You Can't edit this range !!");
            isHolidayBlocked = true;
            break;
          }
        } else {
          if (currentTime.isAfter(startTime) && currentTime.isBefore(endTime)) {
            debugPrint("It's in Range:==> ");
            debugPrint("Range Index :==> " + arrIndex.toString());
            debugPrint("Range Start Date @@:==> " + startTime.toString());
            debugPrint("Range End Date @@:==> " + endTime.toString());

            fromDate = interval[0].date.toString();
            toDate = interval[1].date.toString();
            if (interval[0].cgScheduleGuid != null) {
              debugPrint("Range cgScheduleGuid @@:==> " +
                  interval[0].cgScheduleGuid.toString());
              cgScheduleGuid = interval[0].cgScheduleGuid.toString();
            }
            selectedHolidayIndex = arrIndex;

            if (isStartDateBlocked != null &&
                isStartDateBlocked &&
                isEndDateBlocked != null &&
                isEndDateBlocked) {
              //showToastMessage("You Can't edit this range !!");
              isDayBlocked = true;
              break;
            }
            if (isStartHolidayDateBlocked != null &&
                isStartHolidayDateBlocked &&
                isEndHolidayDateBlocked != null &&
                isEndHolidayDateBlocked) {
              // showToastMessage("You Can't edit this range !!");
              isHolidayBlocked = true;
              break;
            }
          } else {
            //Logic for multi unavailable dates remove
            // selectedAvailableIndex = arrIndex;
          }
        }
      }
    }

    if (isDayBlocked) {
      //showToastMessage("You Can't edit this range !!");

      debugPrint(
          "It's in Range from and toDate:==> " + fromDate + "," + toDate);
      var now = new DateTime.now();
      // 0 is equal / positive value greater / negative value being less
      if (day.date.compareTo(now) < 0 && day.date.compareTo(now) < 0) {
        showDialog(
            context: context,
            builder: (_) => AlertOverlay(
                AppLocalizations.of(context).translate('ongoingContract'),
                AppLocalizations.of(context).translate('youCannotChangeOrDeleteOnGoingSchedules'),
                 AppLocalizations.of(context).translate('okay')));
      } else {
        debugPrint("It's a future contract date :==> " + day.date.toString());
      }
    } else {
      if (newAddHolidayIndex != -1)
        intervals.removeAt(newAddHolidayIndex); //Add only one range logic

      if (cgScheduleGuid != "" && fromDate != "" && toDate != "") {
        var selectedDate = masterDate;
        var monthFormatter = new DateFormat('MM');
        var yearFormatter = new DateFormat('yyyy');
        String currentDateMonth = monthFormatter.format(DateTime.now());
        String selectedDateMonth = monthFormatter.format(selectedDate);
        debugPrint("selectedDateMonth :==>" + selectedDateMonth.toString());
        debugPrint("currentDateMonth:==>" + currentDateMonth.toString());

        var currentDate = new DateTime.now();
        debugPrint(
            "CompareTo :==>" + masterDate.compareTo(currentDate).toString());
        if (int.parse(selectedDateMonth) >= int.parse(currentDateMonth)) {
          if (selectedHolidayIndex != -1) {
            Navigator.of(context)
                .push(
                  new MaterialPageRoute(
                    builder: (_) =>
                        new ScheduleDetails(fromDate, toDate, cgScheduleGuid),
                  ),
                )
                .then((val) =>
                    val ? getAssignmentAndHolidayList(homeNotifier) : null);
          }
        }
      } else {
        selectStartEndDateProcess(homeNotifier, true);
        cgScheduleGuid = "";
      }
    }
  }

  selectStartEndDateProcess(HomeNotifier homeNotifier, bool isStartDate) {
    var selectedDate = masterDate;
    var monthFormatter = new DateFormat('MM');
    var yearFormatter = new DateFormat('yyyy');
    String currentDateMonth = monthFormatter.format(DateTime.now());
    String selectedDateMonth = monthFormatter.format(selectedDate);
    debugPrint("selectedDateMonth :==>" + selectedDateMonth.toString());
    debugPrint("currentDateMonth:==>" + currentDateMonth.toString());

    var currentDate = new DateTime.now();
    debugPrint("CompareTo :==>" + masterDate.compareTo(currentDate).toString());
    if (int.parse(selectedDateMonth) >= int.parse(currentDateMonth)) {
      if (isStartDate) {
        _selectStartingDate(homeNotifier, context);
      } else {
        _selectEndDate(homeNotifier, context);
      }
    } else {
      //  showToastMessage("You can't add or edit the previous month unavailable dates");
      //   showDialog(
      //       context: context,
      //       builder: (_) => AlertOverlay(
      //            AppLocalizations.of(context).translate('unavailableDates'),
      //           AppConstants.YOU_CANNOT_ADD_EDIT_UNAVAILABLE_DATES,
      //            AppLocalizations.of(context).translate('okay')));
    }
  }

  List<Widget> buildCalendar(HomeNotifier homeNotifier, BuildContext context) {
    var currentDate = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String currentFormatedDate = formatter.format(currentDate);
    print("Current date now :==>" + currentDate.toString());
    print("Current date formated:==>" + currentFormatedDate);

    List<Widget> list = [];
    final days = getDays();

    final weeks = partition(days, 7);

    for (final week in weeks) {
      final List<Widget> weekL = [];

      for (Day day in week) {
        if (start != null &&
            day.date == start.date &&
            day.inInterval == false) {
          day = Day(
            date: day.date,
            ignore: day.ignore,
            inInterval: true,
            isStart: true,
            isEnd: true,
          );
        }

        weekL.add(Expanded(
          child: GestureDetector(
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(day.isStart ? 50 : 0),
                topRight: Radius.circular(day.isEnd ? 50 : 0),
                bottomLeft: Radius.circular(day.isStart ? 50 : 0),
                bottomRight: Radius.circular(day.isEnd ? 50 : 0),
              ),
              // borderRadius: BorderRadius.all(Radius.circular(50)),

              child: Container(
//                color: day.inInterval
//                    ? widget.selectionColor
//                    : Color.fromRGBO(0, 0, 0, 0),

                color: day.inInterval
                    //?(day.isContractDate) ? Colors.green: (day.isHolidayBlocked)? Colors.brown:widget.selectionColor
                    ? (day.date.isBefore(currentDate) &&
                            (day.isBlocked != null && day.isBlocked))
                        ? Colors.green
                        : (day.date.isAfter(currentDate) &&
                                (day.isBlocked != null && day.isBlocked))
                            ? Colors.brown
                            : (day.date.isAtSameMomentAs(currentDate))
                                ? Colors.brown
                                : selectionColor
                    : Color.fromRGBO(0, 0, 0, 0),
                margin: EdgeInsets.fromLTRB(0.0, 1.3, 0.0, 1.3),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      day.date.day.toString(),
                      style: day.ignore
                          ? (day.inInterval
                              ? TextStyle(
                                  color: selectedDateTextColor,
                                )
                              : TextStyle(
                                  color: dateTextColor,
                                ))
                          : (day.inInterval
                              ? TextStyle(
                                  color: selectedIgnoreTextColor,
                                )
                              : TextStyle(
                                  color: ignoreTextColor,
                                )),
                    ),
                  ),
                ),
              ),
            ),
            onTap: () {
              //Get selected date
              var selectedDate = masterDate;
              String currentDateMonth = monthFormatter.format(DateTime.now());
              String currentDateYear = yearFormatter.format(DateTime.now());
              String selectedDateMonth = monthFormatter.format(selectedDate);
              String selectedDateYear = yearFormatter.format(selectedDate);
              debugPrint("selectedDateMonth :==>" + selectedDateMonth.toString());
              debugPrint("currentDateMonth:==>" + currentDateMonth.toString());
              debugPrint("selectedDateYear :==>" + selectedDateYear.toString());
              debugPrint("currentDateYear :==>" + currentDateYear.toString());
            if((int.parse(selectedDateMonth) >=
                  int.parse(currentDateMonth)) &&
                  (int.parse(selectedDateYear) >=
                      int.parse(currentDateYear))){
              click(day, homeNotifier, context);
            }else{
              debugPrint("You can't Add or Edit for previous months");
            }

            },
          ),
        ));
      }

      list.add(Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: weekL,
        ),
      ));
    }

    return list;
  }

  Future<void> _selectStartingDate(
      HomeNotifier homeNotifier, BuildContext context) async {
    var currentDate = new DateTime.now();

    var dateFormatter = new DateFormat('dd');

    //For current Date
    String currentDateMonth = monthFormatter.format(currentDate);

    //For master date
    String masterDay = dateFormatter.format(masterDate);
    String masterDateMonth = monthFormatter.format(masterDate);
    String masterDateYear = yearFormatter.format(masterDate);
    print("Current date now :==>" + currentDate.toString());
    print("currentDateMonth formatted month :==>" + currentDateMonth);
    print("master date formatted month:==>" + masterDateMonth);
    print("master date formatted year:==>" + masterDateYear);
    print("master date formatted day:==>" + masterDay);

    bool isAssignmentDayInBlockedRange = false;
    bool isHolidayDateInRange = false;

    final DateTime picked = await showDatePicker(
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light().copyWith(
              primary: colorLiteOrange,
            ),
          ),
          child: child,
        );
      },
      context: context,
      initialDate: (currentDateMonth == masterDateMonth)
          ? DateTime.now()
          : DateTime(int.parse(masterDateYear), int.parse(masterDateMonth), 1),
      helpText:AppLocalizations.of(context).translate("selectStartingDate"),
      locale: Locale(globalLanguageCode),
      cancelText: AppLocalizations.of(context).translate("cancel"),
      confirmText: AppLocalizations.of(context).translate("okay"),
      //firstDate: DateTime.now(),
      //lastDate: DateTime(2101));
      firstDate: (currentDateMonth == masterDateMonth)
          ? DateTime.now()
          : DateTime(int.parse(masterDateYear), int.parse(masterDateMonth), 1),
      lastDate: DateTime(int.parse(masterDateYear), int.parse(masterDateMonth),
          LastDateOfMonthArr[int.parse(masterDateMonth) - 1]),
    );
    if (picked == null) {
      print("cancel starting date of datepicker");
    }
    if (picked != null) {
      debugPrint("Picked Starting date :==>" + picked.toString());
      setState(() {
        selectedStartDate = picked;
      });

      homeNotifier.fromDate = selectedStartDate;

      ///Check selected starting date in range
      for (int arrIndex = 0; arrIndex < intervals.length; arrIndex++) {
        var interval = intervals[arrIndex];
        for (int InnerArrIndex = 0;
            InnerArrIndex < interval.length - 1;
            InnerArrIndex++) {
          final startTime = interval[0].date;
          final endTime = interval[1].date;
          final currentTime = selectedStartDate;

          final isStartDateBlocked = interval[0].isBlocked;
          final isEndDateBlocked = interval[1].isBlocked;

          final isStartHolidayDateBlocked = interval[0].isHolidayBlocked;
          final isEndHolidayDateBlocked = interval[1].isHolidayBlocked;

          if (currentTime.isAtSameMomentAs(startTime)) {
            debugPrint("It's same as startDate:==> ");
            debugPrint("Range Index :==> " + arrIndex.toString());
            debugPrint("Range Start Date @@:==> " + startTime.toString());
            debugPrint("Range End Date @@:==> " + endTime.toString());
            //isStartingDateInRange = true;
            //break;
            if (isStartDateBlocked != null &&
                isStartDateBlocked &&
                isEndDateBlocked != null &&
                isEndDateBlocked) {
              //showToastMessage("You Can't edit this range !!");
              isAssignmentDayInBlockedRange = true;
              break;
            }

            if (isStartHolidayDateBlocked != null &&
                isStartHolidayDateBlocked &&
                isEndHolidayDateBlocked != null &&
                isEndHolidayDateBlocked) {
              //showToastMessage("You Can't edit this range !!");
              isHolidayDateInRange = true;
              break;
            }
          } else if (currentTime.isAtSameMomentAs(endTime)) {
            debugPrint("It's same as endDate :==> ");
            debugPrint("Range Index :==> " + arrIndex.toString());
            debugPrint("Range Start Date @@:==> " + startTime.toString());
            debugPrint("Range End Date @@:==> " + endTime.toString());
//            isStartingDateInRange = true;
//            break;
            if (isStartDateBlocked != null &&
                isStartDateBlocked &&
                isEndDateBlocked != null &&
                isEndDateBlocked) {
              //showToastMessage("You Can't edit this range !!");
              isAssignmentDayInBlockedRange = true;
              break;
            }

            if (isStartHolidayDateBlocked != null &&
                isStartHolidayDateBlocked &&
                isEndHolidayDateBlocked != null &&
                isEndHolidayDateBlocked) {
              //showToastMessage("You Can't edit this range !!");
              isHolidayDateInRange = true;
              break;
            }
          } else {
            if (currentTime.isAfter(startTime) &&
                currentTime.isBefore(endTime)) {
              debugPrint("Starting date It's in Range:==> ");
              debugPrint(
                  "Starting date  Range Index :==> " + arrIndex.toString());
              debugPrint("Starting date  Range Start Date @@:==> " +
                  startTime.toString());
              debugPrint(
                  "Starting date  Range End Date @@:==> " + endTime.toString());
//              isStartingDateInRange = true;
//              break;
              if (isStartDateBlocked != null &&
                  isStartDateBlocked &&
                  isEndDateBlocked != null &&
                  isEndDateBlocked) {
                //showToastMessage("You Can't edit this range !!");
                isAssignmentDayInBlockedRange = true;
                break;
              }
              if (isStartHolidayDateBlocked != null &&
                  isStartHolidayDateBlocked &&
                  isEndHolidayDateBlocked != null &&
                  isEndHolidayDateBlocked) {
                //showToastMessage("You Can't edit this range !!");
                isHolidayDateInRange = true;
                break;
              }
            } else {
              // _selectEndDate(context);
              debugPrint(
                  "Starting date is Already in selected Range  else block @@:==> ");
            }
          }
        }
      }
      if (!isAssignmentDayInBlockedRange && !isHolidayDateInRange) {
        //_selectEndDate(homeNotifier, context);
        selectStartEndDateProcess(homeNotifier, false);
      } else {
        debugPrint("Starting date is Already in blocked Range @@:==> ");
        // showToastMessage("Starting date is Already in blocked Range !!");
        if (isHolidayDateInRange) {
          debugPrint(
              "Holiday Ending date is in Range final else block @@:==> ");
          //showToastMessage("Holiday Ending date is Already in blocked Range !!");
          showDialog(
              context: context,
              builder: (_) => AlertOverlay(
                   AppLocalizations.of(context).translate('unavailableDates'),
                   AppLocalizations.of(context).translate('youCannotChangeUnavailableDate'),
                  //AppConstants.YOU_CANNOT_CHANGE_HOLIDAY_DATE,
                   AppLocalizations.of(context).translate('okay')));
        }
        if (isAssignmentDayInBlockedRange) {
          debugPrint(
              "Assignment starting date is in Range final else block @@:==> ");
          //showToastMessage("Holiday Ending date is Already in blocked Range !!");
          showDialog(
              context: context,
              builder: (_) => AlertOverlay(
                  AppLocalizations.of(context).translate('ongoingContract'),
                  AppLocalizations.of(context).translate('youCannotChangeOrDeleteOnGoingSchedules'),
                   AppLocalizations.of(context).translate('okay')));
        }
      }
    }
  }

  void showToastMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM // also possible "TOP" and "CENTER"
        );
  }

  Future<void> _selectEndDate(
      HomeNotifier homeNotifier, BuildContext context) async {
    var currentDate = new DateTime.now();

    //For current Date
    String currentDateMonth = monthFormatter.format(currentDate);

    //For master date
    String masterDay = dateFormatter.format(masterDate);
    String masterDateMonth = monthFormatter.format(masterDate);
    String masterDateYear = yearFormatter.format(masterDate);
    print("Current date now :==>" + currentDate.toString());
    print("currentDateMonth formatted month :==>" + currentDateMonth);
    print("master date formatted month:==>" + masterDateMonth);
    print("master date formatted year:==>" + masterDateYear);
    print("master date formatted day:==>" + masterDay);

    final DateTime picked = await showDatePicker(
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme: ColorScheme.light().copyWith(
                primary: colorLiteOrange,
              ),
            ),
            child: child,
          );
        },
        context: context,
        initialDate: (currentDateMonth == masterDateMonth)
            ? DateTime.now()
            : DateTime(
                int.parse(masterDateYear), int.parse(masterDateMonth), 1),
        helpText:AppLocalizations.of(context).translate("selectEndingDate"),
        locale: Locale(globalLanguageCode),
        cancelText: AppLocalizations.of(context).translate("cancel"),
        confirmText: AppLocalizations.of(context).translate("okay"),
        // firstDate: DateTime(2015, 8),
        // lastDate: DateTime(2101));
        firstDate: (currentDateMonth == masterDateMonth)
            ? DateTime.now()
            : DateTime(
                int.parse(masterDateYear), int.parse(masterDateMonth), 1),
        lastDate: DateTime(
            int.parse(masterDateYear),
            int.parse(masterDateMonth),
            LastDateOfMonthArr[int.parse(masterDateMonth) - 1]));
    //if (picked != null && picked != selectedEndDate) {

    if (picked == null) {
      print("cancel endingdate of datepicker");
      setState(() {
        selectedEndDate = selectedStartDate;
      });
      getEndingDateLogic(homeNotifier);
    }
    if (picked != null) {
      debugPrint("Picked Ending date :==>" + picked.toString());
      // 0 is equal / positive value greater / negative value being less
      if (selectedStartDate.compareTo(picked) <= 0) {
        setState(() {
          selectedEndDate = picked;
        });

        getEndingDateLogic(homeNotifier);
      } else {
        showDialog(
            context: context,
            builder: (_) => AlertOverlay(
                 AppLocalizations.of(context).translate('unavailableDates'),
                 AppLocalizations.of(context).translate('toDateShouldGreaterFromDate'),
                //AppConstants.TO_DATE_SHOULD_BE_GRATER_THEN_FROM_DATE,
                 AppLocalizations.of(context).translate('okay')));
      }
    }
  }

  void getEndingDateLogic(HomeNotifier homeNotifier) {
    bool isHolidayEndingDateInRange = false;
    bool isEndingDayInBlockedRange = false;

    homeNotifier.toDate = selectedEndDate;

    ///Check selected starting date in range
    for (int arrIndex = 0; arrIndex < intervals.length; arrIndex++) {
      var interval = intervals[arrIndex];
      for (int InnerArrIndex = 0;
          InnerArrIndex < interval.length - 1;
          InnerArrIndex++) {
        final startTime = interval[0].date;
        final endTime = interval[1].date;
        final currentTime = selectedEndDate;
        final isStartDateBlocked = interval[0].isBlocked;
        final isEndDateBlocked = interval[1].isBlocked;

        final isStartHolidayBlocked = interval[0].isHolidayBlocked;
        final isEndHolidayBlocked = interval[1].isHolidayBlocked;

        if (currentTime.isAtSameMomentAs(startTime)) {
          debugPrint("It's same as startDate:==> ");
          debugPrint("Range Index :==> " + arrIndex.toString());
          debugPrint("Range Start Date @@:==> " + startTime.toString());
          debugPrint("Range End Date @@:==> " + endTime.toString());
//          isEndingDateInRange = true;
//          break;
          if (isStartDateBlocked != null &&
              isStartDateBlocked &&
              isEndDateBlocked != null &&
              isEndDateBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isEndingDayInBlockedRange = true;
            break;
          }

          if (isStartHolidayBlocked != null &&
              isStartHolidayBlocked &&
              isEndHolidayBlocked != null &&
              isEndHolidayBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isHolidayEndingDateInRange = true;
            break;
          }
        } else if (currentTime.isAtSameMomentAs(endTime)) {
          debugPrint("It's same as endDate :==> ");
          debugPrint("Range Index :==> " + arrIndex.toString());
          debugPrint("Range Start Date @@:==> " + startTime.toString());
          debugPrint("Range End Date @@:==> " + endTime.toString());
//          isEndingDateInRange = true;
//          break;
          if (isStartDateBlocked != null &&
              isStartDateBlocked &&
              isEndDateBlocked != null &&
              isEndDateBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isEndingDayInBlockedRange = true;
            break;
          }
        } else if (currentTime.isAfter(startTime) &&
            currentTime.isBefore(endTime)) {
          debugPrint("Ending date It's in Range:==> ");
          debugPrint("Ending date  Range Index :==> " + arrIndex.toString());
          debugPrint(
              "Ending date  Range Start Date @@:==> " + startTime.toString());
          debugPrint(
              "Ending date  Range End Date @@:==> " + endTime.toString());
//            isEndingDateInRange = true;
//            break;
          if (isStartDateBlocked != null &&
              isStartDateBlocked &&
              isEndDateBlocked != null &&
              isEndDateBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isEndingDayInBlockedRange = true;
            break;
          }

          if (isStartHolidayBlocked != null &&
              isStartHolidayBlocked &&
              isEndHolidayBlocked != null &&
              isEndHolidayBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isHolidayEndingDateInRange = true;
            break;
          }
        } else if (startTime.isAfter(selectedStartDate) &&
            startTime.isBefore(selectedEndDate)) {
          debugPrint("Selecting date It's in Range:==> ");
          debugPrint("Selecting date  Range Index :==> " + arrIndex.toString());
          debugPrint("Selecting date  Range Start Date @@:==> " +
              startTime.toString());
          debugPrint(
              "Selecting date  Range End Date @@:==> " + endTime.toString());
//            isEndingDateInRange = true;
//            break;
          if (isStartDateBlocked != null &&
              isStartDateBlocked &&
              isEndDateBlocked != null &&
              isEndDateBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isEndingDayInBlockedRange = true;
            break;
          }

          if (isStartHolidayBlocked != null &&
              isStartHolidayBlocked &&
              isEndHolidayBlocked != null &&
              isEndHolidayBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isHolidayEndingDateInRange = true;
            break;
          }
        } else if (endTime.isAfter(selectedStartDate) &&
            endTime.isBefore(selectedEndDate)) {
          debugPrint("Selecting date It's in Range:==> ");
          debugPrint("Selecting date  Range Index :==> " + arrIndex.toString());
          debugPrint("Selecting date  Range Start Date @@:==> " +
              startTime.toString());
          debugPrint(
              "Selecting date  Range End Date @@:==> " + endTime.toString());
//            isEndingDateInRange = true;
//            break;
          if (isStartDateBlocked != null &&
              isStartDateBlocked &&
              isEndDateBlocked != null &&
              isEndDateBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isEndingDayInBlockedRange = true;
            break;
          }

          if (isStartHolidayBlocked != null &&
              isStartHolidayBlocked &&
              isEndHolidayBlocked != null &&
              isEndHolidayBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isHolidayEndingDateInRange = true;
            break;
          }
        } else {
          // _selectEndDate(context);
          debugPrint(
              "Ending date is Already in selected Range  else block @@:==> ");
        }
      }
    }
    if (isEndingDayInBlockedRange) {
      debugPrint("Ending date is in Range final else block @@:==> ");
      //showToastMessage("Ending date is Already in blocked Range !!");
      showDialog(
          context: context,
          builder: (_) => AlertOverlay(
              AppLocalizations.of(context).translate('ongoingContract'),
              AppLocalizations.of(context).translate('youCannotChangeOrDeleteOnGoingSchedules'),
               AppLocalizations.of(context).translate('okay')));
    } else if (isHolidayEndingDateInRange) {
      debugPrint("Holiday Ending date is in Range final else block @@:==> ");
      //showToastMessage("Holiday Ending date is Already in blocked Range !!");
      showDialog(
          context: context,
          builder: (_) => AlertOverlay(
              AppLocalizations.of(context).translate('unavailableDates'),
              AppLocalizations.of(context).translate('youCannotChangeUnavailableDate'),
              //AppConstants.YOU_CANNOT_CHANGE_HOLIDAY_DATE,
              AppLocalizations.of(context).translate('okay')));
    } else {
      intervals.add([
        Day(
            // date: DateTime(interval[0].year, interval[0].month, interval[0].day),
            date: selectedStartDate,
            ignore: null,
            inInterval: null,
            isEnd: null,
            isStart: null,
            isBlocked: false,
            isContractDate: false,
            isHolidayBlocked: false,
            isNewlyAddedOne: true,

            //Assignment list
            customerName: null,
            offerNumber: null,
            netToSalary: null,
            //Holiday list
            cgScheduleGuid: null),
        Day(
            //date: DateTime(interval[1].year, interval[1].month, interval[1].day),
            date: selectedEndDate,
            ignore: null,
            inInterval: null,
            isEnd: null,
            isStart: null,
            isBlocked: false,
            isContractDate: false,
            isHolidayBlocked: false,
            isNewlyAddedOne: true,

            //Assignment list
            customerName: null,
            offerNumber: null,
            netToSalary: null,
            //Holiday list
            cgScheduleGuid: null)
      ]);

      intervals = mergeInterval(intervals);
    }
  }
}
