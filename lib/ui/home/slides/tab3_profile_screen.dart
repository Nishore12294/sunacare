import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/Offers/SetCareGiverProfileImgResponse.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/core/notifier/profile_notifier.dart';
import 'package:suna_care/ui/changepassword/change_password_screen.dart';
import 'package:suna_care/ui/login_screen.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/ui/profile/change_language_screen.dart';
import 'package:suna_care/ui/profile/notification_settings_screen.dart';
import 'package:suna_care/ui/profile/notifications_list_screen.dart';
import 'package:suna_care/ui/profile/view_profile_screen.dart';
import 'package:suna_care/utils/AppLanguage.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';
//import 'package:ishare/screens/custom_expansion_tile.dart' as custom;
import 'package:suna_care/ui/offers/custom_expansion_tile.dart' as custom;
import 'package:suna_care/utils/widget_helper/custom_switch.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';


class Tab3ProfileScreen extends StatefulWidget {
  static const routeName = '/tab_3_profile_screen';
  String offerGuid = "";

  Tab3ProfileScreen({this.offerGuid});

  @override
  _Tab3ProfileScreenState createState() => _Tab3ProfileScreenState();
}

class _Tab3ProfileScreenState extends State<Tab3ProfileScreen> {
  final log = getLogger('Tab3ProfileScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  String offerGuid;
  ProfileNotifier profileNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  var _keyValidationForm = GlobalKey<FormState>();
  final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  _Tab3ProfileScreenState();
    bool isFromOfferDetails = false;
  @override
  void initState() {
    super.initState();
    setInitialData();
  }

  setInitialData() async {

    if (widget != null) {
      offerGuid = widget.offerGuid;
      log.d('offerGuid: ==>'+offerGuid);
      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>"+accessToken);
    }




  }


  @override
  Widget build(BuildContext context) {
    isFromOfferDetails = ModalRoute.of(context).settings.arguments??false;
      return ChangeNotifierProvider<ProfileNotifier>(
        builder: (context) => ProfileNotifier(context,isFromWhichScreen:"profileScreen",offerGuid:""),
        child: Scaffold(
          backgroundColor: colorWhite,
          key: _scaffoldKey,

          body: Consumer<ProfileNotifier>(
            builder: (context, profileNotifier, _) => ModalProgressHUD(
              inAsyncCall: profileNotifier.isLoading,
              child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Container(
                    color: colorWhite,
                    child:  Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                      Expanded(
                      flex: 1,
                      child:  Container(
                            color: colorWhite,

                            child:Container(
                              //height: 270,
                              color: colorWhite,
                              child: Column(
                                //mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[

                                    SizedBox(

                                      height: AppConstants.SIDE_MARGIN * 2,
                                      child:Container(
                                        color: colorLitePinkBG,
                                      ),
                                    ),
                                    topHeader(profileNotifier),

                                    SizedBox(

                                      height: AppConstants.SIDE_MARGIN ,
                                      child:Container(
                                        color: colorLitePinkBG,
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child:  _buildWidgetMainContent(context, profileNotifier),),


                                  ]),
                            )

                          )),

                        SizedBox(

                          height: AppConstants.SIDE_MARGIN,
                        ),

                      ],
                    ),
                  )),
            ),
          ),
        ),
      );


  }

  /////////////////////
  /*  build helpers */
  ////////////////////


  Widget topHeader(ProfileNotifier profileNotifier) {
    return Container(
      color: colorLitePinkBG,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: InkWell(

              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(

                    child: Container(
                      color: colorLitePinkBG,

                    ),
                  ),
                  SizedBox(
                    width: AppConstants.SIDE_MARGIN / 1.5,
                  ),
                  InkWell(
                    onTap: () {
                      backButtonPressed();
                    },
                    // child: Container(
                    //   color: colorLitePinkBG,
                    //
                    // ),
                    child: Visibility(
                      visible:isFromOfferDetails ,
                      child:  Container(
                        alignment: Alignment.center,
                        child: Image.asset(
                          AppImages.IMAGE_BACK,
                          height: 25,
                        ),
                      ),
                    ),

                  ),
                ],
              ),

            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              //AppConstants.PROFILE,
              AppLocalizations.of(context).translate('profile'),
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                height: 20,
              ),
            ),
          ),
        ],
      ),

    );
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }



  Widget _buildWidgetMainContent(BuildContext context, ProfileNotifier profileNotif) {

    profileNotifier = profileNotif;


    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);


       return buildWidgetProfileItem(
        context,
        imageWidth,
        cardRadio);
  }

  Widget buildWidgetProfileItem(BuildContext context,double imageWidth,  Radius cardRadio) {
    List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];
    var appLanguage = Provider.of<AppLanguage>(context);

      return  SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child:GestureDetector(
        onTap: () {
          log.d('onPress: Offers details');

        },
        child:Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              SizedBox(
                height: AppConstants.SIDE_MARGIN,
                child: Container(
                  color: colorWhite,
                ),
              ),

        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              GestureDetector(


                child: Stack(children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(left:10,right: 2, bottom: 2),
                      // child:   Image.asset(
                      //   AppImages.IMAGE_PROFILE_PHOTO_PLACEHOLDER,
                      //   height: 100,
                      //   width: 105,
                      // ),
                      child: CircleAvatar(
                        backgroundImage:
                        (profileNotifier.imageLocalFile !=null)? FileImage(profileNotifier.imageLocalFile)
                        : (profileNotifier.getCareGiverApprovedImgResp !=null && profileNotifier.getCareGiverApprovedImgResp.webMethodOutputRows !=null
                            && profileNotifier.getCareGiverApprovedImgResp.webMethodOutputRows.webMethodOutputRows[0] !=null
                            && profileNotifier.getCareGiverApprovedImgResp.webMethodOutputRows.webMethodOutputRows[0].imgUrl !=null
                            && profileNotifier.getCareGiverApprovedImgResp.webMethodOutputRows.webMethodOutputRows[0].imgUrl != ""
                        )?
                        CachedNetworkImageProvider(Uri.decodeFull(profileNotifier.getCareGiverApprovedImgResp.webMethodOutputRows.webMethodOutputRows[0].imgUrl),
                          // placeholder: (context, url) => Image.asset(
                          //   AppImages.IMAGE_PROFILE_PHOTO_PLACEHOLDER,
                          //   //fit: BoxFit.cover,
                          //   height: 50.0,
                          //   width: 50.0,
                          //
                          // ),
                          // errorWidget: (context, url, error) =>  Image.asset(
                          //   AppImages.IMAGE_PROFILE_PHOTO_PLACEHOLDER,
                          //   //fit: BoxFit.cover,
                          //   height: 50.0,
                          //   width: 50.0,
                          // ),
                          //fit: BoxFit.fitHeight,
                        ):AssetImage(AppImages.IMAGE_PROFILE_PHOTO_PLACEHOLDER)
                        ,
                        radius: 50.0,
                        foregroundColor: colorWhite,
                        backgroundColor: colorBlack,
                      ),

                  ),
                  Padding(padding: EdgeInsets.fromLTRB(67.0, 60.0, 0.0, 0.0),
                  child:  Positioned.fill(

                    child: GestureDetector(
                      onTap: (){
                        showDialog(
                            context: context,
                            builder: (BuildContext context) =>
                                _buildPopUpDialogForImageSelection(
                                    context, profileNotifier));
                      },
                    child:Align(
                      alignment: Alignment.bottomRight,
                      child: Image.asset(
                        AppImages.IMAGE_PROFILE_PHOTO_REUPLOAD,
                        height: 55,
                        width: 60,
                      ),
                     ),
                    ),
                  ),),


                ]),
              ),


            ],
        ),


              Container(
                decoration: BoxDecoration(
                    color: colorWhite,
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                child: Container(
                  padding: EdgeInsets.all(AppConstants.SIDE_MARGIN / 1.5),
                  child: Row(
                   // mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [


                      SizedBox(
                        width: AppConstants.SIDE_MARGIN / 2,
                      ),
                      Flexible(
                          fit: FlexFit.tight,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN / 4,
                                ),
                                Text(
                                  //AppConstants.PROILE_ACCOUNT,
                                    AppLocalizations.of(context).translate('account'),
                                  textAlign: TextAlign.center,
                                  style: getStyleBody1(context).copyWith(
                                      color: colorPencilBlack,
                                      fontWeight: AppFont.fontWeightRegular,
                                      fontSize: 23),
                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN,
                                ),
                                //View Profile
                                Row(

                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [


                                    Image.asset(
                                      AppImages.IMAGE_PROFILE_VIEWPROFILE,
                                      height: 20,
                                      width: 18,
                                    ),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN / 1.5,
                                    ),
                                    Flexible(
                                      fit: FlexFit.tight,
                                      child:  Text(
                                        //AppConstants.VIEW_PROFILE,
                                        AppLocalizations.of(context).translate('viewProfile'),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        style: getFormTitleStyle(context).copyWith(
                                            color: colorBlack,
                                            fontWeight: AppFont.fontWeightRegular,
                                            fontSize: 14
                                        ),
                                      ),
                                    ),

                                    GestureDetector(
                                      onTap: () async {
                                        log.d('onPress: View profile');
                                        Navigator.pushNamed(context, ViewProfileScreen.routeName);

                                      },
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(0.0, 0.0, 8.0, 0.0),
                                        child: Image.asset(
                                          AppImages.IMAGE_FORWARD,
                                          height: 20,
                                        ),
                                      ),
                                    ),

                                  ],
                                ),

                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN,
                                ),
                                //Change Password
                                Row(

                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [


                                    Image.asset(
                                      AppImages.IMAGE_PROFILE_PASSWORD,
                                      height: 20,
                                      width: 18,
                                    ),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN / 1.5,
                                    ),
                                    Flexible(
                                      fit: FlexFit.tight,
                                      child:  Text(
                                        //AppConstants.PROFILE_CHANGE_PASSWORD,
                                        AppLocalizations.of(context).translate('changePassword'),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        style: getFormTitleStyle(context).copyWith(
                                            color: colorBlack,
                                            fontWeight: AppFont.fontWeightRegular,
                                            fontSize: 14
                                        ),
                                      ),
                                    ),

                                    GestureDetector(
                                      onTap: () async {
                                        log.d('onPress: Change Password');

                                        Navigator.pushNamed(context, ChangePasswordScreen.routeName);
                                      },
                                      child:  Padding(
                                        padding: EdgeInsets.fromLTRB(0.0, 0.0, 8.0, 0.0),
                                        child:Image.asset(
                                        AppImages.IMAGE_FORWARD,
                                        height: 20,
                                      ),),
                                    ),

                                  ],
                                ),


                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN,
                                ),

                                //Change Language
                                Row(

                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [


                                    Image.asset(
                                      AppImages.IMAGE_PROFILE_LANGUAGE,
                                      height: 20,
                                      width: 18,
                                    ),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN / 1.5,
                                    ),
                                    Flexible(
                                      fit: FlexFit.tight,
                                      child:  Text(
                                        //AppConstants.PROFILE_CHANGE_LANGUAGE,
                                        AppLocalizations.of(context).translate('changeLanguage'),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        style: getFormTitleStyle(context).copyWith(
                                            color: colorBlack,
                                            fontWeight: AppFont.fontWeightRegular,
                                            fontSize: 14
                                        ),
                                      ),
                                    ),

                                    GestureDetector(
                                      onTap: () async {
                                        log.d('onPress: Language');
                                        Navigator.pushNamed(context, ChangeLanguageScreen.routeName);

                                      },
                                     child: Padding(
                                      padding: EdgeInsets.fromLTRB(0.0, 0.0, 8.0, 0.0),
                                      child: Image.asset(
                                        AppImages.IMAGE_FORWARD,
                                        height: 20,
                                      ),
                                    ),),

                                  ],
                                ),

                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN,
                                ),

                                //Notification
                                Row(

                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [


                                    Image.asset(
                                      AppImages.IMAGE_PROFILE_NOTIFICATIONS,
                                      height: 20,
                                       width: 18,
                                    ),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN / 1.5,
                                    ),
                                    Flexible(
                                      fit: FlexFit.tight,
                                      child:  Text(
                                        //AppConstants.PROFILE_NOTIFICATION,
                                        AppLocalizations.of(context).translate('notifications'),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        style: getFormTitleStyle(context).copyWith(
                                            color: colorBlack,
                                            fontWeight: AppFont.fontWeightRegular,
                                            fontSize: 14
                                        ),
                                      ),
                                    ),

                                    GestureDetector(
                                      onTap: () async {
                                        log.d('onPress: notification');
                                        //Navigator.pushNamed(context, NotificationListScreen.routeName);

                                        Navigator.pushNamed(context, NotificationSettingsScreen.routeName);
                                      },

                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(0.0, 0.0, 8.0, 0.0),
                                      child: Image.asset(
                                        AppImages.IMAGE_FORWARD,
                                        height: 20,
                                      ),),
                                    ),

                                  ],
                                ),

                              ///////////////Auth block/////////
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN*1.5,
                                ),
                                Text(
                                  //AppConstants.PROFILE_PRIVACY,
                                  AppLocalizations.of(context).translate('privacy'),
                                  textAlign: TextAlign.center,
                                  style: getStyleBody1(context).copyWith(
                                      color: colorPencilBlack,
                                      fontWeight: AppFont.fontWeightRegular,
                                      fontSize: 23),
                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN,
                                ),

                               Visibility(
                                visible: Platform.isIOS,
                                 //visible: true,
                                 child:  Column(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                   crossAxisAlignment: CrossAxisAlignment.center,
                                   children: [

                                     Row(
                                         mainAxisAlignment: MainAxisAlignment.center,
                                         crossAxisAlignment: CrossAxisAlignment.center,
                                         children: <Widget>[
                                           Image.asset(
                                             AppImages.IMAGE_PROFILE_FACEID,
                                             height: 20,
                                             width: 18,
                                           ),
                                           SizedBox(
                                             width: AppConstants.SIDE_MARGIN / 1.5,
                                           ),
                                           Expanded(
                                             flex: 1,
                                             child:Align(
                                               alignment: Alignment.centerLeft,
                                               child: Text(
                                                  // AppConstants.PROFILE_FACE_ID,
                                                   AppLocalizations.of(context).translate('faceID'),
                                                   style: getStyleBody1(context).copyWith(
                                                       color: colorPencilBlack,
                                                       fontWeight: AppFont.fontWeightRegular,
                                                       fontSize: 16
                                                   )
                                               ),
                                             ),
                                           ),
                                           GestureDetector(
                                               onTap:()=> _faceIDSetToggle(profileNotifier),
                                               behavior: HitTestBehavior.translucent,
                                               child: Center(
                                                 child: Column(
                                                   mainAxisAlignment: MainAxisAlignment.center,
                                                   crossAxisAlignment: CrossAxisAlignment.center,
                                                   children: <Widget>[
                                                     SizedBox(height: 5,),
                                                     CustomSwitch(switched: profileNotifier.faceIdToggleSwitch),
                                                   ],
                                                 ),
                                               )),


                                         ]

                                     ),

                                     SizedBox(
                                       height: AppConstants.SIDE_MARGIN,

                                     ),
                                   ],


                                 ),



                               ),





                                Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Image.asset(
                                        AppImages.IMAGE_PROFILE_FINGER_PRINT,
                                        height: 20,
                                        width: 18,
                                      ),
                                      SizedBox(
                                        width: AppConstants.SIDE_MARGIN / 1.65,
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child:Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                             // AppConstants.PROFILE_FINGERPRINT,
                                              AppLocalizations.of(context).translate('fingerprint'),
                                              style: getStyleBody1(context).copyWith(
                                                  color: colorPencilBlack,
                                                  fontWeight: AppFont.fontWeightRegular,
                                                  fontSize: 16
                                              )
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap:()=> _fingerPrintToggle(profileNotifier),
                                          behavior: HitTestBehavior.translucent,
                                          child: Center(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                SizedBox(height: 5,),
                                                CustomSwitch(switched: profileNotifier.fingerPrintToggleSwitch),
                                              ],
                                            ),
                                          )),


                                    ]

                                ),



                            ///Logout block
                            Column(

                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [

                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN,
                                ),

                                Divider(
                                  thickness: 0.5,
                                  color: colorGray,
                                ),

                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN,
                                ),




                                //Logout
                                Row(

                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [


                                    Image.asset(
                                      AppImages.IMAGE_PROFILE_LOGOUT,
                                      height: 20,
                                      width: 18,
                                    ),
                                    SizedBox(
                                      width: AppConstants.SIDE_MARGIN / 1.5,
                                    ),
                                    Flexible(
                                      fit: FlexFit.tight,
                                      child:  GestureDetector(
                                        onTap: () async {
                                          log.d('onPress: Logout text');



                                        },
                                        child:  Text(
                                          //AppConstants.LOGOUT,
                                          AppLocalizations.of(context).translate('logout'),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: false,
                                          style: getFormTitleStyle(context).copyWith(
                                              color: colorBlack,
                                              fontWeight: AppFont.fontWeightRegular,
                                              fontSize: 14
                                          ),
                                        ),

                                      ),
                                    ),


                                    GestureDetector(
                                      onTap: () async {
                                        log.d('onPress: Logout arrow');
                                        showDialog(
                                          context: context,
                                          builder: (_) =>
                                              ExitAlert(
                                                //AppConstants.PROFILE_SIGNOUT_ALERT,
                                                //AppConstants.ARE_YOU_SURE_LOGOUT,
                                               // AppConstants.YES,
                                                //AppConstants.NO,
                                                AppLocalizations.of(context).translate('signOut'),
                                                AppLocalizations.of(context).translate('areYouSureThatYouWantToSignOut'),
                                                AppLocalizations.of(context).translate('yes'),
                                                AppLocalizations.of(context).translate('no'),

                                              ),
                                        );


                                      },

                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(0.0, 0.0, 8.0, 0.0),
                                        child: Image.asset(
                                          AppImages.IMAGE_FORWARD,
                                          height: 20,
                                        ),),
                                    ),



                                  ],
                                ),

                              ])




                              ],


                            ),
                          ))
                    ],
                  ),
                ),
              ),

            ]),


      ));


  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////


  void _faceIDSetToggle(ProfileNotifier profileNotifier) {
    profileNotifier.faceIdToggleSwitch = !profileNotifier.faceIdToggleSwitch;
    debugPrint("profileNotifier.faceIdToggleSwitch:==>"+profileNotifier.faceIdToggleSwitch.toString());
  }

  Future<void> _fingerPrintToggle(ProfileNotifier profileNotifier) async {
    profileNotifier.fingerPrintToggleSwitch =
    !profileNotifier.fingerPrintToggleSwitch;
    debugPrint("profileNotifier.fingerPrintToggleSwitch:==>" +
        profileNotifier.fingerPrintToggleSwitch.toString());
    print('profile isBioAuthAvailable:==>' +
        profileNotifier.isBioAuthAvailable.toString());

    //Store local data for finger print
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (profileNotifier.fingerPrintToggleSwitch) {
      prefs.setBool(AppConstants.KEY_FINGER_PRINT_ENABLED, true);
    } else {
      prefs.setBool(AppConstants.KEY_FINGER_PRINT_ENABLED, false);
    }
    bool isPrefFingerPrintEnabled = prefs.getBool(
        AppConstants.KEY_FINGER_PRINT_ENABLED) ?? false;
    print('isPrefFingerPrintEnabled:==>' + isPrefFingerPrintEnabled.toString());
  }
  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Widget _buildPopUpDialogForImageSelection(BuildContext context,
      ProfileNotifier profileNotifier) {
    double widget = getScreenWidth(context);

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(16),
          child: Center(
            child: SizedBox(
              //height: height / 3.0,
              width: widget - 32,
              child: Card(
                color: colorWhite,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6),
                ),
                elevation: 10.0,
                child: Padding(
                    padding: const EdgeInsets.only(
                        top: 24.0, bottom: 32.0, left: 24.0, right: 24.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                           // 'Choose one option ',
                            AppLocalizations.of(context).translate('chooseOneOption'),
                            textAlign: TextAlign.center,
                            style: getStyleSubHeading(context).copyWith(
                                color: Colors.black,
                                fontWeight: AppFont.fontWeightSemiBold)),
                        verticalSpace(8),
                        Divider(
                          height: 1.0,
                          color: colorDividerGrey,
                        ),
                        verticalSpace(16),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Icon(
                              Icons.photo_library_sharp,
                              color: colorPrimary,
                            ),
                            RaisedButton(
                              color: colorWhite,
                              textColor: colorBlack,
                              padding: EdgeInsets.only(left: 16),
                              elevation: 0.0,
                              child: Text(
                               // AppConstants.GALLERY,
                                AppLocalizations.of(context).translate('pickFromGallery'),
                                style: getStyleButtonText(context)
                                    .copyWith(color: colorBlack),
                                overflow: TextOverflow.ellipsis,
                                softWrap: true,
                              ),
                              onPressed: () {
                                try {
                                  openGallery(profileNotifier, context);
                                } catch (e) {
                                  log.e('Error Camera : ' + e.toString());
                                }
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Icon(
                              Icons.camera_alt_sharp,
                              color: colorPrimary,
                            ),
                            RaisedButton(
                              color: colorWhite,
                              textColor: colorBlack,
                              padding: EdgeInsets.only(left: 16),
                              elevation: 0.0,
                              child: Text(
                                //AppConstants.CAMERA,
                                AppLocalizations.of(context).translate('captureCamera'),
                                style: getStyleButtonText(context)
                                    .copyWith(color: colorBlack),
                                overflow: TextOverflow.ellipsis,
                                softWrap: true,
                              ),
                              onPressed: () {
                                try {
                                  openCamera(profileNotifier, context);
                                } catch (e) {
                                  log.e('Error Camera : ' + e.toString());
                                }
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      ],
                    )),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget verticalSpace(double height) {
    return SizedBox(
      height: height,
    );
  }

  Future openGallery(ProfileNotifier profileNotifier,
      BuildContext context) async {
    await ImagePicker.pickImage(source: ImageSource.gallery,  imageQuality: 30, maxHeight: 500,maxWidth: 500).then((value) {
      if (value != null) {
        //  stateEditProfile.imageLocalFile = value;
        _validateImage(value,profileNotifier);

      }
    });
  }

  Future openCamera(ProfileNotifier profileNotifier,
      BuildContext context) async {
    await ImagePicker.pickImage(source: ImageSource.camera,imageQuality: 30, maxHeight: 500,maxWidth: 500).then((value) {
      if (value != null) {
        // stateEditProfile.imageLocalFile = value;
        _validateImage(value,profileNotifier);
      }
    });
  }
  Future _validateImage(File file, ProfileNotifier profileNotifier) async {
    if(file != null) {
      var decodedImage = await decodeImageFromList(file.readAsBytesSync());

      List<int> imageBytes = file.readAsBytesSync();
      print("imageBytes:==>"+imageBytes.toString());

      // for (int index = 0; index < imageBytes.length; index++){
      //   debugPrint("myByteArr :==>"+imageBytes[index].toString());
      // }

      LogPrint(imageBytes);


      String base64Image = base64Encode(imageBytes);
      Uint8List bytes = base64.decode(base64Image);
      LogPrint("base64Image:==>"+base64Image.toString());
      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>"+accessToken);

      SetCareGiverProfileImgResponse response =  await profileNotifier.callApiSetOrUploadProfile(accessToken, base64Image);
     // SetCareGiverProfileImgResponse response =  await profileNotifier.callApiSetOrUploadProfile(accessToken, imageBytes);
      if(response != null && response.webMethodOutputRows != null
          && response.webMethodOutputRows.webMethodOutputRows[0] != null
          && response.webMethodOutputRows.webMethodOutputRows[0].cgProfilePictureStatus == "Added"
      ){


        showDialog(
          context: context,
          builder: (_) => AlertOverlay(
              AppConstants.PROFILE_ALERT,
              AppConstants.PROFILE_PIC_UPLOADED_UNDER_VALIDATION,
              AppConstants.OKAY),
        );

        // showDialog(
        //     context: context,
        //     builder: (_) => AlertOverlayWithRefersh(
        //         AppConstants.OPEN_OFFERS_ALERT,
        //         //AppConstants.TOKEN_VALID_HOLIDAYCGSCHEDULE_VALID_START_END_DATE_UPDATED,
        //         "Image Uploaded successfully",
        //         AppConstants.OKAY,offerGuid));
      }else{
        if(response != null && response.outputRowErrors != null
            && response.outputRowErrors.outputRowErrors[0] != null
            &&response.outputRowErrors.outputRowErrors[0].textMessage != ""
        ){
          showDialog(
            context: context,
            builder: (_) => AlertOverlay(AppConstants.PROFILE_ALERT,response.outputRowErrors.outputRowErrors[0].textMessage,AppConstants.OKAY),
          );



        }

      }
      if((decodedImage.width >= 150 && decodedImage.width <= 256) && (decodedImage.height >= 110 && decodedImage.height <= 256)){
        //profileEditNotifier.imageLocalFile = file;
        profileNotifier.imageLocalFile = file;
        log.d(file.path.toString()+ " " + decodedImage.width.toString() + " x " + decodedImage.height.toString());
      } else{
        // profileEditNotifier.imageLocalFile = file;
        profileNotifier.imageLocalFile = file;
        log.d("erooorr"+file.path.toString()+ " " + decodedImage.width.toString() + " x " + decodedImage.height.toString());
        // profileEditNotifier.showCustomSnackBarMessageWithContext(AppConstants.IMAGE_PROFILE_PROTOCOL, bgColor: colorGMailRed, txtColor: colorWhite, ctx: _scfoldContext);
      }
    }
  }


  static void LogPrint(Object object) async {
    int defaultPrintLength = 1020;
    if (object == null || object.toString().length <= defaultPrintLength) {
      print(object);
    } else {
      String log = object.toString();
      int start = 0;
      int endIndex = defaultPrintLength;
      int logLength = log.length;
      int tmpLogLength = log.length;
      while (endIndex < logLength) {
        print(log.substring(start, endIndex));
        endIndex += defaultPrintLength;
        start += defaultPrintLength;
        tmpLogLength -= defaultPrintLength;
      }
      if (tmpLogLength > 0) {
        print(log.substring(start, logLength));
      }
    }
  }

}


class ExitAlert extends StatefulWidget {
  String title,subTitle,buttonOneTitle,buttonTwoTitle;
  ExitAlert(this.title,this.subTitle,this.buttonOneTitle,this.buttonTwoTitle,);
  @override
  State<StatefulWidget> createState() => ExitAlertState(this.title,this.subTitle,this.buttonOneTitle,this.buttonTwoTitle);
}

class ExitAlertState extends State<ExitAlert>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  String title,subTitle,buttonOneTitle,buttonTwoTitle;
  ExitAlertState(this.title,this.subTitle,this.buttonOneTitle,this.buttonTwoTitle);
  @override
  void initState() {
    super.initState();

    // controller =
    //     AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    // scaleAnimation =
    //     CurvedAnimation(parent: controller, curve: Curves.elasticInOut);
    //
    // controller.addListener(() {
    //   setState(() {});
    // });
    //
    // controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: 180.0,

              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10.0, left: 20.0, right: 20.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[

                                Text(
                                  widget.title,
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightExtraBold,
                                      fontSize: 17
                                  ),

                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN/4,
                                ),
                                Text(
                                  widget.subTitle!= null ?widget.subTitle:"",
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                    color: colorBlack,
                                    fontWeight: AppFont.fontWeightRegular,
                                    //fontSize: (widget.subTitle!= null && widget.subTitle.length > 10)?15: 17
                                    fontSize: (widget.subTitle!= null && widget.subTitle.length > 10)?15: 17,


                                  ),
                                ),
                              ])


                      )),

                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN*4,
                            height: 45,
                            child:   RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () async {

                                //Store local data for Auto login
                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                String firstLoginValue = prefs.getString(AppConstants.KEY_LANG_FIRST_LOGIN)??"";
                                // debugPrint("firstLoginValue :==>"+firstLoginValue);
                                prefs.setString(AppConstants.KEY_LANG_FIRST_LOGIN, "");

                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                               // Navigator.canPop(context) ? Navigator.pop(context) : '';
                                Navigator.pushNamedAndRemoveUntil(context, LoginScreen.routeName, (route) => false);
                                //Navigator.pushNamed(context, LoginScreen.routeName);
                              },
                              color: colorDarkRed,
                              textColor: colorWhite,
                              elevation: 5,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonOneTitle,
                                style: getStyleButtonText(context).copyWith(
                                    color: colorWhite,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                            height: 45,
                          ),
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN*4,
                            height: 45,
                            child:   RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                              },
                              color: colorDarkRed,
                              textColor: colorDarkRed,
                              elevation: 1,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: colorDarkRed, width: 2),
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonTwoTitle,
                                style: getStyleButtonText(context).copyWith(
                                    color: Colors.white,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              )),
      ),
    );
  }
}



