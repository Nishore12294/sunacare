import 'package:meta/meta.dart';

class CalendarData {

  //Assignment list
  // "CustomerName": "Robert Jakubetz",
  // "OfferNumber": "110954-001",
  // "ArrivalDate": "2020-10-20",
  // "PlannedEndDate": "2021-03-14",
  // "NetToSalary": "25000.0000"
  //Holiday list
  // "CGScheduleNumber": "CG-SKD-00001048",
  // "FromDate": "2021-02-01",
  // "ToDate": "2021-02-04"

  //Assignment list
  String CustomerName;
  String OfferNumber;
  String NetToSalary;
  //Holiday list
  String CGScheduleNumber;


  //common
   List<DateTime> dates;
   final bool isBlocked;
   final bool isContractDate;
   final bool isHolidayBlocked;



   CalendarData({
    @required this.dates,
    @required this.isBlocked,
    @required this.isContractDate,
    @required this.isHolidayBlocked,

    @required this.CustomerName,
    @required this.OfferNumber,
    @required this.NetToSalary,
    @required this.CGScheduleNumber,


  });
}
