import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/DeleteHolidayResponse.dart';
import 'package:suna_care/core/data/remote/request_response/availability/holiday/EditHolidayResponse.dart';
import 'package:suna_care/core/notifier/home_notifier.dart';
import 'package:suna_care/ui/home/home_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_localizations.dart';

import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/core/data/remote/request_response/login/response_login.dart';
import 'package:suna_care/core/notifier/login_notifier.dart';

import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:intl/intl.dart';

class ScheduleDetails extends StatefulWidget {
  static const routeName = '/schedule_details';
  String fromDate = "", toDate = "", cgScheduleGuid = "";

  ScheduleDetails(this.fromDate, this.toDate, this.cgScheduleGuid);

  @override
  _ScheduleDetailsState createState() => _ScheduleDetailsState();
}

class _ScheduleDetailsState extends State<ScheduleDetails> {
  final log = getLogger('ScheduleDetails');
  final _keyValidationForm =
      GlobalKey<FormState>(debugLabel: '_scheduleDetailsKey');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var LastDateOfMonthArr = const [
    31,
    28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31
  ];
  DateTime masterDate;
  DateTime currentDate = DateTime.now();

  //Picker
  DateTime selectedStartDate = DateTime.now();
  String scheduleFormatedStartDate = "",
      cgScheduleGuid = "",
      fromDate = "",
      toDate = "";
  DateTime selectedEndDate = DateTime.now();

  @override
  void initState() {
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: colorBlack, // navigation bar color
      statusBarColor: colorLightGray, // status bar color
    ));

    setInitialData(); //
  }

  setInitialData() async {
    Future.delayed(Duration.zero, () {
      setState(() {
        /* final  Map<String, Object> argData = ModalRoute.of(context).settings.arguments;
        print("cgScheduleGuid Initial state data ${argData['cgScheduleGuid']}");
        print("fromDate Initial state data ${argData['fromDate']}");
        print("toDate Initial state data ${argData['toDate']}");
        cgScheduleGuid = argData['cgScheduleGuid'];
        fromDate = argData['fromDate'];
        toDate = argData['toDate'];*/

        if (widget != null) {
          fromDate = widget.fromDate;
          toDate = widget.toDate;
          masterDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(fromDate);
          cgScheduleGuid = widget.cgScheduleGuid;

          selectedStartDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(fromDate);
          selectedEndDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(toDate);

        }

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<HomeNotifier>(
      builder: (context) => HomeNotifier(context, true),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: colorWhite,
//          image: DecorationImage(
//              image: AssetImage(
//                AppImages.IMAGE_LOGIN_BG,
//              ),
//              fit: BoxFit.fill),
        ),
        child: WillPopScope(
          onWillPop: () {
            backButtonPressed();
          },
          child: Scaffold(
            backgroundColor: Colors.transparent,
            key: _scaffoldKey,
            body: Consumer<HomeNotifier>(
              builder: (context, homeNotifier, _) => Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  ModalProgressHUD(
                      // color: colorDarkRed,
                      color: Colors.transparent,
                      inAsyncCall: homeNotifier.isLoading,
                      child: GestureDetector(
                          onTap: () {
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          },
                          child: Container(
                            //alignment: Alignment.center,
                            child: Column(
                              //mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN * 1.7,
                                ),
                                _buildWidgetImageLogo(homeNotifier),
                                buildWidgetScheduleCard(homeNotifier),
                              ],
                            ),
                          ))),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////
  Widget _buildWidgetImageLogo(HomeNotifier homeNotifier) {
    return Container(
      height: getScreenHeight(context) / 10,
      //padding: EdgeInsets.only(top: 64, bottom: 32),
      alignment: Alignment.topCenter,
      child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            topHeader(homeNotifier),
            SizedBox(
              height: AppConstants.SIDE_MARGIN * 1,
            ),
          ]),
    );
  }

  void backButtonPressed() {
    // homeNotifier.isScheduleDetailsPage = true;
    // if(homeNotifier.isScheduleDetailsPage){
    //   homeNotifier.getInitialData();
    // }
    // Provider.of<HomeNotifier>(context).getInitialData();
    //Provider.of<HomeNotifier>(context, listen: false).getInitialData();

    Navigator.pop(context, true);
  }

  Widget topHeader(HomeNotifier homeNotifier) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: InkWell(
            onTap: () {
              backButtonPressed();
            },
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
                    color: Colors.transparent,
                    alignment: Alignment.centerLeft,
                    child: Image.asset(
                      AppImages.IMAGE_BACK,
                      height: 25,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      deleteHolidayAPI(homeNotifier);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.8, 0.0),
                      color: Colors.transparent,
                      alignment: Alignment.centerLeft,
                      child: Image.asset(
                        AppImages.IMAGE_DELETE,
                        height: 25,
                      ),
                    ),
                  ),
                ]),
          ),
        ),
        Expanded(
          flex: 2,
          child: Text(
            //AppConstants.SCHEDULE_DETAILS,
            AppLocalizations.of(context).translate('scheduleDetails'),
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
              fontWeight: AppFont.fontWeightMedium,
              fontSize: 18,
              fontFamily:'NPSunaGrotesk',),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
            color: Colors.transparent,
            alignment: Alignment.centerRight,
            child: Image.asset(
              AppImages.IMAGE_APP_LOGO,
              height: 20,
            ),
          ),
        ),
      ],
    );
  }

  void deleteHolidayAPI(HomeNotifier homeNotifier) async {
    String accessToken = await AppSharedPreference().getUserToken();
    debugPrint("accessToken:==>" + accessToken);
    debugPrint("cgScheduleGuid:==>" + cgScheduleGuid);
    if (accessToken != null && cgScheduleGuid != null) {
      DeleteHolidayResponse deleteHolidayResponse =
          await homeNotifier.callApiDeleteHoliday(accessToken, cgScheduleGuid);
      if (deleteHolidayResponse != null) {
        homeNotifier.deleteHolidayResponse = deleteHolidayResponse;
        if (homeNotifier.deleteHolidayResponse != null &&
            homeNotifier.deleteHolidayResponse.webMethodOutputRows != null &&
            homeNotifier.deleteHolidayResponse.webMethodOutputRows
                    .webMethodOutputRows !=
                null) {
          if (homeNotifier.deleteHolidayResponse.webMethodOutputRows
                  .webMethodOutputRows[0].deleteHolidayCgScheduleStatus ==
              "Deleted") {
            showDialog(
                context: context,
                builder: (_) => AlertOverlayWithRefersh(
                     AppLocalizations.of(context).translate('unavailableDates'),
                     AppLocalizations.of(context).translate('unavailableDatesDeletedSuccessfully'),
                   // AppConstants.UNAVAILABLE_DATE_DELETED_SUCCESSFULLY,
                     AppLocalizations.of(context).translate('okay')));

            //Navigator.pop(context);
          } else {
            showDialog(
                context: context,
                builder: (_) => AlertOverlay(
                     AppLocalizations.of(context).translate('unavailableDates'),
                    homeNotifier
                            .deleteHolidayResponse
                            .webMethodOutputRows
                            .webMethodOutputRows[0]
                            .deleteHolidayCgScheduleStatus ??
                        "",
                     AppLocalizations.of(context).translate('okay')));
          }
        } else if (homeNotifier.deleteHolidayResponse != null &&
            homeNotifier.deleteHolidayResponse.outputRowErrors != null &&
            homeNotifier
                    .deleteHolidayResponse.outputRowErrors.outputRowErrors !=
                null &&
            homeNotifier
                    .deleteHolidayResponse.outputRowErrors.outputRowErrors[0] !=
                null &&
            homeNotifier.deleteHolidayResponse.outputRowErrors
                    .outputRowErrors[0].textMessage !=
                null &&
            homeNotifier.deleteHolidayResponse.outputRowErrors
                    .outputRowErrors[0].textMessage !=
                "") {
             if (homeNotifier.deleteHolidayResponse.outputRowErrors
                .outputRowErrors[0].textMessage ==
                "InvalidToken") {
              showDialog(
                  context: context,
                  builder: (_) => AlertOverlay(
                      AppLocalizations.of(context).translate('unavailableDates'),
                      AppLocalizations.of(context).translate('tokenIsInvalid'),
                     // AppConstants.TOKEN_INVALID,
                      AppLocalizations.of(context).translate('okay')));
            } else if (homeNotifier.deleteHolidayResponse.outputRowErrors
                .outputRowErrors[0].textMessage ==
                "InvalidHolidayCGScheduleGuid") {
              showDialog(
                  context: context,
                  builder: (_) => AlertOverlay(
                      AppLocalizations.of(context).translate('unavailableDates'),
                      AppLocalizations.of(context).translate('tokenHolidayCGScheduleGuidIsInvalid'),
                      //AppConstants.TOKEN_VALID_HOLIDAYCGSCHEDULEID_INVALID,
                      AppLocalizations.of(context).translate('okay')));
            } else if (homeNotifier.deleteHolidayResponse.outputRowErrors
                .outputRowErrors[0].textMessage ==
                "Unexpected") {
              showDialog(
                  context: context,
                  builder: (_) => AlertOverlay(
                      AppLocalizations.of(context).translate('unavailableDates'),
                      AppLocalizations.of(context).translate('unexpectedError'),
                      //AppConstants.UNEXPECTED_ERROR,
                      AppLocalizations.of(context).translate('okay')));
            }else{

              showDialog(
                  context: context,
                  builder: (_) => AlertOverlay(
                       AppLocalizations.of(context).translate('unavailableDates'),
                      homeNotifier.deleteHolidayResponse.outputRowErrors
                          .outputRowErrors[0].textMessage,
                       AppLocalizations.of(context).translate('okay')));
            }


        }
      }
    }
  }

  void editHolidayAPI(HomeNotifier homeNotifier) async {
    log.d('saved value from SP: ${await AppSharedPreference().getUserToken()}');
    String accessToken = await AppSharedPreference().getUserToken();
    debugPrint("accessToken:==>" + accessToken);
    debugPrint("cgScheduleGuid:==>" + cgScheduleGuid);
    var dateFormatter = new DateFormat('dd');
    var monthFormatter = new DateFormat('MM');
    var yearFormatter = new DateFormat('yyyy');
    String fromDateDay = dateFormatter
        .format(new DateFormat("yyyy-MM-dd hh:mm:ss").parse(fromDate));
    String fromDateMonth = monthFormatter
        .format(new DateFormat("yyyy-MM-dd hh:mm:ss").parse(fromDate));
    String fromDateYear = yearFormatter
        .format(new DateFormat("yyyy-MM-dd hh:mm:ss").parse(fromDate));

    String startDate = fromDateYear + "" + fromDateMonth + "" + fromDateDay;
    debugPrint("finalFromDate:==>" + startDate);

    String toDateDay = dateFormatter
        .format(new DateFormat("yyyy-MM-dd hh:mm:ss").parse(toDate));
    String toDateMonth = monthFormatter
        .format(new DateFormat("yyyy-MM-dd hh:mm:ss").parse(toDate));
    String toDateYear = yearFormatter
        .format(new DateFormat("yyyy-MM-dd hh:mm:ss").parse(toDate));
    String endDate = toDateYear + "" + toDateMonth + "" + toDateDay;

    debugPrint("finalToDate:==>" + endDate);

    //homeNotifier.callApiEditHoliday(accessToken, startDate, endDate);
    if (accessToken != null && cgScheduleGuid != null) {
      EditHolidayResponse editHolidayResponse = await homeNotifier
          .callApiEditHoliday(accessToken, cgScheduleGuid, startDate, endDate);
      if (editHolidayResponse != null) {
        homeNotifier.editHolidayResponse = editHolidayResponse;
        if (homeNotifier.editHolidayResponse != null &&
            homeNotifier.editHolidayResponse.webMethodOutputRows != null &&
            homeNotifier.editHolidayResponse.webMethodOutputRows
                    .webMethodOutputRows !=
                null) {
          if (homeNotifier.editHolidayResponse.webMethodOutputRows
                  .webMethodOutputRows[0].editHolidayCgScheduleStatus ==
              "Updated") {
            showDialog(
                context: context,
                builder: (_) => AlertOverlayWithRefersh(
                     AppLocalizations.of(context).translate('unavailableDates'),
                     AppLocalizations.of(context).translate('youHaveSuccessfullyUpdatedYourUnavailDates'),
                    //AppConstants.YOU_HAVE_SUCCESSFULLY_UPDATED_YOUR_UNAVAILABLE_DATES,
                     AppLocalizations.of(context).translate('okay')));

            //Navigator.pop(context);
          } else {
            showDialog(
                context: context,
                builder: (_) => AlertOverlay(
                     AppLocalizations.of(context).translate('unavailableDates'),
                    homeNotifier
                            .editHolidayResponse
                            .webMethodOutputRows
                            .webMethodOutputRows[0]
                            .editHolidayCgScheduleStatus ??
                        "",
                     AppLocalizations.of(context).translate('okay')));
          }
        } else if (homeNotifier.editHolidayResponse != null &&
            homeNotifier.editHolidayResponse.outputRowErrors != null &&
            homeNotifier.editHolidayResponse.outputRowErrors.outputRowErrors !=
                null &&
            homeNotifier
                    .editHolidayResponse.outputRowErrors.outputRowErrors[0] !=
                null &&
            homeNotifier.editHolidayResponse.outputRowErrors.outputRowErrors[0]
                    .textMessage !=
                null &&
            homeNotifier.editHolidayResponse.outputRowErrors.outputRowErrors[0]
                    .textMessage !=
                "") {
          if (homeNotifier.editHolidayResponse.outputRowErrors
                  .outputRowErrors[0].textMessage ==
              "InvalidHolidayCGScheduleGuid") {
            showDialog(
                context: context,
                builder: (_) => AlertOverlay(
                     AppLocalizations.of(context).translate('unavailableDates'),
                     AppLocalizations.of(context).translate('tokenIsValidHolidayCgScheduleGuidIsInvalid'),
                    //AppConstants.TOKEN_VALID_HOLIDAYCGSCHEDULE_INVALID,
                     AppLocalizations.of(context).translate('okay')));
          } else if (homeNotifier.editHolidayResponse.outputRowErrors
                  .outputRowErrors[0].textMessage ==
              "InvalidToken") {
            showDialog(
                context: context,
                builder: (_) => AlertOverlay(
                    AppLocalizations.of(context).translate('unavailableDates'),
                    AppLocalizations.of(context).translate('tokenIsInvalid'),
                    //AppConstants.TOKEN_INVALID,
                    AppLocalizations.of(context).translate('okay')));
          } else if (homeNotifier.editHolidayResponse.outputRowErrors
                  .outputRowErrors[0].textMessage ==
              "Overlapped") {
            showDialog(
                context: context,
                builder: (_) => AlertOverlay(
                     AppLocalizations.of(context).translate('unavailableDates'),
                     AppLocalizations.of(context).translate('tokenHolidayCGScheduleGuidIsValidButDateOverlap'),
                     //AppConstants.TOKEN_VALID_HOLIDAYCGSCHEDULE_VALID_DATE_OVERLAPPED,
                     AppLocalizations.of(context).translate('okay')));
          } else if (homeNotifier.editHolidayResponse.outputRowErrors
                  .outputRowErrors[0].textMessage ==
              "Unexpected") {
            showDialog(
                context: context,
                builder: (_) => AlertOverlay( AppLocalizations.of(context).translate('unavailableDates'),
                    AppLocalizations.of(context).translate('unexpectedError'),
                   // AppConstants.UNEXPECTED_ERROR,
                    AppLocalizations.of(context).translate('okay')));
          } else {
            showDialog(
                context: context,
                builder: (_) => AlertOverlay(
                     AppLocalizations.of(context).translate('unavailableDates'),
                    homeNotifier.editHolidayResponse.outputRowErrors
                        .outputRowErrors[0].textMessage,
                     AppLocalizations.of(context).translate('okay')));
          }
        }
      }
    }
  }

  Widget buildWidgetScheduleCard(HomeNotifier homeNotifier) {
    final DateFormat dateFormatter = DateFormat('dd MMM, yyyy');

    return Column(
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.only(
                top: 5.0, left: 30.0, right: 30.0, bottom: 8.0),
            child: Theme(
              data: ThemeData(
                  hintColor: colorBlack,
                  primaryColor: colorDarkRed,
                  primaryColorDark: colorLiteOrange),
              child: Form(
                key: _keyValidationForm,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN * 1.5,
                    ),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          //From
                          Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                 // AppConstants.FROM,
                                  AppLocalizations.of(context).translate('from'),
                                  textAlign: TextAlign.left,
                                  style: getStyleBody2(context).copyWith(
                                      fontWeight: AppFont.fontWeightSemiBold,
                                      color: (homeNotifier.currentPageNo == 0)
                                          ? colorBlack
                                          : colorLightGrey,
                                      fontSize: 14),
                                ),
                                RaisedButton(
                                  onPressed: () {
                                    _selectStartingDate(homeNotifier, context);
                                  },
                                  color: Colors.white,
                                  textColor: colorWhite,
                                  elevation: 0,
                                  padding: EdgeInsets.only(
                                      top: AppConstants.SIDE_MARGIN / 5,
                                      bottom: AppConstants.SIDE_MARGIN / 5),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              AppConstants.SIDE_MARGIN))),
                                  child: Text(
                                    //(homeNotifier.scheduleFormatedFromDate != null)?dateFormatter.format(homeNotifier.scheduleFormatedFromDate):"DD/MM/YYYY",
                                    (fromDate != null)
                                        ? dateFormatter
                                            .format(new DateFormat(
                                                    "yyyy-MM-dd hh:mm:ss")
                                                .parse(fromDate))
                                            .toString()
                                        : "DD/MM/YYYY",
                                    //homeNotifier.scheduleFormatedFromDate.toString(),
                                    style: getStyleDisplay1(context).copyWith(
                                        color: colorBlack, fontSize: 14),
                                  ),
                                ),
                              ]),
                          SizedBox(
                            height: AppConstants.SIDE_MARGIN / 4,
                          ),
                          Divider(
                            thickness: 0.25,
                            color: (homeNotifier.currentPageNo == 0)
                                ? Colors.black
                                : Colors.white,
                          ),
                          SizedBox(
                            height: AppConstants.SIDE_MARGIN / 4,
                          ),
                          //To
                          Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  //AppConstants.TO,
                                  AppLocalizations.of(context).translate('to'),
                                  textAlign: TextAlign.left,
                                  style: getStyleBody2(context).copyWith(
                                      fontWeight: AppFont.fontWeightSemiBold,
                                      color: (homeNotifier.currentPageNo == 0)
                                          ? colorBlack
                                          : colorLightGrey,
                                      fontSize: 14),
                                ),
                                RaisedButton(
                                  onPressed: () {
                                    _selectEndDate(homeNotifier, context);
                                  },
                                  color: Colors.white,
                                  textColor: colorWhite,
                                  elevation: 0,
                                  padding: EdgeInsets.only(
                                      top: AppConstants.SIDE_MARGIN / 5,
                                      bottom: AppConstants.SIDE_MARGIN / 5),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              AppConstants.SIDE_MARGIN))),
                                  child: Text(
                                    // (homeNotifier.scheduleFormatedFromDate != null)?dateFormatter.format(homeNotifier.scheduleFormatedFromDate):"DD/MM/YYYY",
                                    (toDate != null)
                                        ? dateFormatter.format(new DateFormat(
                                                "yyyy-MM-dd hh:mm:ss")
                                            .parse(toDate))
                                        : "DD/MM/YYYY",
                                    //homeNotifier.scheduleFormatedToDate.toString(),
                                    style: getStyleDisplay1(context).copyWith(
                                        color: colorBlack, fontSize: 14),
                                  ),
                                ),
                              ]),
                        ]),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN * 2,
                    ),
                    ButtonTheme(
                      minWidth: double.infinity,
                      child: RaisedButton(
                        highlightElevation: 8.0,
                        onPressed: () async {
                          // Navigator.pushNamed(context, HomeScreen.routeName);
                          // 0 is equal / positive value greater / negative value being less
                          if(selectedStartDate.compareTo(selectedEndDate) <= 0){
                            editHolidayAPI(homeNotifier);
                          }else{
                            showDialog(
                                context: context,
                                builder: (_) => AlertOverlay(
                                     AppLocalizations.of(context).translate('unavailableDates'),
                                     AppLocalizations.of(context).translate('toDateShouldGreaterFromDate'),
                                    //AppConstants.TO_DATE_SHOULD_BE_GRATER_THEN_FROM_DATE,
                                     AppLocalizations.of(context).translate('okay')));
                          }

                        },
                        color: colorLiteOrange,
                        textColor: colorWhite,
                        elevation: 1,
                        padding: EdgeInsets.only(
                            top: AppConstants.SIDE_MARGIN / 1.5,
                            bottom: AppConstants.SIDE_MARGIN / 1.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(AppConstants.SIDE_MARGIN))),
                        child: Text(
                          //AppConstants.EDIT_UNAVAILABLE_DATE,
                          AppLocalizations.of(context).translate('editUnavailableDates'),
                          style: getStyleButtonText(context).copyWith(
                              color: colorWhite,
                              letterSpacing: 1,
                              fontWeight: AppFont.fontWeightBold),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  OutlineInputBorder _buildOutlineInputBorder() {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: colorLiteWhiteBorder,
        //color: Colors.transparent,
      ),
      borderRadius: const BorderRadius.all(
        const Radius.circular(AppConstants.SIDE_MARGIN),
      ),
    );
  }

  InputDecoration _buildTextDecoration(String hint) {
    return InputDecoration(
        isDense: true,
        hintText: hint,
        alignLabelWithHint: true,
        filled: true,
        contentPadding: EdgeInsets.all(16));
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Future<void> _selectStartingDate(
      HomeNotifier homeNotifier, BuildContext context) async {
    var currentDate = new DateTime.now();
    var monthFormatter = new DateFormat('MM');
    var yearFormatter = new DateFormat('yyyy');
    var dateFormatter = new DateFormat('dd');

    //For current Date
    String currentDateMonth = monthFormatter.format(currentDate);

    //For master date
    String masterDay = dateFormatter.format(masterDate);
    String masterDateMonth = monthFormatter.format(masterDate);
    String masterDateYear = yearFormatter.format(masterDate);
    print("Current date now :==>" + currentDate.toString());
    print("currentDateMonth formatted month :==>" + currentDateMonth);
    print("master date formatted month:==>" + masterDateMonth);
    print("master date formatted year:==>" + masterDateYear);
    print("master date formatted day:==>" + masterDay);

    bool isStartingDayInBlockedRange = false;
    bool isStartingDateInRange = false;

    final DateTime picked = await showDatePicker(
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme: ColorScheme.light().copyWith(
                primary: colorLiteOrange,
              ),
            ),
            child: child,
          );
        },
        context: context,
        initialDate: (currentDateMonth == masterDateMonth)
            ? DateTime.now()
            : DateTime(
                int.parse(masterDateYear), int.parse(masterDateMonth), 1),
        helpText: "Select Starting date",
        //firstDate: DateTime.now(),
        //lastDate: DateTime(2101));
        firstDate: (currentDateMonth == masterDateMonth)
            ? DateTime.now()
            : DateTime(
                int.parse(masterDateYear), int.parse(masterDateMonth), 1),
        lastDate: DateTime(
            int.parse(masterDateYear),
            int.parse(masterDateMonth),
            LastDateOfMonthArr[int.parse(masterDateMonth) - 1]));
    if (picked != null) {
      debugPrint("Picked Starting date :==>" + picked.toString());
      debugPrint("Picked Starting date compared value :==>" + selectedStartDate.compareTo(selectedEndDate).toString());

      //if(picked.compareTo(selectedEndDate) <= 0){
        setState(() {
          selectedStartDate = picked;
          final DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm:ss');
          final String formatted = formatter.format(selectedStartDate);
          // homeNotifier.scheduleFormatedFromDate = formatted;
          setState(() {
            fromDate = formatter.format(selectedStartDate);
          });
          print(formatted);
          print("FromDate :==>" + fromDate);
        });
        _selectEndDate(homeNotifier, context);
      // }else{
      //   showDialog(
      //       context: context,
      //       builder: (_) => AlertOverlay(
      //            AppLocalizations.of(context).translate('unavailableDates'),
      //           "From date should be lesser than To date",
      //            AppLocalizations.of(context).translate('okay')));
      // }


    }
  }

  Future<void> _selectEndDate(
      HomeNotifier homeNotifier, BuildContext context) async {
    var currentDate = new DateTime.now();
    var monthFormatter = new DateFormat('MM');
    var yearFormatter = new DateFormat('yyyy');
    var dateFormatter = new DateFormat('dd');

    //For current Date
    String currentDateMonth = monthFormatter.format(currentDate);

    //For master date
    String masterDay = dateFormatter.format(masterDate);
    String masterDateMonth = monthFormatter.format(masterDate);
    String masterDateYear = yearFormatter.format(masterDate);
    print("Current date now :==>" + currentDate.toString());
    print("currentDateMonth formatted month :==>" + currentDateMonth);
    print("master date formatted month:==>" + masterDateMonth);
    print("master date formatted year:==>" + masterDateYear);
    print("master date formatted day:==>" + masterDay);

    bool isEndingDateInRange = false;
    bool isEndingDayInBlockedRange = false;
    final DateTime picked = await showDatePicker(
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme: ColorScheme.light().copyWith(
                primary: colorLiteOrange,
              ),
            ),
            child: child,
          );
        },
        context: context,
        initialDate: (currentDateMonth == masterDateMonth)
            ? DateTime.now()
            : DateTime(
                int.parse(masterDateYear), int.parse(masterDateMonth), 1),
        helpText: "Select Ending date",
        // firstDate: DateTime(2015, 8),
        // lastDate: DateTime(2101));
        firstDate: (currentDateMonth == masterDateMonth)
            ? DateTime.now()
            : DateTime(
                int.parse(masterDateYear), int.parse(masterDateMonth), 1),
        lastDate: DateTime(
            int.parse(masterDateYear),
            int.parse(masterDateMonth),
            LastDateOfMonthArr[int.parse(masterDateMonth) - 1]));
    //if (picked != null && picked != selectedEndDate) {
    if (picked != null) {
      debugPrint("Picked Ending date :==>" + picked.toString());

      var currentDate = new DateTime.now();
      debugPrint("CompareTo :==>"+selectedStartDate.compareTo(picked).toString());
      // 0 is equal / positive value greater / negative value being less
      if(selectedStartDate.compareTo(picked) <= 0){
        setState(() {
          selectedEndDate = picked;
          //final DateFormat formatter = DateFormat('dd MMM, yyyy');
          //final String formatted = formatter.format(selectedEndDate);
          // homeNotifier.scheduleFormatedToDate = formatted;
          // print(formatted);

          final DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm:ss');
          final String formatted = formatter.format(selectedEndDate);
          // homeNotifier.scheduleFormatedToDate = formatted;
          setState(() {
            toDate = formatter.format(selectedEndDate);
          });
          print(formatted);
          print("toDate from Picker :==>" + toDate);
        });
        // homeNotifier.toDate = selectedEndDate;
      }else{
        showDialog(
            context: context,
            builder: (_) => AlertOverlay(
                 AppLocalizations.of(context).translate('unavailableDates'),
                 AppLocalizations.of(context).translate('toDateShouldGreaterFromDate'),
                //AppConstants.TO_DATE_SHOULD_BE_GRATER_THEN_FROM_DATE,
                 AppLocalizations.of(context).translate('okay')));
      }

    } else {
      setState(() {
        selectedEndDate = selectedStartDate;
        //final DateFormat formatter = DateFormat('dd MMM, yyyy');
        //final String formatted = formatter.format(selectedEndDate);
        // homeNotifier.scheduleFormatedToDate = formatted;
        // print(formatted);

        final DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm:ss');
        final String formatted = formatter.format(selectedEndDate);
        // homeNotifier.scheduleFormatedToDate = formatted;
        setState(() {
          toDate = formatter.format(selectedEndDate);
        });
        print(formatted);
        print("toDate from Picker :==>" + toDate);
      });
    }
  }
}

class AlertOverlayWithRefersh extends StatefulWidget {
  String title, subTitle, buttonTitle;

  AlertOverlayWithRefersh(this.title, this.subTitle, this.buttonTitle);

  @override
  State<StatefulWidget> createState() =>
      AlertOverlayState(this.title, this.subTitle, this.buttonTitle);
}

class AlertOverlayState extends State<AlertOverlayWithRefersh>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  String title, subTitle, buttonTitle;

  AlertOverlayState(this.title, this.subTitle, this.buttonTitle);

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: 200.0,
              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10.0, left: 20.0, right: 20.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  widget.title,
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightExtraBold,
                                      fontSize: 17),
                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN / 4,
                                ),
                                Text(
                                  widget.subTitle != null
                                      ? widget.subTitle
                                      : "",
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      fontWeight: AppFont.fontWeightRegular,
                                      fontSize: (widget.subTitle != null &&
                                              widget.subTitle.length > 10)
                                          ? 14
                                          : 16),
                                ),
                              ]))),
                  Expanded(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: AppConstants.SIDE_MARGIN * 4,
                        height: 45,
                        child: RaisedButton(
                          highlightElevation: 8.0,
                          onPressed: () {
                            Navigator.canPop(context)
                                ? Navigator.pop(context)
                                : '';
                            Navigator.canPop(context)
                                ? Navigator.pop(context, true)
                                : '';
                          },
                          color: colorDarkRed,
                          textColor: colorWhite,
                          elevation: 1,
                          padding: EdgeInsets.only(
                              top: AppConstants.SIDE_MARGIN / 1.5,
                              bottom: AppConstants.SIDE_MARGIN / 1.5),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(AppConstants.SIDE_MARGIN))),
                          child: Text(
                            widget.buttonTitle,
                            style: getStyleButtonText(context).copyWith(
                                color: colorWhite,
                                letterSpacing: 1,
                                fontWeight: AppFont.fontWeightBold),
                          ),
                        ),
                      ),
                    ],
                  ))
                ],
              )),
        ),
      ),
    );
  }
}
