import 'package:meta/meta.dart';

class Day {
  final DateTime date;
  final bool ignore;
  final bool inInterval;
  final bool isStart;
  final bool isEnd;
  final bool isBlocked;
  final bool isContractDate;
  final bool isHolidayBlocked;
  final bool isNewlyAddedOne;

  //Assignment list
  String customerName;
  String offerNumber;
  String netToSalary;
  //Holiday list
  String cgScheduleNumber;
  String cgScheduleGuid;

  Day({
    @required this.date,
    @required this.ignore,
    @required this.inInterval,
    @required this.isStart,
    @required this.isEnd,
    @required this.isBlocked,
    @required this.isContractDate,
    @required this.isHolidayBlocked,
    @required this.isNewlyAddedOne,

    @required this.customerName,
    @required this.offerNumber,
    @required this.netToSalary,
    @required this.cgScheduleNumber,
    @required this.cgScheduleGuid,
  });
}
