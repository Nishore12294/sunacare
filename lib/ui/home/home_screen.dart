import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:minimize_app/minimize_app.dart';
import 'package:suna_care/core/data/remote/pushnotification/NotifiMsgResp.dart';
import 'package:suna_care/core/notifier/home_notifier.dart';
import 'package:suna_care/ui/home/slides/tab1_home.dart';
import 'package:suna_care/ui/home/slides/tab2_offers_list_screen.dart';
import 'package:suna_care/ui/home/slides/tab3_profile_screen.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_globals.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:provider/provider.dart';


class HomeScreen extends StatefulWidget {
  static const routeName = '/home_screen';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  final double guideLineLeft = 16;
  double itemCardRadius = 6.0;
  double safeAreaHigh = 0.0;
  double headerLabelHigh = AppConstants.APP_BAR_HIGH;
  double space = 16.0;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  var log = getLogger('home_screen');
  HomeNotifier mHomeNotifier;
  @override
  void initState() {
    super.initState();
    //firebaseCloudMessaging_Listeners(context);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<HomeNotifier>(
      builder: (context) => HomeNotifier(context,false),
      child: Consumer<HomeNotifier>(
        builder: (context, homeNotifier, _) => ModalProgressHUD(
          inAsyncCall: homeNotifier.isLoading,
          color: colorTransparent,
          child: new WillPopScope(
              onWillPop:(){
                MinimizeApp.minimizeApp();
              } ,
            child:Scaffold(
                key: homeNotifier.scaffoldKey,
                backgroundColor: colorAppBg,
                // body: Center(child: Container(child: Text("Login as " + userModel.fillName, style: getStyleButtonText(context).copyWith(color: colorPencilBlack, fontWeight: AppFont.fontWeightSemiBold),),),),//buildBodyContent(context, homeNotifier),
                body: buildBodyContent(context, homeNotifier),
                bottomNavigationBar: _buildBottomNavigationBar(homeNotifier)),
          ),

        ),
      ),
    );
  }

  void firebaseCloudMessaging_Listeners(BuildContext context) {
    if (Platform.isIOS) iOSPermission();

    try {
      _firebaseMessaging.getToken().then((token) {
        print("token  in homescreen:==> ${token}");
      });

      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('on message  in homescreen :==> $message');
          showToastMessage('on message  in homescreen :==> $message');
          // toastMessage("On message :==>" + message.toString());
          //print('on message for $message['notification']['body']');
          if (Platform.isIOS) {
            //messageHdlrForIOS(message);
          } else {

            //if(!globalAndroidIsOnMsgExecuted){
            //  messageHdlrForAndroid(message,false);
            //  globalAndroidIsOnMsgExecuted = true;
            //}

          }
        },
        onResume: (Map<String, dynamic> message) async {
          print('on resume  in homescreen:==>$message');
          showToastMessage('on resume  in homescreen:==> $message');
          //toastMessage("On Resume :==>" + message.toString());
          if (Platform.isIOS) {
            //_navigateToItemDetailIOS(context, message, false);
            //messageHdlrForIOS(message);
          } else {
            //if(!globalAndroidIsOnResumeExecuted){
              //showAndroidNotifAlertFromBackground(context, message).show();
              messageHdlrForAndroid(message,true);

              globalAndroidIsOnResumeExecuted = true;
            //}
          }
        },
        onLaunch: (Map<String, dynamic> message) async {
          print('on launch  in homescreen:==> $message');
          showToastMessage('on launch :==> $message');
          //toastMessage("On Launch :==>" + message.toString());




          if (!globalIsLaunch) {
            if (Platform.isIOS) {
              //_navigateToItemDetailIOS(context, message, true);
              //messageHdlrForIOS(message);
              globalIsLaunch = true;
            } else {
              //showAndroidNotifAlertFromBackground(context, message).show();
              messageHdlrForAndroid(message,true);
              globalIsLaunch = true;
            }
          }



          //After logout the application It will redirect
          if(globalAfterLogoutPageNo > -1)
            messageHdlrForAndroid(message,true);

        },
      );
    } on Exception catch (e) {
      // TODO
    }
  }

  Future<dynamic> messageHdlrForAndroid(Map<String, dynamic> message,bool isBgMsg) async {

    final dynamic msgBodyData1  = message['data'];
   // print("msgBodyData1 :==>"+Uri.decodeFull(msgBodyData1.toString()));
    print("msgBodyData1 content :==>"+msgBodyData1['content'].toString());



    final dynamic msgBodyContent  = message['data']['content'];
    print("msgBodyContent in homescreen:==>"+msgBodyContent.toString());
    notifiMsgRespFromJson(msgBodyContent.toString());

    NotifiMsgResp notifiMsgResp = notifiMsgRespFromJson(msgBodyContent.toString());
    print("notifiMsgResp.title  in homescreen :==>"+notifiMsgResp.title);
    print("notifiMsgResp.id  in homescreen :==>"+notifiMsgResp.id.toString());
    globalAfterLogoutPageNo = notifiMsgResp.id;

    if(mHomeNotifier != null){
      if(notifiMsgResp.id != null && notifiMsgResp.id == 1 || notifiMsgResp.id == 3){
        mHomeNotifier.currentPageNo = 0;
      }else if( notifiMsgResp.id != null && notifiMsgResp.id == 2 ){
        mHomeNotifier.currentPageNo = 2;
      }else if( notifiMsgResp.id != null && notifiMsgResp.id == 4 ){
        mHomeNotifier.currentPageNo = 1;
      }else{
        mHomeNotifier.currentPageNo = 0;
      }

      mHomeNotifier.dashboardPageViewController.animateToPage(
          mHomeNotifier.currentPageNo,
          duration: Duration(milliseconds: 500),
          curve: Curves.ease);
      globalAfterLogoutPageNo = -1;
    }







  }

void iOSPermission() {
  _firebaseMessaging.requestNotificationPermissions(
      IosNotificationSettings(sound: true, badge: true, alert: true));
  _firebaseMessaging.onIosSettingsRegistered
      .listen((IosNotificationSettings settings) {
    //print("Settings registered : $settings");
  });
}

  Widget buildBodyContent(BuildContext context, HomeNotifier homeNotifier) {
    mHomeNotifier = homeNotifier;
    firebaseCloudMessaging_Listeners(context);

    return Center(
      child: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(
                      //top: safeAreaHigh + headerLabelHigh + space
                      top: 0),
                  child: Column(
                    children: <Widget>[
                      //_buildContentArea(context, homeNotifier),
                      Expanded(
                          child: PageView(
                        controller: homeNotifier.dashboardPageViewController,
                        physics: ClampingScrollPhysics(),
                        children: <Widget>[
                          _buildHomeScreen(context),
                          _buildOffersScreen(context),
                          _buildProfileScreen(context)
                        ],
                        onPageChanged: (currentPage) {
                          log.d("Current Page Number : " +
                              currentPage.toString());
                          homeNotifier.currentPageNo = currentPage;
                          if (currentPage == 0) {
                            homeNotifier.homeTabTopTabSize =
                                AppConstants.AVAILABILITY_TAB;
                            homeNotifier.formTitle = AppConstants.HOME;
                          } else if (currentPage == 1) {
                            homeNotifier.homeTabTopTabSize =
                                AppConstants.OPEN_OFFER_TAB;
                            homeNotifier.formTitle = AppConstants.OPEN_OFFER;
                          } else if (currentPage == 2) {
                            homeNotifier.homeTabTopTabSize =
                                AppConstants.PROFILE_TAB;
                            homeNotifier.formTitle = AppConstants.PROFILE;
                          } else {}
                        },
                      ))
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildHomeScreen(BuildContext context) {
   return Tab1HomeScreen(context: context);
  }

  Widget _buildOffersScreen(BuildContext context) {
    return Tab2OffersListScreen();


  }
  Widget _buildProfileScreen(BuildContext context) {

    return Tab3ProfileScreen();
    // return Container(
    //   child: Center(
    //     child: Text(
    //       "Coming soon",
    //       style: getStyleBody1(context).copyWith(
    //           color: colorPencilBlack,
    //           fontWeight: AppFont.fontWeightSemiBold,
    //           fontSize: 16),
    //     ),
    //   ),
    // );

  }

  void showToastMessage(String message) {
    // Fluttertoast.showToast(
    //     msg: message,
    //     toastLength: Toast.LENGTH_SHORT,
    //     gravity: ToastGravity.BOTTOM // also possible "TOP" and "CENTER"
    // );
  }

  tabClicked(int currentPageNo, HomeNotifier homeNotifier) {
    homeNotifier.currentPageNo = currentPageNo;
    homeNotifier.dashboardPageViewController.animateToPage(
        homeNotifier.currentPageNo,
        duration: Duration(milliseconds: 500),
        curve: Curves.ease);
  }

  Widget _buildBottomNavigationBar(HomeNotifier homeNotifier) {
    return Container(

      padding: EdgeInsets.only(
          left: AppConstants.SIDE_MARGIN / 3,
          right: AppConstants.SIDE_MARGIN / 3,
         // bottom: AppConstants.SIDE_MARGIN/3
      ),
      height: AppBar().preferredSize.height + AppConstants.SIDE_MARGIN/1 ,
      alignment: Alignment.center,
      child:
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Expanded(
            child: InkWell(
          //highlightColor:  Colors.orange,
          child: Container(
            height: 70,
            //color: Colors.blue,
            decoration: BoxDecoration(
                color: (homeNotifier.currentPageNo == 0)
                    ? colorTransparent
                    : colorTransparent,
                border: Border.all(
                  color: (homeNotifier.currentPageNo == 0)
                      ? colorTransparent
                      : colorTransparent,
                ),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  (homeNotifier.currentPageNo == 0)
                      ? AppImages.IMAGE_HOME_SCREEN_HOME_GREEN
                      : AppImages.IMAGE_HOME_SCREEN_AVAIL_GRAY,
                  width: 18.0,
                  height: 18.0,
                  fit: BoxFit.fill,
                ),
                SizedBox(
                  width: AppConstants.TAB_TEXT_SPACE,
                ),
                Text(
                    //AppConstants.AVAILABILITY,
                    AppLocalizations.of(context).translate('availability'),
                    style: getStyleBody2(context).copyWith(
                        fontWeight: AppFont.fontWeightSemiBold,
                        color: (homeNotifier.currentPageNo == 0)
                            ? colorBlack
                            : colorLightGrey)),
                SizedBox(
                  width: AppConstants.TAB_TEXT_SPACE,
                ),
                Divider(
                  thickness: 2.0,
                  color:  (homeNotifier.currentPageNo == 0) ? Colors.black : colorLitePinkBG,
                )
              ],
            ),
          ),
          onTap: () {
            tabClicked(0, homeNotifier);
          },
        )),
        Expanded(
          child: InkWell(
            child: Container(
              height: 70,
              decoration: BoxDecoration(
                  color: (homeNotifier.currentPageNo == 1)
                      ? colorTransparent
                      : colorTransparent,
                  border: Border.all(
                    color: (homeNotifier.currentPageNo == 1)
                        ? colorTransparent
                        : colorTransparent,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    (homeNotifier.currentPageNo == 1)
                        ? AppImages.IMAGE_HOME_SCREEN_OFFER_GREEN
                        : AppImages.IMAGE_HOME_SCREEN_OFFER_GRAY,
                    width: 18.0,
                    height: 18.0,
                    fit: BoxFit.fill,
                  ),
                  SizedBox(
                    width: AppConstants.TAB_TEXT_SPACE,
                  ),
                  Text(
                      //AppConstants.OPEN_OFFER,
                      AppLocalizations.of(context).translate('openOffer'),
                      style: getStyleBody2(context).copyWith(
                          fontWeight: AppFont.fontWeightSemiBold,
                          color: (homeNotifier.currentPageNo == 1)
                          ? colorBlack
                          : colorLightGrey)),
                  Divider(
                    thickness: 2.0,
                    color:  (homeNotifier.currentPageNo == 1) ? Colors.black : colorLitePinkBG,
                  )
                ],
              ),
            ),
            onTap: () {
              tabClicked(1, homeNotifier);
            },
          ),
        ),
        Expanded(
          child: InkWell(
            child: Container(
              height: 70,
              decoration: BoxDecoration(
                  color: (homeNotifier.currentPageNo == 2)
                      ? colorTransparent
                      : colorTransparent,
                  border: Border.all(
                    color: (homeNotifier.currentPageNo == 2)
                        ? colorTransparent
                        : colorTransparent,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    (homeNotifier.currentPageNo == 2)
                        ? AppImages.IMAGE_HOME_SCREEN_PROFILE_GREEN
                        : AppImages.IMAGE_HOME_SCREEN_PROFILE_GRAY,
                    width: 18.0,
                    height: 18.0,
                    fit: BoxFit.fill,
                  ),
                  SizedBox(
                    width: AppConstants.TAB_TEXT_SPACE,
                  ),
                  Text(
                      //AppConstants.PROFILE,
                      AppLocalizations.of(context).translate('profile'),
                      style: getStyleBody2(context).copyWith(
                          fontWeight: AppFont.fontWeightSemiBold,
                          color: (homeNotifier.currentPageNo == 2)
                              ? colorBlack
                              : colorLightGrey)),
                  Divider(
                    thickness: 2.0,
                    color:  (homeNotifier.currentPageNo == 2) ? Colors.black : colorLitePinkBG,
                  )
                ],
              ),
            ),
            onTap: () {
              tabClicked(2, homeNotifier);
            },
          ),
        ),
      ]),
      decoration: BoxDecoration(
          color: colorLitePinkBG,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(AppConstants.SIDE_MARGIN / 1.5),
              topRight: Radius.circular(AppConstants.SIDE_MARGIN / 1.5)),
          boxShadow: [BoxShadow(color: colorDividerGrey, blurRadius: 1.2)]),
    );
  }
}
