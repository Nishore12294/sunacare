import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart';
import 'package:suna_care/core/notifier/change_password_notifier.dart';
import 'package:suna_care/core/notifier/forgot_password_notifier.dart';
import 'package:suna_care/core/notifier/verification_code_notifier.dart';
import 'package:suna_care/ui/forgotpassword/create_new_password_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_custom_icon.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/core/data/remote/request_response/forgotpassword/response_forgot_password.dart';
import '../login_screen.dart';



class VerificationCodeScreen extends StatefulWidget {
  static const routeName = '/verification_code_screen';

  @override
  _VerificationScreenState createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationCodeScreen> {
  final log = getLogger('VerificationCodeScreen');
  final _keyValidationForm = GlobalKey<FormState>(debugLabel: '_VerificationScreenKey');
  bool isOldPasswordVisible = false;
  bool isNewPasswordVisible = false;
  bool isConfirmPasswordVisible = false;
  String otpValue = "",enteredOTPValue = "";

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String emailId = "";
  @override
  void initState() {
    super.initState();
    setInitialData();
  }

  setInitialData() async {
    Future.delayed(Duration.zero, () {
      setState(() {
        final  Map<String, Object> argData = ModalRoute.of(context).settings.arguments;
        print("emailId Initial state data ${argData['emailId']}");
        print("emailId Initial state data ${argData['otpValue']}");
        emailId = argData['emailId'];
        otpValue = argData['otpValue'];
      });
    });

  }


  @override
  Widget build(BuildContext context) {


    return ChangeNotifierProvider<VerficationCodeNotifier>(
      builder: (context) => VerficationCodeNotifier(),
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
          backgroundColor: colorLitePinkBG,
          key: _scaffoldKey,

          body: Consumer<VerficationCodeNotifier>(
            builder: (context, verificationCodeNotifier, _) => Stack(
              fit: StackFit.expand,
              children: <Widget>[

                ModalProgressHUD(
                  color: Colors.transparent,
                  inAsyncCall: verificationCodeNotifier.isLoading,
                  child: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    },
                    child: Container(
                        alignment: Alignment.topCenter,
                        child:  Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              _buildWidgetImageLogo(),

                             Image.asset(
                                  AppImages.IMAGE_VERFI_CODE,
                                  height: 85,
                                  width: 120,
                                ),

                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/2,
                              ),
                              Text(
                                //AppConstants.CONFIRMATION,
                                AppLocalizations.of(context).translate('confirmation'),
                                textAlign: TextAlign.center,
                                style: getStyleCaption(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightMedium,
                                    fontSize: 28,
                                     fontFamily: 'lato_bold',
                                ),

                              ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/2,
                              ),
                              Text(
                               // AppConstants.VERFICATION_CODE_TIPS,
                                AppLocalizations.of(context).translate('pleaseTypeTheVerificationCodeSentToEmailAddress'),
                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightRegular,
                                    fontSize: 16,
                                    height: 1.3,

                                ),

                              ),
                              Text(
                                emailId??"",
                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightRegular,
                                    fontSize: 15
                                ),

                              ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/2,
                              ),
                              Form(
                               // key: formKey,
                                child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0, horizontal: 30),
                                    child: PinCodeTextField(
                                      appContext: context,
                                      pastedTextStyle: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      length: 6,
                                      //obscureText: true,
                                     // obscuringCharacter: '*',
                                      blinkWhenObscuring: true,
                                      animationType: AnimationType.fade,
                                      validator: (v) {

                                      },
                                      pinTheme: PinTheme(

                                        shape: PinCodeFieldShape.circle,
                                        borderRadius: BorderRadius.circular(5),
                                        borderWidth: 0.0,
                                        fieldHeight: 50,
                                        fieldWidth: 40,
                                        activeFillColor: Colors.white,
                                        selectedColor: Colors.white,
                                        selectedFillColor: Colors.white,
                                        inactiveColor: Colors.white,
                                        inactiveFillColor: Colors.white,
                                      ),
                                      cursorColor: Colors.black,
                                      animationDuration: Duration(milliseconds: 300),
                                      backgroundColor: colorLitePinkBG,
                                      enableActiveFill: true,

                                      //errorAnimationController: errorController,
                                      controller: verificationCodeNotifier.textEditingController,
                                      keyboardType: TextInputType.number,
                                      boxShadows: [
                                        BoxShadow(
                                          offset: Offset(0, 1),
                                          color: Colors.black12,
                                          blurRadius: 10,
                                        )
                                      ],
                                      onCompleted: (v) {
                                        print("Completed");
                                      },
                                      onChanged: (value) {
                                        print("onchanged  value:==>"+value);
                                        setState(() {
                                          //otpValue = value;
                                          enteredOTPValue = value;
                                        });

                                        print("onchanged otp value:==>"+enteredOTPValue);
                                      },
                                      beforeTextPaste: (text) {
                                        print("Allowing to paste $text");
                                        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                                        //but you can show anything you want here, like your pop up saying wrong paste format or etc
                                        return true;
                                      },
                                    )),
                              ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/4,
                              ),

                              GestureDetector(
                                onTap: () async => {
                                //Api
                                  resendCode(verificationCodeNotifier)
                                },
                                child:   Text(
                                  //" "+AppConstants.RESEND_CODE,
                                 AppLocalizations.of(context).translate('resendCode'),
                                  style: getStyleBody1(context).copyWith(
                                      color: colorLiteOrange,
                                      fontWeight: AppFont.fontWeightSemiBold),
                                  textAlign: TextAlign.end,
                                ),
                              ),
                              new Expanded(
                                flex: 2,
                                child:SingleChildScrollView(
                                  physics: BouncingScrollPhysics(),
                                  child:buildWidgetVerificationCard(verificationCodeNotifier),
                                ),

                              ),

                            ],
                          ),
                        )),
                  )
              ],
            ),
          )

          //_buildSignUpLabel(),
          ),
    );
  }
  
  resendCode(VerficationCodeNotifier verificationCodeNotifier) async{
    verificationCodeNotifier.emailId = emailId;

    ForgotPasswordResponse forgotPasswordResponse = await verificationCodeNotifier.callApiForgotPassword();

    if(forgotPasswordResponse != null &&  forgotPasswordResponse.webMethodOutputRows != null &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows !=null
        &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus !=null
        &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus == "TokenGenerated") {

      showDialog(
        context: context,
        builder: (_) =>
            AlertOverlay(
                AppLocalizations.of(context).translate('verificationCodeAlert'),
                AppLocalizations.of(context).translate('codeResentSuccessfully'),
              // AppConstants.CODE_RESENT_SUCCESSFULLY,
                AppLocalizations.of(context).translate('okay')),
      );
    } else {
      if(forgotPasswordResponse != null &&  forgotPasswordResponse.webMethodOutputRows != null &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows !=null
          &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus !=null) {
        showDialog(
          context: context,
          builder: (_) =>
              AlertOverlay(AppLocalizations.of(context).translate('verificationCodeAlert'),
                  forgotPasswordResponse.webMethodOutputRows
                      .webMethodOutputRows[0].forgotPasswordStatus,
                  AppLocalizations.of(context).translate('okay')),
        );
      }
      else {
        showDialog(
          context: context,
          builder: (_) =>
              AlertOverlay(AppLocalizations.of(context).translate('verificationCodeAlert'),
                 // AppConstants.ENTER_VALID_EMAIL,
                  AppLocalizations.of(context).translate('pleaseEnterValidEmailAddress'),
                  AppLocalizations.of(context).translate('okay')),
        );
      }
    }
  }

  /////////////////////
  /*  build helpers */
  ////////////////////
  Widget _buildWidgetImageLogo() {
   // _showSnackBarMessage(otpValue??"");
    return Container(
      height: getScreenHeight(context) / 5,
      //padding: EdgeInsets.only(top: 64, bottom: 32),
      alignment: Alignment.center,
      child: topHeader()
    );
  }

  Widget buildWidgetVerificationCard(VerficationCodeNotifier verificationCodeNotifier) {
    

    return Column(
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.only(
                top: 20.0, left: 30.0, right: 30.0, bottom: 8.0),
            child: Theme(
              data: ThemeData(
                  hintColor: colorBlack,
                  primaryColor: colorDarkRed,
                  primaryColorDark: colorLiteOrange),
              child: Form(
                key: _keyValidationForm,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    SizedBox(
                      height: AppConstants.SIDE_MARGIN/2,
                    ),
                    ButtonTheme(
                      minWidth: double.infinity,

                      child: RaisedButton(

                        highlightElevation: 8.0,
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());

                          if(enteredOTPValue.length == 6){

                            print('OTP final value :==>'+enteredOTPValue);
                            //Navigator.pushNamed(context, CreateNewPasswordScreen.routeName);
                            Navigator.pushNamed(context, CreateNewPasswordScreen.routeName,arguments: {"emailId":emailId,"otpValue":enteredOTPValue,});
                          }else  if(enteredOTPValue.trim().toString() == ""){
                            showDialog(
                              context: context,
                              builder: (_) =>
                                  AlertOverlay(
                                      AppLocalizations.of(context).translate('verificationCodeAlert'),
                                      AppLocalizations.of(context).translate('pleaseEnterVerificationCode'),
                                      //AppConstants.ENTER_VERFICIATION_CODE,
                                      AppLocalizations.of(context).translate('okay')),
                            );
                          }else{
                            showDialog(
                              context: context,
                              builder: (_) =>
                                  AlertOverlay(AppLocalizations.of(context).translate('verificationCodeAlert'),
                                      AppLocalizations.of(context).translate('youHaveEnteredIncorrectVerificationCode'),
                                      //AppConstants.ENTER_VALID_VERFICIATION_CODE,
                                      AppLocalizations.of(context).translate('okay')),
                            );
                          }
                        },
                        color: colorLiteOrange,
                        textColor: colorWhite,
                        elevation: 1,
                        padding: EdgeInsets.only(
                            top: AppConstants.SIDE_MARGIN / 1.5,
                            bottom: AppConstants.SIDE_MARGIN / 1.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                        ),
                        child: Text(
                          //AppConstants.VERIFY_CODE,
                          AppLocalizations.of(context).translate('verifyCode'),
                          style: getStyleButtonText(context).copyWith(
                              color: colorWhite,
                              letterSpacing: 1,
                              fontWeight: AppFont.fontWeightBold),
                        ),
                      ),


                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),


                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
  void redirectToLogin() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  InputDecoration _buildTextDecoration(String email) {
    return InputDecoration(
        isDense: true,
        hintText: email,
        alignLabelWithHint: true,
        filled: true,
        contentPadding: EdgeInsets.all(16));
  }

  OutlineInputBorder _buildOutlineInputBorder(){
    return  OutlineInputBorder(
      borderSide: BorderSide(
        color: colorLiteWhiteBorder,
      ),
      borderRadius: const BorderRadius.all(

        const Radius.circular(AppConstants.SIDE_MARGIN),
      ),
    );
  }

  Widget topHeader(){
    return  new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 1,
      child:  InkWell(
        onTap: () {
          Navigator.canPop(context) ? Navigator.pop(context) : '';
        },
        child: Container(
          margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
          color: Colors.transparent,
          alignment: Alignment.centerRight,
          child:Image.asset(
            AppImages.IMAGE_BACK,
            height: 25,
          ),
        ),
      ),


        ),
        Expanded(
          flex: 2,
          child:Text(
            //AppConstants.VERIFICATION_CODE,
            AppLocalizations.of(context).translate('verificationCode'),
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
                  fontWeight: AppFont.fontWeightMedium,
                  fontSize: 23,
                  fontFamily:'NPSunaGrotesk',
            ),

          ),
        ),

        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
            color: Colors.transparent,
            alignment: Alignment.centerRight,
            child:Image.asset(
              AppImages.IMAGE_APP_LOGO,
              // width: getScreenWidth(context) / 4,
              height: 20,
            ),),

        ),


      ],
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  String _validateNameField(String value) {
    return value.trim().length < 1 ? 'Field can\'t be empty' : null;
  }

}
