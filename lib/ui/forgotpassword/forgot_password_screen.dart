import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart';
import 'package:suna_care/core/data/remote/request_response/forgotpassword/response_forgot_password.dart';
import 'package:suna_care/core/notifier/change_password_notifier.dart';
import 'package:suna_care/core/notifier/forgot_password_notifier.dart';
import 'package:suna_care/ui/forgotpassword/create_new_password_screen.dart';
import 'package:suna_care/ui/forgotpassword/verification_code_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_custom_icon.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/app_log_helper.dart';

import '../login_screen.dart';



class ForgotPasswordScreen extends StatefulWidget {
  static const routeName = '/forgot_password_screen';

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final log = getLogger('ForgotPasswordScreen');
  final _keyValidationForm = GlobalKey<FormState>(debugLabel: '_forgotpasswordscreenkey');
  bool isOldPasswordVisible = false;
  bool isNewPasswordVisible = false;
  bool isConfirmPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    setInitialData(); //aa
  }

  setInitialData() async {

  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ForgotPasswordNotifier>(
      builder: (context) => ForgotPasswordNotifier(),
      child: Scaffold(
          backgroundColor: colorLitePinkBG,
          key: _scaffoldKey,

          body: Consumer<ForgotPasswordNotifier>(
            builder: (context, forgotPasswordNotifier, _) => Stack(
              fit: StackFit.expand,
              children: <Widget>[

                ModalProgressHUD(
                  color: Colors.transparent,
                  inAsyncCall: forgotPasswordNotifier.isLoading,
                  child: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    },
                    child: Container(
                        alignment: Alignment.topCenter,
                        child:  Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              _buildWidgetImageLogo(),

                             Image.asset(
                                  AppImages.IMAGE_CHANGEPWD_LOCK,
                                  height: 100,
                                  width: 105,
                                ),

                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/2,
                              ),
                              Text(
                                //AppConstants.FORGOT_PASSWORD+"?",
                                AppLocalizations.of(context).translate('forgotPassword')+"?",

                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 28
                                ),

                              ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/1.5,
                              ),
                              Text(
                                //AppConstants.FORGOT_PASSWORD_TIPS,
                                AppLocalizations.of(context).translate('enteryourRegisteredEmail'),
                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightRegular,
                                    fontSize: 14
                                ),

                              ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN,
                              ),
                              new Expanded(
                                flex: 2,
                                child:SingleChildScrollView(
                                  physics: BouncingScrollPhysics(),
                                  child:_buildWidgetForgotPasswordCard(forgotPasswordNotifier),
                                ),

                              ),

                            ],
                          ),
                        )),
                  )
              ],
            ),
          )

          //_buildSignUpLabel(),
          ),
    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////
  Widget _buildWidgetImageLogo() {
    return Container(
      height: getScreenHeight(context) / 5,
      //padding: EdgeInsets.only(top: 64, bottom: 32),
      alignment: Alignment.center,
      child: topHeader()
    );
  }

  Widget _buildWidgetForgotPasswordCard(ForgotPasswordNotifier forgotPasswordNotifier) {
    

    return Column(
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.only(
                top: 20.0, left: 30.0, right: 30.0, bottom: 8.0),
            child: Theme(
              data: ThemeData(
                  hintColor: colorBlack,
                  primaryColor: colorDarkRed,
                  primaryColorDark: colorLiteOrange),
              child: Form(
                key: _keyValidationForm,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                        controller: forgotPasswordNotifier.textEditEmailId,
                        focusNode: forgotPasswordNotifier.emailFocus,
                        keyboardType: TextInputType.emailAddress,
                        //textCapitalization: TextCapitalization.sentences,
                        textInputAction: TextInputAction.next,
                        validator: _validateNameField,
                        style: getFormStyleText(context),
                        onFieldSubmitted: (String value) {
                          //FocusScope.of(context).requestFocus(forgotPasswordNotifier.passwordFocus);
                        },
                        decoration: _buildTextDecoration(
                            //AppConstants.ENTER_EMAIL_ADDRESS
                            AppLocalizations.of(context).translate('enterEmailAddress'),
                        )
                            .copyWith(
                          fillColor: colorWhite,
                          prefixIcon: Image.asset(
                            AppImages.IMAGE_EMAIL,
                            // width: getScreenWidth(context) / 4,
                            width: 5,
                            height: 5,
                          ),
                          focusedBorder: _buildOutlineInputBorder(),
                          enabledBorder: _buildOutlineInputBorder(),
                        )
                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),


                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            //AppConstants.REMEMBER_PASSWORD+"?",
                           ""+AppLocalizations.of(context).translate('rememberPassword')+"?",
                            style: getStyleBody1(context).copyWith(
                                color: colorPencilBlack,
                                fontWeight: AppFont.fontWeightSemiBold),
                            textAlign: TextAlign.end,
                          ),
                          GestureDetector(
                            onTap: () => redirectToLogin(),
                            child: Text(
                             // " " + AppConstants.LOGIN_HERE+".",
                              " " +  AppLocalizations.of(context).translate('loginHere')+".",
                              style: getStyleBody1(context).copyWith(
                                  color: colorLiteOrange,
                                  fontWeight: AppFont.fontWeightSemiBold),
                              textAlign: TextAlign.end,
                            ),
                          )
                        ]),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),
                    ButtonTheme(
                      minWidth: double.infinity,

                      child: RaisedButton(

                        highlightElevation: 8.0,
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          //if (_keyValidationForm.currentState.validate()) {
                            //_onClickButtonLogIn(forgotPasswordNotifier);
                        //  }

                          if(forgotPasswordNotifier.textEditEmailId.text.toString() == ""){
                            showDialog(
                              context: context,
                              builder: (_) => AlertOverlay(
                                  AppLocalizations.of(context).translate('forgotPasswordAlert'),
                                  //AppConstants.ENTER_EMAILID,
                                  AppLocalizations.of(context).translate('pleaseEnterYourEmailAddress'),
                                  AppLocalizations.of(context).translate('okay')),
                            );

                          }else{
                            bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(forgotPasswordNotifier.textEditEmailId.text.toString().trim());
                            if(!emailValid) {
                              showDialog(
                                context: context,
                                builder: (_) =>
                                    AlertOverlay(
                                        AppLocalizations.of(context).translate('forgotPasswordAlert'),
                                        //AppConstants.ENTER_VALID_EMAIL,
                                        AppLocalizations.of(context).translate('pleaseEnterValidEmailAddress'),
                                         AppLocalizations.of(context).translate('okay')),
                              );
                            }else{
                              _onClickButtonForgotPassword(forgotPasswordNotifier);


                            }



                          }
                        },
                        color: colorLiteOrange,
                        textColor: colorWhite,
                        elevation: 1,
                        padding: EdgeInsets.only(
                            top: AppConstants.SIDE_MARGIN / 1.5,
                            bottom: AppConstants.SIDE_MARGIN / 1.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                        ),
                        child: Text(
                          //AppConstants.SEND,
                          AppLocalizations.of(context).translate('send'),
                          style: getStyleButtonText(context).copyWith(
                              color: colorWhite,
                              letterSpacing: 1,
                              fontWeight: AppFont.fontWeightBold),
                        ),
                      ),


                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),


                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
  void redirectToLogin() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  InputDecoration _buildTextDecoration(String email) {
    return InputDecoration(
        isDense: true,
        hintText: email,
        alignLabelWithHint: true,
        filled: true,
        contentPadding: EdgeInsets.all(16));
  }

  OutlineInputBorder _buildOutlineInputBorder(){
    return  OutlineInputBorder(
      borderSide: BorderSide(
        color: colorWhite,
      ),
      borderRadius: const BorderRadius.all(

        const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
      ),
    );
  }

  Widget topHeader(){
    return  new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 1,
      child:  InkWell(
        onTap: () {
          Navigator.canPop(context) ? Navigator.pop(context) : '';
        },
        child: Container(
          margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
          color: Colors.transparent,
          alignment: Alignment.centerLeft,
          child:Image.asset(
            AppImages.IMAGE_BACK,
            height: 25,
          ),
        ),
      ),


        ),
        Expanded(
          flex: 2,
          child:Text(
            //AppConstants.FORGOT_PASSWORD+"?",
            AppLocalizations.of(context).translate('forgotPassword')+"?",
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
              fontWeight: AppFont.fontWeightMedium,
              fontSize: 20.5,
              fontFamily:'NPSunaGrotesk',
            ),

          ),
        ),

        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
            color: Colors.transparent,
            alignment: Alignment.centerRight,
            child:Image.asset(
              AppImages.IMAGE_APP_LOGO,
              // width: getScreenWidth(context) / 4,
              height: 20,
            ),),

        ),


      ],
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////
  void _onClickButtonForgotPassword(ForgotPasswordNotifier forgotPasswordNotifier) async {
    print('Action Tap: _onClickButtonForgotPassword button');

    //Api: 1st
    ForgotPasswordResponse forgotPasswordResponse = await forgotPasswordNotifier.callApiForgotPassword();
    if(forgotPasswordResponse != null &&  forgotPasswordResponse.webMethodOutputRows != null &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows !=null
        &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus !=null
        &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus == "TokenGenerated") {

      Navigator.pushNamed(context, VerificationCodeScreen.routeName,arguments: {"emailId":forgotPasswordNotifier.textEditEmailId.text.toString(),"otpValue":forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus ??"",});

    } else { //NeverLogged\
      if(forgotPasswordResponse != null &&  forgotPasswordResponse.webMethodOutputRows != null &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows !=null
          &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus !=null
          &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus =="NeverLogged"
      ) {
        showDialog(
          context: context,
          builder: (_) =>
              AlertOverlay( AppLocalizations.of(context).translate('forgotPasswordAlert'),
                  //AppConstants.LOG_INTO_APP,
                  AppLocalizations.of(context).translate('logIntoTheApp'),
                   AppLocalizations.of(context).translate('okay')),
        );
      }else if(forgotPasswordResponse != null &&  forgotPasswordResponse.webMethodOutputRows != null &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows !=null
          &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus !=null
          &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus =="invalid-email") {
        showDialog(
          context: context,
          builder: (_) =>
              AlertOverlay(
                  AppLocalizations.of(context).translate('forgotPasswordAlert'),
                  AppLocalizations.of(context).translate('pleaseEnterValidEmailAddress'),
                  //AppConstants.ENTER_VALID_EMAIL,
                   AppLocalizations.of(context).translate('okay')),
        );
      }else if(forgotPasswordResponse != null &&  forgotPasswordResponse.webMethodOutputRows != null &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows !=null
          &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus !=null
          &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus == AppConstants.NO_INTERNET_CONNECTION) {
        showDialog(
          context: context,
          builder: (_) =>
              AlertOverlay(
                  AppLocalizations.of(context).translate('noNetworkConnection'),
                  AppLocalizations.of(context).translate('noInternetConnection'),
                  //AppConstants.NO_INTERNET_CONNECTION_TITLE,
                  //AppConstants.NO_INTERNET_CONNECTION,
                   AppLocalizations.of(context).translate('okay')),
        );
      }else if(forgotPasswordResponse != null &&  forgotPasswordResponse.webMethodOutputRows != null &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows !=null
          &&  forgotPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].forgotPasswordStatus !=null) {
        showDialog(
          context: context,
          builder: (_) =>
              AlertOverlay( AppLocalizations.of(context).translate('forgotPasswordAlert'),
                  forgotPasswordResponse.webMethodOutputRows
                      .webMethodOutputRows[0].forgotPasswordStatus,
                   AppLocalizations.of(context).translate('okay')),
        );
      }
      else {

      }
    } //show: snackBar toast

  }



  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  String _validateNameField(String value) {
    return value.trim().length < 1 ? 'Field can\'t be empty' : null;
  }

}
