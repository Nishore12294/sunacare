import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/createpassword/response_create_password.dart';
import 'package:suna_care/core/notifier/create_new_password_notifier.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_custom_icon.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/app_log_helper.dart';

import '../login_screen.dart';



class CreateNewPasswordScreen extends StatefulWidget {
  static const routeName = '/create_new_password';

  @override
  _CreateNewPasswordScreenState createState() => _CreateNewPasswordScreenState();
}

class _CreateNewPasswordScreenState extends State<CreateNewPasswordScreen> {
  final log = getLogger('CreateNewPasswordScreen');
  final _keyValidationForm = GlobalKey<FormState>(debugLabel: '_createPasswordkey');
  bool isNewPasswordVisible = false;
  bool isConfirmPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String emailId = "",verificationCode = "";
  @override
  void initState() {
    super.initState();
    setInitialData(); //aa
  }

  setInitialData() async {

  }

  @override
  Widget build(BuildContext context) {
    final  Map<String, Object> argData = ModalRoute.of(context).settings.arguments;
    print("emailId data ${argData['emailId']}");
    print("otpValue data ${argData['otpValue']}");
    emailId = argData['emailId'];
    verificationCode = argData['otpValue'];
    return ChangeNotifierProvider<CreatePasswordNotifier>(
      builder: (context) => CreatePasswordNotifier(),
      child: Scaffold(
          backgroundColor: colorLitePinkBG,
          key: _scaffoldKey,
          body: Consumer<CreatePasswordNotifier>(
            builder: (context, createPasswordNotifier, _) => Stack(
              fit: StackFit.expand,
              children: <Widget>[
                ModalProgressHUD(
                  color: Colors.transparent,
                  inAsyncCall: createPasswordNotifier.isLoading,
                  child: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    },
                    child: Container(
                        alignment: Alignment.topCenter,
                        child:  Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              _buildWidgetImageLogo(),

                             Image.asset(
                                  AppImages.IMAGE_CHANGEPWD_LOCK,
                               height: 100,
                               width: 105,
                                ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/2,
                              ),
                              Text(
                                //AppConstants.CREAET_NEW_A_PASSWORD,
                                AppLocalizations.of(context).translate('createNewPassword'),
                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 25
                                ),
                              ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/2,
                              ),
                              Text(
                                //AppConstants.CREATE_PASSWORD_TIPS,
                                AppLocalizations.of(context).translate('yourNewPasswordMustDifferentPreviousPasswords'),
                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightRegular,
                                    fontSize: 16
                                ),
                              ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/2,
                              ),
                              new Expanded(
                                flex: 2,
                                child:SingleChildScrollView(
                                  physics: BouncingScrollPhysics(),
                                  child:buildWidgetCreatePasswordCard(createPasswordNotifier),
                                ),

                              ),
                            ],
                          ),
                        )),
                  )
              ],
            ),
          )

          //_buildSignUpLabel(),
          ),
    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////
  Widget _buildWidgetImageLogo() {
    return Container(
      height: getScreenHeight(context) / 5,
      //padding: EdgeInsets.only(top: 64, bottom: 32),
      alignment: Alignment.center,
      child: topHeader()
    );
  }

  Widget buildWidgetCreatePasswordCard(CreatePasswordNotifier createPasswordNotifier) {
    

    return Column(
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.only(
                top: 20.0, left: 30.0, right: 30.0, bottom: 8.0),
            child: Theme(
              data: ThemeData(
                  hintColor: colorBlack,
                  primaryColor: colorDarkRed,
                  primaryColorDark: colorLiteOrange),
              child: Form(
                key: _keyValidationForm,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    TextFormField(
                      controller: createPasswordNotifier.textEditNewPwd,
                      focusNode: createPasswordNotifier.newPwdFocus,
                      obscureText: !isNewPasswordVisible,
                      keyboardType: TextInputType.text,
                      validator: _validateNameField,
                      textInputAction: TextInputAction.next,
                      style: getFormStyleText(context),

                      decoration:
                      _buildTextDecoration(
                           //AppConstants.ENTER_NEW_PASSWORD,
                           AppLocalizations.of(context).translate('enterNewPassword'),
                      ).copyWith(
                        fillColor: colorWhite,
                        prefixIcon: Image.asset(
                          AppImages.IMAGE_PASSWORD,
                          // width: getScreenWidth(context) / 4,
                          width: 5,
                          height: 5,
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(isNewPasswordVisible
                              ? Icons.visibility
                              : Icons.visibility_off),
                          onPressed: () {
                            setState(() {
                              isNewPasswordVisible = !isNewPasswordVisible;
                            });
                          },
                        ),
                        focusedBorder: _buildOutlineInputBorder(),
                        enabledBorder: _buildOutlineInputBorder(),
                      ),
                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),
                    TextFormField(
                      controller: createPasswordNotifier.textEditConfirmPwd,
                      focusNode: createPasswordNotifier.confirmPwdFocus,
                      obscureText: !isConfirmPasswordVisible,
                      keyboardType: TextInputType.text,
                      validator: _validateNameField,
                      textInputAction: TextInputAction.done,
                      style: getFormStyleText(context),
                      decoration:
                      _buildTextDecoration(
                          AppLocalizations.of(context).translate('confirmNewPassword'),
                          //AppConstants.ENTER_CONFIRM_PASSWORD
                      ).copyWith(
                        fillColor: colorWhite,
                        prefixIcon: Image.asset(
                          AppImages.IMAGE_PASSWORD,
                          // width: getScreenWidth(context) / 4,
                          width: 5,
                          height: 5,
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(isConfirmPasswordVisible
                              ? Icons.visibility
                              : Icons.visibility_off),
                          onPressed: () {
                            setState(() {
                              isConfirmPasswordVisible = !isConfirmPasswordVisible;
                            });
                          },
                        ),
                        focusedBorder: _buildOutlineInputBorder(),
                        enabledBorder: _buildOutlineInputBorder(),
                      ),
                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN*1.5,
                    ),


                    ButtonTheme(
                      minWidth: double.infinity,

                      child: RaisedButton(

                        highlightElevation: 8.0,
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          //if (_keyValidationForm.currentState.validate()) {
                            //_onClickButtonLogIn(createPasswordNotifier);
                        //  }

                          if(createPasswordNotifier.textEditNewPwd.text.toString() == ""&& createPasswordNotifier.textEditConfirmPwd.text.toString() == ""){
                            showDialog(
                              context: context,
                              builder: (_) => AlertOverlay(
                                  AppLocalizations.of(context).translate('createNewPassword'),
                                  AppLocalizations.of(context).translate('pleaseEnterNewPassword'),
                                  //AppConstants.VD_ENTER_NEW_PASSWORD,
                                  AppLocalizations.of(context).translate('okay')),
                            );

                          }else if(createPasswordNotifier.textEditNewPwd.text.toString() == ""){
                            showDialog(
                              context: context,
                              builder: (_) => AlertOverlay(
                                  AppLocalizations.of(context).translate('createNewPassword'),
                                  AppLocalizations.of(context).translate('pleaseEnterNewPassword'),
                                  //AppConstants.VD_ENTER_NEW_PASSWORD,
                                  AppLocalizations.of(context).translate('okay')),
                            );

                          }else if(createPasswordNotifier.textEditConfirmPwd.text.toString() == ""){
                            showDialog(
                              context: context,
                              builder: (_) => AlertOverlay(
                                  AppLocalizations.of(context).translate('createNewPassword'),
                                  AppLocalizations.of(context).translate('pleaseEnterConfirmNewPassword'),
                                  //AppConstants.VD_ENTER_CONFIRM_PASSWORD,
                                  AppLocalizations.of(context).translate('okay')),
                            );

                          }else{
                            if(createPasswordNotifier.textEditConfirmPwd.text.toString() != createPasswordNotifier.textEditNewPwd.text.toString()){
                              showDialog(
                                context: context,
                                builder: (_) =>
                                    AlertOverlay(
                                        AppLocalizations.of(context).translate('createNewPassword'),
                                        AppLocalizations.of(context).translate('passwordsDoNotMatch'),
                                       // AppConstants.ENTER_PASSWORDS_NOT_EQUAL,
                                        AppLocalizations.of(context).translate('okay')),
                              );
                            }else {
                              _onClickButtonCreatePassword(createPasswordNotifier);
//                              showDialog(
//                                context: context,
//                                builder: (_) =>
//                                    AlertOverlay(AppLocalizations.of(context).translate('createNewPassword'),
//                                        "Password Changed",
//                                        AppLocalizations.of(context).translate('okay')),
//                              );
                            }
                          }
                        },
                        color: colorLiteOrange,
                        textColor: colorWhite,
                        elevation: 1,
                        padding: EdgeInsets.only(
                            top: AppConstants.SIDE_MARGIN / 1.5,
                            bottom: AppConstants.SIDE_MARGIN / 1.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                        ),
                        child: Text(
                          //AppConstants.RESET_PASSWORD,
                          AppLocalizations.of(context).translate('resetPassword'),
                          style: getStyleButtonText(context).copyWith(
                              color: colorWhite,
                              letterSpacing: 1,
                              fontWeight: AppFont.fontWeightBold),
                        ),
                      ),


                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),


                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
  InputDecoration _buildTextDecoration(String email) {
    return InputDecoration(
        isDense: true,
        hintText: email,
        alignLabelWithHint: true,
        filled: true,
        contentPadding: EdgeInsets.all(16));
  }

  OutlineInputBorder _buildOutlineInputBorder(){
    return  OutlineInputBorder(
      borderSide: BorderSide(
        color: colorWhite,
        //color: Colors.transparent,
      ),
      borderRadius: const BorderRadius.all(

        const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
      ),
    );
  }

  Widget topHeader(){
    return  new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 1,
      child:  InkWell(
        onTap: () {
          Navigator.canPop(context) ? Navigator.pop(context) : '';
        },
        child: Container(
          margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
          color: Colors.transparent,
          alignment: Alignment.centerRight,
          child:Image.asset(
            AppImages.IMAGE_BACK,
            height: 25,
          ),
        ),
      ),


        ),
        Expanded(
          flex: 2,
          child:Text(
            //AppConstants.CREAET_NEW_PASSWORD,
            AppLocalizations.of(context).translate('newPassword'),
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',
            ),

          ),
        ),

        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
            color: Colors.transparent,
            alignment: Alignment.centerRight,
            child:Image.asset(
              AppImages.IMAGE_APP_LOGO,
              // width: getScreenWidth(context) / 4,
              height: 20,
            ),),

        ),
      ],
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////
  void _onClickButtonCreatePassword(CreatePasswordNotifier createPasswordNotifier) async {
    print('Action Tap: create password button');

    //Api: 1st
    CreatePasswordResponse createPasswordResponse = await createPasswordNotifier.callApiForgotSetOrCreatePassword(emailId,verificationCode);
    if(createPasswordResponse != null && createPasswordResponse.webMethodOutputRows != null
        && createPasswordResponse.webMethodOutputRows.webMethodOutputRows != null
        && createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status != null){
      if( createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status == "Updated_Token") {
        AppSharedPreference().saveStringValue(AppConstants.KEY_USER_PASSWORD,
            createPasswordNotifier.textEditConfirmPwd.text.trim());
       // _showSnackBarMessage("Password changed Successfully..");
        showSuccessDialog();
        Future.delayed(Duration(seconds: 2)).whenComplete(() {
          Navigator.pushNamedAndRemoveUntil(
              context, LoginScreen.routeName, (e) => false);
        });
      }else{

        if(createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status == "MultipleEmailMatch"){
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(
                      AppLocalizations.of(context).translate('createNewPassword'),
                      AppLocalizations.of(context).translate('emailValidButMatchToMultiCGRec'),
                      //AppConstants.EMAIL_IS_VALID_MATCH_MULTI_CG_REC,
                      AppLocalizations.of(context).translate('okay')));
        }
        else if(createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status == "InvalidForgotPasswordToken"){
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(
                      AppLocalizations.of(context).translate('createNewPassword'),
                      AppLocalizations.of(context).translate('TheVerificationCodeInvalidEnterValidVerificationCode'),
                      //AppConstants.INVALID_VERIFICATION_CREATE_PASSWORD,
                      AppLocalizations.of(context).translate('okay')));
        }
        else if(createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status == "Resigned"){
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(AppLocalizations.of(context).translate('createNewPassword'),
                      AppLocalizations.of(context).translate('emailOrPasswordValidCGInActiveAndResigned'),
                      //AppConstants.EMAIL_VALID_CG_INACTIVE_RESIGNED,
                      AppLocalizations.of(context).translate('okay')));
        }else if(createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status == "InvalidPassword"){
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(AppLocalizations.of(context).translate('createNewPassword'),
                     // AppConstants.ENTER_INVALID_EMAIL_PASSWORD,
                      AppLocalizations.of(context).translate('youHaveEnteredInvalidEmailPassword'),
                      AppLocalizations.of(context).translate('okay')));
        }else if(createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status == "InvalidEmail"){
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(AppLocalizations.of(context).translate('createNewPassword'),
                     // AppConstants.ENTER_INVALID_EMAIL_PASSWORD,
                      AppLocalizations.of(context).translate('youHaveEnteredInvalidEmailPassword'),
                      AppLocalizations.of(context).translate('okay')));
        }else if(createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status == "Blacklisted"){
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(AppLocalizations.of(context).translate('createNewPassword'),
                      //AppConstants.EMAIL_PASSWORD_VALID_CG_INACTIVE_BLACKLISTED,
                      AppLocalizations.of(context).translate('yourAccountBlockListedPleaseContactAdmin'),
                      AppLocalizations.of(context).translate('okay')));
        }else if(createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status == "Unexpected"){
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(AppLocalizations.of(context).translate('createNewPassword'),
                      createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status,
                      AppLocalizations.of(context).translate('okay')));
        }else{
          showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(AppLocalizations.of(context).translate('createNewPassword'),
                      createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status,
                      AppLocalizations.of(context).translate('okay')));
        }

//        showDialog(
//            context: context,
//            builder: (_) =>
//                AlertOverlay(AppLocalizations.of(context).translate('createNewPassword'),
//                    createPasswordResponse.webMethodOutputRows.webMethodOutputRows[0].status,
//                    AppLocalizations.of(context).translate('okay')),
//          );
        }
    } else {
        showDialog(
          context: context,
          builder: (_) =>
              AlertOverlay(
                  AppLocalizations.of(context).translate('createNewPassword'),
                  AppLocalizations.of(context).translate('somethingWentWrongPleaseTryAgainLater'),
                  //AppConstants.SOME_THING_WENT_WRONG,
                  AppLocalizations.of(context).translate('okay')),
        );

    }
  }
  void showSuccessDialog(){
    showDialog(
      context: context,
      builder: (_) =>
          AlertOverlay(
              AppLocalizations.of(context).translate('createNewPassword'),
              AppLocalizations.of(context).translate('passwordChangedSuccessfully'),
              //AppConstants.CREATE_PASSWORD_CHANGED_SUCCESSFULLY,
              AppLocalizations.of(context).translate('okay')),
    );
  }


  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  String _validateNameField(String value) {
    return value.trim().length < 1 ? 'Field can\'t be empty' : null;
  }

}
