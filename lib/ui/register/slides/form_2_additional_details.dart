import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:suna_care/core/data/remote/request_response/register/signup/response_signup.dart';
import 'package:suna_care/core/data/remote/request_response/register/verifyEmailOtp/response_verify_email_otp.dart';
import 'package:suna_care/core/notifier/signup_notifier.dart';
import 'package:suna_care/ui/register/pending_approval_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/widget_helper/custom_switch.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:provider/provider.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:supercharged/supercharged.dart';

import '../../login_screen.dart';

enum _SwitchBoxProps { paddingLeft, color, text, rotation }
class Form2AdditionalDetailsScreen extends StatefulWidget {
  BuildContext context;


  Form2AdditionalDetailsScreen({this.context});

  @override
  Form2AdditionalDetailsScreenState createState() => Form2AdditionalDetailsScreenState(context);
}

class Form2AdditionalDetailsScreenState extends State<Form2AdditionalDetailsScreen> {
  BuildContext context;
   //bool driverLicSwitch = false,smokingEnableSwitch = false,speakingGermanLangSwitch = false,expInCareSwitch = false,transfWheelchairSwitch5 = false,nightShiftSwitch = false,ableTcTwoPeopleSwitch = false;

  Form2AdditionalDetailsScreenState(this.context);
  var log = getLogger("FormOtpScreen");

  final _keyForm2AdditionalDetailsScreen = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Consumer<SignUpNotifier>(
        builder: (context, signUpNotifier, _) => Theme(
            data: ThemeData(
                hintColor: colorDividerGrey,
                primaryColor: colorPrimary,
                primaryColorDark: colorPrimary),
            child: buildWidgetAdditionalDetails(signUpNotifier, context)));
  }

  /////////////////////
  /*  build helpers */
  ////////////////////
  void _driverLicenseToggle(SignUpNotifier signUpNotifier) {
    //setState(() {
      signUpNotifier.driverLicSwitch = !signUpNotifier.driverLicSwitch;
      debugPrint("signUpNotifier.driverLicSwitch:==>"+signUpNotifier.driverLicSwitch.toString());
   // });
  }
  void _smokingToggle(SignUpNotifier signUpNotifier) {

      signUpNotifier.smokingEnableSwitch = !signUpNotifier.smokingEnableSwitch;
  }

  void _speakingLangToggle(SignUpNotifier signUpNotifier) {

    signUpNotifier.speakingGermanLangSwitch = !signUpNotifier.speakingGermanLangSwitch;

  }
  void _expInCareToggle(SignUpNotifier signUpNotifier) {

    signUpNotifier.expInCareSwitch = !signUpNotifier.expInCareSwitch;
  }

  void _transWheelChairToggle(SignUpNotifier signUpNotifier) {
    signUpNotifier.transfWheelchairSwitch = !signUpNotifier.transfWheelchairSwitch;
  }
  void _nightShitToggle(SignUpNotifier signUpNotifier) {
      signUpNotifier.nightShiftSwitch = !signUpNotifier.nightShiftSwitch;
  }

  void _tc2PeopleToggle(SignUpNotifier signUpNotifier) {
    signUpNotifier.ableTcTwoPeopleSwitch = !signUpNotifier.ableTcTwoPeopleSwitch;
  }
  Widget buildWidgetAdditionalDetails(
      SignUpNotifier signUpNotifier, BuildContext context) {
    signUpNotifier.context = context;
   /* List<String> experienceList = [
      'No',
      'Max 6 Months',
      '6 Months - 1 Year',
      '1 Year - 2 Years',
      'Over 2 years',
    ];*/

    //New Values = OPtions
    // 10     = No
    // 20     = Under 1 Year
    // 30     = 1 - 3 Years
    // 40     = 3 - 5 Years
    // 50     = 5 - 10 Years
    // 60     = Over 10 Years
    List<String> experienceList = [
      'No',
      'Under 1 Year',
      '1 - 3 Years',
      '3 - 5 Years',
      '5 - 10 Years',
      'Over 10 Years',
    ];
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: <Widget>[

          Container(
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 16.0, left: 30.0, right: 30.0, bottom: 8.0),
              child: Theme(
                data: ThemeData(
                    hintColor: colorBorderLine,
                    primaryColor: colorGradientPink,
                    primaryColorDark: colorGradientPink),
                child: Form(
                  key: _keyForm2AdditionalDetailsScreen,
                  child:SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: <Widget>[
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[

                              SizedBox(
                                height: AppConstants.SIDE_MARGIN*2.2,

                              ),
                              Expanded(
                                flex: 1,
                                child:Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    //  AppConstants.DRIVER_LICENCE,
                                      AppLocalizations.of(context).translate('driverLicence'),
                                      style: getStyleBody1(context).copyWith(
                                          color: colorPencilBlack,
                                          fontWeight: AppFont.fontWeightSemiBold,
                                          fontSize: 16
                                      )
                                  ),
                                ),
                              ),
                              GestureDetector(
                                  onTap:()=> _driverLicenseToggle(signUpNotifier),
                                  behavior: HitTestBehavior.translucent,
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(height: 5,),
                                        CustomSwitch(switched: signUpNotifier.driverLicSwitch),
                                      ],
                                    ),
                                  )),


                            ]

                        ),

                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Divider(
                          color: colorDividerGrey,
                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child:Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    //AppConstants.SMOKING,
                                    AppLocalizations.of(context).translate('smoking'),
                                    style: getStyleBody1(context).copyWith(
                                        color: colorPencilBlack,
                                        fontWeight: AppFont.fontWeightSemiBold,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                  onTap:()=> _smokingToggle(signUpNotifier),
                                  behavior: HitTestBehavior.translucent,
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(height: 5,),
                                        CustomSwitch(switched: signUpNotifier.smokingEnableSwitch),
                                      ],
                                    ),
                                  )),


                            ]

                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Divider(
                          color: colorDividerGrey,
                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child:Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    //AppConstants.SPEAKING_GERMAN_LANGUAGE,
                                    AppLocalizations.of(context).translate('speakingGermanLanguage'),
                                    style: getStyleBody1(context).copyWith(
                                        color: colorPencilBlack,
                                        fontWeight: AppFont.fontWeightSemiBold,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                  onTap:()=> _speakingLangToggle(signUpNotifier),
                                  behavior: HitTestBehavior.translucent,
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(height: 5,),
                                        CustomSwitch(switched: signUpNotifier.speakingGermanLangSwitch),
                                      ],
                                    ),
                                  )),


                            ]

                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Divider(
                          color: colorDividerGrey,
                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
//                        Row(
//                            mainAxisAlignment: MainAxisAlignment.center,
//                            crossAxisAlignment: CrossAxisAlignment.center,
//                            children: <Widget>[
//                              Expanded(
//                                flex: 1,
//                                child:Align(
//                                  alignment: Alignment.centerLeft,
//                                  child: Text(AppConstants.EXPERIENCE_IN_CARE,
//                                    style: getStyleBody1(context).copyWith(
//                                        color: colorLiteOrange,
//                                        fontWeight: AppFont.fontWeightSemiBold,
//                                        fontSize: 16),
//                                  ),
//                                ),
//                              ),
//                              GestureDetector(
//                                  onTap:()=> _expInCareToggle(signUpNotifier),
//                                  behavior: HitTestBehavior.translucent,
//                                  child: Center(
//                                    child: Column(
//                                      mainAxisAlignment: MainAxisAlignment.center,
//                                      crossAxisAlignment: CrossAxisAlignment.center,
//                                      children: <Widget>[
//                                        SizedBox(height: 5,),
//                                        CustomSwitch(switched: signUpNotifier.expInCareSwitch),
//                                      ],
//                                    ),
//                                  )),
//
//
//                            ]
//
//                        ),


//                        SizedBox(
//                          height: AppConstants.SIDE_MARGIN/2,
//
//                        ),
//                        Divider(
//                          color: colorDividerGrey,
//                        ),
//                        SizedBox(
//                          height: AppConstants.SIDE_MARGIN/2,
//
//                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child:Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    //AppConstants.WILLING_TO_DO_TRANFER,
                                    AppLocalizations.of(context).translate('willingTransferwheelchair'),
                                    style: getStyleBody1(context).copyWith(
                                        color: colorPencilBlack,
                                        fontWeight: AppFont.fontWeightSemiBold,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                  onTap:()=> _transWheelChairToggle(signUpNotifier),
                                  behavior: HitTestBehavior.translucent,
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(height: 5,),
                                        CustomSwitch(switched: signUpNotifier.transfWheelchairSwitch),
                                      ],
                                    ),
                                  )),


                            ]

                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Divider(
                          color: colorDividerGrey,
                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child:Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                   // AppConstants.WILLING_TO_DO_NIGHT_SHIFTS,
                                    AppLocalizations.of(context).translate('willingNightShifts'),
                                    style: getStyleBody1(context).copyWith(
                                        color: colorPencilBlack,
                                        fontWeight: AppFont.fontWeightSemiBold,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                  onTap:()=> _nightShitToggle(signUpNotifier),
                                  behavior: HitTestBehavior.translucent,
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(height: 5,),
                                        CustomSwitch(switched: signUpNotifier.nightShiftSwitch),
                                      ],
                                    ),
                                  )),


                            ]

                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Divider(
                          color: colorDividerGrey,
                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child:Align(
                                  alignment: Alignment.centerLeft,

                                  child: Text(
                                    //AppConstants.ABLE_TO_TAKE_CARE_2PEOPLE,
                                    AppLocalizations.of(context).translate('ableTakeCareTwoPeople'),
                                    style: getStyleBody1(context).copyWith(
                                        color: colorPencilBlack,
                                        fontWeight: AppFont.fontWeightSemiBold,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                  onTap:()=> _tc2PeopleToggle(signUpNotifier),
                                  behavior: HitTestBehavior.translucent,
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(height: 5,),
                                        CustomSwitch(switched: signUpNotifier.ableTcTwoPeopleSwitch),
                                      ],
                                    ),
                                  )),


                            ]

                        ),

                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),
                        Divider(
                          color: colorDividerGrey,
                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN/2.5,

                        ),


                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child:  SizedBox(
                                  height: AppConstants.SIDE_MARGIN*2.4,
                                  child: DropdownButtonFormField(
                                    style: TextStyle(color: colorPencilBlack,
                                        fontWeight: AppFont.fontWeightSemiBold
                                    ),
                                    decoration:
                                    _buildTextDecoration(
                                      //AppConstants.EXPERIENCE_IN_CARE,
                                      AppLocalizations.of(context).translate('experienceInCare'),

                                    )
                                        .copyWith(
                                      isDense: true,
                                      hintStyle: TextStyle(color: colorPencilBlack,
                                          fontWeight: AppFont.fontWeightSemiBold
                                      ),
                                      fillColor: colorWhite,

//                                      prefixIcon: Image.asset(
//                                        AppImages.IMAGE_GENDER,
//                                        // width: getScreenWidth(context) / 4,
//                                        width: 5,
//                                        height: 5,
//                                      ),
                                      focusedBorder: new OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: colorWhite,
                                          //color: Colors.transparent,
                                        ),
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(AppConstants.SIDE_MARGIN),
                                        ),
                                      ),
                                      enabledBorder: new OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: colorWhite,
                                          //color: Colors.transparent,
                                        ),
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(AppConstants.SIDE_MARGIN),
                                        ),
                                      ),
                                    ),
                                    value: signUpNotifier.experienceInCareStr,
                                    onChanged: (String Value) {
                                      setState(() {
                                        signUpNotifier.experienceInCareStr = Value;


//                                      if(Value == 'Male')
//                                        signUpNotifier.experienceInCareStr = "10";
//                                      else
//                                        signUpNotifier.experienceInCareStr = "20";

                                      });
                                    },
                                    items: experienceList
                                        .map((item) => DropdownMenuItem(
                                        value: item, child: Text("$item")))
                                        .toList(),
                                  ),
                                ),

                              ),



                            ]

                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,

                        ),


                        ButtonTheme(
                          minWidth: double.infinity,
                          child: RaisedButton(
                            highlightElevation: 8.0,
                            onPressed: () {

//                              if(!signUpNotifier.driverLicSwitch&&!signUpNotifier.smokingEnableSwitch
//                              &&!signUpNotifier.speakingGermanLangSwitch && !signUpNotifier.transfWheelchairSwitch
//                              &&!signUpNotifier.nightShiftSwitch && !signUpNotifier.ableTcTwoPeopleSwitch
//                              )
//                              {
//                                showDialog(
//                                  context: context,
//                                  builder: (_) =>
//                                      AlertOverlay( AppLocalizations.of(context).translate('signUp'),
//                                          AppConstants.PLEASE_SELECT_OPTIONS_SUBMIT,
//                                           AppLocalizations.of(context).translate('okay')),
//                                );
//                              }else if(!signUpNotifier.driverLicSwitch){
//                                showDialog(
//                                  context: context,
//                                  builder: (_) =>
//                                      AlertOverlay( AppLocalizations.of(context).translate('signUp'),
//                                          AppConstants.DIRVER_LICENSE_INVALID_VALUE,
//                                           AppLocalizations.of(context).translate('okay')),
//                                );
//                              }else if(!signUpNotifier.smokingEnableSwitch){
//                                showDialog(
//                                  context: context,
//                                  builder: (_) =>
//                                      AlertOverlay( AppLocalizations.of(context).translate('signUp'),
//                                          AppConstants.SMOKING_CONTAIN_INVALID_VALUE,
//                                           AppLocalizations.of(context).translate('okay')),
//                                );
//                              }else if(!signUpNotifier.speakingGermanLangSwitch){
//                                showDialog(
//                                  context: context,
//                                  builder: (_) =>
//                                      AlertOverlay( AppLocalizations.of(context).translate('signUp'),
//                                          AppConstants.SPEAKING_GERMAN_LANG_INVALID_VALUE,
//                                           AppLocalizations.of(context).translate('okay')),
//                                );
//                              }
//
//                              else if(!signUpNotifier.transfWheelchairSwitch){
//                                showDialog(
//                                  context: context,
//                                  builder: (_) =>
//                                      AlertOverlay( AppLocalizations.of(context).translate('signUp'),
//                                          AppConstants.WILLING_TO_DO_TRANS_CONTAIN_INVALID_VALUE,
//                                           AppLocalizations.of(context).translate('okay')),
//                                );
//                              }else if(!signUpNotifier.nightShiftSwitch){
//                                showDialog(
//                                  context: context,
//                                  builder: (_) =>
//                                      AlertOverlay( AppLocalizations.of(context).translate('signUp'),
//                                          AppConstants.WILLING_TO_DO_NIGHTSHIFTS_CONTAIN_INVALID_VALUE,
//                                           AppLocalizations.of(context).translate('okay')),
//                                );
//                              }else if(!signUpNotifier.ableTcTwoPeopleSwitch){
//                                showDialog(
//                                  context: context,
//                                  builder: (_) =>
//                                      AlertOverlay( AppLocalizations.of(context).translate('signUp'),
//                                          AppConstants.ABLE_TO_TAKE_CARE_OF_2PEOPLE,
//                                           AppLocalizations.of(context).translate('okay')),
//                                );
//                              }

                              if(signUpNotifier.experienceInCareStr == null || signUpNotifier.experienceInCareStr == "" )
                              {

                                showDialog(
                                  context: context,
                                  builder: (_) =>
                                      AlertOverlay( AppLocalizations.of(context).translate('signUp'),
                                        // AppConstants.PLEASE_SELECT_EXPERIENCE_CONTAIN,
                                          AppLocalizations.of(context).translate('pleaseSelectExperienceInCare'),
                                           AppLocalizations.of(context).translate('okay')),
                                );
                              }else{

                                 _onClickButtonSubmit(signUpNotifier);
                              }

                            },
                            color: colorLiteOrange,
                            textColor: colorWhite,
                            elevation: 1,
                            padding: EdgeInsets.only(
                                top: AppConstants.SIDE_MARGIN / 1.5,
                                bottom: AppConstants.SIDE_MARGIN / 1.5),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(AppConstants.SIDE_MARGIN*1.5))),
                            child: Text(
                              //AppConstants.SUBMIT,
                              AppLocalizations.of(context).translate('submit'),
                              style: getStyleButtonText(context).copyWith(
                                  color: colorWhite,
                                  letterSpacing: 1,
                                  fontWeight: AppFont.fontWeightBold),
                            ),
                          ),
                        ),

                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                               // AppConstants.ALREADY_HAVE_AN_ACCOUNT,
                                AppLocalizations.of(context).translate('alreadyHaveAnAccount'),
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightSemiBold),
                                textAlign: TextAlign.end,
                              ),
                              GestureDetector(
                                onTap: () => redirectToLogin(),
                                child: Text(
                                  //" " + AppConstants.LOGIN_HERE+".",
                                 " "+ AppLocalizations.of(context).translate('loginHere')+".",
                                  style: getStyleBody1(context).copyWith(
                                      color: colorLiteOrange,
                                      fontWeight: AppFont.fontWeightSemiBold),
                                  textAlign: TextAlign.end,
                                ),
                              )
                            ]),
                      ],
                    ),
                  ),

                ),
              ),
            ),
          ),
        ],
      ),
    );


  }


  InputDecoration _buildTextDecoration(String value) {
    return InputDecoration(
        isDense: true,
        hintText: value,
        // labelText: value,
        //alignLabelWithHint: true,
        filled: true,
        hintStyle: TextStyle(color: colorDarkGrey),
        contentPadding: EdgeInsets.all(16));
  }
  void redirectToLogin() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }



  /////////////////////
  /*On click-actions*/
  ////////////////////
  void _onClickButtonSubmit(SignUpNotifier signUpNotifier) async {
    print('Action Tap: Additional details');
    print(signUpNotifier.getSignUpRequest().toJson().toString());
    SignUpResponse signUpResponse = await signUpNotifier.callApiSignUp(signUpNotifier.getSignUpRequest(),);
    if (signUpResponse != null) {
      try {
        if (signUpResponse != null && signUpResponse.webMethodOutputRows != null
            && signUpResponse.webMethodOutputRows.webMethodOutputRows[0] != null
            && signUpResponse.webMethodOutputRows.webMethodOutputRows[0].leadNumber != null
            && signUpResponse.webMethodOutputRows.webMethodOutputRows[0] != "") {

          Navigator.pushReplacementNamed(context, PendingApprovalScreen.routeName);

        }else  if (signUpResponse != null && signUpResponse.outputRowErrors != null
            && signUpResponse.outputRowErrors.outputRowErrors[0] != null
            && signUpResponse.outputRowErrors.outputRowErrors[0].textMessage != null
            && signUpResponse.outputRowErrors.outputRowErrors[0].textMessage != ""
            && signUpResponse.outputRowErrors.outputRowErrors[0].columnName != null
            && signUpResponse.outputRowErrors.outputRowErrors[0].columnName != ""
        ){
          debugPrint("signUpResponse.outputRowErrors.outputRowErrors[0].columnName :==>"+signUpResponse.outputRowErrors.outputRowErrors[0].columnName);
          debugPrint("signUpResponse.outputRowErrors.outputRowErrors[0].textMessage :==>"+signUpResponse.outputRowErrors.outputRowErrors[0].textMessage);
          showDialog(
            context: context,
            builder: (_) => AlertOverlay(
                 AppLocalizations.of(context).translate('signUp'),
                signUpResponse.outputRowErrors.outputRowErrors[0].columnName+" " ?? "" +" "
                  +signUpResponse.outputRowErrors.outputRowErrors[0].textMessage,
                 AppLocalizations.of(context).translate('okay')),
          );

        }else  if (signUpResponse != null && signUpResponse.outputRowErrors != null
            && signUpResponse.outputRowErrors.outputRowErrors[0] != null
            && signUpResponse.outputRowErrors.outputRowErrors[0].textMessage != null
            && signUpResponse.outputRowErrors.outputRowErrors[0].textMessage != ""
        ){
          debugPrint("signUpResponse.outputRowErrors.outputRowErrors[0].textMessage :==>"+signUpResponse.outputRowErrors.outputRowErrors[0].textMessage);

          if(signUpResponse.outputRowErrors.outputRowErrors[0].textMessage == "QualificationPending"){
            showDialog(
              context: context,
              builder: (_) => AlertOverlay(
                   AppLocalizations.of(context).translate('signUp'),
                   AppLocalizations.of(context).translate('yourEmailAddressIsAlreadyUsedPendingApproval'),
                  //AppConstants.EMAIL_ALREADY_USED_PENDING_APPROVAL,
                   AppLocalizations.of(context).translate('okay')),
            );
          }else if(signUpResponse.outputRowErrors.outputRowErrors[0].textMessage == "ProfileCreated"){
            showDialog(
              context: context,
              builder: (_) =>
                  EmailAlreadyExistAlert(
                      //AppConstants.EMAIL_EXISTS,
                      AppLocalizations.of(context).translate('emailExists'),
                      //AppConstants.YOUR_EMAIL_ALREADY_USED_LOGIN_INSTEAD,
                      AppLocalizations.of(context).translate('yourEmailAddssAlreadyUsedLoginInstead'),
                      //AppConstants.LOGIN,
                      AppLocalizations.of(context).translate('login'),
                     // AppConstants.CLOSE,
                      AppLocalizations.of(context).translate('close'),
                  ),
            );
          }else if(signUpResponse.outputRowErrors.outputRowErrors[0].textMessage == AppConstants.NO_INTERNET_CONNECTION){
            showDialog(
              context: context,
              builder: (_) =>
                  AlertOverlay(
                      //AppConstants.NO_INTERNET_CONNECTION_TITLE,
                      AppLocalizations.of(context).translate('noNetworkConnection'),
                      //AppConstants.NO_INTERNET_CONNECTION,
                      AppLocalizations.of(context).translate('noInternetConnection'),
                       AppLocalizations.of(context).translate('okay')),
            );
          }else{
            showDialog(
              context: context,
              builder: (_) => AlertOverlay(
                   AppLocalizations.of(context).translate('signUp'),
                  " "+signUpResponse.outputRowErrors.outputRowErrors[0].textMessage,
                   AppLocalizations.of(context).translate('okay')),
            );
          }


        } else {
          signUpNotifier.showSnackBarMessage(ctx: context, message:  AppConstants.SOME_THING_WENT_WRONG,);
        }
      } catch (e) {
        log.e("Error : " + e.toString());
        signUpNotifier.showSnackBarMessage(
            ctx: context, message: AppConstants.PLEASE_TRY_AGAIN);
      }
    } else {
      signUpNotifier.showSnackBarMessage(
          ctx: context, message: AppConstants.PLEASE_TRY_AGAIN);
    }

  }


}


class EmailAlreadyExistAlert extends StatefulWidget {
  String title,subTitle,buttonOneTitle,buttonTwoTitle;
  EmailAlreadyExistAlert(this.title,this.subTitle,this.buttonOneTitle,this.buttonTwoTitle,);
  @override
  State<StatefulWidget> createState() => EmailAlreadyExistAlertState(this.title,this.subTitle,this.buttonOneTitle,this.buttonTwoTitle);
}

class EmailAlreadyExistAlertState extends State<EmailAlreadyExistAlert>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  String title,subTitle,buttonOneTitle,buttonTwoTitle;
  EmailAlreadyExistAlertState(this.title,this.subTitle,this.buttonOneTitle,this.buttonTwoTitle);
  @override
  void initState() {
    super.initState();

    // controller =
    //     AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    // scaleAnimation =
    //     CurvedAnimation(parent: controller, curve: Curves.elasticInOut);
    //
    // controller.addListener(() {
    //   setState(() {});
    // });
    //
    // controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child:  Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: 200.0,

              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10.0, left: 20.0, right: 20.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[

                                Text(
                                  widget.title,

                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightExtraBold,
                                      fontSize: 17
                                  ),

                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN/4,
                                ),
                              Container(
                                  height: 55,
                               child:Text(
                                  widget.subTitle!= null ?widget.subTitle:"",
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,

                                      fontWeight: AppFont.fontWeightRegular,
                                      fontSize:15
                                  ),
                                )),
                              ])


                      )),
                Container(
                  height: 55,
                  child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN*4,
                            height: 45,
                            child:   RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                                //Navigator.pushNamed(context, LoginScreen.routeName);
                              },
                              color: colorDarkRed,
                              textColor: colorWhite,
                              elevation: 5,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonOneTitle,
                                textAlign: TextAlign.center,
                                style: getStyleButtonText(context).copyWith(
                                    color: colorWhite,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                      SizedBox(
                        width: 10,
                        height: 45,
                      ),
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN*4,
                            height: 45,
                            child:   RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                              },
                              color: colorWhite,
                              textColor: colorDarkRed,
                              elevation: 1,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: colorDarkRed, width: 2),
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonTwoTitle,
                                textAlign: TextAlign.center,
                                style: getStyleButtonText(context).copyWith(
                                    color: colorDarkRed,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              )),
      ),
    );
  }
}
