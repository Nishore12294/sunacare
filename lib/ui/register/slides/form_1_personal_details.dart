import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/register/signup/response_signup.dart';
import 'package:suna_care/core/notifier/signup_notifier.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_custom_icon.dart';
import 'package:suna_care/utils/app_date_picker.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:provider/provider.dart';

class Form1PersonalDetailsScreen extends StatefulWidget {
  BuildContext context;

  Form1PersonalDetailsScreen({this.context});

  @override
  Form1PersonalDetailsScreenState createState() =>
      Form1PersonalDetailsScreenState();
}

class Form1PersonalDetailsScreenState
    extends State<Form1PersonalDetailsScreen> {
  var log = getLogger("FormPersonalDetailScreenState");
  final _keyValidationForm = GlobalKey<FormState>();
  //String genderDropDownValue;

  @override
  Widget build(BuildContext context) {
    return Consumer<SignUpNotifier>(
        builder: (context, signUpNotifier, _) => Theme(
            data: ThemeData(
                hintColor: colorDividerGrey,
                primaryColor: colorPrimary,
                primaryColorDark: colorPrimary),
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                ModalProgressHUD(
                  inAsyncCall: signUpNotifier.isLoading,
                  child: GestureDetector(
                      onTap: () {
                        FocusScope.of(context).requestFocus(new FocusNode());
                      },
                      child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: _buildWidgetPersonalDetailCard(
                              signUpNotifier, context))),
                ),
              ],
            )));
  }

  /////////////////////
  /*  build helpers */
  ////////////////////

  Widget _buildWidgetPersonalDetailCard(
      SignUpNotifier signUpNotifier, BuildContext context) {
    List<String> genderList = [
      'Male',
      'Female',
    ];

    return Column(
      children: <Widget>[
        Container(
            child: Padding(
          padding: const EdgeInsets.only(
              top: 16.0, left: 30.0, right: 30.0, bottom: 8.0),
          child: Theme(
            data: ThemeData(
                hintColor: colorBlack,
                primaryColor: colorDarkRed,
                primaryColorDark: colorLiteOrange),
            child:GestureDetector(
              onTap: (){
                FocusScope.of(context).requestFocus(FocusNode());

              },
              child: Container(
                alignment: Alignment.center,
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Form(
                    key: _keyValidationForm,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,
                        ),
                        TextFormField(
                            controller: signUpNotifier.textEditFirstName,
                            focusNode: signUpNotifier.firstNameFocus,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.sentences,
                            textInputAction: TextInputAction.next,
                            validator: _validateNameField,
                            style: getFormStyleText(context),
                            onFieldSubmitted: (String value) {

                                FocusScope.of(context)
                                    .requestFocus(signUpNotifier.lastNameFocus);

                            },
                            decoration:
                            _buildTextDecoration(
                                //AppConstants.FIRST_NAME,
                                 AppLocalizations.of(context).translate('firstName'),
                            )
                                .copyWith(
                              fillColor: colorWhite,
                              prefixIcon: Image.asset(
                                AppImages.IMAGE_NAME,
                                width: 5,
                                height: 5,
                              ),
                              focusedBorder: new OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: colorWhite,
                                ),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                                ),
                              ),
                              enabledBorder: new OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: colorWhite,
                                  //color: Colors.transparent,
                                ),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                                ),
                              ),
                            )),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,
                        ),
                        TextFormField(
                          controller: signUpNotifier.textEditLastName,
                          focusNode: signUpNotifier.lastNameFocus,
                          obscureText: false,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.sentences,
                          validator: _validateNameField,
                          textInputAction: TextInputAction.next,
                          style: getFormStyleText(context),
                          onFieldSubmitted: (String value) {
                            FocusScope.of(context)
                                .requestFocus(signUpNotifier.emailIdFocus);
                          },
                          decoration: _buildTextDecoration(
                            //AppConstants.LAST_NAME,
                            AppLocalizations.of(context).translate('lastName'),
                          )
                              .copyWith(
                            fillColor:colorWhite,
                            prefixIcon: Image.asset(
                              AppImages.IMAGE_NAME,
                              width: 5,
                              height: 5,
                            ),
                            focusedBorder: new OutlineInputBorder(
                              borderSide: BorderSide(
                                color: colorWhite,
                              ),
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                              ),
                            ),
                            enabledBorder: new OutlineInputBorder(
                              borderSide: BorderSide(
                                color: colorWhite,
                              ),
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,
                        ),
                        TextFormField(
                            controller: signUpNotifier.textEditEmailId,
                            focusNode: signUpNotifier.emailIdFocus,
                            keyboardType: TextInputType.emailAddress,
                            //textCapitalization: TextCapitalization.sentences,
                            textInputAction: TextInputAction.next,
                            validator: _validateNameField,
                            style: getFormStyleText(context),
                            onFieldSubmitted: (String value) {
                              FocusScope.of(context)
                                  .requestFocus(signUpNotifier.phoneNoFocus);
                            },
                            decoration:
                            _buildTextDecoration(
                             // AppConstants.EMAIL_ADDRESS,
                              AppLocalizations.of(context).translate('emailAddress'),
                            )
                                .copyWith(
                              fillColor: colorWhite,
                              prefixIcon: Image.asset(
                                AppImages.IMAGE_EMAIL,
                                // width: getScreenWidth(context) / 4,
                                width: 5,
                                height: 5,
                              ),
                              focusedBorder: new OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: colorWhite,
                                  //color: Colors.transparent,
                                ),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                                ),
                              ),
                              enabledBorder: new OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: colorWhite,
                                  //color: Colors.transparent,
                                ),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                                ),
                              ),
                            )),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,
                        ),
                        TextFormField(
                          //key: _emailKey,
                            inputFormatters:[FilteringTextInputFormatter.allow(RegExp('[0-9]'))],
                            controller: signUpNotifier.textEditPhoneNo,
                            focusNode: signUpNotifier.phoneNoFocus,
                            keyboardType: TextInputType.phone,
                            textCapitalization: TextCapitalization.sentences,
                            textInputAction: TextInputAction.done,
                            validator: _validateNameField,
                            style: getFormStyleText(context),
                            onFieldSubmitted: (String value) {
                              //FocusScope.of(context).requestFocus(signUpNotifier.genderFocus);
                              FocusScope.of(context).requestFocus(FocusNode());

                            },
                            decoration:
                            _buildTextDecoration(
                                //AppConstants.MOBILE_NUMBER,
                                AppLocalizations.of(context).translate('mobileNumber'),
                            )
                                .copyWith(
                              fillColor: colorWhite,
                              prefixIcon: Image.asset(
                                AppImages.IMAGE_MOBILE,
                                // width: getScreenWidth(context) / 4,
                                width: 5,
                                height: 5,
                              ),
                              focusedBorder: new OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: colorWhite,
                                  //color: Colors.transparent,
                                ),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                                ),
                              ),
                              enabledBorder: new OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: colorWhite,
                                  //color: Colors.transparent,
                                ),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                                ),
                              ),
                            )),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,
                        ),
//                      TextFormField(
//                          //key: _emailKey,
//                          controller: signUpNotifier.textEditGender,
//                          focusNode: signUpNotifier.genderFocus,
//                          keyboardType: TextInputType.text,
//                          textCapitalization: TextCapitalization.sentences,
//                          textInputAction: TextInputAction.next,
//                          validator: _validateNameField,
//                          style: getFormStyleText(context),
//                          onFieldSubmitted: (String value) {
//
//                          },
//                          decoration: _buildTextDecoration(AppConstants.GENDER)
//                              .copyWith(
//                            fillColor: colorAccentMild,
//                            prefixIcon: Image.asset(
//                              AppImages.IMAGE_GENDER,
//                              width: 5,
//                              height: 5,
//                            ),
//                            focusedBorder: new OutlineInputBorder(
//                              borderSide: BorderSide(
//                                color: colorLiteWhiteBorder,
//                              ),
//                              borderRadius: const BorderRadius.all(
//                                const Radius.circular(AppConstants.SIDE_MARGIN),
//                              ),
//                            ),
//                            enabledBorder: new OutlineInputBorder(
//                              borderSide: BorderSide(
//                                color: colorLiteWhiteBorder,
//                              ),
//                              borderRadius: const BorderRadius.all(
//                                const Radius.circular(AppConstants.SIDE_MARGIN),
//                              ),
//                            ),
//                          )),


                      GestureDetector(
                        onTap: (){
                          FocusScope.of(context).requestFocus(FocusNode());

                        },
                        child:  SizedBox(
                          height: AppConstants.SIDE_MARGIN*2.4,
                          child: DropdownButtonFormField(
                            style: getFormStyleText(context),
                            decoration:
                            _buildTextDecoration(
                               // AppConstants.GENDER,
                                AppLocalizations.of(context).translate('gender'),
                            )
                                .copyWith(
                              isDense: true,
                              hintStyle: TextStyle(color: colorDarkGrey),
                              fillColor: colorWhite,

                              prefixIcon: Image.asset(
                                AppImages.IMAGE_GENDER,
                                // width: getScreenWidth(context) / 4,
                                width: 5,
                                height: 5,
                              ),
                              focusedBorder: new OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: colorWhite,
                                  //color: Colors.transparent,
                                ),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                                ),
                              ),
                              enabledBorder: new OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: colorWhite,
                                  //color: Colors.transparent,
                                ),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(AppConstants.SIDE_MARGIN* 1.5),
                                ),
                              ),
                            ),
                            value: signUpNotifier.genderStr,
                            onChanged: (String Value) {
                              FocusScope.of(context).requestFocus(new FocusNode());
                              setState(() {
                                signUpNotifier.genderStr = Value;

                              });
                            },
                            items: genderList
                                .map((genderItem) => DropdownMenuItem(
                                value: genderItem, child: Text("$genderItem")))
                                .toList(),
                          ),
                        ),

                      ),

                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,
                        ),
                        ButtonTheme(
                          minWidth: double.infinity,
                          child: RaisedButton(
                            highlightElevation: 5.0,
                            onPressed: () {
                              // FocusScope.of(context).requestFocus(FocusNode());
                              //  if (_keyValidationForm.currentState.validate()) {
                              //_onClickButtonLogIn(signUpNotifier);
                              // }

                              if (signUpNotifier.textEditFirstName.text.trim()
                                  .toString() ==
                                  "" &&
                                  signUpNotifier.textEditLastName.text.trim()
                                      .toString() ==
                                      "" &&
                                  signUpNotifier.textEditEmailId.text.trim()
                                      .toString() ==
                                      "" &&
                                  signUpNotifier.textEditPhoneNo.text.trim()
                                      .toString() ==
                                      "" &&
                                  ( signUpNotifier.genderStr == null ||  signUpNotifier.genderStr ==
                                      "")) {
                                showDialog(
                                  context: context,
                                  builder: (_) => AlertOverlay(
                                      AppLocalizations.of(context).translate('signUp'),
                                      AppLocalizations.of(context).translate('pleaseEnterYourFirstName'),
                                      //AppConstants.ENTER_FIRSTNAME,
                                      AppLocalizations.of(context).translate('okay')),
                                );
                              } else if (signUpNotifier.textEditFirstName.text.trim()
                                  .toString() ==
                                  "") {
                                showDialog(
                                  context: context,
                                  builder: (_) => AlertOverlay(
                                      AppLocalizations.of(context).translate('signUp'),
                                      AppLocalizations.of(context).translate('pleaseEnterYourFirstName'),
                                      //AppConstants.ENTER_FIRSTNAME,
                                      AppLocalizations.of(context).translate('okay')),
                                );
                              } else if (signUpNotifier.textEditLastName.text.trim()
                                  .toString() ==
                                  "") {
                                showDialog(
                                  context: context,
                                  builder: (_) => AlertOverlay(
                                      AppLocalizations.of(context).translate('signUp'),
                                      AppLocalizations.of(context).translate('pleaseEnterYourLastName'),
                                      //AppConstants.ENTER_LASTNAME,
                                      AppLocalizations.of(context).translate('okay')),
                                );
                              } else if (signUpNotifier.textEditEmailId.text.trim()
                                  .toString() ==
                                  "") {
                                showDialog(
                                  context: context,
                                  builder: (_) => AlertOverlay(
                                      AppLocalizations.of(context).translate('signUp'),
                                      AppLocalizations.of(context).translate('pleaseEnterYourEmailAddress'),
                                      //AppConstants.ENTER_EMAILID,
                                      AppLocalizations.of(context).translate('okay')),
                                );
                              } else if (signUpNotifier.textEditPhoneNo.text.trim()
                                  .toString() ==
                                  "") {
                                showDialog(
                                  context: context,
                                  builder: (_) => AlertOverlay(
                                      AppLocalizations.of(context).translate('signUp'),
                                      AppLocalizations.of(context).translate('pleaseEnterYourMobileNumber'),
                                      //AppConstants.ENTER_MOBILE,
                                      AppLocalizations.of(context).translate('okay')),
                                );
                              }
                            else if ( signUpNotifier.genderStr == null ||  signUpNotifier.genderStr == "") {
                              showDialog(
                                context: context,
                                builder: (_) => AlertOverlay(
                                    AppLocalizations.of(context).translate('signUp'),
                                    AppLocalizations.of(context).translate('pleaseSelectYourGender'),
                                   // AppConstants.PLEASE_SELECT_GENDER,
                                    AppLocalizations.of(context).translate('okay')),
                              );
                            }
                              else {
                                bool emailValid = RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(signUpNotifier.textEditEmailId.text.trim()
                                    .toString());
                                if (!emailValid) {
                                  showDialog(
                                    context: context,
                                    builder: (_) => AlertOverlay(
                                        AppLocalizations.of(context).translate('signUp'),
                                        AppLocalizations.of(context).translate('pleaseEnterValidEmailAddress'),
                                        //AppConstants.ENTER_VALID_EMAIL,
                                        AppLocalizations.of(context).translate('okay')),
                                  );
                                } else if (signUpNotifier
                                    .textEditPhoneNo.text.trim().length <
                                    6 ||
                                    signUpNotifier.textEditPhoneNo.text.trim().length >
                                        14) {
                                  showDialog(
                                    context: context,
                                    builder: (_) => AlertOverlay(
                                        AppLocalizations.of(context).translate('signUp'),
                                        AppLocalizations.of(context).translate('pleaseEnterValidMobileNumber'),
                                        //AppConstants.ENTER_VALID_PHONENO,
                                        AppLocalizations.of(context).translate('okay')),
                                  );
                                } else {
                                  FocusScope.of(context).requestFocus(FocusNode());
                                  signUpNotifier.registerFormsPageViewController
                                      .nextPage(
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.ease);
                                }
                              }
                            },
                            color: colorGreen,
                            textColor: colorWhite,
                            elevation: 1,
                            padding: EdgeInsets.only(
                                top: AppConstants.SIDE_MARGIN / 1.5,
                                bottom: AppConstants.SIDE_MARGIN / 1.5),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(AppConstants.SIDE_MARGIN*1.5))),
                            child: Text(
                              //AppConstants.NEXT,
                              AppLocalizations.of(context).translate('next'),
                              style: getStyleButtonText(context).copyWith(
                                  color: colorWhite,
                                  letterSpacing: 1,
                                  fontWeight: AppFont.fontWeightBold),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                //AppConstants.ALREADY_HAVE_AN_ACCOUNT,
                                AppLocalizations.of(context).translate('alreadyHaveAnAccount'),
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightSemiBold),
                                textAlign: TextAlign.end,
                              ),
                              GestureDetector(
                                onTap: () => redirectToLogin(),
                                child: Text(
                                  //" " + AppConstants.LOGIN_HERE+".",
                                  AppLocalizations.of(context).translate('loginHere'),
                                  style: getStyleBody1(context).copyWith(
                                      color: colorLiteOrange,
                                      fontWeight: AppFont.fontWeightSemiBold),
                                  textAlign: TextAlign.end,
                                ),
                              )
                            ]),
                      ],
                    ),
                  ),
                ),
              ),
            ),

          ),
        )),
      ],
    );
  }

  void redirectToLogin() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  InputDecoration _buildTextDecoration(String value) {
    return InputDecoration(
        isDense: true,
        hintText: value,
        // labelText: value,
        //alignLabelWithHint: true,
        filled: true,
        hintStyle: TextStyle(color: colorDarkGrey),
        contentPadding: EdgeInsets.all(16));
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  void saveUserCredential(String emailId, String password) async {
    AppSharedPreference()
        .saveStringValue(AppConstants.KEY_USER_EMAIL_ID, emailId);
    AppSharedPreference()
        .saveStringValue(AppConstants.KEY_USER_PASSWORD, password);
  }

  String _validateNameField(String value) {
    return value.trim().length < 1 ? 'Field can\'t be empty' : null;
  }

  String _validatePassword(String value) {
    if (value.trim().length < 1) {
      return 'Field can\'t be empty';
    } else if (value.trim().length < 8 || value.trim().length > 15) {
      return 'Please use 8 to 15 characters password';
    } else {
      return null;
    }
  }
}

