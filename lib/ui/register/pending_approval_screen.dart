import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/createpassword/response_create_password.dart';
import 'package:suna_care/core/notifier/create_new_password_notifier.dart';
import 'package:suna_care/core/notifier/pending_approval_notifier.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_custom_icon.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/app_log_helper.dart';

import '../login_screen.dart';



class PendingApprovalScreen extends StatefulWidget {
  static const routeName = '/pending_approval_screen';

  @override
  _PendingApprovalScreenState createState() => _PendingApprovalScreenState();
}

class _PendingApprovalScreenState extends State<PendingApprovalScreen> {
  final log = getLogger('PendingApprovalScreen');
  final _keyValidationForm = GlobalKey<FormState>(debugLabel: '_pendingApprovalScreenkey');
  bool isNewPasswordVisible = false;
  bool isConfirmPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String emailId = "",verificationCode = "";
  @override
  void initState() {
    super.initState();
    setInitialData(); //aa
  }

  setInitialData() async {

  }

  @override
  Widget build(BuildContext context) {
    final bgColor = Colors.white;
    var width = MediaQuery.of(context).size.width;
    return ChangeNotifierProvider<PendingApprovalNotifier>(
      builder: (context) => PendingApprovalNotifier(),
      child: Scaffold(
          backgroundColor: colorLitePinkBG,
          key: _scaffoldKey,
          body: Consumer<PendingApprovalNotifier>(
            builder: (context, pendinApprovalNotifier, _) => Stack(
              fit: StackFit.expand,
              children: <Widget>[
                ModalProgressHUD(
                  inAsyncCall: pendinApprovalNotifier.isLoading,
                  child: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    },
                    child: Container(
                      color: colorLitePinkBG,
                        alignment: Alignment.topCenter,
                        child:  Column(
                            //mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              _buildWidgetImageLogo(),


                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/2,
                              ),
                              Text(
                                //AppConstants.WE_ARE_EVALUATIING,
                                AppLocalizations.of(context).translate('weEvaluatingYourProfile'),
                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 24
                                ),
                              ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/1.3,
                              ),
                              Text(
                                //AppConstants.WE_WILL_EMAIL_OR_MESSAGE,
                                AppLocalizations.of(context).translate('weWillEmailOrMessage'),
                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightRegular,
                                    fontSize: 15
                                ),
                              ),
                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/4,
                              ),

                              buildWidgetPendingPasswordCard(pendinApprovalNotifier),

                              new Expanded(

                                flex: 5,
                                child: new Container(

                                  width: width,
                                  color: colorLitePinkBG,//
                                  child: DecoratedBox(

                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(AppImages.IMAGE_PENDING_APPROVAL),
                                          fit: BoxFit.fill),
                                    ),),
                                ),

                              ),
                            ],
                          ),
                        )),
                  )
              ],
            ),
          )

          //_buildSignUpLabel(),
          ),
    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////
  Widget _buildWidgetImageLogo() {
    return Container(
      height: getScreenHeight(context) / 6,
      //padding: EdgeInsets.only(top: 64, bottom: 32),
      alignment: Alignment.center,
      child: topHeader()
    );
  }

  Widget buildWidgetPendingPasswordCard(PendingApprovalNotifier pendingApprovalNotifier) {
    

    return Column(
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.only(
                top: 20.0, left: 30.0, right: 30.0, bottom: 8.0),
            child: Theme(
              data: ThemeData(
                  hintColor: colorBlack,
                  primaryColor: colorDarkRed,
                  primaryColorDark: colorLiteOrange),
              child: Form(
                key: _keyValidationForm,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    ButtonTheme(
                      //minWidth: double.infinity,
                      minWidth: 100,

                      child: RaisedButton(

                        highlightElevation: 8.0,

                        onPressed: () {
                          if(Navigator.canPop(context)) {
                            Navigator.pop(context);
                          }

                        },
                        color: colorLiteOrange,
                        textColor: colorWhite,
                        elevation: 5.0,
                        padding: EdgeInsets.only(
                            top: AppConstants.SIDE_MARGIN / 1.5,
                            bottom: AppConstants.SIDE_MARGIN / 1.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                        ),
                        child: Text(
                          //AppConstants.OKAY,
                          AppLocalizations.of(context).translate('okay'),
                          style: getStyleButtonText(context).copyWith(
                              color: colorWhite,
                              letterSpacing: 1,
                              fontWeight: AppFont.fontWeightBold),
                        ),
                      ),


                    ),

                    SizedBox(
                      height: AppConstants.SIDE_MARGIN/5,
                    ),



                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
  InputDecoration _buildTextDecoration(String email) {
    return InputDecoration(
        isDense: true,
        hintText: email,
        alignLabelWithHint: true,
        filled: true,
        contentPadding: EdgeInsets.all(16));
  }

  OutlineInputBorder _buildOutlineInputBorder(){
    return  OutlineInputBorder(
      borderSide: BorderSide(
        color: colorLiteWhiteBorder,
        //color: Colors.transparent,
      ),
      borderRadius: const BorderRadius.all(

        const Radius.circular(AppConstants.SIDE_MARGIN),
      ),
    );
  }

  Widget topHeader(){
    return  new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 1,
      child:  InkWell(
        onTap: () {
          Navigator.canPop(context) ? Navigator.pop(context) : '';
        },
        child: Container(

        ),
      ),


        ),
        Expanded(
          flex: 3,
          child:Text(
            //AppConstants.PENDING_APPROVAL,
            AppLocalizations.of(context).translate('pendingApproval'),
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightSemiBold,
                fontSize: 23
            ),

          ),
        ),

        Expanded(
          flex: 1,
          child: Container(

            child:Image.asset(
              AppImages.IMAGE_APP_LOGO,
              // width: getScreenWidth(context) / 4,
              height: 20,
            ),),

        ),
      ],
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////

  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }


}
