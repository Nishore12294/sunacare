import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:suna_care/core/notifier/signup_notifier.dart';
import 'package:suna_care/ui/register/slides/form_2_additional_details.dart';
import 'package:suna_care/ui/register/slides/form_1_personal_details.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';

import '../login_screen.dart';
import 'dotted_line.dart';

class SignUpScreen extends StatefulWidget {
  static const routeName = '/signup_screen';

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final log = getLogger('SignupScreen');

  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _emailFocus.addListener(_onFocusChangeEmail);
    _passwordFocus.addListener(_onFocusChangePassword);
    setInitialData();
  }

  setInitialData() async {}

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SignUpNotifier>(
      builder: (context) => SignUpNotifier(),
      child: WillPopScope(child: DecoratedBox(
        decoration: BoxDecoration(
         image: DecorationImage(
             image: AssetImage(
               AppImages.IMAGE_SIGNUP_BG_WITH_CIRCLE,
             ),
             fit: BoxFit.fill),

        ),
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.transparent,
          key: _scaffoldKey,
          body: Consumer<SignUpNotifier>(
            builder: (context, signUpNotifier, _) =>
                Stack(fit: StackFit.expand, children: <Widget>[

                  ModalProgressHUD(
                    color: Colors.transparent,
                    inAsyncCall: signUpNotifier.isLoading,
                    child: GestureDetector(
                      onTap: () {
                        FocusScope.of(context).requestFocus(new FocusNode());
                      },
                      child: Container(
                          child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN*1.5,
                                ),
                              topHeader(signUpNotifier),

                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN*1.8,
                                ),
                                 Row(
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        firstCircle(signUpNotifier),
                                        SizedBox(
                                          child: DottedLine(),
                                          width: AppConstants.SIDE_MARGIN*1.5,
                                        ),
                                        secondCircle(signUpNotifier),
                                      ]),


                                Expanded(
                                    flex: 5 ,
                                  // Slides Container
                                    child: PageView(
                                      controller:
                                      signUpNotifier.registerFormsPageViewController,
                                      physics: NeverScrollableScrollPhysics(),
                                      children: <Widget>[
                                        _buildRegisterForm(context),
                                        _buildAdditionalDetailsScreen(context),
                                      ],
                                      onPageChanged: (currentPage) {
                                        log.d("Current Page Number : " +
                                            currentPage.toString());
                                        signUpNotifier.currentPageIndex = currentPage;
                                        if (currentPage == 0) {
                                          signUpNotifier.formTitle =
                                              AppConstants.PERSONAL_INFORMATION;
                                        } else if (currentPage == 1) {
                                          signUpNotifier.formTitle = AppConstants.ADDITIONAL_INFORMATION;
                                        }  else {}


                                      },
                                    )),

                              ]

                          )
                      ),
                    ),
                  ),
                ]),
          ),
        ),
      ), onWillPop: () async => false),
    );
  }

  Widget firstCircle(SignUpNotifier signUpNotifier) {

    return  new Container(
      width: 32,
      height: 32,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: (signUpNotifier.currentPageIndex == 0) ? colorGreen:colorGray,
          width: 2,
        ),
        color: (signUpNotifier.currentPageIndex == 0) ? colorGreen:colorGray,
      ),
      child:Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
              '1',textAlign: TextAlign.center,
             style: getStyleBody1(context).copyWith(
            color: (signUpNotifier.currentPageIndex == 0) ?colorWhite:colorWhite,
            fontWeight: AppFont.fontWeightBold,
            fontSize: 13,
             ),

          ),

        ],
      ),
    );
  }
  Widget secondCircle(SignUpNotifier signUpNotifier) {

    return  new Container(
      width: 32,
      height: 32,
      decoration: BoxDecoration(
        border: Border.all(
          color: colorGreen,
          width: 2,
        ),
        shape: BoxShape.circle,
        color: (signUpNotifier.currentPageIndex == 1) ? colorGreen:colorLitePinkBG,
      ),
      child:Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('2',textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: (signUpNotifier.currentPageIndex == 1) ?colorWhite:colorGreen,
                  fontWeight: AppFont.fontWeightBold,
                  fontSize: 13)),

        ],
      ),
    );
  }

  void backButton(SignUpNotifier signUpNotifier){
    signUpNotifier
        .removeUserCred()
        .whenComplete(() {
      _controlPageSwipe(signUpNotifier);
    });
  }
  Widget topHeader(SignUpNotifier signUpNotifier){
    return  new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      //mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Expanded(
          flex: 1,
          child:  InkWell(
            onTap: () {
              backButton(signUpNotifier);
            },
            child:   Container(
              margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
              color: Colors.transparent,
              alignment: Alignment.centerLeft,
              child:Image.asset(
                AppImages.IMAGE_BACK,
                height: 25,
              ),),
          ),


        ),
        Expanded(
          flex: 1,
          child:Text(
            //AppConstants.SIGN_UP,
            AppLocalizations.of(context).translate('signUp'),
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                 fontFamily:'NPSunaGrotesk',
            ),

          ),
        ),

        Expanded(

          flex: 1,
          child: Container(
            margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
            color: Colors.transparent,
            alignment: Alignment.centerRight,
            child:Image.asset(
              AppImages.IMAGE_APP_LOGO,
              height: 20,
            ),),

        ),
      ],
    );
  }


  Widget _buildAdditionalDetailsScreen(BuildContext context) {
    return Form2AdditionalDetailsScreen(context: context);
  }

  Widget _buildRegisterForm(BuildContext context) {
    return Form1PersonalDetailsScreen(context: context);
  }

  void _onFocusChangeEmail() {
    log.d('Email ' + _emailFocus.hasFocus.toString());
    setState(() {});
  }

  void _onFocusChangePassword() {
    log.d('Password ' + _passwordFocus.hasFocus.toString());
    setState(() {});
  }

  void _controlPageSwipe(SignUpNotifier signUpNotifier) {
    if (signUpNotifier.currentPageIndex == 0) {
      // Email ID Entering page
//      _showAlarmPopUp(
//          signUpNotifier: signUpNotifier,
//          title: 'Warning',
//          subTitle: 'Are you sure want to cancel the signup',
//          pageIndex: 0);

      showDialog(
        context: context,
        builder: (_) =>
            ExitAlert(
                //AppConstants.SIGNUP_ALERT,
               //AppConstants.ARE_YOU_SURE_CANCEL_SIGNUP,
                AppLocalizations.of(context).translate('signUp'),
                AppLocalizations.of(context).translate('wantToCancelSignupProcess'),
                AppLocalizations.of(context).translate('okay'),AppLocalizations.of(context).translate('cancel'),),
      );
    } else if (signUpNotifier.currentPageIndex == 1) {
      // OTP Entering page
      signUpNotifier.registerFormsPageViewController.animateToPage(0,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    } else if (signUpNotifier.currentPageIndex == 2) {
      // register Form basic page
      _showAlarmPopUp(
          signUpNotifier: signUpNotifier,
          title: 'Change Mail ID',
          subTitle: 'Do you want proceed with Different Mail ID',
          pageIndex: 2);
    } else if (signUpNotifier.currentPageIndex == 3) {
      // ic upload Form
      signUpNotifier.registerFormsPageViewController.animateToPage(2,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    }
  }

  void _showAlarmPopUp(
      {SignUpNotifier signUpNotifier,
      String title,
      String subTitle,
      int pageIndex}) {
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(subTitle),
        insetAnimationDuration: Duration(seconds: 2),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () {
              Navigator.canPop(context) ? Navigator.pop(context) : '';
            },
            child: Text(
               // AppConstants.NO
                AppLocalizations.of(context).translate('no')
            ),
          ),
          CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              Navigator.canPop(context) ? Navigator.pop(context) : '';
              if (pageIndex == 2) {
                signUpNotifier.registerFormsPageViewController.animateToPage(0,
                    duration: Duration(milliseconds: 500), curve: Curves.ease);
              } else if (pageIndex == 0) {
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
              }
            },
            child: Text(
               // AppConstants.YES
                AppLocalizations.of(context).translate('yes')
            ),
          ),
        ],
      ),
    );
  }
}

class ExitAlert extends StatefulWidget {
  String title,subTitle,buttonOneTitle,buttonTwoTitle;
  ExitAlert(this.title,this.subTitle,this.buttonOneTitle,this.buttonTwoTitle,);
  @override
  State<StatefulWidget> createState() => ExitAlertState(this.title,this.subTitle,this.buttonOneTitle,this.buttonTwoTitle);
}

class ExitAlertState extends State<ExitAlert>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  String title,subTitle,buttonOneTitle,buttonTwoTitle;
  ExitAlertState(this.title,this.subTitle,this.buttonOneTitle,this.buttonTwoTitle);
  @override
  void initState() {
    super.initState();

    // controller =
    //     AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    // scaleAnimation =
    //     CurvedAnimation(parent: controller, curve: Curves.elasticInOut);
    //
    // controller.addListener(() {
    //   setState(() {});
    // });
    //
    // controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child:  Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: 180.0,

              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10.0, left: 20.0, right: 20.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[

                                Text(
                                  widget.title,
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightExtraBold,
                                      fontSize: 17
                                  ),

                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN/4,
                                ),
                                Text(
                                  widget.subTitle!= null ?widget.subTitle:"",
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      fontWeight: AppFont.fontWeightRegular,
                                      //fontSize: (widget.subTitle!= null && widget.subTitle.length > 10)?15: 17
                                      fontSize: (widget.subTitle!= null && widget.subTitle.length > 10)?15: 17,


                                  ),
                                ),
                              ])


                      )),

                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN*4,
                            height: 45,
                            child:   RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                                //Navigator.pushNamed(context, LoginScreen.routeName);
                              },
                              color: colorDarkRed,
                              textColor: colorWhite,
                              elevation: 5,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonOneTitle,
                                style: getStyleButtonText(context).copyWith(
                                    color: colorWhite,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                            height: 45,
                          ),
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN*4,
                            height: 45,
                            child:   RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context) ? Navigator.pop(context) : '';
                              },
                              color: colorDarkRed,
                              textColor: colorDarkRed,
                              elevation: 1,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: colorDarkRed, width: 2),
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonTwoTitle,
                                style: getStyleButtonText(context).copyWith(
                                    color: Colors.white,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              )),
      ),
    );
  }
}


