import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangResponse.dart';
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/leave_us_msg/GetCGLeaveMsgResponse.dart';
import 'package:suna_care/core/notifier/call_request_notifier.dart';
import 'package:suna_care/core/notifier/leave_us_msg_notifier.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/core/notifier/profile_notifier.dart';
import 'package:suna_care/core/notifier/schedule_appointment_notifier.dart';
import 'package:suna_care/ui/changepassword/change_password_screen.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_meeting_calendar_screen.dart';
import 'package:suna_care/utils/AppLanguage.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/alert_overlay_with_refersh.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_globals.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';

//import 'package:ishare/screens/custom_expansion_tile.dart' as custom;
import 'package:suna_care/ui/offers/custom_expansion_tile.dart' as custom;
import 'package:suna_care/utils/widget_helper/custom_switch.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

class LeaveUsMessageScreen extends StatefulWidget {
  static const routeName = '/leave_us_msg_screen';
  String id = "";

  LeaveUsMessageScreen({this.id});

  @override
  _LeaveUsMessageScreenState createState() => _LeaveUsMessageScreenState();
}

class _LeaveUsMessageScreenState extends State<LeaveUsMessageScreen> {
  final log = getLogger('LeaveUsMessageScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  String offerGuid;
  LeaveUsMessageNotifier leaveUsMessageNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');

  _LeaveUsMessageScreenState();

  @override
  void initState() {
    super.initState();
    setInitialData();
  }

  setInitialData() async {
    if (widget != null) {
      offerGuid = widget.id;
      log.d('id: ==>' + widget.id.toString());
      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>" + accessToken);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LeaveUsMessageNotifier>(
      builder: (context) => LeaveUsMessageNotifier(context,
          isFromWhichScreen: "", offerGuid: offerGuid),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: colorLitePinkBG,
          image: DecorationImage(
              image: AssetImage(
                AppImages.IMAGE_LOGIN_BG_WITH_CIRCLE,
              ),
              fit: BoxFit.fill),
        ),
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          backgroundColor: colorTransparent,
          key: _scaffoldKey,
          body: Consumer<LeaveUsMessageNotifier>(
            builder: (context, leaveUsMessageNotifier, _) =>
                ModalProgressHUD(
              inAsyncCall: leaveUsMessageNotifier.isLoading,
              child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Container(
                    color: colorTransparent,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            color: colorTransparent,
                            child: Container(
                              color: colorTransparent,
                              child: Column(children: <Widget>[
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN * 2,
                                  child: Container(
                                    color: colorLitePinkBG,
                                  ),
                                ),
                                topHeader(leaveUsMessageNotifier),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN,
                                  child: Container(
                                    color: colorLitePinkBG,
                                  ),
                                ),
                                _buildWidgetMainContent(
                                    context, leaveUsMessageNotifier),
                              ]),
                            )),
                        SizedBox(
                          height: AppConstants.SIDE_MARGIN,
                        ),
                      ],
                    ),
                  )),
            ),
          ),
        ),
      ),
    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////

  Widget topHeader(LeaveUsMessageNotifier leaveUsMessageNotifier) {
    return Container(
      color: colorLitePinkBG,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: InkWell(
              onTap: () {
                Navigator.canPop(context) ? Navigator.pop(context) : '';
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Image.asset(
                  AppImages.IMAGE_BACK,
                  height: 25,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Text(
              //AppLocalizations.of(context).translate('leaveUsMessage'),
              AppLocalizations.of(context).translate('leaveUsMessage'),
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 20.5,
                fontFamily: 'NPSunaGrotesk',
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                height: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  Widget _buildWidgetMainContent(
      BuildContext context, LeaveUsMessageNotifier leaveUsMessageNotif) {
    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);
    leaveUsMessageNotifier = leaveUsMessageNotif;
    //leaveUsMessageNotifier.isLeaveUsMsgSelected = isLeaveUsMsgSelected;

    return buildWidgetViewProfile(context, imageWidth, cardRadio);
  }

  Widget buildWidgetViewProfile(
      BuildContext context, double imageWidth, Radius cardRadio) {
    List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];

    return GestureDetector(
      onTap: () {
        log.d('onPress:  ');
      },
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
            child: Padding(
          padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
          child: Container(
            margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: AppConstants.SIDE_MARGIN * 1.5,
                  ),
                  Text(
                    //AppConstants.PLEASE_SHARE_ANYTHING,
                    AppLocalizations.of(context).translate('pleaseShareAnything'),
                    textAlign: TextAlign.center,
                    style: getStyleBody1(context).copyWith(
                        color: colorPencilBlack,
                        fontWeight: AppFont.fontWeightRegular,
                        fontSize: 15),
                  ),
                  SizedBox(
                    height: AppConstants.SIDE_MARGIN,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: colorWhite,
                        border: Border.all(
                          color: colorWhite,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(12))
                    ),
                    //color: colorWhite,
                    margin: EdgeInsets.all(8.0),
                    height: getScreenHeight(context) / 3,
                    padding: EdgeInsets.only(bottom: 40.0),
                    child:Padding(

                      padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                      child:TextField(
                        maxLines: 50,
                        controller: leaveUsMessageNotifier.textEditMessage,
                        decoration: InputDecoration(
                         // hintText: AppConstants.ENTER_MESSAGE,
                          hintText:      AppLocalizations.of(context).translate('enterMessage'),
                          // border: OutlineInputBorder(),
                          border: InputBorder.none,
                        ),
                      ),
                    )


                  ),
                  SizedBox(
                    height: AppConstants.SIDE_MARGIN,
                  ),
                  ButtonTheme(
                    minWidth: double.infinity,
                    child: RaisedButton(
                      highlightElevation: 8.0,
                      onPressed: () async {
                        FocusScope.of(context).requestFocus(FocusNode());
                        leaveUsMessageNotifier.isLeaveUsMsgSelected =
                            false;
                        String accessToken = await AppSharedPreference().getUserToken();
                        debugPrint("accessToken:==>"+accessToken);
                        debugPrint("offerNumberValue:==>"+offerNumberValue);
                        GetCgLeaveMsgResponse getCgLeaveMsgResponse = await leaveUsMessageNotifier.callApiLeaveUsMessage(accessToken,leaveUsMessageNotifier.textEditMessage.text.trim().toString(),offerNumberValue);

                        if( getCgLeaveMsgResponse != null &&
                        getCgLeaveMsgResponse.webMethodOutputRows!= null &&
                        getCgLeaveMsgResponse.webMethodOutputRows.webMethodOutputRows[0] != null &&
                            getCgLeaveMsgResponse.webMethodOutputRows.webMethodOutputRows[0].status == "Success"){

                          showDialog(
                            context: context,
                            builder: (_) => AlertOverlayWithRefresh(
                                AppLocalizations.of(context).translate('leaveUsMessage'),
                                AppLocalizations.of(context).translate('messageSentSuccessfully'),
                               // "Message send successfully.",
                                AppLocalizations.of(context).translate('okay'),""),
                          );
                          //Navigator.pop(context);
                        }else{
                          if( getCgLeaveMsgResponse != null &&
                              getCgLeaveMsgResponse.outputRowErrors!= null &&
                              getCgLeaveMsgResponse.outputRowErrors.outputRowErrors[0] != null &&
                              getCgLeaveMsgResponse.outputRowErrors.outputRowErrors[0].textMessage != ""){
                            showDialog(
                                context: context,
                                builder: (_) =>
                                    AlertOverlay(AppLocalizations.of(context).translate('leaveUsMessage'),
                                        getCgLeaveMsgResponse.outputRowErrors.outputRowErrors[0].textMessage,
                                        AppLocalizations.of(context).translate('okay')));
                          }
                        }


                      },
                      color: (leaveUsMessageNotifier.isLeaveUsMsgSelected)
                          ? colorWhite
                          : colorLiteOrange,
                      elevation: 1,
                      padding: EdgeInsets.only(
                          top: AppConstants.SIDE_MARGIN / 1.5,
                          bottom: AppConstants.SIDE_MARGIN / 1.5),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: colorLiteOrange, width: 1),
                          borderRadius: BorderRadius.all(
                              Radius.circular(AppConstants.SIDE_MARGIN * 1.5))),
                      child: Text(
                       // AppConstants.SEND,
                        AppLocalizations.of(context).translate('send'),
                        style: getStyleButtonText(context).copyWith(
                            color: (leaveUsMessageNotifier
                                    .isLeaveUsMsgSelected)
                                ? colorLiteOrange
                                : colorWhite,
                            letterSpacing: 1,
                            fontWeight: AppFont.fontWeightBold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: AppConstants.SIDE_MARGIN,
                  ),
                ]),
          ),
        )),
      ),
    );
  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

/////////////////////
/*On click-actions*/
////////////////////

}
