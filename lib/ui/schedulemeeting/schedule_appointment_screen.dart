import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangResponse.dart';
import 'package:suna_care/core/notifier/call_request_notifier.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/core/notifier/profile_notifier.dart';
import 'package:suna_care/core/notifier/schedule_appointment_notifier.dart';
import 'package:suna_care/ui/changepassword/change_password_screen.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/ui/schedulemeeting/leave_us_msg_screen.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_meeting_calendar_screen.dart';
import 'package:suna_care/utils/AppLanguage.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/alert_overlay_with_refersh.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';
//import 'package:ishare/screens/custom_expansion_tile.dart' as custom;
import 'package:suna_care/ui/offers/custom_expansion_tile.dart' as custom;
import 'package:suna_care/utils/widget_helper/custom_switch.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';


class ScheduleAppointmentScreen extends StatefulWidget {
  static const routeName = '/schedule_appointment_screen';
  String id = "";

  ScheduleAppointmentScreen({this.id});

  @override
  _ScheduleAppointmentScreenState createState() => _ScheduleAppointmentScreenState();
}

class _ScheduleAppointmentScreenState extends State<ScheduleAppointmentScreen> {
  final log = getLogger('ScheduleAppointmentScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  String offerGuid;
  ScheduleAppointmentNotifier scheduleAppointmentNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  _ScheduleAppointmentScreenState();

  @override
  void initState() {
    super.initState();
    setInitialData();
  }

  setInitialData() async {

    if (widget != null) {
      offerGuid = widget.id;
      log.d('id: ==>'+ widget.id.toString());
      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>"+accessToken);
    }







  }


  @override
  Widget build(BuildContext context) {

      return ChangeNotifierProvider<ScheduleAppointmentNotifier>(
        builder: (context) => ScheduleAppointmentNotifier(context,isFromWhichScreen:"",offerGuid:offerGuid),
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          backgroundColor: colorLitePinkViewProfileBG,
          key: _scaffoldKey,

          body: Consumer<ScheduleAppointmentNotifier>(
            builder: (context, scheduleAppointmentNotifier, _) => ModalProgressHUD(
              inAsyncCall: scheduleAppointmentNotifier.isLoading,
              child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Container(
                    color: colorLitePinkViewProfileBG,
                    child:  Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                          Container(
                            color: colorLitePinkViewProfileBG,

                            child:Container(
                              color: colorLitePinkViewProfileBG,
                              child: Column(
                                  children: <Widget>[

                                    SizedBox(

                                      height: AppConstants.SIDE_MARGIN * 2,
                                      child:Container(
                                        color: colorLitePinkBG,
                                      ),
                                    ),
                                    topHeader(scheduleAppointmentNotifier),

                                    SizedBox(

                                      height: AppConstants.SIDE_MARGIN ,
                                      child:Container(
                                        color: colorLitePinkBG,
                                      ),
                                    ),

                                    _buildWidgetMainContent(context, scheduleAppointmentNotifier),

                                  ]),
                            )

                          ),

                        SizedBox(

                          height: AppConstants.SIDE_MARGIN,
                        ),

                      ],
                    ),
                  )),
            ),
          ),
        ),
      );


  }

  /////////////////////
  /*  build helpers */
  ////////////////////


  Widget topHeader(ScheduleAppointmentNotifier scheduleAppointmentNotifier) {
    return Container(
      color: colorLitePinkBG,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child:  InkWell(
              onTap: () {
                Navigator.canPop(context) ? Navigator.pop(context) : '';
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child:Image.asset(
                  AppImages.IMAGE_BACK,
                  height: 25,
                ),
              ),
            ),


          ),
          Expanded(
            flex: 3,
            child: Text(
              AppConstants.SCHEDULE_LEAVE_US_MSG_TITLE,
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 22.5,
                fontFamily:'NPSunaGrotesk',),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                height: 20,
              ),
            ),
          ),
        ],
      ),

    );
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  Widget _buildWidgetMainContent(BuildContext context, ScheduleAppointmentNotifier profileNotif)  {
    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);
   scheduleAppointmentNotifier = profileNotif;
   //scheduleAppointmentNotifier.isLeaveUsMsgSelected = isLeaveUsMsgSelected;


       return buildWidgetViewProfile(
        context,
        imageWidth,
        cardRadio);
  }

  Widget buildWidgetViewProfile(BuildContext context,double imageWidth,  Radius cardRadio) {
    List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];


      return GestureDetector(
        onTap: () {
          log.d('onPress:  ');

        },
        child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child:Container(
      child:Padding(
        padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
          child :Container(
            margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
            child:  Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [



                  SizedBox(
                    height: AppConstants.SIDE_MARGIN,
                  ),

                  Text(
                    AppConstants.FIFTY_MINUTES_MEETING ,
                    textAlign: TextAlign.center,
                    style: getStyleBody1(context).copyWith(
                        color: colorPencilBlack,
                        fontWeight: AppFont.fontWeightSemiBold,
                        fontSize: 23),
                  ),

                  SizedBox(
                    height: AppConstants.SIDE_MARGIN,
                  ),
                  Text(
                    "Thursday,Dec 18th 2020" ,
                    textAlign: TextAlign.center,
                    style: getStyleBody1(context).copyWith(
                        color: colorPencilBlack,
                        fontWeight: AppFont.fontWeightRegular,
                        fontSize: 15),
                  ),

                  Text(
                    "9.00 am - 9.15 am" ,
                    textAlign: TextAlign.center,
                    style: getStyleBody1(context).copyWith(
                        color: colorPencilBlack,
                        fontWeight: AppFont.fontWeightRegular,
                        fontSize: 15),
                  ),


                  SizedBox(
                    height: AppConstants.SIDE_MARGIN*1.5,
                  ),


                  Text(
                    AppConstants.PLEASE_SHARE_ANYTHING ,
                    textAlign: TextAlign.center,
                    style: getStyleBody1(context).copyWith(
                        color: colorPencilBlack,
                        fontWeight: AppFont.fontWeightRegular,
                        fontSize: 15),
                  ),

                  SizedBox(
                    height: AppConstants.SIDE_MARGIN,
                  ),
                  Container(
                    margin: EdgeInsets.all(8.0),
                    height: getScreenHeight(context)/3,
                    padding: EdgeInsets.only(bottom: 40.0),
                    child: TextField(
                      maxLines: 50,
                      decoration: InputDecoration(
                        hintText: AppConstants.ENTER_MESSAGE,
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),



                  SizedBox(
                    height: AppConstants.SIDE_MARGIN,
                  ),

                  ButtonTheme(
                    minWidth: double.infinity,

                    child: RaisedButton(

                      highlightElevation: 8.0,
                      onPressed: () async {
                        FocusScope.of(context).requestFocus(FocusNode());
                        scheduleAppointmentNotifier.isLeaveUsMsgSelected = false;


                      },
                      color: (scheduleAppointmentNotifier.isLeaveUsMsgSelected)?colorWhite:colorLiteOrange,
                      elevation: 1,
                      padding: EdgeInsets.only(
                          top: AppConstants.SIDE_MARGIN / 1.5,
                          bottom: AppConstants.SIDE_MARGIN / 1.5),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: colorLiteOrange, width: 1),
                          borderRadius: BorderRadius.all( Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                      ),
                      child: Text(
                        AppConstants.SCHDULE_APPOINTMENT,
                        style: getStyleButtonText(context).copyWith(
                            color:(scheduleAppointmentNotifier.isLeaveUsMsgSelected)?colorLiteOrange:colorWhite,
                            letterSpacing: 1,
                            fontWeight: AppFont.fontWeightBold),
                      ),
                    ),


                  ),


                  SizedBox(
                    height: AppConstants.SIDE_MARGIN,
                  ),




                ]),

          ),

      )
            ),
    ),



      );


  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////



 

}



