import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jiffy/jiffy.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:quiver/iterables.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/availability/assignment/AssignmentListResponse.dart'
    as AssignmentList;
import 'package:suna_care/core/data/remote/request_response/availability/holiday/AddHolidayResponse.dart'
    as AddHolidayResp;
import 'package:suna_care/core/data/remote/request_response/availability/holiday/HolidayListResponse.dart'
    as HolidayList;
import 'package:suna_care/core/notifier/schedule_meeting_calendar_notifier.dart';
import 'package:suna_care/core/notifier/home_notifier.dart';
import 'package:suna_care/ui/home/availability/calendarData.dart';
import 'package:suna_care/ui/home/availability/day.dart';
import 'package:suna_care/ui/home/availability/schedule_details.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_meeting_time_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:provider/provider.dart';
//import 'package:table_calendar/table_calendar.dart';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class ScheduleMeetingScreen extends StatefulWidget {
  static const routeName = '/schedule_meeting_screen';

  ScheduleMeetingScreen();

  @override
  ScheduleMeetingScreenState createState() => ScheduleMeetingScreenState();
}

class ScheduleMeetingScreenState extends State<ScheduleMeetingScreen> {
  BuildContext mContext;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  ScheduleMeetingScreenState();

  //CalendarController _controller = CalendarController();

  var log = getLogger("ScheduleMeetingScreen");
  var LastDateOfMonthArr = const [
    31,
    28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31
  ];
  DateTime masterDate = DateTime.now();

  //Picker
  DateTime selectedStartDate = DateTime.now();
  String scheduleFormatedStartDate = "";
  DateTime selectedEndDate = DateTime.now();
  String selectedDate;
  ///Multi date range picker
  Day start;
  List<List<Day>> intervals = [];
  bool deleteConfirm = false;
  Day deleteDay;
  List<Day> deleteInterval;
  bool onlyOne = false;
  Color selectionColor = Colors.red;
  Color buttonColor = Colors.lightGreenAccent;
  Color buttonTextColor = Colors.black;
  Color primaryTextColor = Colors.black;
  Color dateTextColor = Colors.black;
  Color ignoreTextColor = Colors.grey;
  Color selectedDateTextColor = Colors.black;
  Color selectedIgnoreTextColor = Colors.black;
  Color backgroundTextColor = Colors.white;

  //final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  final DateFormat dateMonthFormatter = DateFormat('ddo MMMM , yyyy');

  int assignmentListSize = 0, holidayListSize = 0;
  var monthFormatter = new DateFormat('MM');
  var yearFormatter = new DateFormat('yyyy');
  var dateFormatter = new DateFormat('dd');

  @override
  void initState() {
    super.initState();

  }



  @override
  Widget build(BuildContext context) {
    mContext = context;
    return ChangeNotifierProvider<ScheduleMeetingCalendarNotifier>(
        builder: (context) => ScheduleMeetingCalendarNotifier(),
        child: Consumer<ScheduleMeetingCalendarNotifier>(
            builder: (context, notifier, _) => Theme(
                data: ThemeData(
                    hintColor: colorDividerGrey,
                    primaryColor: colorPrimary,
                    accentColor: colorYellow,
                    primaryColorDark: colorPrimary),
                child: _buildWidgetHomeCard(notifier,
                    context)))); //_buildWidgetSendEmailIdCard(notifier, context)));
  }

  /////////////////////
  /*  build helpers */
  ////////////////////

  Widget _buildWidgetHomeCard(ScheduleMeetingCalendarNotifier notifier, BuildContext context) {
    notifier.context = context;

    //Get selected date
    var selectedDate = masterDate;
    String currentDateMonth = monthFormatter.format(DateTime.now());
    String currentDateYear = yearFormatter.format(DateTime.now());
    String selectedDateMonth = monthFormatter.format(selectedDate);
    String selectedDateYear = yearFormatter.format(selectedDate);
    debugPrint("selectedDateMonth :==>" + selectedDateMonth.toString());
    debugPrint("currentDateMonth:==>" + currentDateMonth.toString());
    debugPrint("selectedDateYear :==>" + selectedDateYear.toString());
    debugPrint("currentDateYear :==>" + currentDateYear.toString());

    var currentDate = new DateTime.now();
    debugPrint("CompareTo :==>" + masterDate.compareTo(currentDate).toString());

    return ChangeNotifierProvider<ScheduleMeetingCalendarNotifier>(
      builder: (context) => ScheduleMeetingCalendarNotifier(),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: colorLitePinkBG,
          image: DecorationImage(
              image: AssetImage(
                AppImages.IMAGE_SELECT_DATE_BG_WITH_CIRCLE,
              ),
              fit: BoxFit.fill),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            key: _scaffoldKey,
            body: Consumer<ScheduleMeetingCalendarNotifier>(
              builder: (context, changePasswordNotifier, _) => Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  ModalProgressHUD(
                    color: Colors.transparent,
                    inAsyncCall: changePasswordNotifier.isLoading,
                    child: GestureDetector(
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());
                        },
                        child: Container(
                          //alignment: Alignment.topCenter,
                          child: Column(

                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                               Container(
                                margin: EdgeInsets.only(top: 10.0),
                                color: colorLitePinkBG,
                                height: 35,
                              ),
                              topHeader(),

                                // Container(
                                //   color: colorWhite,
                                //   child:  Column(
                                //
                                //       mainAxisAlignment: MainAxisAlignment.start,
                                //       crossAxisAlignment: CrossAxisAlignment.center,
                                //       children: <Widget>[
                                //         Container(
                                //           margin: EdgeInsets.only(top: 10.0),
                                //           color: colorWhite,
                                //           height: 25,
                                //         ),
                                //
                                //         SizedBox(
                                //           height: AppConstants.SIDE_MARGIN,
                                //         ),
                                //
                                //         Text(
                                //           AppConstants.FIFTY_MINUTES_MEETING ,
                                //           textAlign: TextAlign.center,
                                //           style: getStyleBody1(context).copyWith(
                                //               color: colorPencilBlack,
                                //               fontWeight: AppFont.fontWeightSemiBold,
                                //               fontSize: 23),
                                //         ),
                                //
                                //         SizedBox(
                                //           height: AppConstants.SIDE_MARGIN,
                                //         ),
                                //         Text(
                                //           "Select a day" ,
                                //           textAlign: TextAlign.center,
                                //           style: getStyleBody1(context).copyWith(
                                //               color: colorPencilBlack,
                                //               fontWeight: AppFont.fontWeightRegular,
                                //               fontSize: 15),
                                //         ),
                                //
                                //       ]),
                                // ),


                              SizedBox(
                                height: AppConstants.SIDE_MARGIN*2,
                              ),

                              //////////////////////////Customized CalendarPage  Design ////////////////////////
                              Padding(

                                padding: const EdgeInsets.all(0.0),
                                child: Column(
                                  children: <Widget>[
                                    getCalendarCard(notifier, context),
                                    Row(
                                      children: <Widget>[],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )),
                  )
                ],
              ),
            )

            //_buildSignUpLabel(),
            ),
      ),
    );
  }

  ////////////// picker page content///////////

  Widget getCalendarCard(ScheduleMeetingCalendarNotifier notifier, BuildContext context) {
    DateFormat formatter = new DateFormat("MMMM yyyy",notifier.languageCode);
    return Card(
      color: colorTransparent,
      elevation: 0.0,
      child: Container(
        color: colorTransparent,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 2.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    padding: EdgeInsets.fromLTRB(0.0, 0.0, 55.0, 0.0),
                    elevation: 0.0,
                    child: Icon(
                      Icons.navigate_before,
                      color: colorLiteOrange,
                    ),
                    onPressed: () {
                      setState(() {
                        DateTime previousMonth =
                            DateTime(masterDate.year, masterDate.month - 1);
                        //masterDate = masterDate.add(Duration(days: -29));
                        masterDate = previousMonth;
                        notifier.masterDate = masterDate;
                        intervals.clear();
                      });
                    },
                    color: colorTransparent,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      formatter.format(masterDate),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: primaryTextColor,
                      ),
                    ),
                  ),
                  RaisedButton(
                    padding: EdgeInsets.fromLTRB(55.0, 0.0, 0.0, 0.0),
                    elevation: 0.0,
                    child: Icon(
                      Icons.navigate_next,
                      color: colorLiteOrange,
                    ),
                    onPressed: () async {
                      setState(() {
                        //masterDate = masterDate.add(Duration(days: 29));
                        DateTime futureMonth =
                            DateTime(masterDate.year, masterDate.month + 1);
                        masterDate = futureMonth;
                        notifier.masterDate = futureMonth;
                        intervals.clear();
                      });
                    },
                    color: colorTransparent,
                  ),
                ],
              ),
              SizedBox(
                height: 2,
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        DateFormat("EEE",notifier.languageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    1))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",notifier.languageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    2))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",notifier.languageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    3))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",notifier.languageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    4))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",notifier.languageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    5))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",notifier.languageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    6))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                      Text(
                        DateFormat("EEE",notifier.languageCode)
                            .format(DateTime(
                                1,
                                1,
                                dateTimeSymbolMap()[Intl.getCurrentLocale()]
                                        .FIRSTDAYOFWEEK +
                                    7))
                            //.toUpperCase()
                            .replaceAll(".", ""),
                        style: TextStyle(
                          color: primaryTextColor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  Column(
                    children: buildCalendar(notifier, context),
                  ),

                  SizedBox(
                    height: AppConstants.SIDE_MARGIN*2,
                  ),

                  Container(padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),

                    child: ButtonTheme(
                      minWidth: double.infinity,


                      child: RaisedButton(


                        highlightElevation: 8.0,
                        onPressed: () async {
                          //FocusScope.of(context).requestFocus(FocusNode());
                          //Navigator.pushNamed(context, ScheduleAppointmentScreen.routeName);
                          if(intervals != null && intervals.isNotEmpty){
                            debugPrint("intervals :==>"+intervals.length.toString());
                            List<Day> interval = intervals[0];
                            debugPrint("interval date :==>"+interval[0].date.toString());
                            Navigator.pushNamed(context, ScheduleMeetingTimeScreen.routeName,
                            arguments: interval[0].date.toString()
                            );
                          }else{
                            showDialog(
                              context: context,
                              builder: (_) =>
                                  AlertOverlay(
                                      //AppConstants.SCHEDULE_APPOINTMENT,
                                      //AppConstants.PLEASE_SELECT_DATE,
                                      AppLocalizations.of(context).translate('scheduleAppointment'),
                                      AppLocalizations.of(context).translate('pleaseSelectDate'),
                                      AppLocalizations.of(context).translate('okay')),
                            );
                          }

                        },
                        color: colorLiteOrange,
                        elevation: 1,
                        padding: EdgeInsets.only(
                            top: AppConstants.SIDE_MARGIN / 1.5,
                            bottom: AppConstants.SIDE_MARGIN / 1.5),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: colorLiteOrange, width: 1),
                            borderRadius: BorderRadius.all( Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                        ),
                        child: Text(
                          //AppConstants.NEXT,
                          AppLocalizations.of(context).translate('next'),
                          style: getStyleButtonText(context).copyWith(
                              color:colorWhite,
                              letterSpacing: 1,
                              fontWeight: AppFont.fontWeightBold),
                        ),
                      ),


                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> buildCalendar(ScheduleMeetingCalendarNotifier notifier, BuildContext context) {
    var currentDate = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String currentFormatedDate = formatter.format(currentDate);
    print("Current date now :==>" + currentDate.toString());
    print("Current date formated:==>" + currentFormatedDate);

    List<Widget> list = [];
    final days = getDays();

    final weeks = partition(days, 7);

    for (final week in weeks) {
      final List<Widget> weekL = [];

      for (Day day in week) {
        if (start != null &&
            day.date == start.date &&
            day.inInterval == false) {
          day = Day(
            date: day.date,
            ignore: day.ignore,
            inInterval: true,
            isStart: true,
            isEnd: true,
          );
        }

        weekL.add(Expanded(
          child: GestureDetector(
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(day.isStart ? 50 : 0),
                topRight: Radius.circular(day.isEnd ? 50 : 0),
                bottomLeft: Radius.circular(day.isStart ? 50 : 0),
                bottomRight: Radius.circular(day.isEnd ? 50 : 0),
              ),
              // borderRadius: BorderRadius.all(Radius.circular(50)),

              child: Container(
//                color: day.inInterval
//                    ? widget.selectionColor
//                    : Color.fromRGBO(0, 0, 0, 0),

                color: day.inInterval
                    //?(day.isContractDate) ? Colors.green: (day.isHolidayBlocked)? Colors.brown:widget.selectionColor
                    ? (day.date.isBefore(currentDate) &&
                            (day.isBlocked != null && day.isBlocked))
                        ? Colors.green
                        : (day.date.isAfter(currentDate) &&
                                (day.isBlocked != null && day.isBlocked))
                            ? Colors.brown
                            : (day.date.isAtSameMomentAs(currentDate))
                                ? Colors.brown
                                : selectionColor
                    : Color.fromRGBO(0, 0, 0, 0),
                margin: EdgeInsets.fromLTRB(0.0, 1.3, 0.0, 1.3),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      day.date.day.toString(),
                      style: day.ignore
                          ? (day.inInterval
                              ? TextStyle(
                                  color: selectedDateTextColor,
                                )
                              : TextStyle(
                                  color: dateTextColor,
                                ))
                          : (day.inInterval
                              ? TextStyle(
                                  color: selectedIgnoreTextColor,
                                )
                              : TextStyle(
                                  color: ignoreTextColor,
                                )),
                    ),
                  ),
                ),
              ),
            ),
            onTap: () {
              //Get selected date
              var selectedDate = masterDate;
              String currentDateMonth = monthFormatter.format(DateTime.now());
              String currentDateYear = yearFormatter.format(DateTime.now());
              String selectedDateMonth = monthFormatter.format(selectedDate);
              String selectedDateYear = yearFormatter.format(selectedDate);
              debugPrint(
                  "selectedDateMonth :==>" + selectedDateMonth.toString());
              debugPrint("currentDateMonth:==>" + currentDateMonth.toString());
              debugPrint("selectedDateYear :==>" + selectedDateYear.toString());
              debugPrint("currentDateYear :==>" + currentDateYear.toString());
              if ((int.parse(selectedDateMonth) >=
                      int.parse(currentDateMonth)) &&
                  (int.parse(selectedDateYear) >= int.parse(currentDateYear))) {
                click(day, notifier, context);
              } else {
                debugPrint("You can't Add or Edit for previous months");
              }
            },
          ),
        ));
      }

      list.add(Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: weekL,
        ),
      ));
    }

    return list;
  }

  Widget topHeader() {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
            flex: 1,
            child: InkWell(
              onTap: () {
                Navigator.canPop(context) ? Navigator.pop(context) : '';
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Image.asset(
                  AppImages.IMAGE_BACK,
                  height: 25,
                ),
              ),
            )),
        Expanded(
          flex: 2,
          child: Container(
            child: Text(
              //AppConstants.SCHEDULE_HEADER_TITLE,
              AppLocalizations.of(context).translate('selectDate'),
              textAlign: TextAlign.center,
              style: getStyleBody1(mContext).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 22,
                fontFamily: 'NPSunaGrotesk',
              ),
            ),
          ),
        ),
        Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
              color: Colors.transparent,
              alignment: Alignment.centerRight,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                // width: getScreenWidth(context) / 4,
                height: 20,
              ),
            )),
      ],
    );
  }

  List<Day> getDays() {
    final List<Day> days = [];

    DateTime start = DateTime(masterDate.year, masterDate.month);
    DateTime finish = DateTime(masterDate.year, masterDate.month + 1);

    start = start.add(Duration(days: -start.weekday));

    start = start.subtract(Duration(
        days:
            (7 - dateTimeSymbolMap()[Intl.getCurrentLocale()].FIRSTDAYOFWEEK) %
                7));

    finish = finish.add(Duration(days: 6 - finish.weekday));

    finish = finish.subtract(Duration(
        days:
            (7 - dateTimeSymbolMap()[Intl.getCurrentLocale()].FIRSTDAYOFWEEK) %
                7));

    for (var i = 0; i <= finish.difference(start).inDays; i++) {
      final date = start.add(Duration(days: i + 1));

      bool inInterval = false;
      bool isStart = false;
      bool isEnd = false;
      bool isBlocked = false;
      bool isHolidayBlocked = false;
      bool isContractDate = false;

      for (final interval in intervals) {
        inInterval = false;
        isStart = false;
        isEnd = false;
        isBlocked = false;
        isHolidayBlocked = false;
        isContractDate = false;
        if (interval[0].date == date) {
          isStart = true;
        }
        if (interval[1].date == date) {
          isEnd = true;
        }

        //Blocked Dated
        if (interval[0].isBlocked != null && interval[0].isBlocked) {
          isBlocked = true;
        }
        if (interval[1].isBlocked != null && interval[1].isBlocked) {
          isBlocked = true;
        }

        //isContractDate
        if (interval[0].isContractDate != null && interval[0].isContractDate) {
          isContractDate = true;
        }
        if (interval[1].isContractDate != null && interval[1].isContractDate) {
          isContractDate = true;
        }

        //isHolidayBlocked
        if (interval[0].isHolidayBlocked != null &&
            interval[0].isHolidayBlocked) {
          isHolidayBlocked = true;
        }
        if (interval[1].isHolidayBlocked != null &&
            interval[1].isHolidayBlocked) {
          isHolidayBlocked = true;
        }

        //SinceEpoch
        if (interval[0].date.millisecondsSinceEpoch <=
                date.millisecondsSinceEpoch &&
            date.millisecondsSinceEpoch <=
                interval[1].date.millisecondsSinceEpoch) {
          inInterval = true;
          break;
        }
      }

      days.add(
        Day(
          date: date,
          ignore: date.month == masterDate.month,
          inInterval: inInterval,
          isStart: isStart,
          isEnd: isEnd,
          isBlocked: isBlocked,
          isHolidayBlocked: isHolidayBlocked,
          isContractDate: isContractDate,
        ),
      );
    }

    return days;
  }

  List<List<Day>> mergeInterval(List<List<Day>> intervals) {
    debugPrint(" :==>");
    final List<List<Day>> result = [];

    intervals.sort((a, b) => a[0]
        .date
        .millisecondsSinceEpoch
        .compareTo(b[0].date.millisecondsSinceEpoch));

    List<Day> currentRange;

    for (final range in intervals) {
      if (range[0].date.millisecondsSinceEpoch >
          range[1].date.millisecondsSinceEpoch) continue;

      if (currentRange == null) {
        currentRange = range;
        continue;
      }

      if (currentRange[1].date.millisecondsSinceEpoch <
          range[0].date.millisecondsSinceEpoch) {
        result.add(currentRange);
        currentRange = range;
      } else if (currentRange[1].date.millisecondsSinceEpoch <
          range[1].date.millisecondsSinceEpoch) {
        currentRange[1] = range[1];
      }
    }

    if (currentRange != null) {
      result.add(currentRange);
    }

    return result;
  }

  void click(Day day, ScheduleMeetingCalendarNotifier notifier, BuildContext context) {
    // if(day.inInterval == true){
    bool isDayBlocked = false;
    bool isHolidayBlocked = false;
    int selectedHolidayIndex = -1;
    int newAddHolidayIndex = -1;
    String cgScheduleGuid = "";
    String fromDate = "";
    String toDate = "";

    debugPrint("date clicked :==>");
    debugPrint(" clicked date :==>" + day.date.toString());



    //Select Date from picker
    _selectDateWithSaturdaySundayRest(notifier,context);

    for (int arrIndex = 0; arrIndex < intervals.length; arrIndex++) {
      var interval = intervals[arrIndex];
      for (int InnerArrIndex = 0;
          InnerArrIndex < interval.length - 1;
          InnerArrIndex++) {
        final startTime = interval[0].date;
        final endTime = interval[1].date;
        final isStartDateBlocked = interval[0].isBlocked;
        final isEndDateBlocked = interval[1].isBlocked;
        final isStartHolidayDateBlocked = interval[0].isHolidayBlocked;
        final isEndHolidayDateBlocked = interval[1].isHolidayBlocked;

        final currentTime = day.date;

        if (interval != null &&
            interval[0].isNewlyAddedOne != null &&
            interval[0].isNewlyAddedOne &&
            interval[1].isNewlyAddedOne != null &&
            interval[1].isNewlyAddedOne) {
          newAddHolidayIndex = arrIndex;
        }

        if (currentTime.isAtSameMomentAs(startTime)) {
          debugPrint("It's same as startDate:==> ");
          debugPrint("Range Index :==> " + arrIndex.toString());
          debugPrint("Range Start Date @@:==> " + startTime.toString());
          debugPrint("Range End Date @@:==> " + endTime.toString());

          fromDate = interval[0].date.toString();
          toDate = interval[1].date.toString();

          if (interval[0].cgScheduleGuid != null) {
            debugPrint("Range cgScheduleGuid @@:==> " +
                interval[0].cgScheduleGuid.toString());
            cgScheduleGuid = interval[0].cgScheduleGuid.toString();
          }
          selectedHolidayIndex = arrIndex;

          if (isStartDateBlocked != null &&
              isStartDateBlocked &&
              isEndDateBlocked != null &&
              isEndDateBlocked) {
            // showToastMessage("You Can't edit this range !!");
            isDayBlocked = true;
            break;
          }

          if (isStartHolidayDateBlocked != null &&
              isStartHolidayDateBlocked &&
              isEndHolidayDateBlocked != null &&
              isEndHolidayDateBlocked) {
            // showToastMessage("You Can't edit this range !!");
            isHolidayBlocked = true;
            break;
          }
        } else if (currentTime.isAtSameMomentAs(endTime)) {
          debugPrint("It's same as endDate :==> ");
          debugPrint("Range Index :==> " + arrIndex.toString());
          debugPrint("Range Start Date @@:==> " + startTime.toString());
          debugPrint("Range End Date @@:==> " + endTime.toString());

          fromDate = interval[0].date.toString();
          toDate = interval[1].date.toString();

          if (interval[0].cgScheduleGuid != null) {
            debugPrint("Range cgScheduleGuid @@:==> " +
                interval[0].cgScheduleGuid.toString());
            cgScheduleGuid = interval[0].cgScheduleGuid.toString();
          }
          selectedHolidayIndex = arrIndex;

          if (isStartDateBlocked != null &&
              isStartDateBlocked &&
              isEndDateBlocked != null &&
              isEndDateBlocked) {
            //showToastMessage("You Can't edit this range !!");
            isDayBlocked = true;
            break;
          }

          if (isStartHolidayDateBlocked != null &&
              isStartHolidayDateBlocked &&
              isEndHolidayDateBlocked != null &&
              isEndHolidayDateBlocked) {
            // showToastMessage("You Can't edit this range !!");
            isHolidayBlocked = true;
            break;
          }
        } else {
          if (currentTime.isAfter(startTime) && currentTime.isBefore(endTime)) {
            debugPrint("It's in Range:==> ");
            debugPrint("Range Index :==> " + arrIndex.toString());
            debugPrint("Range Start Date @@:==> " + startTime.toString());
            debugPrint("Range End Date @@:==> " + endTime.toString());

            fromDate = interval[0].date.toString();
            toDate = interval[1].date.toString();
            if (interval[0].cgScheduleGuid != null) {
              debugPrint("Range cgScheduleGuid @@:==> " +
                  interval[0].cgScheduleGuid.toString());
              cgScheduleGuid = interval[0].cgScheduleGuid.toString();
            }
            selectedHolidayIndex = arrIndex;

            if (isStartDateBlocked != null &&
                isStartDateBlocked &&
                isEndDateBlocked != null &&
                isEndDateBlocked) {
              //showToastMessage("You Can't edit this range !!");
              isDayBlocked = true;
              break;
            }
            if (isStartHolidayDateBlocked != null &&
                isStartHolidayDateBlocked &&
                isEndHolidayDateBlocked != null &&
                isEndHolidayDateBlocked) {
              // showToastMessage("You Can't edit this range !!");
              isHolidayBlocked = true;
              break;
            }
          } else {
            //Logic for multi unavailable dates remove
            // selectedAvailableIndex = arrIndex;
          }
        }
      }
    }


  }


  //////// New calendar picker start //////
  DateTime selectedDate1 = DateTime.now();
  DateTime initialData;

  bool defineSelectable(DateTime val) {
    DateTime now = DateTime.now();


//make it return true on initialDate
    if(val.compareTo(initialData)==0){
      return true;
    }
    // // disabled all days before today
    // if (val.isBefore(now) ) {
    //   return true;
    // }

// disabled all days except Friday
    switch (val.weekday) {
      case (DateTime.sunday):
        return false;
        break;
      case (DateTime.saturday):
        return false;
        break;
      default:
        return true;
    }


  }


  int daysToAdd(int todayIndex, int targetIndex) {
    print('todayIndex $todayIndex');
    print('targetIndex $targetIndex');
    if (todayIndex == 5) { // must jump to next week
      return 3;
    }else if (todayIndex == 6) { // must jump to next week
      return 2;
    } else if (todayIndex == 7 ){ // must jump to next week
      return 1;
    } else {
      return 0; // date is matched
    }
  }


  DateTime defineInitialDate() {

    var currentDate = new DateTime.now();
    var monthFormatter = new DateFormat('MM');
    var yearFormatter = new DateFormat('yyyy');
    var dateFormatter = new DateFormat('dd');


    //For current Date
    String currentDateMonth = monthFormatter.format(currentDate);

    //For master date
    String masterDay = dateFormatter.format(masterDate);
    String masterDateMonth = monthFormatter.format(masterDate);
    String masterDateYear = yearFormatter.format(masterDate);
    print("Current date now :==>" + currentDate.toString());
    print("currentDateMonth formatted month :==>" + currentDateMonth);
    print("master date formatted month:==>" + masterDateMonth);
    print("master date formatted year:==>" + masterDateYear);
    print("master date formatted day:==>" + masterDay);

    //DateTime now = DateTime.now();
    DateTime now = (currentDateMonth == masterDateMonth) ? DateTime.now() : DateTime(int.parse(masterDateYear), int.parse(masterDateMonth), 1);

   // int dayOffset = daysToAdd(now.weekday, DateTime.friday);
    int dayOffset;
    //dayOffset = daysToAdd(now.weekday, DateTime.sunday+1);
    dayOffset = daysToAdd(now.weekday, DateTime.sunday);


    print('dayOffset: $dayOffset');
    return now.add(Duration(days: dayOffset));
  }

  Future<Null> _selectDateWithSaturdaySundayRest(ScheduleMeetingCalendarNotifier notifier,BuildContext context) async {

    var currentDate = new DateTime.now();
    var monthFormatter = new DateFormat('MM');
    var yearFormatter = new DateFormat('yyyy');
    var dateFormatter = new DateFormat('dd');


    //For current Date
    String currentDateMonth = monthFormatter.format(currentDate);

    //For master date
    String masterDay = dateFormatter.format(masterDate);
    String masterDateMonth = monthFormatter.format(masterDate);
    String masterDateYear = yearFormatter.format(masterDate);
    print("Current date now :==>" + currentDate.toString());
    print("currentDateMonth formatted month :==>" + currentDateMonth);
    print("master date formatted month:==>" + masterDateMonth);
    print("master date formatted year:==>" + masterDateYear);
    print("master date formatted day:==>" + masterDay);

    initialData = defineInitialDate();
    print('defineInitialDate: ${initialData}');
    print('defineSelectable: $defineSelectable');
    DateTime firstDate =  (currentDateMonth == masterDateMonth) ? DateTime.now() : DateTime(int.parse(masterDateYear), int.parse(masterDateMonth), 1);
    DateTime lastDate =  DateTime(int.parse(masterDateYear),int.parse(masterDateMonth), LastDateOfMonthArr[int.parse(masterDateMonth) - 1]);
    print('firstDate:'  +firstDate.toString());
    print('lastDate:'  +lastDate.toString());
    print('defineSelectable: $defineSelectable');
    final DateTime picked = await showDatePicker(
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme: ColorScheme.light().copyWith(
                primary: colorLiteOrange,
              ),
            ),
            child: child,
          );
        },
        context: context,
        initialDate: initialData,
        selectableDayPredicate: defineSelectable,
        // firstDate: DateTime(2021),
        // lastDate: DateTime(2125));
        locale: Locale(notifier.languageCode),
        cancelText: AppLocalizations.of(context).translate("cancel"),
        confirmText: AppLocalizations.of(context).translate("okay"),

    //initialDate: (currentDateMonth == masterDateMonth) ? DateTime.now() : DateTime(int.parse(masterDateYear), int.parse(masterDateMonth), 1),
    firstDate: (currentDateMonth == masterDateMonth) ? DateTime.now() : DateTime(int.parse(masterDateYear), int.parse(masterDateMonth), 1),
    //firstDate:DateTime.now().subtract(Duration(days: 2)),
    lastDate: DateTime(int.parse(masterDateYear),int.parse(masterDateMonth), LastDateOfMonthArr[int.parse(masterDateMonth) - 1]));

    if (picked != null) selectedDate1 = picked;
     print('Select Date: $selectedDate1');


    setState(() {
      selectedStartDate = picked;
      final DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm:ss');
      final String formatted = formatter.format(selectedStartDate);

      setState(() {
        selectedDate = formatter.format(selectedStartDate);
      });
      print(formatted);
      print("selectedDate :==>" + selectedDate);
      addDateInCalendar();
    });



  }

  //////// New calendar picker end //////

  @override
  void dispose() {
    this.mContext = null;
    super.dispose();
  }

  void addDateInCalendar(){
    if(intervals != null && intervals.length >0)
      intervals.clear();

    intervals.add([
      Day(
          date: selectedStartDate,
          ignore: null,
          inInterval: null,
          isEnd: null,
          isStart: null,
          isBlocked: false,
          isContractDate: false,
          isHolidayBlocked: false,
          isNewlyAddedOne: true,

          //Assignment list
          customerName: null,
          offerNumber: null,
          netToSalary: null,
          //Holiday list
          cgScheduleGuid: null),
      Day(
          date: selectedStartDate,
          ignore: null,
          inInterval: null,
          isEnd: null,
          isStart: null,
          isBlocked: false,
          isContractDate: false,
          isHolidayBlocked: false,
          isNewlyAddedOne: true,

          //Assignment list
          customerName: null,
          offerNumber: null,
          netToSalary: null,
          //Holiday list
          cgScheduleGuid: null)
    ]);

    intervals = mergeInterval(intervals);
  }

/////////////////////
/*On click-actions*/

  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

//////////////MultiRange picker page content///////////

}
