import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/profile/SetPreferredLangResponse.dart';
import 'package:suna_care/core/notifier/call_request_notifier.dart';
import 'package:suna_care/core/notifier/offers_notifier.dart';
import 'package:suna_care/core/notifier/profile_notifier.dart';
import 'package:suna_care/ui/changepassword/change_password_screen.dart';
import 'package:suna_care/ui/offers/custom_expansion_tile.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_meeting_calendar_screen.dart';
import 'package:suna_care/utils/AppLanguage.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/alert_overlay_with_refersh.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/widget_helper/custom_expendable_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_log_helper.dart';
//import 'package:ishare/screens/custom_expansion_tile.dart' as custom;
import 'package:suna_care/ui/offers/custom_expansion_tile.dart' as custom;
import 'package:suna_care/utils/widget_helper/custom_switch.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

import 'leave_us_msg_screen.dart';


class CallRequestScreen extends StatefulWidget {
  static const routeName = '/call_request_screen';
  String id = "";

  CallRequestScreen({this.id});

  @override
  _CallRequestScreenState createState() => _CallRequestScreenState();
}

class _CallRequestScreenState extends State<CallRequestScreen> {
  final log = getLogger('CallRequestScreen');
  bool isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int balanceHit = 1;
  String offerGuid;
  CallRequestNotifier callRequestNotifier;
  double heightOfModalBottomSheet = 0;
  bool estimateIsExpanded = true;
  final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  _CallRequestScreenState();

  @override
  void initState() {
    super.initState();
    setInitialData();
  }

  setInitialData() async {


      String accessToken = await AppSharedPreference().getUserToken();
      debugPrint("accessToken:==>"+accessToken);

  }


  @override
  Widget build(BuildContext context) {

      return ChangeNotifierProvider<CallRequestNotifier>(
        builder: (context) => CallRequestNotifier(context,isFromWhichScreen:""),
        child: Scaffold(
          backgroundColor: colorLitePinkViewProfileBG,
          key: _scaffoldKey,

          body: Consumer<CallRequestNotifier>(
            builder: (context, callRequestNotifier, _) => ModalProgressHUD(
              inAsyncCall: callRequestNotifier.isLoading,
              child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Container(
                    color: colorLitePinkViewProfileBG,
                    child:  Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                          Container(
                            color: colorLitePinkViewProfileBG,

                            child:Container(
                              color: colorLitePinkViewProfileBG,
                              child: Column(
                                  children: <Widget>[

                                    SizedBox(

                                      height: AppConstants.SIDE_MARGIN * 2,
                                      child:Container(
                                        color: colorLitePinkBG,
                                      ),
                                    ),
                                    topHeader(callRequestNotifier),

                                    SizedBox(

                                      height: AppConstants.SIDE_MARGIN ,
                                      child:Container(
                                        color: colorLitePinkBG,
                                      ),
                                    ),
                              SingleChildScrollView(
                                  physics: BouncingScrollPhysics(),
                                  child:
                                    _buildWidgetMainContent(context, callRequestNotifier),
                              ),

                                  ]),
                            )

                          ),

                        SizedBox(

                          height: AppConstants.SIDE_MARGIN,
                        ),

                      ],
                    ),
                  )),
            ),
          ),
        ),
      );


  }

  /////////////////////
  /*  build helpers */
  ////////////////////


  Widget topHeader(CallRequestNotifier callRequestNotifier) {
    return Container(
      color: colorLitePinkBG,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child:  InkWell(
              onTap: () {
                Navigator.canPop(context) ? Navigator.pop(context) : '';
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child:Image.asset(
                  AppImages.IMAGE_BACK,
                  height: 25,
                ),
              ),
            ),


          ),
          Expanded(
            flex: 3,
            child: Text(
              //AppConstants.CALL_REQUEST_TITLE,
              AppLocalizations.of(context).translate('callRequest'),
              textAlign: TextAlign.center,
              style: getStyleBody1(context).copyWith(
                  color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 22.5,
                fontFamily:'NPSunaGrotesk',),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                height: 20,
              ),
            ),
          ),
        ],
      ),

    );
  }

  void backButtonPressed() {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  Widget _buildWidgetMainContent(BuildContext context, CallRequestNotifier profileNotif)  {
    double imageWidth = getScreenWidth(context) / 5;
    Radius cardRadio = Radius.circular(AppConstants.SIDE_MARGIN / 1.3);
   callRequestNotifier = profileNotif;
   //callRequestNotifier.isLeaveUsMsgSelected = isLeaveUsMsgSelected;


       return buildWidgetViewProfile(
        context,
        imageWidth,
        cardRadio);
  }

  Widget buildWidgetViewProfile(BuildContext context,double imageWidth,  Radius cardRadio) {
    List cardBGColors = [colorLitePink, colorLiteGreen, colorLiteBlue];


      return GestureDetector(
        onTap: () {
          log.d('onPress:  ');

        },
        child:Container(
      child:Padding(
        padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
          child :Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                SizedBox(
                  height: AppConstants.SIDE_MARGIN/1.5,
                ),

                Text(
                 // AppConstants.CALL_REQUEST_CURRENTLY_THERE ,
                  AppLocalizations.of(context).translate('currentlyThereNoAgentsAvailable'),
                  textAlign: TextAlign.center,
                  style: getStyleBody1(context).copyWith(
                      color: colorPencilBlack,
                      fontWeight: AppFont.fontWeightSemiBold,
                      fontSize: 23),
                ),

                SizedBox(
                  height: AppConstants.SIDE_MARGIN,
                ),
              Text(
                  //AppConstants.CALL_REQUEST_PLEASE_SELECT ,
                  AppLocalizations.of(context).translate('pleaseSelectOptionsBelowSetupAppointment'),
                  textAlign: TextAlign.center,
                  style: getStyleBody1(context).copyWith(
                      color: colorPencilBlack,
                      fontWeight: AppFont.fontWeightRegular,
                      fontSize: 15),
                ),


                SizedBox(
                  height: AppConstants.SIDE_MARGIN,
                ),

                 Container(

                    width: getScreenWidth(context),
                    height: getScreenWidth(context)/1.6,
                    color: colorLitePinkBG,//
                    child: DecoratedBox(

                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(AppImages.IMAGE_CALL_REQUEST),
                            fit: BoxFit.fill),
                      ),),
                  ),



                SizedBox(
                  height: AppConstants.SIDE_MARGIN*1.2,
                ),

                ButtonTheme(
                  minWidth: double.infinity,

                  child: RaisedButton(

                    highlightElevation: 8.0,
                    onPressed: () async {
                      FocusScope.of(context).requestFocus(FocusNode());
                      callRequestNotifier.isLeaveUsMsgSelected = false;

                      Navigator.pushNamed(context, ScheduleMeetingScreen.routeName);
                    },
                    color: (callRequestNotifier.isLeaveUsMsgSelected)?colorWhite:colorLiteOrange,
                    elevation: 1,
                    padding: EdgeInsets.only(
                        top: AppConstants.SIDE_MARGIN / 1.5,
                        bottom: AppConstants.SIDE_MARGIN / 1.5),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: colorLiteOrange, width: 1),
                        borderRadius: BorderRadius.all( Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                    ),
                    child: Text(
                     // AppConstants.SET_UP_APPOINTMENT,
                      AppLocalizations.of(context).translate('setUpAppointment'),
                      style: getStyleButtonText(context).copyWith(
                          color:(callRequestNotifier.isLeaveUsMsgSelected)?colorLiteOrange:colorWhite,
                          letterSpacing: 1,
                          fontWeight: AppFont.fontWeightBold),
                    ),
                  ),


                ),


                SizedBox(
                  height: AppConstants.SIDE_MARGIN,
                ),


                ButtonTheme(
                  minWidth: double.infinity,

                  child: RaisedButton(

                    highlightElevation: 8.0,
                    onPressed: () async {
                      FocusScope.of(context).requestFocus(FocusNode());
                      callRequestNotifier.isLeaveUsMsgSelected = true;
                      Navigator.pushNamed(context, LeaveUsMessageScreen.routeName);

                    },
                    color: (!callRequestNotifier.isLeaveUsMsgSelected)?colorWhite:colorLiteOrange,
                    elevation: 1,
                    padding: EdgeInsets.only(
                        top: AppConstants.SIDE_MARGIN / 1.5,
                        bottom: AppConstants.SIDE_MARGIN / 1.5),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: colorLiteOrange, width: 1),
                        borderRadius: BorderRadius.all(Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                    ),
                    child: Text(
                      //AppConstants.LEAVE_US_A_MESSAGE,
                      AppLocalizations.of(context).translate('leaveUsMessage'),
                      style: getStyleButtonText(context).copyWith(
                          color: (!callRequestNotifier.isLeaveUsMsgSelected)?colorLiteOrange:colorWhite,
                          letterSpacing: 1,
                          fontWeight: AppFont.fontWeightBold),
                    ),
                  ),


                ),



                SizedBox(
                  height: AppConstants.SIDE_MARGIN/2,
                ),



              ])
      )
            ),



      );


  }

  Widget _buildDivider() {
    return Container(
      height: 1,
      width: double.infinity,
      color: colorDividerGrey,
      margin: EdgeInsets.only(
          top: AppConstants.SIDE_MARGIN / 1.5,
          bottom: AppConstants.SIDE_MARGIN / 1.5),
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////



 

}



