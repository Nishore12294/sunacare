import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jiffy/jiffy.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:quiver/iterables.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/availability/assignment/AssignmentListResponse.dart'
    as AssignmentList;
import 'package:suna_care/core/data/remote/request_response/availability/holiday/AddHolidayResponse.dart'
    as AddHolidayResp;
import 'package:suna_care/core/data/remote/request_response/availability/holiday/HolidayListResponse.dart'
    as HolidayList;
import 'package:suna_care/core/data/remote/request_response/schedule_leave_msg/schedule/ScheduleAppointmentResponse.dart';
import 'package:suna_care/core/notifier/schedule_meeting_calendar_notifier.dart';
import 'package:suna_care/core/notifier/home_notifier.dart';
import 'package:suna_care/core/notifier/schedule_meeting_calendar_notifier.dart';
import 'package:suna_care/core/notifier/schedule_meeting_calendar_notifier.dart';
import 'package:suna_care/ui/home/availability/calendarData.dart';
import 'package:suna_care/ui/home/availability/day.dart';
import 'package:suna_care/ui/home/availability/schedule_details.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_appointment_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/alert_overlay_with_refersh.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_globals.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:provider/provider.dart';
//import 'package:table_calendar/table_calendar.dart';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'ScheduleMeetingTime.dart';
import 'call_request_screen.dart';

class ScheduleMeetingTimeScreen extends StatefulWidget {
  static const routeName = '/schedule_time_screen';

  ScheduleMeetingTimeScreen();

  @override
  ScheduleMeetingTimeScreenState createState() => ScheduleMeetingTimeScreenState();
}

class ScheduleMeetingTimeScreenState extends State<ScheduleMeetingTimeScreen> {
  BuildContext mContext;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  ScheduleMeetingTimeScreenState();

  //CalendarController _controller = CalendarController();

  var log = getLogger("ScheduleMeetingTimeScreen");
  var LastDateOfMonthArr = const [
    31,
    28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31
  ];
  DateTime masterDate = DateTime.now();

  //Picker
  DateTime selectedStartDate = DateTime.now();
  String scheduleFormatedStartDate = "";
  DateTime selectedEndDate = DateTime.now();

  ///Multi date range picker
  Day start;
  List<List<Day>> intervals = [];
  bool deleteConfirm = false;
  Day deleteDay;
  List<Day> deleteInterval;
  bool onlyOne = false;
  Color selectionColor = Colors.red;
  Color buttonColor = Colors.lightGreenAccent;
  Color buttonTextColor = Colors.black;
  Color primaryTextColor = Colors.black;
  Color dateTextColor = Colors.black;
  Color ignoreTextColor = Colors.grey;
  Color selectedDateTextColor = Colors.black;
  Color selectedIgnoreTextColor = Colors.black;
  Color backgroundTextColor = Colors.white;

  //final DateFormat dateMonthFormatter = DateFormat('dd MMM, yyyy');
  final DateFormat dateMonthFormatter = DateFormat('ddo MMMM , yyyy');

  int assignmentListSize = 0, holidayListSize = 0;
  var monthFormatter = new DateFormat('MM');
  var yearFormatter = new DateFormat('yyyy');
  var dateFormatter = new DateFormat('dd');
  DateTime argDateTime = DateTime.now();
  String languageCode = "en";

  @override
  void initState() {
    super.initState();
    //setUpInitalData();

    //Get Assignment List and Holiday List data and load into the Intervals list
    // getInitialData(ScheduleMeetingCalendarNotifier);

    //Prepare the list<Day> to load data into calendar
    // loadInitialData(ScheduleMeetingCalendarNotifier);
  }



  @override
  Widget build(BuildContext context) {
    final  String argDate = ModalRoute.of(context).settings.arguments;
    debugPrint("Argument Date :==>"+argDate);
     argDateTime = DateTime.parse(argDate);
    mContext = context;
    return ChangeNotifierProvider<ScheduleMeetingCalendarNotifier>(
        builder: (context) => ScheduleMeetingCalendarNotifier(),
        child: Consumer<ScheduleMeetingCalendarNotifier>(
            builder: (context, ScheduleMeetingCalendarNotifier, _) => Theme(
                data: ThemeData(
                    hintColor: colorDividerGrey,
                    primaryColor: colorPrimary,
                    accentColor: colorYellow,
                    primaryColorDark: colorPrimary),
                child: _buildWidgetHomeCard(ScheduleMeetingCalendarNotifier,
                    context)))); //_buildWidgetSendEmailIdCard(ScheduleMeetingCalendarNotifier, context)));
  }

  /////////////////////
  /*  build helpers */
  ////////////////////

  Widget _buildWidgetHomeCard(ScheduleMeetingCalendarNotifier sheduleMeetingCalendarNotif, BuildContext context) {
    sheduleMeetingCalendarNotif.context = context;

    //Get selected date
    var selectedDate = masterDate;
    String currentDateMonth = monthFormatter.format(DateTime.now());
    String currentDateYear = yearFormatter.format(DateTime.now());
    String selectedDateMonth = monthFormatter.format(selectedDate);
    String selectedDateYear = yearFormatter.format(selectedDate);
    debugPrint("selectedDateMonth :==>" + selectedDateMonth.toString());
    debugPrint("currentDateMonth:==>" + currentDateMonth.toString());
    debugPrint("selectedDateYear :==>" + selectedDateYear.toString());
    debugPrint("currentDateYear :==>" + currentDateYear.toString());

    var currentDate = new DateTime.now();
    debugPrint("CompareTo :==>" + masterDate.compareTo(currentDate).toString());

    return ChangeNotifierProvider<ScheduleMeetingCalendarNotifier>(
      builder: (context) => ScheduleMeetingCalendarNotifier(),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: colorLitePinkBG,
          // image: DecorationImage(
          //     image: AssetImage(
          //       AppImages.IMAGE_SIGNUP_BG_WITH_CIRCLE,
          //     ),
          //     fit: BoxFit.fill
          // ),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            key: _scaffoldKey,
            body: Consumer<ScheduleMeetingCalendarNotifier>(
              builder: (context, scheduleMeetingCalendarNotifier, _) => Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  ModalProgressHUD(
                    color: Colors.transparent,
                    inAsyncCall: scheduleMeetingCalendarNotifier.isLoading,
                    child: GestureDetector(
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());
                        },
                        child: Container(
                          //alignment: Alignment.topCenter,
                          child: Column(

                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                               Container(
                                margin: EdgeInsets.only(top: 10.0),
                                color: colorLitePinkBG,
                                height: 35,
                              ),
                              topHeader(),


                              SizedBox(
                                height: AppConstants.SIDE_MARGIN*2,
                              ),
                              Text(
                               // "Thursday",
                                DateFormat('EEEE',scheduleMeetingCalendarNotifier.languageCode).format(argDateTime),
                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 23),
                              ),
                              Text(
                                //"Dec 18th 2020",
                                Jiffy([argDateTime.year, argDateTime.month, argDateTime.day]).format(" MMM do, yyyy",),
                                textAlign: TextAlign.center,
                                style: getStyleBody1(context).copyWith(
                                    color: colorPencilBlack,
                                    fontWeight: AppFont.fontWeightSemiBold,
                                    fontSize: 23),
                              ),

                              SizedBox(
                                height: AppConstants.SIDE_MARGIN/2,
                              ),

                              // Text(
                              //   AppConstants.SELECT_A_TIME,
                              //   textAlign: TextAlign.center,
                              //   style: getStyleBody1(context).copyWith(
                              //       color: colorPencilBlack,
                              //       fontWeight: AppFont.fontWeightSemiBold,
                              //       fontSize: 15),
                              // ),Text(
                              //   AppConstants.DURATION,
                              //   textAlign: TextAlign.center,
                              //   style: getStyleBody1(context).copyWith(
                              //       color: colorPencilBlack,
                              //       fontWeight: AppFont.fontWeightRegular,
                              //       fontSize: 15),
                              // ),
                              // SizedBox(
                              //   height: AppConstants.SIDE_MARGIN,
                              // ),

                              Container(
                                height: getScreenHeight(context)/2,
                                margin: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
                                child:  ListView.builder(
                                itemCount: scheduleMeetingCalendarNotifier.meetingTiming.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return  Column(

                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                       children: [
                                         SizedBox(height:10),
                                         ButtonTheme(
                                           minWidth: double.infinity,

                                           child: RaisedButton(

                                             highlightElevation: 8.0,
                                             onPressed: () async {

                                               for(int i = 0; i < scheduleMeetingCalendarNotifier.meetingTiming.length; i++) {
                                                 print("before scheduleMeetingCalendarNotifier.meetingTiming :==>" + scheduleMeetingCalendarNotifier.meetingTiming[i].isSelected.toString());
                                               }

                                               try{
                                                 setState(() {
                                                   for(int arrIndx = 0; arrIndx < scheduleMeetingCalendarNotifier.meetingTiming.length; arrIndx++) {
                                                     print("between scheduleMeetingCalendarNotifier.meetingTiming :==>"+scheduleMeetingCalendarNotifier.meetingTiming[arrIndx].isSelected.toString());
                                                     if (arrIndx == index) {
                                                       setState(() {
                                                         scheduleMeetingCalendarNotifier.meetingTiming[arrIndx].isSelected = true;
                                                       });
                                                     } else {
                                                       setState(() {
                                                         scheduleMeetingCalendarNotifier.meetingTiming[arrIndx].isSelected = false;
                                                       });
                                                     }
                                                   }
                                                 });


                                              for(int i = 0; i < scheduleMeetingCalendarNotifier.meetingTiming.length; i++) {
                                                print("final scheduleMeetingCalendarNotifier.meetingTiming :==>" + scheduleMeetingCalendarNotifier.meetingTiming[i].isSelected.toString());

                                              }
                                               }catch (e) {
                                                 print("exception :==>"+e.toString());
                                               }

                                               //setState(() {
                                                 //here am trying to implement single selection for the options in the list but it don't work well

                                               //});
                                             },
                                             color: scheduleMeetingCalendarNotifier.meetingTiming[index].isSelected ?colorGreen:colorWhite,
                                             elevation: 1,
                                             padding: EdgeInsets.only(
                                                 top: AppConstants.SIDE_MARGIN / 1.5,
                                                 bottom: AppConstants.SIDE_MARGIN / 1.5),
                                             shape: RoundedRectangleBorder(
                                                 side: BorderSide(color: colorGreen, width: 1),
                                                 borderRadius: BorderRadius.all(Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                                             ),
                                             child: Text(
                                                 scheduleMeetingCalendarNotifier.meetingTiming[index].title,
                                               style: getStyleButtonText(context).copyWith(
                                                   color: scheduleMeetingCalendarNotifier.meetingTiming[index].isSelected ?colorWhite:colorGreen,
                                                   letterSpacing: 1,
                                                   fontWeight: AppFont.fontWeightBold),
                                             ),
                                           ),


                                         ),
                                       //Text(scheduleMeetingCalendarNotifier.meetingTiming[index].title),
                                         SizedBox(height:10),
                                       ],

                                    );
                                },
                              ),
                              ),

                              Container(padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),

                               child: ButtonTheme(
                                  minWidth: double.infinity,


                                  child: RaisedButton(


                                    highlightElevation: 8.0,
                                    onPressed: () async {
                                      FocusScope.of(context).requestFocus(FocusNode());
                                      //Navigator.pushNamed(context, ScheduleAppointmentScreen.routeName);
                                      String accessToken = await AppSharedPreference().getUserToken();
                                      debugPrint("accessToken:==>"+accessToken);

                                      String appointmentDate = "";
                                      String startingTime = "";
                                      String endingTime = "";
                                      for(int arrIndex =0;arrIndex<scheduleMeetingCalendarNotifier.meetingTiming.length;arrIndex++){
                                        debugPrint("isSelected:==>"+scheduleMeetingCalendarNotifier.meetingTiming[arrIndex].isSelected.toString());

                                        if(scheduleMeetingCalendarNotifier.meetingTiming[arrIndex].isSelected){
                                          String day = (argDateTime.day < 9)?"0"+argDateTime.day.toString():argDateTime.day.toString();
                                          String month = (argDateTime.month < 9)?"0"+argDateTime.month.toString():argDateTime.month.toString();
                                          if(arrIndex == 0){
                                            appointmentDate = argDateTime.year.toString()+month+day;
                                            startingTime = "080000";
                                            endingTime = "100000";
                                          }else if(arrIndex == 1){
                                            appointmentDate = argDateTime.year.toString()+month.toString()+day;
                                            startingTime = "100000";
                                            endingTime = "120000";
                                          }else if(arrIndex == 2){
                                            appointmentDate = argDateTime.year.toString()+month+day;
                                            startingTime = "120000";
                                            endingTime = "140000";
                                          }else if(arrIndex == 3){
                                            appointmentDate = argDateTime.year.toString()+month+day;
                                            startingTime = "140000";
                                            endingTime = "160000";
                                          }else if(arrIndex == 4){
                                            appointmentDate = argDateTime.year.toString()+month+day;
                                            startingTime = "160000";
                                            endingTime = "180000";
                                          }
                                        }
                                      }


                                      debugPrint("appointmentDate:==>"+appointmentDate+","+startingTime+","+endingTime);
                                      debugPrint("offerNumberValue:==>"+offerNumberValue);


                                      //ScheduleAppointmentResponse scheduleAppointmentResponse = null;
                                     ScheduleAppointmentResponse scheduleAppointmentResponse = await scheduleMeetingCalendarNotifier.callApiScheduleAppointment(accessToken,appointmentDate,startingTime,endingTime,offerNumberValue);
                                      if(
                                      scheduleAppointmentResponse != null
                                      && scheduleAppointmentResponse.webMethodOutputRows != null
                                      && scheduleAppointmentResponse.webMethodOutputRows.webMethodOutputRows[0] != null
                                      && scheduleAppointmentResponse.webMethodOutputRows.webMethodOutputRows[0].status == "Success"
                                      ){
                                        showDialog(
                                          context: context,
                                          builder: (_) => AlertOverlayWithRefresh(
                                              AppLocalizations.of(context).translate('scheduleAppointment'),
                                              AppLocalizations.of(context).translate('youSuccessfullyScheduledAppointment'),
                                              //AppConstants.APPOINTMENT_SCHEDULED_SUCCESS,
                                              AppLocalizations.of(context).translate('okay'),""),
                                        );
                                      }else{
                                            if( scheduleAppointmentResponse != null &&
                                            scheduleAppointmentResponse.outputRowErrors!= null &&
                                          scheduleAppointmentResponse.outputRowErrors.outputRowErrors[0] != null &&
                                          scheduleAppointmentResponse.outputRowErrors.outputRowErrors[0].textMessage != ""){
                                            showDialog(
                                            context: context,
                                            builder: (_) =>
                                            AlertOverlay(AppLocalizations.of(context).translate('scheduleAppointment'),
                                            scheduleAppointmentResponse.outputRowErrors.outputRowErrors[0].textMessage,
                                            AppLocalizations.of(context).translate('okay')));
                                            }
                                      }


                                    },
                                    color: colorLiteOrange,
                                    elevation: 1,
                                    padding: EdgeInsets.only(
                                        top: AppConstants.SIDE_MARGIN / 1.5,
                                        bottom: AppConstants.SIDE_MARGIN / 1.5),
                                    shape: RoundedRectangleBorder(
                                        side: BorderSide(color: colorLiteOrange, width: 1),
                                        borderRadius: BorderRadius.all( Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                                    ),
                                    child: Text(
                                      //AppConstants.FIX_SCHEDULE,
                                      AppLocalizations.of(context).translate('fixSchedule'),
                                      style: getStyleButtonText(context).copyWith(
                                          color:colorWhite,
                                          letterSpacing: 1,
                                          fontWeight: AppFont.fontWeightBold),
                                    ),
                                  ),


                                ),
                              )


                            ],
                          ),
                        )),
                  )
                ],
              ),
            )

            //_buildSignUpLabel(),
            ),
      ),
    );
  }

  ////////////// picker page content///////////

  Widget topHeader() {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
            flex: 1,
            child: InkWell(
              onTap: () {
                Navigator.canPop(context) ? Navigator.pop(context) : '';
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Image.asset(
                  AppImages.IMAGE_BACK,
                  height: 25,
                ),
              ),
            )),
        Expanded(
          flex: 2,
          child: Container(
            child: Text(
              //AppConstants.SCHEDULE_TIME_TITLE,
              AppLocalizations.of(context).translate('selectTimeSlot'),
              textAlign: TextAlign.center,
              style: getStyleBody1(mContext).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 20.5,
                fontFamily: 'NPSunaGrotesk',
              ),
            ),
          ),
        ),
        Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
              color: Colors.transparent,
              alignment: Alignment.centerRight,
              child: Image.asset(
                AppImages.IMAGE_APP_LOGO,
                // width: getScreenWidth(context) / 4,
                height: 20,
              ),
            )),
      ],
    );
  }

  @override
  void dispose() {
    this.mContext = null;
    super.dispose();
  }

/////////////////////
/*On click-actions*/

  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

//////////////MultiRange picker page content///////////

}



class AlertOverlayWithRefresh extends StatefulWidget {
  String title, subTitle, buttonTitle,additionalArg;

  AlertOverlayWithRefresh(this.title, this.subTitle, this.buttonTitle,this.additionalArg);

  @override
  State<StatefulWidget> createState() =>
      AlertOverlayState(this.title, this.subTitle, this.buttonTitle,this.additionalArg);
}

class AlertOverlayState extends State<AlertOverlayWithRefresh>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  String title, subTitle, buttonTitle,additionalArg;

  AlertOverlayState(this.title, this.subTitle, this.buttonTitle,this.additionalArg);

  @override
  void initState() {
    super.initState();

    // controller =
    //     AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    // scaleAnimation =
    //     CurvedAnimation(parent: controller, curve: Curves.elasticInOut);
    //
    // controller.addListener(() {
    //   setState(() {});
    // });
    //
    // controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child:  Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: 200.0,
              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10.0, left: 20.0, right: 20.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  widget.title,
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightExtraBold,
                                      fontSize: 17),
                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN / 3.5,
                                ),
                                Text(
                                  widget.subTitle != null
                                      ? widget.subTitle
                                      : "",
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      fontWeight: AppFont.fontWeightRegular,
                                      fontSize: (widget.subTitle != null &&
                                          widget.subTitle.length > 10)
                                          ? 14
                                          : 16),
                                ),
                              ]))),
                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN * 4,
                            height: 45,
                            child: RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context)? Navigator.pop(context): '';
                                Navigator.canPop(context)? Navigator.pop(context): '';
                                Navigator.canPop(context)? Navigator.pop(context): '';
                                //Navigator.pushNamed(context, HomeScreen.routeName);

                              },
                              color: colorDarkRed,
                              textColor: colorWhite,
                              elevation: 1,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonTitle,
                                style: getStyleButtonText(context).copyWith(
                                    color: colorWhite,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              )),
      ),
    );
  }
}
