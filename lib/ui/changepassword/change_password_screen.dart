import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:suna_care/core/data/local/app_shared_preference.dart';
import 'package:suna_care/core/data/remote/request_response/change_password/response_change_password.dart';
import 'package:suna_care/core/notifier/change_password_notifier.dart';
import 'package:suna_care/ui/home/home_screen.dart';
import 'package:suna_care/utils/alert_overlay.dart';
import 'package:suna_care/utils/app_custom_icon.dart';
import 'package:suna_care/utils/app_localizations.dart';
import 'package:suna_care/utils/app_screen_dimen.dart';
import 'package:suna_care/utils/widget_helper/custom_gradient_circle_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:suna_care/utils/app_colors.dart';
import 'package:suna_care/utils/app_constants.dart';
import 'package:suna_care/utils/app_font.dart';
import 'package:suna_care/utils/app_images.dart';
import 'package:suna_care/utils/app_log_helper.dart';
import 'package:suna_care/utils/app_text_style.dart';
import 'package:suna_care/utils/app_log_helper.dart';

import '../login_screen.dart';



class ChangePasswordScreen extends StatefulWidget {
  static const routeName = '/change_password';

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final log = getLogger('ChangePasswordScreen');
  final _keyValidationForm = GlobalKey<FormState>(debugLabel: '_changePasswordKey');
  bool isOldPasswordVisible = false;
  bool isNewPasswordVisible = false;
  bool isConfirmPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    setInitialData(); //aa
  }

  setInitialData() async {

  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ChangePasswordNotifier>(
      builder: (context) => ChangePasswordNotifier(),
    child: DecoratedBox(
      decoration: BoxDecoration(
      color: colorLitePinkBG,
      image: DecorationImage(
      image: AssetImage(
           AppImages.IMAGE_LOGIN_BG_WITH_CIRCLE,
      ),
      fit: BoxFit.fill),
    ),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          key: _scaffoldKey,
          body: Consumer<ChangePasswordNotifier>(
            builder: (context, changePasswordNotifier, _) => Stack(
              fit: StackFit.expand,
              children: <Widget>[

                ModalProgressHUD(
                  color: Colors.transparent,
                  inAsyncCall: changePasswordNotifier.isLoading,
                  child: GestureDetector(
                      onTap: () {
                        FocusScope.of(context).requestFocus(new FocusNode());
                      },
                      child: Container(
                        alignment: Alignment.topCenter,
                        child:  Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            _buildWidgetImageLogo(),

                            Image.asset(
                              AppImages.IMAGE_CHANGEPWD_LOCK,
                              height: 100,
                              width: 105,
                            ),


                            new Expanded(
                              flex: 2,
                              child:SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                child:_buildWidgetChangePasswordCard(changePasswordNotifier),
                              ),

                            ),
//                              Expanded(
//                                flex: 1 ,
//
//                                child: Container(
//                                  width: getScreenWidth(context),
//                                  height: getScreenHeight(context)/3,
//                                  color: colorWhite,//
//                                  child: DecoratedBox(
//
//                                    decoration: BoxDecoration(
//                                      image: DecorationImage(
//                                          image: AssetImage(AppImages.IMAGE_LOGIN_BG),
//                                          fit: BoxFit.fill),
//                                    ),),
//                                ),
//                              ),

                          ],
                        ),
                      )),
                )
              ],
            ),
          )

        //_buildSignUpLabel(),
      ),
    ),

    );
  }

  /////////////////////
  /*  build helpers */
  ////////////////////
  Widget _buildWidgetImageLogo() {
    return Container(
      height: getScreenHeight(context) / 5,
      //padding: EdgeInsets.only(top: 64, bottom: 32),
      alignment: Alignment.center,
      child: topHeader()
    );
  }

  Widget _buildWidgetChangePasswordCard(ChangePasswordNotifier changePasswordNotifier) {
    

    return Column(
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.only(
                top: 20.0, left: 30.0, right: 30.0, bottom: 8.0),
            child: Theme(
              data: ThemeData(
                  hintColor: colorBlack,
                  primaryColor: colorDarkRed,
                  primaryColorDark: colorLiteOrange),
              child: Form(
                key: _keyValidationForm,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
//                    TextFormField(
//                      controller: changePasswordNotifier.textEditOldPwd,
//                      focusNode: changePasswordNotifier.oldPwdFocus,
//                      obscureText: !isOldPasswordVisible,
//                      keyboardType: TextInputType.text,
//                      validator: _validateNameField,
//                      textInputAction: TextInputAction.next,
//                      style: getFormStyleText(context),
//
//                      decoration:_buildTextDecoration(AppConstants.OLD_PASSWORD).copyWith(
//                        fillColor: colorLiteWhiteBG,
//                        prefixIcon: Image.asset(
//                          AppImages.IMAGE_PASSWORD,
//                          // width: getScreenWidth(context) / 4,
//                          width: 5,
//                          height: 5,
//                        ),
//                        suffixIcon: IconButton(
//                          icon: Icon(isOldPasswordVisible
//                              ? Icons.visibility
//                              : Icons.visibility_off),
//                          onPressed: () {
//                            setState(() {
//                              isOldPasswordVisible = !isOldPasswordVisible;
//                            });
//                          },
//                        ),
//                        focusedBorder: _buildOutlineInputBorder(),
//                        enabledBorder: _buildOutlineInputBorder(),
//                      ),
//                    ),
//                    SizedBox(
//                      height: 16,
//                    ),
                    TextFormField(
                      controller: changePasswordNotifier.textEditNewPwd,
                      focusNode: changePasswordNotifier.newPwdFocus,
                      obscureText: !isNewPasswordVisible,
                      keyboardType: TextInputType.text,
                      validator: _validateNameField,
                      textInputAction: TextInputAction.next,
                      style: getFormStyleText(context),

                      decoration:
                      _buildTextDecoration(
                        //AppConstants.NEW_PASSWORD,
                        AppLocalizations.of(context).translate('enterNewPassword'),
                      ).copyWith(
                        fillColor: colorLiteWhiteBG,
                        prefixIcon: Image.asset(
                          AppImages.IMAGE_PASSWORD,
                          // width: getScreenWidth(context) / 4,
                          width: 5,
                          height: 5,
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(isNewPasswordVisible
                              ? Icons.visibility
                              : Icons.visibility_off),
                          onPressed: () {
                            setState(() {
                              isNewPasswordVisible = !isNewPasswordVisible;
                            });
                          },
                        ),
                        focusedBorder: _buildOutlineInputBorder(),
                        enabledBorder: _buildOutlineInputBorder(),
                      ),
                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),
                    TextFormField(
                      controller: changePasswordNotifier.textEditConfirmPwd,
                      focusNode: changePasswordNotifier.confirmPwdFocus,
                      obscureText: !isConfirmPasswordVisible,
                      keyboardType: TextInputType.text,
                      validator: _validateNameField,
                      textInputAction: TextInputAction.done,
                      style: getFormStyleText(context),
                      decoration:
                      _buildTextDecoration(
                          //AppConstants.CONFIRM_PASSWORD,
                          AppLocalizations.of(context).translate('confirmNewPassword'),
                      ).copyWith(
                        fillColor: colorLiteWhiteBG,
                        prefixIcon: Image.asset(
                          AppImages.IMAGE_PASSWORD,
                          // width: getScreenWidth(context) / 4,
                          width: 5,
                          height: 5,
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(isConfirmPasswordVisible
                              ? Icons.visibility
                              : Icons.visibility_off),
                          onPressed: () {
                            setState(() {
                              isConfirmPasswordVisible = !isConfirmPasswordVisible;
                            });
                          },
                        ),
                        focusedBorder: _buildOutlineInputBorder(),
                        enabledBorder: _buildOutlineInputBorder(),
                      ),
                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),


                    ButtonTheme(
                      minWidth: double.infinity,

                      child: RaisedButton(

                        highlightElevation: 8.0,
                        onPressed: () async {
                          FocusScope.of(context).requestFocus(FocusNode());

                          if( changePasswordNotifier.textEditNewPwd.text.toString() == ""&& changePasswordNotifier.textEditConfirmPwd.text.toString() == ""){
                            showDialog(
                              context: context,
                              builder: (_) => AlertOverlay(
                                  AppLocalizations.of(context).translate('changePassword'),
                                  AppLocalizations.of(context).translate('pleaseEnterNewPassword'),
                                  //AppConstants.PLEASE_ENTER_NEW_PASSWORD,
                                  AppLocalizations.of(context).translate('okay')),
                            );

                          }
//                          else if(changePasswordNotifier.textEditOldPwd.text.toString() == "" ){
//                            showDialog(
//                              context: context,
//                              builder: (_) => AlertOverlay(AppLocalizations.of(context).translate('changePassword'),AppConstants.ENTER_OLD_PASSWORD,AppLocalizations.of(context).translate('okay')),
//                            );
//
//                          }
                          else if(changePasswordNotifier.textEditNewPwd.text.toString() == ""){
                            showDialog(
                              context: context,
                              builder: (_) => AlertOverlay(
                                  AppLocalizations.of(context).translate('changePassword'),
                                  AppLocalizations.of(context).translate('pleaseEnterNewPassword'),
                                  //AppConstants.PLEASE_ENTER_NEW_PASSWORD,
                                  AppLocalizations.of(context).translate('okay')),
                            );

                          }else if(changePasswordNotifier.textEditConfirmPwd.text.toString() == ""){
                            showDialog(
                              context: context,
                              builder: (_) => AlertOverlay(
                                  AppLocalizations.of(context).translate('changePassword'),
                                  AppLocalizations.of(context).translate('pleaseEnterConfirmNewPassword'),
                                 // AppConstants.PLEASE_ENTER_CONFIRM_NEW_PASSWORD,
                                  AppLocalizations.of(context).translate('okay')),
                            );

                          }else{
                            if(changePasswordNotifier.textEditConfirmPwd.text.toString() != changePasswordNotifier.textEditNewPwd.text.toString()){
                              showDialog(
                                context: context,
                                builder: (_) =>
                                    AlertOverlay(
                                        AppLocalizations.of(context).translate('changePassword'),
                                        AppLocalizations.of(context).translate('passwordsDoNotMatch'),
                                        //AppConstants.ENTER_PASSWORDS_NOT_EQUAL,
                                        AppLocalizations.of(context).translate('okay')),
                              );
                            }else {
//                              showDialog(
//                                context: context,
//                                builder: (_) =>
//                                    AlertOverlay(AppLocalizations.of(context).translate('changePassword'),
//                                        "Password Changed",
//                                        AppLocalizations.of(context).translate('okay')),
//                              );
                              //Store local data
                              //SharedPreferences prefs = await SharedPreferences.getInstance();
                              //prefs.setString('FIREST_LOGIN', "0");



                             // Navigator.pushNamed(context, HomeScreen.routeName);


                              _onClickButtonChangePassword(changePasswordNotifier);
                            }
                          }
                        },
                        color: colorLiteOrange,
                        textColor: colorWhite,
                        elevation: 1,
                        padding: EdgeInsets.only(
                            top: AppConstants.SIDE_MARGIN / 1.5,
                            bottom: AppConstants.SIDE_MARGIN / 1.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(AppConstants.SIDE_MARGIN*1.5))
                        ),
                        child: Text(
                          AppLocalizations.of(context).translate('done'),
                          //AppConstants.DONE,
                          style: getStyleButtonText(context).copyWith(
                              color: colorWhite,
                              letterSpacing: 1,
                              fontWeight: AppFont.fontWeightBold),
                        ),
                      ),


                    ),
                    SizedBox(
                      height: AppConstants.SIDE_MARGIN,
                    ),


                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
  InputDecoration _buildTextDecoration(String email) {
    return InputDecoration(
        isDense: true,
        hintText: email,
        alignLabelWithHint: true,
        filled: true,
        contentPadding: EdgeInsets.all(16));
  }

  OutlineInputBorder _buildOutlineInputBorder(){
    return  OutlineInputBorder(
      borderSide: BorderSide(
        color: colorLiteWhiteBorder,
        //color: Colors.transparent,
      ),
      borderRadius: const BorderRadius.all(

        const Radius.circular(AppConstants.SIDE_MARGIN*1.5),
      ),
    );
  }

  Widget topHeader(){
    return  new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 1,
      child:  InkWell(
        onTap: () {
          Navigator.canPop(context) ? Navigator.pop(context) : '';
        },
        child: Container(
          margin: EdgeInsets.fromLTRB(30.0, 0.0, 0.8, 0.0),
          color: Colors.transparent,
          alignment: Alignment.centerLeft,
          child:Image.asset(
            AppImages.IMAGE_BACK,
            height: 25,
          ),
        ),
      ),


        ),
        Expanded(
          flex: 2,
          child:Text(
            //AppConstants.CHANGE_PASSWORD,
            AppLocalizations.of(context).translate('changePassword'),
            textAlign: TextAlign.center,
            style: getStyleBody1(context).copyWith(
                color: colorPencilBlack,
                fontWeight: AppFont.fontWeightMedium,
                fontSize: 23,
                fontFamily:'NPSunaGrotesk',
            ),

          ),
        ),

        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
            color: Colors.transparent,
            alignment: Alignment.centerRight,
            child:Image.asset(
              AppImages.IMAGE_APP_LOGO,
              // width: getScreenWidth(context) / 4,
              height: 20,
            ),),

        ),
      ],
    );
  }

  /////////////////////
  /*On click-actions*/
  ////////////////////
  void _onClickButtonChangePassword(ChangePasswordNotifier changePasswordNotifier) async {
    print('Action Tap: changePassword button');

    //Api: 1st
    ChangePasswordResponse changePasswordResponse = await changePasswordNotifier.callApiChangePassword();
    if(changePasswordResponse != null && changePasswordResponse.webMethodOutputRows != null && changePasswordResponse.webMethodOutputRows.webMethodOutputRows[0] != null){
        if(changePasswordResponse.webMethodOutputRows.webMethodOutputRows[0].changeAppPasswordStatus  == 'PasswordUpdated') {
          AppSharedPreference().saveStringValue(AppConstants.KEY_USER_PASSWORD, changePasswordNotifier.textEditNewPwd.text.trim());
         // _showSnackBarMessage(AppConstants.PASSWORD_UPDATED_SUCCESSFULLY);
          showDialog(
            context: context,
            builder: (_) =>
                AlertOverlayWithRefersh(
                    AppLocalizations.of(context).translate('changePassword'),
                    AppLocalizations.of(context).translate('passwordUpdatedSuccessfully'),
                    //AppConstants.PASSWORD_UPDATED_SUCCESSFULLY,
                    AppLocalizations.of(context).translate('okay'),""),
          );

        } else if(changePasswordResponse == null && changePasswordResponse.webMethodOutputRows == null && changePasswordResponse.webMethodOutputRows.webMethodOutputRows[0] == null
            && changePasswordResponse.webMethodOutputRows.webMethodOutputRows[0].changeAppPasswordStatus == 'Blacklisted') {
          showDialog(
            context: context,
            builder: (_) =>
                AlertOverlay(
                    AppLocalizations.of(context).translate('changePassword'),
                    AppLocalizations.of(context).translate('yourProfileBlacklisted'),
                    //AppConstants.PROFILE_BLACKLISTED,
                    AppLocalizations.of(context).translate('okay')),
          );
        }else {
          showDialog(
            context: context,
            builder: (_) =>
                AlertOverlay(AppLocalizations.of(context).translate('changePassword'),
                    changePasswordResponse.webMethodOutputRows.webMethodOutputRows[0].changeAppPasswordStatus,
                    AppLocalizations.of(context).translate('okay')),
          );
        }

    }else  if(changePasswordResponse != null && changePasswordResponse.outputRowErrors != null && changePasswordResponse.outputRowErrors.outputRowErrors[0] != null){
        if(changePasswordResponse.outputRowErrors.outputRowErrors[0].textMessage == "InvalidOldAppPassword") {
          showDialog(
            context: context,
            builder: (_) =>
                AlertOverlay(
                    AppLocalizations.of(context).translate('changePassword'),
                    AppLocalizations.of(context).translate('invalidOldAppPassword'),
                    //AppConstants.INVALID_OLD_APP_PASSWORD,
                    AppLocalizations.of(context).translate('okay')),
          );
        }else if(changePasswordResponse.outputRowErrors.outputRowErrors[0].textMessage == "PasswordNotSet") {
          showDialog(
            context: context,
            builder: (_) =>
                AlertOverlay(
                    AppLocalizations.of(context).translate('changePassword'),
                    AppLocalizations.of(context).translate('passwordNotSet'),
                    //AppConstants.PASSWORD_NOT_SET,
                    AppLocalizations.of(context).translate('okay')),
          );
        }else if(changePasswordResponse.outputRowErrors.outputRowErrors[0].textMessage == AppConstants.NO_INTERNET_CONNECTION){
          showDialog(
            context: context,
            builder: (_) =>
                AlertOverlay(
                    AppLocalizations.of(context).translate('noNetworkConnection'),
                    AppLocalizations.of(context).translate('noInternetConnection'),
                   // AppConstants.NO_INTERNET_CONNECTION_TITLE,
                   // AppConstants.NO_INTERNET_CONNECTION,
                    AppLocalizations.of(context).translate('okay')),
          );
        }else {
          showDialog(
            context: context,
            builder: (_) =>
                AlertOverlay(AppLocalizations.of(context).translate('changePassword'),
                    changePasswordResponse.outputRowErrors.outputRowErrors[0].textMessage,
                    AppLocalizations.of(context).translate('okay')),
          );
        }

    }else{
      showDialog(
        context: context,
        builder: (_) =>
            AlertOverlay(
                AppLocalizations.of(context).translate('changePassword'),
                AppLocalizations.of(context).translate('somethingWentWrongPleaseTryAgainLater'),
                //AppConstants.SOME_THING_WENT_WRONG,
                AppLocalizations.of(context).translate('okay')),
      );
    }

  }



  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  String _validateNameField(String value) {
    return value.trim().length < 1 ? 'Field can\'t be empty' : null;
  }




}




class AlertOverlayWithRefersh extends StatefulWidget {
  String title, subTitle, buttonTitle,additionalArg;

  AlertOverlayWithRefersh(this.title, this.subTitle, this.buttonTitle,this.additionalArg);

  @override
  State<StatefulWidget> createState() =>
      AlertOverlayState(this.title, this.subTitle, this.buttonTitle,this.additionalArg);
}

class AlertOverlayState extends State<AlertOverlayWithRefersh>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  String title, subTitle, buttonTitle,offerGuid;

  AlertOverlayState(this.title, this.subTitle, this.buttonTitle,this.offerGuid);

  @override
  void initState() {
    super.initState();

    // controller =
    //     AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    // scaleAnimation =
    //     CurvedAnimation(parent: controller, curve: Curves.elasticInOut);
    //
    // controller.addListener(() {
    //   setState(() {});
    // });
    //
    // controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child:  Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: 200.0,
              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10.0, left: 20.0, right: 20.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  widget.title,
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      letterSpacing: 1,
                                      fontWeight: AppFont.fontWeightExtraBold,
                                      fontSize: 17),
                                ),
                                SizedBox(
                                  height: AppConstants.SIDE_MARGIN / 3.5,
                                ),
                                Text(
                                  widget.subTitle != null
                                      ? widget.subTitle
                                      : "",
                                  textAlign: TextAlign.center,
                                  style: getStyleButtonText(context).copyWith(
                                      color: colorBlack,
                                      fontWeight: AppFont.fontWeightRegular,
                                      fontSize: (widget.subTitle != null &&
                                          widget.subTitle.length > 10)
                                          ? 14
                                          : 16),
                                ),
                              ]))),
                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: AppConstants.SIDE_MARGIN * 4,
                            height: 45,
                            child: RaisedButton(
                              highlightElevation: 8.0,
                              onPressed: () {
                                Navigator.canPop(context)
                                    ? Navigator.pop(context)
                                    : '';
                                Navigator.pushNamed(context, HomeScreen.routeName);

                              },
                              color: colorDarkRed,
                              textColor: colorWhite,
                              elevation: 1,
                              padding: EdgeInsets.only(
                                  top: AppConstants.SIDE_MARGIN / 1.5,
                                  bottom: AppConstants.SIDE_MARGIN / 1.5),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(AppConstants.SIDE_MARGIN))),
                              child: Text(
                                widget.buttonTitle,
                                style: getStyleButtonText(context).copyWith(
                                    color: colorWhite,
                                    letterSpacing: 1,
                                    fontWeight: AppFont.fontWeightBold),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              )),
      ),
    );
  }
}


