import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:suna_care/ui/changepassword/change_password_screen.dart';
import 'package:suna_care/ui/forgotpassword/create_new_password_screen.dart';
import 'package:suna_care/ui/forgotpassword/forgot_password_screen.dart';
import 'package:suna_care/ui/forgotpassword/verification_code_screen.dart';
import 'package:suna_care/ui/home/availability/schedule_details.dart';
import 'package:suna_care/ui/home/home_screen.dart';
import 'package:suna_care/ui/home/slides/tab3_profile_screen.dart';
import 'package:suna_care/ui/login_screen.dart';
import 'package:suna_care/ui/offers/filter_options_screen.dart';
import 'package:suna_care/ui/offers/offers_details_screen.dart';
import 'package:suna_care/ui/home/slides/tab2_offers_list_screen.dart';
import 'package:suna_care/ui/offers/offers_profile_upload_screen.dart';
import 'package:suna_care/ui/profile/change_language_screen.dart';
import 'package:suna_care/ui/profile/notification_settings_screen.dart';
import 'package:suna_care/ui/profile/notifications_list_screen.dart';
import 'package:suna_care/ui/profile/view_profile_screen.dart';
import 'package:suna_care/ui/register/pending_approval_screen.dart';
import 'package:suna_care/ui/register/signup_screen.dart';
import 'package:suna_care/ui/schedulemeeting/call_request_screen.dart';
import 'package:suna_care/ui/schedulemeeting/leave_us_msg_intial_screen.dart';
import 'package:suna_care/ui/schedulemeeting/leave_us_msg_screen.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_appointment_screen.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_meeting_calendar_screen.dart';
import 'package:suna_care/ui/schedulemeeting/schedule_meeting_time_screen.dart';
import 'package:suna_care/ui/welcome_screen.dart';
import 'package:suna_care/utils/app_log_helper.dart';

final log = getLogger('Router');

class MyRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    log.i(
        'generateRoute | to :${settings.name} arguments:${settings.arguments}');
    switch (settings.name) {
      case LoginScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => LoginScreen(), settings: settings);

      case SignUpScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => SignUpScreen(), settings: settings);

      case ChangePasswordScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => ChangePasswordScreen(), settings: settings);

      case HomeScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => HomeScreen(), settings: settings);

      case ForgotPasswordScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => ForgotPasswordScreen(), settings: settings);

      case CreateNewPasswordScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => CreateNewPasswordScreen(), settings: settings);
      case VerificationCodeScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => VerificationCodeScreen(), settings: settings);
      case PendingApprovalScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => PendingApprovalScreen(), settings: settings);
      // case ScheduleDetails.routeName:
      //         return MaterialPageRoute(
      //             builder: (_) => ScheduleDetails(), settings: settings);
      case Tab2OffersListScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => Tab2OffersListScreen(), settings: settings);
      case FilterOptionsScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => FilterOptionsScreen(), settings: settings);

      // case OffersProfileUploadScreen.routeName:
      //   return MaterialPageRoute(
      //       builder: (_) => OffersProfileUploadScreen(), settings: settings);

      // case OffersDetailsScreen.routeName:
      //   return MaterialPageRoute(
      //       builder: (_) => OffersDetailsScreen(), settings: settings);

      case Tab3ProfileScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => Tab3ProfileScreen(), settings: settings);

      case ViewProfileScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => ViewProfileScreen(), settings: settings);

      case ChangeLanguageScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => ChangeLanguageScreen(), settings: settings);

      case NotificationListScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => NotificationListScreen(), settings: settings);

      case NotificationSettingsScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => NotificationSettingsScreen(), settings: settings);


    case ScheduleMeetingScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => ScheduleMeetingScreen(), settings: settings);

      case ScheduleMeetingTimeScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => ScheduleMeetingTimeScreen(), settings: settings);

      case CallRequestScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => CallRequestScreen(), settings: settings);

        case LeaveUsMessageInitialScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => LeaveUsMessageInitialScreen(), settings: settings);

      case ScheduleAppointmentScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => ScheduleAppointmentScreen(), settings: settings);

      case LeaveUsMessageScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => LeaveUsMessageScreen(), settings: settings);

      default:
        //return CupertinoPageRoute(builder: (_) => Container());
        return MaterialPageRoute(
            builder: (_) => WelcomeScreen(), settings: settings);
    }
  }
}
