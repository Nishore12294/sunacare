//package com.direction.sunacare
package com.sunacarepl.sc

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant

//class MainActivity: FlutterActivity() {
//}


class MainActivity: FlutterFragmentActivity() {
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
    }
//    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
//    {
//        KeyguardManager km =(KeyguardManager) getSystemService (KEYGUARD_SERVICE);
//        if (km.isKeyguardSecure()) {
//            Intent authIntent = km.createConfirmDeviceCredentialIntent(getString(R.string.dialog_title_auth), getString(R.string.dialog_msg_auth));
//            startActivityForResult(authIntent, INTENT_AUTHENTICATE);
//        }
//    }
//
//    @override
//    protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        if (requestCode == INTENT_AUTHENTICATE) {
//            if (resultCode == RESULT_OK) {
////do something you want when pass the security
//                Log.e
//            }
//        }
//    }
}